<?php
/*
Plugin Name: WooMobiPay - WooCommerce Gateway
Plugin URI: http://www.diyenmomjang.info/
Description: Extends WooCommerce by Adding the Mobile Money and orange money Pay Gateway.
Version: 1.1.2
Author: Diyen Momjang
Author URI: http://www.diyenmomjang.info/
*/

/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Copyright 2018 Diyen Momjang
 */

/*define('MOBILE_MONEY_SP_MTN_CAMEROON', 'mtn_cameroon');
define('MOBILE_MONEY_SP_ORANGE_CAMEROON', 'orange_cameroon');*/

define( 'WOOMOBIPAY_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
define( 'WOOMOBIPAY_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'WOOMOBIPAY_PLUGIN_NAME', plugin_basename( __FILE__ ) );

define('WOOMOBIPAY_VERSION', '1.1.2');

// Include our Gateway Class and Register Payment Gateway with WooCommerce
function woomobipay_init()
{
    // If the parent WC_Payment_Gateway class doesn't exist
    // it means WooCommerce is not installed on the site
    // so do nothing
    if (!class_exists('WC_Payment_Gateway')) return;

    // load_plugin_textdomain( 'woo-mobipay', false, WOOMOBIPAY_PLUGIN_PATH . '/languages' );
    load_plugin_textdomain( 'woo-mobipay', false, dirname(plugin_basename(__FILE__)).'/languages/' );

    include_once('libs/loader.php');
    include_once('functions/dm-loader.php');

    // If we made it this far, then include our Gateway Class
    include_once('woocommerce-mobile-money-mtn.php');
    include_once('woocommerce-orange-money.php');
    // include_once('woocommerce-zingerpay.php');


    // Now that we have successfully included our class,
    // Lets add it to WooCommerce
    function woomobipay_add_gateway($methods)
    {
        $methods[] = 'WC_Gateway_Mobile_Money_MTN';
        $methods[] = 'WC_Gateway_Orange_Money';
        // $methods[] = 'WC_Gateway_Zingerpay';
        return $methods;
    }

    add_filter('woocommerce_payment_gateways', 'woomobipay_add_gateway');
}
add_action('plugins_loaded', 'woomobipay_init', 0);

/*function woomobipay_plugin_load_textdomain() {

}
add_action( 'after_setup_theme', 'woomobipay_plugin_load_textdomain' );*/

// Add custom action links
function woomobipay_action_links($links)
{

    $plugin_links = array(
        '<a href="' . admin_url('admin.php?page=wc-settings&tab=checkout') . '">' . __('Settings', 'woo-mobipay') . '</a>',
    );

    // Merge our new link with the default ones
    return array_merge($plugin_links, $links);
}

add_filter('plugin_action_links_' . plugin_basename(__FILE__), 'woomobipay_action_links');

// Enqueue scripts and styles
function woomobipay_scripts()
{
    wp_register_script('dm-script', plugin_dir_url(__FILE__) . 'assets/js/dm_script.js', array('jquery'), WOOMOBIPAY_VERSION, true);
    wp_enqueue_style('dm-style', plugin_dir_url(__FILE__) . 'assets/css/dm_style.css');
    wp_enqueue_script('dm-script');
}

add_action('wp_enqueue_scripts', 'woomobipay_scripts');

// Enqueue admin scripts and styles
function woomobipay_admin_scripts()
{
    wp_register_script('dm-admin-script', plugin_dir_url(__FILE__) . 'assets/js/admin_script.js', array('jquery'), WOOMOBIPAY_VERSION, true);
    wp_enqueue_style('dm-style', plugin_dir_url(__FILE__) . 'assets/css/dm_style.css');
    wp_enqueue_script('dm-admin-script');
}

add_action('admin_enqueue_scripts', 'woomobipay_admin_scripts');

function woomobipay_load_textdomain() {
    load_plugin_textdomain( 'woo-mobipay', false, dirname( plugin_basename(__FILE__) ) . '/lang/' );
}

add_action('plugins_loaded', 'woomobipay_load_textdomain');
