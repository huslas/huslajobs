/*
 * Created by Diyen Momjang (http://diyenmomjang.info)
 */
jQuery(document).ready(function ($) {

    /*
     * MTN Mobile Money
     */

    function setEnvironment() {
        if ($('#woocommerce_woomobipay_sandbox').prop('checked')) {
            $('#woocommerce_woomobipay_environment').prop('disabled', true);
        } else {
            $('#woocommerce_woomobipay_environment').prop('disabled', false);
        }
    }

    setEnvironment();

    $(document).on('change', '#woocommerce_woomobipay_sandbox', function () {
        setEnvironment();
    });
});
