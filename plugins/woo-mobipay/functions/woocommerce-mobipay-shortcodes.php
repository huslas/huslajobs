<?php
/*
 * The order_id must be passed in the get query
 */
class WC_Gateway_Zingerpay_Shortcode {

    private $zinger_gateway;

    /**
     * WC_Gateway_Zingerpay_Shortcode constructor.
     */
    public function __construct()
    {
        $this->zinger_gateway =  new WC_Gateway_Zingerpay();
        add_shortcode('handle_zingerpay_response', array($this, 'handle_zingerpay_response_callback'));
    }

    function handle_zingerpay_response_callback()
    {
        $order_id = (isset($_GET['order_id'])) ? (int)$_GET['order_id'] : 0;
        $txn_response_code = (isset($_GET['vpc_TxnResponseCode'])) ? $_GET['vpc_TxnResponseCode'] : '';

        if($txn_response_code == '0') {
            try {
                $order = new WC_Order($order_id);
                if ($order) {
                    $this->handle_successful_transaction($order);
                } else {
                    $this->display_error("order does not exist");
                }
            } catch (Exception $e) {
                $this->display_error($e->getMessage());
            }
        } else {
            $message = ($_GET['vpc_Message']) ? $_GET['vpc_Message'] : '';
            $this->display_error($message);
        }
    }

    function handle_successful_transaction(WC_Order $order) {

        global $woocommerce;
        // Mark as on-hold (we're awaiting the cheque)
        //$order->update_status('on-hold', __( 'Awaiting cheque payment', 'woocommerce' ));
        //Complete payment
        $order->payment_complete();

        // Reduce stock levels
        $order->reduce_order_stock();

        // Remove cart
        $woocommerce->cart->empty_cart();

        $return_url = $this->zinger_gateway->get_return_url($order);

        echo '<script>window.location="' . $return_url . '"</script>';
    }

    function display_error($message) {
        $message_color = $this->zinger_gateway->get_option('errorColor', 'red');

        echo '<span style="color: ' . $message_color . '">' . $message . '</span>';
    }
}

new WC_Gateway_Zingerpay_Shortcode();










