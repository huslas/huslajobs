<?php

/*
 * This contains the the available api endpoints
 *
 * Create an endpoint with the following simple steps
 *
 * 1. add the endpoint name to the $api_methods array below under the correct request type
 *
 * 2. create a php file in the "callbacks" directory (callbacks/$endpoint.php)
 *    with the same name as the endpoints name.
 *    NB: Do not forget to add the '.php ' extension
 *
 * 3. add the callback function for the endpoint.
 *    The function name should be the name of the endpoint with a '_callback' prefix.
 *    For example: {$endpoint_name}_callback
 *
 * NB: The array is arranged in alphabetical order for readability
 */

function dm_register_wp_api_endpoints()
{
    $api_methods = array(
        'GET' => array(
            // 'notif_url',
        ),
        'POST' => array(
            'notif_url',
        ),
    );

    foreach ($api_methods as $api_method => $api_endpoints) {

        foreach ($api_endpoints as $api_endpoint) {

            $file_path = 'callbacks/' . $api_endpoint . '.php';
            include $file_path;

            register_rest_route('dm', '/' . $api_endpoint, array(
                'methods' => $api_method,
                'callback' => $api_endpoint . '_callback',
                'permission_callback' => 'permissionCallback'
            ));
        }
    }
}

add_action('rest_api_init', 'dm_register_wp_api_endpoints');

function permissionCallback($request) {
    if (WP_REST_Server::CREATABLE !== $request->get_method()) {
        return false;
    }
    wp_populate_basic_auth_from_authorization_header();
    return true;
}
