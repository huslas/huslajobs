<?php

// Orange Money return page

require_once("../../../wp-load.php");

// Redirect if payment is complete
if (isset($_GET['order_id']) && !empty($_GET['order_id'])) {
    $order_id = $_GET['order_id'];

     $order = wc_get_order($order_id);

    // Get an instance of the WC_Order object

    if ($order->is_paid()) {

        // $order = WC_get_order($order_id);

        // Get the order key
        $order_key = $order->get_order_key();

        if ( wp_redirect(  get_option( 'siteurl' ) . '/checkout/order-received/'.$order_id.'/?key='.$order_key ) ) {
            exit;
        }

        // echo '<script>window.location.href = "' . $this->get_return_url($order) . '"</script>';
    } else {
        wc_add_notice(__('Payment failed', 'woo-mobipay'), 'error');

        if ( wp_redirect( wc_get_checkout_url() ) ) {
            exit;
        }
    }
}
