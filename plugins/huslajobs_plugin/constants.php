<?php

global $wpdb;

define( 'HUSLA_JOBS_URL', plugin_dir_url( __FILE__ ) );
define( 'HUSLA_JOBS_DIR', plugin_dir_path( __FILE__ ) );
define( 'HUSLA_JOBS_TEMPLATES_DIR', plugin_dir_path( __FILE__ ) . 'templates/' );
define( 'HUSLA_JOBS_JS_URL', HUSLA_JOBS_URL . 'js' );
define( 'HUSLA_JOBS_CSS_URL', HUSLA_JOBS_URL . 'css' );
define( 'HUSLA_JOBS_IMAGE_URL', HUSLA_JOBS_URL . 'images' );
define( 'HUSLA_JOBS_ASSESTS_ADMINURL', home_url( '/' ) . '/wp-admin' );

define( 'HUSLA_JOBS_FRONTEND_DIR', plugin_dir_path( __FILE__ ) . 'frontend' );

define( 'HUSLA_JOBS_LIB_DIR', plugin_dir_path( __FILE__ ) . 'lib' );
define( 'HUSLA_JOBS_MODELS_DIR', plugin_dir_path( __FILE__ ) . 'models' );

define( 'HUSLA_JOBS_INC_DIR', plugin_dir_path( __FILE__ ) . 'inc' );
define( 'HUSLA_JOBS_AJAX_DIR', plugin_dir_path( __FILE__ ) . 'ajax' );

define( 'HUSLA_JOBS_MIGRATIONS_DIR', plugin_dir_path( __FILE__ ) . 'migrations' );

define( 'HUSLA_JOBS_FONTS_DIR', plugin_dir_path( __FILE__ ) . 'fonts' );

define( 'HUSLA_JOBS_LIB_URL', HUSLA_JOBS_URL . 'lib' );

define( 'HUSLA_JOBS_ADMIN_DIR', plugin_dir_path( __FILE__ ) . 'admin' );
define( 'HUSLA_JOBS_CLIENT_DIR', plugin_dir_path( __FILE__ ) . 'client' );

define( 'HUSLA_JOBS_CLIENT_MODULE_DIR', plugin_dir_path( __FILE__ ) . 'client/modules' );

define( 'HUSLA_JOBS_CLIENT_MODULE_URL', HUSLA_JOBS_URL . 'client/modules' );

define( 'HUSLA_JOBS_ADMIN_MODULE_DIR', HUSLA_JOBS_DIR . 'admin/modules' );

define( 'HUSLA_JOBS_ADMIN_MODULE_URL', HUSLA_JOBS_URL . 'admin/modules' );

define( 'WP_TABLE_PREFIX', $wpdb->prefix );
define( 'HUSLA_TABLE_PREFIX', WP_TABLE_PREFIX. 'husla_' );

