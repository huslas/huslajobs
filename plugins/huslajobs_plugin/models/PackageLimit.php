<?php

namespace huslajobs;

class PackageLimit extends HuslaModel {
	protected static string $table_name = HUSLA_TABLE_PREFIX . 'package_limits';

	public function package(): Package {

		$package = Package::where( 'id', '=', $this->package_id )->first();

		return $package;
	}
    /**
     * @return string
     */
    public static function tableName(): string
    {
        return  self::$table_name;
    }
}
