<?php

namespace huslajobs;


class JobType extends HuslaModel
{
	protected static string $table_name = HUSLA_TABLE_PREFIX . 'job_types';
    public function __construct() {
        parent::__construct();
    }

	/**
	 * @return array<Package>
	 */
	public function packages(): array
	{
		$packages = [];
		if (sizeof(PackageLimit::all())) {
			$limits   = PackageLimit::where('job_type_id', '=', $this->id)->get();
			foreach ($limits as $limit) {
				array_push($packages, $limit->package());
			}
		}
		return $packages;
	}

	/**
	 * @return array<Job>
	 */
	public function jobs(): array
	{
		if (sizeof(Job::all()) > 0) {
			$jobs = Job::where('job_type_id', '=', $this->id)->get();
			return $jobs;
		}
		return [];
	}

    public static function tableName(): string
    {
        return  self::$table_name;
    }


}
