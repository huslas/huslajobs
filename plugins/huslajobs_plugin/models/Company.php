<?php

namespace huslajobs;

class Company extends HuslaModel {

    protected static string $table_name = HUSLA_TABLE_PREFIX . 'companies';

    public function __construct() {
        parent::__construct();
    }

    public function delete(): bool {
        foreach ( $this->profiles() as $profile ) {
            if ( ! $profile->delete() ) {
                return false;
            }
        }
        foreach ( $this->jobs() as $job ) {
            if ( ! $job->delete() ) {
                return false;
            }
        }
//       subscriptions are linked directly with the user account
//		foreach ( $this->subscriptions() as $subscription ) {
//			if ( ! $subscription->delete() ) {
//				return false;
//			}
//		}

        foreach ( $this->applications() as $application ) {
            if ( ! $application->delete() ) {
                return false;
            }
        }

//        package subscriptions is linked directly with the user account
//		foreach ( $this->packages() as $package ) {
//			if ( ! $package->delete() ) {
//				return false;
//			}
//		}

        return parent::delete();
    }

    /**
     * @return array<Job>
     */
    public function jobs(): array {

        $jobs = Job::where( 'account_id', '=', $this->id )->get();

        return $jobs;
    }

    /**
     * @return array<Subscription>
     */
    public function subscriptions(): array {
        $subscriptions = Subscription::where( 'account_id', '=', $this->id )->get();

        return $subscriptions;
    }

    /**
     * @return array<JobApplication>
     */
    public function applications(): array {
        $applications = JobApplication::where( 'account_id', '=', $this->id )->get();

        return $applications;
    }

    /**
     * @return array<Profile>
     */
    public function profiles(): array {
        $profiles = Profile::where( 'account_id', '=', $this->id )->get();

        return $profiles;
    }

    /**
     * @return array<Package>
     */
    public function packages(): array {
        return Package::where( 'account_id', '=', $this->id )->get();
    }

    /**
     * @return string
     */
    public static function tableName(): string
    {
        return  self::$table_name;
    }

}
