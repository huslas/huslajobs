<?php

namespace huslajobs;

class Job extends HuslaModel {
	protected static string $table_name = HUSLA_TABLE_PREFIX . 'jobs';

    public function __construct() {
        parent::__construct();
    }
	/**
	 * @return array<JobApplication>
	 */
	public function applications(): array {
		return JobApplication::where( 'job_id', '=', $this->id )->get();
	}

	public function delete(): bool {
		foreach ( $this->applications() as $application ) {
			if ( ! $application->delete() ) {
				return false;
			}
		}

		return parent::delete();
	}

    /**
     * @return array<Currency>
     */
    public function currency(): array
    {
        return Currency::where('id', '=', $this->currency_id)->get();
    }

    /**
     * @return array<JobType>
     */
    public function jobType(): array
    {
        return JobType::where('id', '=', $this->job_type_id)->get();
    }

    /**
     * @return array<Category>
     */
    public function categories(): array
    {
        $categories = [];
        $jobCategories = explode(',',$this->categories);
        foreach ($jobCategories as $category_id){
            $categories[] = Category::where('id', '=', intval($category_id))->first();
        }
        return $categories;
    }

    /**
     * @return string
     */
    public static function tableName(): string
    {
        return  self::$table_name;
    }
}