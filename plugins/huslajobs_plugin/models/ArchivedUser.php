<?php

namespace huslajobs;

class ArchivedUser extends HuslaModel
{
    protected static string $table_name = HUSLA_TABLE_PREFIX . 'archived_users';

    public function __construct() {
        parent::__construct();
    }
}