<?php

namespace huslajobs;

class Subscription extends HuslaModel {
	protected static string $table_name = HUSLA_TABLE_PREFIX . 'subscriptions';

	public function __construct() {
		parent::__construct();
	}

	// todo: DELETE THIS METHOD
	public function account(): Account {
		$account = Account::where( 'id', '=', $this->account_id )->first();

		return $account;
	}

	public function user(): WpUser {
		$user = WpUser::where( 'ID', '=', $this->wp_user_id )->first();

		return $user;
	}

	public function package(): Package {
		$package = Package::where( 'id', '=', $this->package_id )->first();

		return $package;
	}
    /**
     * @return string
     */
    public static function tableName(): string
    {
        return  self::$table_name;
    }
}