<?php

namespace huslajobs;

/**
 * Add models to be included
 */

function addModelsIncludes( $includes ) {
	$models = [
		HUSLA_JOBS_MODELS_DIR . '/Account.php',//
        HUSLA_JOBS_MODELS_DIR . '/ArchivedUser.php',//
//		HUSLA_JOBS_MODELS_DIR . '/Provider.php',//
		HUSLA_JOBS_MODELS_DIR . '/Company.php',//
//		HUSLA_JOBS_MODELS_DIR . '/FreelanceProvider.php',//
		HUSLA_JOBS_MODELS_DIR . '/Job.php',//
		HUSLA_JOBS_MODELS_DIR . '/JobSeeker.php',//
		HUSLA_JOBS_MODELS_DIR . '/JobType.php',//
		HUSLA_JOBS_MODELS_DIR . '/Package.php',//
		HUSLA_JOBS_MODELS_DIR . '/Profile.php',//
		HUSLA_JOBS_MODELS_DIR . '/ProfileCategory.php',//
		HUSLA_JOBS_MODELS_DIR . '/Subscription.php',//
		HUSLA_JOBS_MODELS_DIR . '/PackageLimit.php',//
		HUSLA_JOBS_MODELS_DIR . '/Category.php',//
		HUSLA_JOBS_MODELS_DIR . '/JobApplication.php',//
        HUSLA_JOBS_MODELS_DIR . '/Currency.php',//
        HUSLA_JOBS_MODELS_DIR . '/JobCategory.php',//
        HUSLA_JOBS_MODELS_DIR . '/WpUser.php',//

    ];


	$includes = array_merge( $includes, $models );

	return $includes;
}

add_filter( 'husla_jobs_includes_filter', 'huslajobs\addModelsIncludes' );