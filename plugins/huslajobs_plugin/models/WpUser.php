<?php

namespace huslajobs;

class WpUser extends HuslaModel {


    protected static string $table_name = WP_TABLE_PREFIX.'users';

	public function __construct() {
		parent::__construct();
    }


    /**
     * @return string
     */
    public static function tableName(): string
    {
        return  self::$table_name;
    }
    /**
     * @return array<Job>
     */
    public function jobs(): array {

        $jobs = Job::where( 'wp_user_id', '=', $this->ID )->get();

        return $jobs;
    }

    /**
     * @return array<Subscription>
     */
    public function subscriptions(): array {
        $subscriptions = Subscription::where( 'wp_user_id', '=', $this->ID )->get();

        return $subscriptions;
    }

    /**
     * @return array<JobApplication>
     */
    public function applications(): array {
        $applications = JobApplication::leftJoin('profiles')
            ->on('profile_id','id')
            ->where( 'wp_user_id', '=', $this->ID )

        ->get();

        return $applications;
    }

    /**
     * @return array<Profile>
     */
    public function profiles(): array {
        $profiles = Profile::where("completed","=",1)->andWhere('wp_user_id', '=', $this->ID )->get();
        return $profiles;
    }

    /**
     * @return array<Package>
     */
    public function packages(): array {
        return Package::where( 'wp_user_id', '=', $this->ID )->get();
    }

    /**
     * @return array
     */
    public function companies(): array {
        return Company::where( 'wp_user_id', '=', $this->ID )->get();
    }
    public function delete(): bool {
        foreach ( $this->profiles() as $profile ) {
            if ( ! $profile->delete() ) {
                return false;
            }
        }
        foreach ( $this->jobs() as $job ) {
            if ( ! $job->delete() ) {
                return false;
            }
        }
        foreach ( $this->companies() as $company ) {
            if ( ! $company->delete() ) {
                return false;
            }
        }

		foreach ( $this->subscriptions() as $subscription ) {
			if ( ! $subscription->delete() ) {
				return false;
			}
		}

        foreach ( $this->applications() as $application ) {
            if ( ! $application->delete() ) {
                return false;
            }
        }

//        package subscriptions is linked directly with the user account
//		foreach ( $this->packages() as $package ) {
//			if ( ! $package->delete() ) {
//				return false;
//			}
//		}

        return parent::delete();
    }

}