import {LoginForm} from "./login-form.js";
import {VLoader} from  "../../../../../inc/global_components/loader/v-loader.js"
import {ReusableFunctions} from "../../../../../js/functions.js";
const Login = {
    data() {
        return {
            form:{
                username:undefined,
                password:undefined,
            },
            successMessage:undefined,
            formErrors:{},
            submitting:false,
            loading:false,
            imgUrl:THEME_URL,
            loginStrings:VueUiStrings.clientFrontEnd.login
        };
    },
    components:{
        LoginForm,
        VLoader
    },
    mixins:[ReusableFunctions],
    methods: {
        redirectUser(data){
            document.location.href = data?.redirect_to;
        }
    },
    mounted(){
        setTimeout(()=>{
            this.closeLoader();
        },200)

    },

    template: `
    <section class="hs-section">
        <div class="row text-center">
            <div class="col-md-12">

                <div class="hs-page-header">
                    <h1 class="page-title p-0 mb-0 ">
                        {{loginStrings.heading.toString()}}
                    </h1>

                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <div class="form">
                    <login-form @user-logged-in="redirectUser"></login-form>
                    <div id="login-links" class="text-center">
                        <p>
                            <a href="/signup" class="hs-primary-text-color-light">{{loginStrings.links.haveAccount.toString()}}</a>
                        </p>

                        <p>
                            <a href="/forgot-password" class="hs-primary-text-color-light">{{loginStrings.links.forgotPassword.toString()}}</a>
                            | <a href="/resend-verification-code" class="hs-primary-text-color-light">{{loginStrings.links.resendCode.toString()}}
                        </a>

                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-4"></div>
        </div>
    </section>
`,
};

export {Login};
