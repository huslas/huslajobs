<?php
    get_header();
    ?>

<div id="job-application-content" class="content-area primary vue-component"></div>
<script type="text/javascript">
    const job_slug = "<?php echo $wp_query->query_vars['job'] ?>"
</script>
<div id="login-nonce">
    <?php wp_nonce_field( 'huslajobs-login', 'huslajobs-login-nonce' ); ?>
</div>
<script src="<?php echo HUSLA_JOBS_CLIENT_MODULE_URL ?>/jobs/resources/index.js" type="module"></script>

<?php get_footer(); ?>
