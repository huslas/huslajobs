import {AllJobs} from "./components/jobs.js";
import {SingleJob} from "./components/single-job.js";
import {JobApplication} from "./components/job-application.js";

// VUE 3 initialization

//jobs initialisation
const jobs = Vue.createApp({
    render() {
        return Vue.h(AllJobs, {});
    },
});
if (document.getElementById('jobs-content')){
    jobs.mount("#jobs-content");
}


//single job view
const singleJob = Vue.createApp({
    render() {
        return Vue.h(SingleJob, {});
    },
});
if (document.getElementById('single-job-content')){
    singleJob.mount('#single-job-content')
}

//job application
const jobApplication = Vue.createApp({
    render() {
        return Vue.h(JobApplication, {});
    },
});

if (document.getElementById('job-application-content')){
    jobApplication.mount('#job-application-content')
}
