import {JobSearchForm} from "../../../dashboard/resources/components/job-search/job-search-form.js";
import {ReusableFunctions} from "../../../../../js/functions.js";
import {LoadingBar} from "../../../../../admin/modules/dashboard/resources/components/LoadingBar.js";
import {LoadingError} from "../../../../../admin/modules/dashboard/resources/components/LoadingError.js";
import {JobCard} from "./job-card.js";
import {VLoader} from "../../../../../inc/global_components/loader/v-loader.js";

const SingleJob = {
    data() {
        return {
            jobSlug: job_slug,
            loading: false,
            job: undefined,
            error: false,
            homeUrl: home_url,
            relatedJobs: [],
            user_id,
            active_account:undefined,
            has_seeker_account:false,
            has_exceed_app_limit:false,
            singleJobStrings:VueUiStrings.clientFrontEnd.singleJob,
            applicationLimitMessage:application_limit_message,
            applicationLimitLinkMessage:application_limit_link_message

        }

    },
    mixins: [ReusableFunctions],
    mounted() {
        const user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) :undefined
        if (user && user.job_applications) {
            if (user.subscriptions){
                for (const subscription of user.subscriptions) {
                    if (parseInt(subscription.package_job_applications) > -1 && (parseInt(user.job_applications) >= parseInt(subscription.package_job_applications))){
                        this.has_exceed_app_limit = true
                    }
                }
            }
        }
        if(this.user_id){
            this.getJobAndRelatedJobs();
        }
        else{
                const loginUrl = this.homeUrl+'/login';
                const jobUrl = window.location.href;
                const url = loginUrl+"?redirect_url="+jobUrl;
                window.location.href = url;
        }
    },
    components: {
        JobSearchForm,
        LoadingBar,
        LoadingError,
        JobCard,
        VLoader

    },
    created(){
        this.setJActiveLink('/jobs')
    },
    template: `<!--  search -->
<VLoader :active="loading" :translucent="true">
    <section id="single-job" class="hs-section">
        <div v-if="error">
            <loading-error @update="getJobAndRelatedJobs"></loading-error>
        </div>
        <div v-else-if="job">

            <div class="row mb-5" v-if="Object.keys(job).length > 0 ">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="header">
                        <h2 class="section-title mb-4 p-0">{{decodeHtml(job.name)}}</h2>
                        <p class="m-0">{{singleJobStrings.headings.posted}}: {{jobsDate(job.created_at)}}</p>
                        <p v-if="job.work" class="m-0">{{singleJobStrings.headings.workLocation}}: {{job.work}} </p>
                    </div>
                    <div class="row body mt-3">
                        <div v-if="job.job_type_name || job.crawled_job_types" class="col-md-3">
                            <h5 class="hs-subtitle">{{singleJobStrings.headings.jobType}}</h5>
                            <p>{{decodeHtml(job.job_type_name) || job.crawled_job_types}}</p>
                        </div>
                        <div v-if="job.category_name || job.crawled_categories" class="col-md-3">
                            <h5 class="hs-subtitle">{{singleJobStrings.headings.jobCategory}}</h5>
                            <p>{{decodeHtml(job.category_name) || job.crawled_categories}}</p>
                        </div>

                        <div v-if="job.experience" class="col-md-3">
                            <h5 class="hs-subtitle">{{singleJobStrings.headings.experience}}</h5>
                            <p>
                                <span v-if="parseInt(job.experience) > 1">{{job.experience}} {{singleJobStrings.headings.years}}</span>
                                <span v-else-if="parseInt(job.experience) == 1">{{job.experience}} {{singleJobStrings.headings.year}}</span>
                                <span v-else>{{singleJobStrings.headings.noExperience}}</span>
                            </p>
                        </div>
                        <div v-if="job.cv_required" class="col-md-3">
                            <h5 class="hs-subtitle">{{singleJobStrings.headings.cv}}</h5>
                            <p>
                                <span v-if="job.cv_required">{{singleJobStrings.required}}</span>
                                <span v-else>{{singleJobStrings.notRequired}}</span>
                            </p>
                        </div>
                        <div v-if="job.salary" class="col-md-3">
                            <h5 class="hs-subtitle">{{singleJobStrings.headings.salary}}</h5>
                            <p>
                                <span v-if="job.salary">{{job.salary}}{{job.currency_code}}</span>
                                <span v-else>--</span>
                            </p>
                        </div>
                        <div v-if="job.country || job.crawled_job_location" class="col-md-3">
                            <h5 class="hs-subtitle">{{singleJobStrings.headings.country}}</h5>
                            <p>{{selectedCountry(job.country) || job.crawled_job_location}}</p>
                        </div>
                        <div v-if="job.state" class="col-md-3">
                            <h5 class="hs-subtitle">{{singleJobStrings.headings.state}}</h5>
                            <p>{{decodeHtml(job.state)}}</p>
                        </div>
                        <div v-if="job.city" class="col-md-3">
                            <h5 class="hs-subtitle">{{singleJobStrings.headings.city}}</h5>
                            <p>{{decodeHtml(job.city)}}</p>
                        </div>
                        <div v-if="job.motivation_required" class="col-md-12">
                            <h5 class="hs-subtitle">{{singleJobStrings.headings.motivation}}</h5>
                            <p>
                                <span v-if="job.motivation_required">{{singleJobStrings.required}}</span>
                                <span v-else>{{singleJobStrings.notRequired}}</span>

                            </p>
                        </div>
                        <div v-if="job.description" class="col-md-12">
                            <h5 class="hs-subtitle">{{singleJobStrings.headings.jobDescription}}</h5>
                            <div id="job-desc"></div>
                        </div>
<!--                        v-if="showJobOwner"-->
                        <div  class="col-md-12 mt-4">
                       
                        <p class="mb-0">
                        <span class="hs-subtitle mr-2">Posted by:</span>
                         <span v-if="job.company_name"> {{job.company_name}}</span>
                         <span v-else>
                         {{job.first_name}} <span>{{job.last_name}}</span></span>
                         </p>
                         <p class="mb-0">
                         <span class="hs-subtitle mr-2" >Address:</span>
                            <span v-if="job.company_address" >{{job.company_address}}</span>
                            <span v-else>
                            {{job.user_city}},
                            {{job.user_state}}
                            <span class="ml-1">{{selectedCountry(job.user_country)}}</span>
                            </span>
                         </p>
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-md-12">
                            <a v-if="job.crawled_job_url" :href="job.crawled_job_url" class="hs-btn hs-btn-primary d-none"
                               target="_blank">{{singleJobStrings.links.apply}}</a>
                                 <h5 v-else-if="has_exceed_app_limit" class="hs-title">
                                    <span class="hs-secondary-text-color">{{applicationLimitMessage}}</span>
                                       <a :href="homeUrl+'/my-account/#/upgrade-account'"
                                       class="hs-primary-text-color">{{applicationLimitLinkMessage}}</a>
                                    </h5>
                            <a v-else :href="homeUrl+'/jobs/'+job.slug+'/apply'" class="hs-btn hs-btn-primary">{{singleJobStrings.links.apply}}</a>
                        </div>
                    </div>
                    <!--        Related data-->
                    <div v-if="relatedJobs.length" class="row mt-5">

                        <div class="col-md-12">
                            <h5 class="hs-title mb-4">{{singleJobStrings.relatedJobs}}</h5>
                        </div>
                        <div class="col-md-12">
                            <job-card v-for="(job,index) in relatedJobs" :key="index" :job-data="[job]"></job-card>
                        </div>

                    </div>
                </div>
                <div class="col-md-1"></div>
            </div>
            <div class="row mb-5" v-else>
                <div class="col-md-12 text-center">
                    <p>{{singleJobStrings.notFound}}</p>
                </div>
            </div>

        </div>
        <div v-else class="text-center">
            <h4 class="hs-title">{{singleJobStrings.notFound}}</h4>
        </div>
    </section>


</VLoader> 
    `
}

export {SingleJob}