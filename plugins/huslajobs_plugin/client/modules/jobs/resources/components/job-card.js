import {ReusableFunctions} from "../../../../../js/functions.js";
import {AnaToolTip} from "../../../../../inc/global_components/tool-tip/ana-tool-tip.js";

const JobCard = {
    props: {
        jobData: {
            type: [Array],
            required: false,
            default: () => ([])
        },
        user:{
            required: false
        }
    },
    data() {
        return {
            jobCardStrings: VueUiStrings.clientFrontEnd.jobs,
            homeUrl: home_url,
            tooltip:undefined,
            userId:user_id,
        }
    },
    methods: {
        jobUrl(job) {
            let url = this.homeUrl + '/jobs/' + job.slug
            return url;
        },

        jobLocation(job) {
            if (job.country) {
                const country = this.selectedCountry(job.country)
                return job.state + ', ' + country
            } else if (job.crawled_job_location) {
                return job.crawled_job_location
            }
        }
    },
    components:{
        AnaToolTip
    },
    mixins: [ReusableFunctions],
    template: `<div class="mb-4 p-4 search-result job-card hs-border-1">
    <div class="d-flex justify-content-between flex-wrap">
        <div class="col-md-10 p-0">
            <h5 v-if="userId" class="hs-title"><a :href="jobUrl(jobData[0])">{{decodeHtml(jobData[0].name)}}</a></h5>
            <h5 v-else class="hs-title">{{decodeHtml(jobData[0].name)}}</h5>
            <p>{{decodeHtml(reduceString(jobData[0].crawled_job_summary ||jobData[0].description,200))}}</p>
        </div>
        <div class="col-m-2 d-flex flex-column align-items-end"></div>

    </div>

    <div class="d-flex justify-content-between flex-wrap align-items-center">
        <div class="col-md-10 p-0 d-flex flex-wrap">
            <span v-if="jobData[0].job_type_name || jobData[0].crawled_job_types"
                  class="mr-3 d-flex align-items-center text-capitalize mb-2 job-card-cat">
<i class="hs-primary-text-color mr-1 fa fa-bookmark" aria-hidden="true"></i>
                <span>{{decodeHtml(jobData[0].job_type_name) || jobData[0].crawled_job_types}}</span>
            </span>
            <span v-if="jobData[0].category_name || jobData[0].crawled_categories"
                  class="mr-3 d-flex align-items-center text-capitalize mb-2 job-card-cat">
<i class="fa fa-briefcase hs-primary-text-color mr-1"></i>               
<span>{{decodeHtml(jobData[0].category_name) || jobData[0].crawled_categories}}</span>
            </span>
            <span v-if="jobData[0].work"
                  class="mr-3 d-flex align-items-center text-capitalize mb-2 job-card-cat">
<i class="fa fa-user hs-primary-text-color mr-1"></i>               
<span>{{jobData[0].work}}</span>
            </span>
            <span v-if="jobData[0].country || jobData[0].crawled_job_location"
                  class="mr-3 d-flex align-items-center text-capitalize job-card-cat mb-2">
                
                   <i class="fa fa-map-marker hs-primary-text-color mr-1" aria-hidden="true"></i>

                <span>{{jobLocation(jobData[0])}}</span>
            </span>
        </div>


        <div class="col-m-2 d-flex flex-column align-items-end">
            <a v-if="userId" :href="jobUrl(jobData[0])" class="hs-btn hs-btn-gray-outline  hs-btn-icon hs-btn-sm">
                <span class="mr-2">{{jobCardStrings.viewJob}}</span>
                <span class="icon fas fa-chevron-right"></span>
            </a>           
             <a v-else :href="homeUrl+'/login?redirect_url='+jobUrl(jobData[0])" class="hs-btn hs-btn-gray-outline  hs-btn-icon hs-btn-sm position-relative"
                @mouseover="tooltip=jobData[0].slug"
                                             @mouseleave="tooltip=undefined"
             >
                <span class="mr-2">{{jobCardStrings.viewJob}}</span>
                <span class="icon fas fa-chevron-right"></span>
                   <ana-tool-tip v-if="showToolTip(tooltip,jobData[0].slug)"
                                                  :message="jobCardStrings.login"></ana-tool-tip>
            </a>
        </div>

    </div>

</div> 
    
    `

}

export {JobCard}