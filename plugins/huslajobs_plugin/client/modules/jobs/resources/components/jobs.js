import {JobSearchForm} from "../../../dashboard/resources/components/job-search/job-search-form.js";
import {JobSearchResults} from "../../../dashboard/resources/components/job-search/job-search-results.js";
import {PaginationComponent} from "../../../dashboard/resources/components/pagination/pagination.js";
import {ReusableFunctions} from "../../../../../js/functions.js";
import {LoadingBar} from "../../../../../admin/modules/dashboard/resources/components/LoadingBar.js";
import {LoadingError} from "../../../../../admin/modules/dashboard/resources/components/LoadingError.js";
import {Pagination} from "../../../../../admin/modules/dashboard/resources/components/Pagination.js";
import {JobCard} from "./job-card.js";
import {JobSearch} from "../../../dashboard/resources/components/job-search/job-search.js";
import {VLoader} from "../../../../../inc/global_components/loader/v-loader.js";

const AllJobs = {
    emits: ['account_jobs'],
    data() {
        return {
            jobs: undefined,
            page: 1,
            perPage: jobs_per_page,
            loading: false,
            error: false,
            deleting: false,
            pages: 0,
            sortField: 'id',
            sort: 'desc',
            searchText: '',
            account_id: this.active_account_id,
            searchFields: "name,description,location,job_type",
            searchResults: undefined,
            total: 0,
            show_results: false,
            job_limit: husla_jobs_limit,
            homeUrl: home_url,
            jobsStrings:VueUiStrings.clientFrontEnd.jobs,
            user:undefined,
            is_Admin
        };
    },
    mixins: [ReusableFunctions],
    components: {LoadingBar, LoadingError, Pagination, JobCard, JobSearchForm, JobSearchResults, JobSearch, VLoader},
    mounted() {
        /**
         * set job limit
         */
        let user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : undefined;
        this.user = user
        if (user && user.subscriptions) {
            for (const subscription of user.subscriptions) {
                const start_date = Date.parse(new Date(subscription.start_date));
                this.job_limit = parseInt( subscription.package_jobs);
            }
        }
        //show all jobs for admins
        if (parseInt(is_Admin)){
            this.job_limit = -1;
        }
        if (this.job_limit &&  (this.job_limit !== -1 && this.perPage > this.job_limit)){
            this.perPage = this.job_limit
        }


        //todo get all
        this.getAllJobs(0);

        // this.startLoader();
    },
    methods: {

        goToPage(page) {
            let itemsPerPage = this.perPage;
            this.page = page;
            if (this.job_limit && parseInt(this.job_limit) !== -1) {
                if (this.page == this.pages) {
                    itemsPerPage = this.job_limit - ((this.page - 1) * this.perPage);
                }
            }
            this.getAllJobs(itemsPerPage);
        },
        showResults(value) {
            this.show_results = value;
        },
        closeSearchResults() {
            this.searchResults.searchResults = undefined;
            this.total = 0;
        }

    },
    created(){
        this.setJActiveLink('/jobs')
    },
    template: `<VLoader :active="loading" :translucent="true">
    <div class="all-jobs">

        <div v-if="error">
            <loading-error @update="getAllJobs"></loading-error>
        </div>
        <section class="hs-section">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <job-search @show-results="showResults" :availableJobs="total"></job-search>
                </div>
                <div class="col-md-1"></div>
            </div>
        </section>
        <div v-if="jobs">
            <section v-if="!show_results" class="hs-section">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div v-if="jobs.length" class="col-md-10">
                        <div class="row mb-1">
                            <div class="col-md-12">
                                <p v-if="job_limit && parseInt(job_limit) !== -1 && (parseInt(total) > parseInt(job_limit) ) "
                                   class="hs-title">{{jobsStrings.results.message.toString()}} {{job_limit}}
                                    {{jobsStrings.of}}
                                    {{total}} {{jobsStrings.jobsAvailable}}. <a :href="homeUrl+ '/my-account/#/upgrade-account'"
                                                                       class="hs-secondary-text-color">
                                        <em v-if="user">{{jobsStrings.results.loginUpgrade}}</em>
                                        <em v-else>{{jobsStrings.results.guestUpgrade}}</em>
                                    </a></p>
                                <p v-else-if="parseInt(total) > 1" class="hs-title">{{jobsStrings.results.message.toString()}} {{total}}
                                    {{jobsStrings.of}}
                                    {{total}} {{jobsStrings.jobsAvailable}}
                                   </p>
                                  <p v-else class="hs-title">{{jobsStrings.results.message.toString()}} {{total}}
                                    {{jobsStrings.of}}
                                    {{total}} {{jobsStrings.jobAvailable}}
                                   </p>
                             
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <job-card v-for="(job,index) in jobs" :key="index" :job-data="[job]" :user="user"></job-card>
                            </div>

                        </div>
                        <pagination v-if="pages>1" :page="page" :pages="pages"
                                    @update="goToPage">

                        </pagination>

                    </div>
                    <div v-else class="col-md-10 text-center">
                        <p class="hs-subtitle">{{jobsStrings.noJobs.toString()}}</p>
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </section>


        </div>

    </div>
</VLoader>
    `
}

export {AllJobs}