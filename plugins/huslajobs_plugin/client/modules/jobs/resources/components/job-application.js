import {world_countries as WorldCountries} from "../../../../../js/countries_data.js";
import {AnaVueSelect} from "../../../../../inc/global_components/ana_select/ana_vue_select.js";
import {CountriesStates} from "../../../../../js/states.js";
import {LoginForm} from "../../../login/resources/components/login-form.js";
import {SignUpForm} from "../../../signup/resources/components/signup-form.js";
import {CountriesData} from "../../../../../js/countries_and_codes.js";
import {LoadingBar} from "../../../../../admin/modules/dashboard/resources/components/LoadingBar.js";
import {LoadingError} from "../../../../../admin/modules/dashboard/resources/components/LoadingError.js";
import {ReusableFunctions} from "../../../../../js/functions.js"
import {JobCard} from "./job-card.js";
import {VLoader} from "../../../../../inc/global_components/loader/v-loader.js";
import {AnaVuePhoneNumber} from "../../../../../inc/global_components/phone-number/ana_vue_phone_number.js";
import {AnaVueFileUpload} from "../../../../../inc/global_components/ana_file_upload/ana_vue_fileupload.js";


const JobApplication = {
    refs: ['jobsModal'],
    data() {
        return {
            formData: {
                applicantName: undefined,
                applicantEmail: undefined,
                applicantPhone: undefined,
                applicantAddress: undefined,
                fileUpload: undefined,
                motivation: undefined,
            },
            progressSteps: [],
            imgUrl: THEME_URL ?? '',
            userId: user_id ?? 0,
            progressBarStyle: "width: 0%; margin-left: 5px",
            formErrors: {},
            submitting: false,
            currentStep: {label: VueUiStrings.clientFrontEnd.jobApplication.steps.application, value: 'application'},
            jobSlug: job_slug,
            relatedJobs: [],
            job: undefined,
            loading: false,
            page: 1,
            perPage: -1,
            pages: 0,
            countries: WorldCountries,
            sortField: 'id',
            sort: 'desc',
            createAction: "login",
            jsUrl: THEME_JS_URL,
            homeUrl: home_url,
            maxUpload: maximum_upload,
            error: false,
            currency: woocommerce_currency,
            user_profiles: [{
                label: VueUiStrings.clientFrontEnd.jobApplication.formFields.profile.options.noProfile,
                value: 'no profile'
            }, {
                label: VueUiStrings.clientFrontEnd.jobApplication.formFields.profile.options.newProfile,
                value: 'new profile'
            }],
            selected_profile: undefined,
            platform_fee: undefined,
            application_data: undefined,
            storage_key: 'applicationData' + job_slug,
            jobApplicationStrings: VueUiStrings.clientFrontEnd.jobApplication,
            user: undefined,
            payFee: false,
            profileId: undefined,
            applicationFeeMessage: application_fee_message,
            applicationFeeLinkMessage: application_fee_link_message,
            isAdmin: parseInt(is_Admin)

        };
    },
    mixins: [ReusableFunctions],

    watch: {
        'formData.applicantName': function (currentVal, oldVal) {
            this.validateInput("applicantName", currentVal, {required: true}, this.jobApplicationStrings.formFields.name.label);
        },
        'formData.applicantEmail': function (currentVal, oldVal) {

            this.validateInput("applicantEmail", currentVal, {required: true}, this.jobApplicationStrings.formFields.email.label);
        },
        'formData.applicantAddress': function (currentVal, oldVal) {
            this.validateInput("applicantAddress", currentVal, {required: true}, this.jobApplicationStrings.formFields.address.label);

        },
        'formData.motivation': function (currentVal, oldVal) {
            if (this.job.motivation_required) {
                this.validateInput("motivation", currentVal, {
                    min: 150,
                    max: 500,
                    required: true
                }, this.jobApplicationStrings.formFields.motivation.label);
            } else {
                this.validateInput("motivation", currentVal, {
                    min: 150,
                    max: 500
                }, this.jobApplicationStrings.formFields.motivation.label);
            }
        },
        'formData.fileUpload': function (currentVal, oldVal) {
            if (this.job.cv_required) {
                this.validateInput("fileUpload", currentVal, {required: true}, this.jobApplicationStrings.formFields.cv.label);
            }
        },
        selected_profile: function (currentVal, oldVal) {
            if (currentVal === 'new profile') {
                let redirect_url = window.location.href;
                let route_to = this.homeUrl + '/my-account/#/add-profile?redirect_url=' + redirect_url;
                window.location.href = route_to;
                this.profileId = undefined;

            } else if (currentVal !== 'new profile' || currentVal !== 'no profile') {
                this.profileId = currentVal;
            } else {
                this.profileId = undefined;
            }
            this.validateInput("selected_profile", currentVal, {required: true}, this.jobApplicationStrings.formFields.profile.errorlabel);
        }
    },
    components: {
        LoginForm,
        SignUpForm,
        LoadingBar,
        LoadingError,
        AnaVueSelect,
        JobCard,
        VLoader,
        AnaVuePhoneNumber,
        AnaVueFileUpload,

    },
    destroyed() {
        this.formData = {
            applicantName: undefined,
            applicantEmail: undefined,
            applicantPhone: undefined,
            city: undefined,
            country: undefined,
            applicationData: undefined,
            motivation: undefined,
        }
    },
    methods: {
        phoneNumberError(err) {
            if (!err && this.formErrors) {
                this.formErrors["applicantPhone"] = this.jobApplicationStrings.formFields.phone.error;
            } else if (err && (this.formErrors && this.formErrors["applicantPhone"])) {
                delete this.formErrors["applicantPhone"]
            }
        },
        goToNext() {
            this.formErrors = {};
            let storage;
            let applicationData = localStorage.getItem(this.storage_key);
            applicationData = applicationData ? JSON.parse(applicationData) : {}
            if (Object.keys(applicationData).length === 0) {
                applicationData = {
                    application: {},
                    user_info: {},
                    payment_fee: {},
                    review: {}
                }
            }
            if (this.currentStep.value.toLowerCase() === 'application') {

                if (this.selected_profile === 'no profile') {
                    this.validateInput("applicantName", this.formData.applicantName, {required: true}, this.jobApplicationStrings.formFields.name.label);
                    this.validateInput("applicantEmail", this.formData.applicantEmail, {required: true}, this.jobApplicationStrings.formFields.email.label);
                    this.validateInput("applicantAddress", this.formData.applicantAddress, {required: true}, this.jobApplicationStrings.formFields.address.label);
                    if (this.job.cv_required) {
                        this.validateInput("fileUpload", this.formData.fileUpload, {required: true}, this.jobApplicationStrings.formFields.cv.label);
                    }
                    if (this.job.motivation_required) {
                        this.validateInput("motivation", this.formData.motivation, {required: true}, this.jobApplicationStrings.formFields.motivation.label);
                    }
                }
                this.validateInput("selected_profile", this.selected_profile, {required: true}, this.jobApplicationStrings.formFields.profile.errorlabel);
            } else if (this.currentStep.value.toLowerCase() === 'login/signup') {
                if (parseInt(this.userId)) {
                    applicationData.user_info['isLoggedIn'] = true;
                } else {
                    toastr.error(this.jobApplicationStrings.errors.login);
                    applicationData.user_info['isLoggedIn'] = false;
                    return;
                }
            } else if (this.currentStep.value.toLowerCase() === 'platform fee') {
                if (!applicationData.payment_fee['payment_complete']) {
                    applicationData.payment_fee['payment_complete'] = undefined
                }
            }
            if (Object.keys(this.formErrors).length === 0) {
                if (this.currentStep.value.toLowerCase() === 'application') {
                    applicationData.application = {
                        "applicantName": this.formData.applicantName,
                        "applicantEmail": this.formData.applicantEmail,
                        "applicantPhone": this.formData.applicantPhone,
                        "applicantAddress": this.formData.applicantAddress,
                        "fileUpload": this.formData.fileUpload,
                        "motivation": this.formData.motivation,
                        "country": this.formData.country,
                        "city": this.formData.city,
                        "completed": true,
                        "profileId": this.profileId,
                        "selected_profile": this.selected_profile
                    }
                } else if (this.currentStep.value.toLowerCase() === 'login/signup') {
                    if (!applicationData.user_info.isLoggedIn) {
                        toastr.error(this.jobApplicationStrings.errors.login);
                        return;
                    }
                    applicationData.user_info['completed'] = true;
                } else if (this.currentStep.value.toLowerCase() === 'platform fee') {
                    if (!applicationData.payment_fee['payment_complete']) {
                        toastr.error(this.jobApplicationStrings.errors.makePayments);
                        return;
                    }

                }

                localStorage.setItem(this.storage_key, JSON.stringify(applicationData));
                this.nextStep();
            } else {
                const firstErrorControl = document.querySelector('.validation-error')?.parentElement;
                // Scroll to first error element
                window.scrollTo(
                    firstErrorControl?.offsetLeft || 0,
                    (firstErrorControl?.offsetTop || 0) - 50 // Subtract 50 for better exposure
                )
            }
        },
        nextStep() {
            const currentStepIndex = this.getObjectIndex(this.progressSteps, this.currentStep.value) + 1;
            const style = this.progressSteps.length > 3 ? "margin-left: 10px;" : "margin-left: 8px;";
            this.styleProgressBar(style, currentStepIndex);
        },
        goToPrevious() {
            const currentStepIndex = this.getObjectIndex(this.progressSteps, this.currentStep.value) - 1;
            const style = "margin-left: 10px;";
            this.styleProgressBar(style, currentStepIndex);
        },
        styleProgressBar(style, index) {
            this.currentStep = this.progressSteps[index];
            let width = ((index) / (this.progressSteps.length - 1) * 100);
            let gap = '5px';
            if (width > 0) {
                gap = ((width / 5) + 5) + 'px';
                width = width + '%'
                width = `width: calc(${width} - ${gap}) !important`;
            } else {
                width = "width: 0% !important";
            }
            this.progressBarStyle = style + width;
            window.scrollTo({
                top: 0,
                left: 0,
                behavior: 'smooth'
            });
        },
        getUserProfiles() {
            let profiles = this.user?.profiles ?? [];
            if (profiles.length) {
                profiles = profiles.map((profile) => {
                    return {
                        label: profile.title,
                        value: profile.id,
                    }
                });
                this.user_profiles = [...this.user_profiles, ...profiles]
            }
        },
        setCurrentStep(payment_complete = undefined) {
            //job data
            let applicationData = localStorage.getItem(this.storage_key);
            if (applicationData) {
                let isEmpty = true;

                applicationData = JSON.parse(applicationData);
                if (payment_complete) {
                    applicationData.payment_fee['payment_complete'] = payment_complete;
                    localStorage.setItem(this.storage_key, JSON.stringify(applicationData))
                }
                this.application_data = applicationData;
                if (applicationData.application && Object.keys(applicationData.application).length > 0) {

                    this.formData.fileUpload = applicationData.application.fileUpload;
                    if (this.formData.fileUpload) {
                        const {file, url} = this.dataURLtoFile(this.formData.fileUpload);
                        this.formData.fileUpload.file = file;
                        this.formData.fileUpload.url = url;
                    }
                    this.profileId = applicationData.application.profileId;
                    this.selected_profile = applicationData.application.selected_profile;
                    this.formData.applicantName = applicationData.application.applicantName;
                    this.formData.applicantEmail = applicationData.application.applicantEmail;
                    this.formData.applicantPhone = applicationData.application.applicantPhone;
                    this.formData.applicantAddress = applicationData.application.applicantAddress;
                    this.formData.motivation = applicationData.application.motivation;
                    this.formData.country = applicationData.application.country;
                    this.formData.city = applicationData.application.city;
                    let index = this.getObjectIndex(this.progressSteps, 'application')
                    this.currentStep = this.progressSteps[index];
                    isEmpty = false;
                }
                if (applicationData.user_info?.isLoggedIn) {
                    let index = this.getObjectIndex(this.progressSteps, 'application');
                    if (!parseInt(this.userId)) {
                        let index = this.getObjectIndex(this.progressSteps, 'login/signup');
                    }
                    this.currentStep = this.progressSteps[index];

                }
                if (applicationData.payment_fee && Object.keys(applicationData.payment_fee).length > 0) {
                    let index = 0
                    if (applicationData.payment_fee.payment_complete) {
                        index = this.getObjectIndex(this.progressSteps, 'platform fee')
                        this.currentStep = this.progressSteps[index];
                    } else if (parseInt(this.userId) && applicationData.payment_fee.payment_complete) {
                        index = this.getObjectIndex(this.progressSteps, 'application')
                        this.currentStep = this.progressSteps[index];
                    } else if (!parseInt(this.userId) && applicationData.payment_fee.payment_complete) {
                        index = this.getObjectIndex(this.progressSteps, 'login/signup');
                        this.currentStep = this.progressSteps[index];
                    }
                    isEmpty = false;
                }
                if (isEmpty) {
                    localStorage.removeItem(this.storage_key)
                } else {
                    this.nextStep();
                }

            }
        },
        redirectUser(data) {
            let applicationData = localStorage.getItem(this.storage_key);
            applicationData = JSON.parse(applicationData);
            applicationData.user_info.isLoggedIn = true;
            localStorage.setItem(this.storage_key, JSON.stringify(applicationData));
            this.bindApplicationModal();
            this.userSubscriptions();
            this.setCurrentStep();
        },
        bindModal() {
            this.loading = false;
            // Get the modal
            this.closeLoader();
            let modal = document.getElementById("jobs-modal");
            // Get the <span> element that closes the modal
            let closeModal = document.getElementById("close-modal");
            let cancelBtn = document.getElementById("cancel-btn");
            let continueBtn = document.getElementById("continue-btn");
            const pathname = window.location.pathname;
            let url = new URL(window.location.href);
            const payment_complete = url.searchParams.get("payment_complete")
            if (payment_complete && localStorage.getItem(this.storage_key)) {
                localStorage.getItem(this.storage_key)
                this.setCurrentStep(payment_complete)
            } else if (payment_complete && !localStorage.getItem(this.storage_key)) {
                url.searchParams.delete("payment_complete");
                window.history.replaceState(null, null, url)
            } else if (!payment_complete && localStorage.getItem(this.storage_key)) {
//           show modal on page load
                modal.style.display = "block";
            }
            const that = this
            // When the user clicks on <span> (x), close the modal
            closeModal.onclick = function () {
                localStorage.setItem(that.storage_key, null);
                localStorage.removeItem(that.storage_key);
                modal.style.display = "none";

            };
            // When the user clicks on <span> (x), close the modal
            cancelBtn.onclick = function () {
                localStorage.setItem(that.storage_key, null)
                localStorage.removeItem(that.storage_key)
                modal.style.display = "none";

            };
            // When the user clicks on <span> (x), close the modal
            continueBtn.onclick = function () {
                modal.style.display = "none";
                //set current state
                that.setCurrentStep()
            };
            //

            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function (event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                    localStorage.removeItem(this.storage_key)
                }
            };
        },
        setSubscription(user) {

            if (this.isAdmin) {
                this.payFee = false;
                this.progressSteps = [{
                    label: this.jobApplicationStrings.steps.application,
                    value: 'application'
                }, {label: this.jobApplicationStrings.steps.review, value: 'review'}];
            }else {
                if (user.subscriptions.length) {
                    for (const subscription of user.subscriptions) {
                        const start_date = Date.parse(new Date(subscription.start_date));
                        if (parseInt(subscription.status) === 1 && isNaN(start_date)) {
                            this.progressSteps = [{
                                label: this.jobApplicationStrings.steps.application,
                                value: 'application'
                            },
                                {label: this.jobApplicationStrings.steps.review, value: 'review'}
                                , {
                                    label: this.jobApplicationStrings.steps.fee,
                                    value: 'platform fee'
                                }];
                            this.payFee = true;
                        } else if (parseInt(subscription.status) === 1 && !isNaN(start_date)) {
                            this.progressSteps = [{
                                label: this.jobApplicationStrings.steps.application,
                                value: 'application'
                            }, {label: this.jobApplicationStrings.steps.review, value: 'review'}];
                        } else {
                            this.payFee = true;
                            //   do nothing.
                        }
                    }
                } else {
                    this.payFee = true;
                    this.progressSteps = [
                        {
                            label: this.jobApplicationStrings.steps.application,
                            value: 'application'
                        },
                        {label: this.jobApplicationStrings.steps.review, value: 'review'},
                        {
                            label: this.jobApplicationStrings.steps.fee,
                            value: 'platform fee'
                        }
                    ];
                }
            }
        },
        setFields() {
            this.getUserProfiles();
            this.formData.applicantEmail = this.user?.user_email;
            this.formData.applicantName = this.user?.first_name ? this.user?.first_name + ' ' + this.user?.last_name : undefined;
            this.formData.applicantPhone = this.user?.phone_number ? this.user?.phone_number : undefined;
            this.formData.city = this.user?.city;
            this.formData.country = this.user?.country;
            this.getJobAndRelatedJobs(true);
            this.setUpPhoneNumber();
        },
        userSubscriptions() {
            let user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : undefined;
            if (user && user !== undefined) {
                this.user = user;
                this.setSubscription(user);
                this.setFields();
            } else {
                const data = new FormData();
                const that = this;
                data.append("action", "get_user_with_accounts");
                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {

                        if (response.data) {
                            that.user = response.data;
                            localStorage.setItem('user', JSON.stringify(that.user))
                            that.setSubscription(user);
                            that.setFields();
                        }
                        // setTimeout(() => {
                        //     that.closeLoader();
                        //     return;
                        // }, 200)
                    })
                    .catch(function (error) {
                        console.log(error)
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.jobApplicationStrings.errors.errorOccurred);
                        }
                        that.loading = false;
                        that.closeLoader();
                    });
            }
        },
        submitForm() {
            this.submitting = true;
            // return;

            if (Object.keys(this.formErrors).length === 0) {
                const data = new FormData();
                const that = this;
                data.append("applicantName", this.formData.applicantName);
                data.append("applicantEmail", this.formData.applicantEmail);
                data.append("applicantPhone", this.formData.applicantPhone);
                data.append("applicantAddress", this.formData.applicantAddress);
                data.append("country", this.selectedCountry(this.formData.country));
                data.append("motivation", this.formData.motivation);
                data.append("cv", this.formData.fileUpload?.file ? this.formData.fileUpload?.file : this.formData.fileUpload?.url ?? '');
                data.append("jobId", this.job?.id);
                data.append("profileId", this.profileId);
                data.append("jobApplicationEmail", this.job?.application_email);
                if (this.payFee) {
                    data.append("package_name", 'Job Application Fee');
                    data.append('package_price', this.platform_fee?.price);
                    data.append('application_type', 'job_application_fee');
                    data.append('job_slug', this.job?.slug);
                    data.append("action", "hs_add_to_cart");

                    axios({
                        method: "post",
                        url: ajaxurl,
                        data: data,
                        headers: {"Content-Type": "multipart/form-data"},
                    })
                        .then(function (response) {
                            if (response.data) {
                                localStorage.removeItem(that.storage_key);
                                localStorage.removeItem('user');
                                setTimeout(() => {
                                    window.location.href = response.data.checkout_url
                                }, 200)
                            }
                        })
                        .catch(function (error) {
                            console.log(error)
                            if (error.response) {
                                toastr.error(error.response.data.data);
                            } else {
                                toastr.error(that.jobApplicationStrings.errors.errorOccurred);
                            }
                            that.submitting = false;

                        });
                } else {
                    data.append("action", "job_application");
                    // return
                    axios({
                        method: "post",
                        url: ajaxurl,
                        data: data,
                        headers: {"Content-Type": "multipart/form-data"},
                    })
                        .then(function (response) {
                            if (response.data) {
                                toastr.success(response.data.message);
                                localStorage.removeItem(that.storage_key);
                                localStorage.removeItem('user');
                                localStorage.setItem('user', JSON.stringify(response.data?.user_data));
                                window.location.href = home_url + '/my-account'//response.data.redirect_url;
                            }
                        })
                        .catch(function (error) {
                            console.log(error)
                            if (error.response) {
                                toastr.error(error.response.data.data);
                            } else {
                                toastr.error(that.jobApplicationStrings.errors.errorOccurred);
                            }
                            that.submitting = false;
                        });
                }
            } else {
                this.submitting = false
            }
        },
        bindApplicationModal() {
            this.loading = false;
            // Get the modal
            if (document.querySelector('#loading-screen')) {
                document.querySelector('#loading-screen').classList.add("d-none")
            }
            let modal = document.getElementById("jobs-application-modal");
            // Get the <span> element that closes the modal
            let closeModal = document.getElementById("close-application-modal");
            let cancelBtn = document.getElementById("cancel-application-btn");
            let continueBtn = document.getElementById("continue-application-btn");
            const pathname = window.location.pathname;
            const user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : undefined
            if (user && user.job_applications) {
                if (user.subscriptions) {
                    for (const subscription of user.subscriptions) {

                        if (parseInt(subscription.package_job_applications) > -1 && (parseInt(user.job_applications) >= parseInt(subscription.package_job_applications))) {
                            modal.style.display = "block";
                            break;
                        }
                    }
                }
            }
            // When the user clicks on <span> (x), close the modal
            closeModal.onclick = function () {
                modal.style.display = "none";
                window.location.href = home_url + '/jobs'
            };
            // When the user clicks on <span> (x), close the modal
            cancelBtn.onclick = function () {
                modal.style.display = "none";
                window.location.href = home_url + '/jobs'
            };
            const that = this;
            // When the user clicks on <span> (x), close the modal
            continueBtn.onclick = function () {
                modal.style.display = "none";
                //set current state
                window.location.href = home_url + '/my-account/#/upgrade-account'
            };
            //

            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function (event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                    localStorage.removeItem('jobData')

                }
            };
        },
        clearFile() {
            this.formData.fileUpload = undefined
            this.$refs.fileInput.value = '';
        },
        getProfileName(profiles, selected_profile) {
            const profile = profiles.find((option) => option.value === selected_profile)
            if (profile) {
                return profile.label;
            } else {
                return "-";
            }
        }
    },
    mounted() {
        if (parseInt(this.userId)) {
            this.userSubscriptions();
        } else {
            const loginUrl = this.homeUrl + '/login';
            const jobUrl = window.location.href;
            const url = loginUrl + "?redirect_url=" + jobUrl;
            window.location.href = url;
            // this.progressSteps = [...this.progressSteps, ...[{
            //     label: this.jobApplicationStrings.steps.application,
            //     value: 'application'
            // }, {
            //     label: this.jobApplicationStrings.steps.login,
            //     value: 'login/signup'
            // }, {
            //     label: this.jobApplicationStrings.steps.fee,
            //     value: 'platform fee'
            // }, {label: this.jobApplicationStrings.steps.review, value: 'review'}]];
        }
        const applicationUrl = new URL(window.location.href);
        const selectedProfile = applicationUrl.searchParams.get("selected_profile");
        if (selectedProfile) {
            this.selected_profile = parseInt(selectedProfile);
        }

    },
    created() {
        this.setJActiveLink('/jobs')
    },
    template: `<VLoader :active="loading" :translucent="true">
    <div class="job-form">
        <div v-if="error">
            <loading-error @update="getJobDependencies"></loading-error>
        </div>
        <div v-else>
            <div class="row text-center">
                <div class="col-md-12">
                    <h1 class="page-title p-0">
                        <span v-if="currentStep.value.toLowerCase() !== 'review'">{{jobApplicationStrings.headings.normal.toString()}}</span>
                        <span v-if="currentStep.value.toLowerCase() === 'review'">{{jobApplicationStrings.headings.review.toString()}}</span>
                    </h1>
                </div>

            </div>
            <div class="row">

                <!--        progress bar                        -->
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="d-flex justify-content-between align-items-center sign-up-stepper-wrapper position-relative">
                        <div id="progress" :style="progressBarStyle"></div>
                        <div v-for="(step,index) in progressSteps" :key="index" :data-title="step"
                             class="sign-up-stepper d-flex justify-content-center align-items-center progress-step"
                             :class="{ 'current-step': step.value.toLowerCase()===currentStep.value.toLowerCase(), 'completed-step': progressSteps.indexOf(currentStep) > index }">
                            <span>{{step.label.toString()}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
            <!--                                  steps             -->
            <div class="row mt-5">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <form class="husla-form" @submit.prevent="submitForm">
                        <!--       application         -->
                        <div v-if="currentStep.value.toLowerCase() === 'application'">
                            <div class="mb-3">
                                    <label for="motivation" class="form-label">{{jobApplicationStrings.formFields.profile.label}}</label>     
                                    <AnaVueSelect v-model="selected_profile" :options="user_profiles"
                                                  :placeholder="jobApplicationStrings.formFields.profile.placeholder"></AnaVueSelect>
                                             <span class="text-danger d-inline-block" v-if="formErrors.selected_profile">{{formErrors.selected_profile}}</span>
                            </div>
                            <div v-if="selected_profile=='no profile'" >
                            <div class="mb-3">
                                <label for="username" class="form-label">{{jobApplicationStrings.formFields.name.label}}
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="text" class="form-control"
                                       :placeholder="jobApplicationStrings.formFields.name.placeholder"
                                       autocomplete="off"
                                       id="username" name="name" v-model="formData.applicantName">
                                <span class="text-danger validation-error" v-if="formErrors.applicantName">{{formErrors.applicantName.toString()}}</span>
                            </div>

                            <div class="row">
                                <div class="mb-3 col-md-6">

                                    <label for="email" class="form-label">{{jobApplicationStrings.formFields.email.label}}
                                        <span class="text-danger">*</span>
                                    </label>
                                    <input type="email" class="form-control" id="email"
                                           :placeholder="jobApplicationStrings.formFields.email.placeholder"
                                           name="email"
                                           v-model="formData.applicantEmail">
                                    <span class="text-danger validation-error" v-if="formErrors.applicantEmail">{{formErrors.applicantEmail.toString()}}</span>
                                </div>
                                <div class="mb-3 col-md-6">
                                    <label for="email" class="form-label">{{jobApplicationStrings.formFields.phone.label}}
                                    </label>
                                    <ana-vue-phone-number v-model="formData.applicantPhone"
                                                          :country-code="formData.country"
                                                          @phoneError="phoneNumberError"></ana-vue-phone-number>
                                    <span class="text-danger validation-error" v-if="formErrors.applicantPhone">{{formErrors.applicantPhone}}</span>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="address" class="form-label">{{jobApplicationStrings.formFields.address.label}}
                                    <span class="text-danger">*</span>
                                </label>
                                <input type="email" class="form-control" id="address"
                                       :placeholder="jobApplicationStrings.formFields.address.placeholder" name="email"
                                       v-model="formData.applicantAddress">
                                <span class="text-danger validation-error" v-if="formErrors.applicantAddress">{{formErrors.applicantAddress.toString()}}</span>
                            </div>

                            <div>
                                <label for="email" class="form-label">{{jobApplicationStrings.formFields.cv.label}}
                                    <span v-if="job?.cv_required" class="text-danger">*</span>
                                </label>
                            </div>

                            <div class="mb-3">
                                <ana-vue-file-upload v-model="formData.fileUpload"
                                                     :maxSizeLabel="jobApplicationStrings.formFields.cv.tips.maxSize"
                                                     :maxUploadSize="maxUpload"
                                                     :typesTitle="jobApplicationStrings.formFields.cv.tips.fileTypes"
                                                     fileTypes="pdf,word"
                                                     :wordUrl="imgUrl + '/document-type/microsoft-word.png'"
                                                     :pdfUrl="imgUrl + '/document-type/pdf-logo.png'"
                                                     acceptedFiles="application/pdf,.doc,.docx,application/msword"
                                                     :uploadButtonText="jobApplicationStrings.formFields.cv.buttons.uploadCv"
                                                     :changeButtonText="jobApplicationStrings.formFields.cv.buttons.changeCv"
                                ></ana-vue-file-upload>
                                <span class="text-danger d-inline-block" v-if="formErrors.fileUpload">{{formErrors.fileUpload}}</span>
                            </div>
                            </div>
                            <div class="mb-3">
                                <label for="motivation" class="form-label">{{jobApplicationStrings.formFields.motivation.label}}
                                    <span v-if="job?.motivation_required" class="text-danger">*</span>
                                </label>
                                <textarea id="motivation" class="form-control" rows="4"
                                          :placeholder="jobApplicationStrings.formFields.motivation.placeholder"
                                          v-model="formData.motivation"></textarea>
                                <span class="text-danger validation-error"
                                      v-if="formErrors.motivation">{{formErrors.motivation}}</span>
                            </div>

                            <div class="mb-4 d-flex justify-content-between">
                                <span class="gray-item"></span>
                                <span class="gray-item"></span>
                                <span class="gray-item"></span>
                            </div>
                        </div>
                        <!--            Login/signup                     -->
                        <div v-if="currentStep.value.toLowerCase() === 'login/signup'">
                            <div class="d-flex justify-content-between flex-column ">
                                <div class="mb-3 row align-items-center">
                                    <div class="col-md-4">

                                        <input type="radio" class="mr-2" id="login-account" name="create_account"
                                               v-model="createAction" value="login"/>
                                        <label for="login-account" class="form-label cursor-pointer">{{jobApplicationStrings.login}}</label>
                                    </div>

                                    <div class="col-md-4">
                                        <h2>{{jobApplicationStrings.or}}</h2>
                                    </div>

                                    <div class="col-md-4">
                                        <input type="radio" class="mr-2" id="signup-account" name="create_account"
                                               v-model="createAction" value="signup"/>
                                        <label cursor-pointer for="signup-account"
                                               class="form-label cursor-pointer">{{jobApplicationStrings.signUp}}</label>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="mb-3 col-sm-12">
                                        <login-form v-if=" createAction=== 'login'"
                                                    @user-logged-in="redirectUser"></login-form>
                                        <sign-up-form v-else @success-message="redirectUser"></sign-up-form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--            Platform fee                    -->
                        <div v-if="currentStep.value.toLowerCase() === 'platform fee'" class="mb-4 row ">
                            <div class="col-md-10 mx-auto text-center">

                                <div v-if="platform_fee"
                                     class="py-5 d-flex justify-content-center hs-border-1 flex-column align-items-center mb-4">

                                    <p class="mb-2 hs-title">{{jobApplicationStrings.platformFee.heading}}</p>
                                    <h5 class="section-title mb-4">{{platform_fee?.price}} {{currency}}</h5>
                                    <h5 class="hs-title">
                                    <span class="hs-secondary-text-color">{{applicationFeeMessage}}</span>
                                       <a :href="homeUrl+'/my-account/#/upgrade-account'"
                                       class="hs-primary-text-color">{{applicationFeeLinkMessage}}</a>
                                    </h5>
                       
                                </div>
                                <p v-if="application_data?.payment_fee?.payment_complete"
                                   class="hs-title hs-primary-text-color">
                                    {{jobApplicationStrings.platformFee.madePayments}}</p>
                            </div>
                        </div>
                        <!--           review                            -->
                        <div v-if="currentStep.value.toLowerCase() === 'review'">
                            <div class="mb-4 row">
                                <div class="col-md-12">
                                
                                <div v-if="selected_profile === 'no profile'">
                                    <div class="mb-2">
                                        <h5 class="hs-subtitle">{{jobApplicationStrings.formFields.name.label}}</h5>
                                        <p>{{formData.applicantName}}</p>
                                    </div>

                                    <div class="mb-2">
                                        <h5 class="hs-subtitle">{{jobApplicationStrings.formFields.email.label}}</h5>
                                        <p>{{formData.applicantEmail}}</p>
                                    </div>
                                    <div class="mb-2">
                                        <h5 class="hs-subtitle">{{jobApplicationStrings.formFields.phone.label}}</h5>
                                        <p v-if="formData.applicantPhone">{{formData.applicantPhone}}</p>
                                        <p v-else>N/A</p>
                                    </div>
                                    <div class="mb-2">
                                        <h5 class="hs-subtitle">{{jobApplicationStrings.formFields.address.label}}</h5>
                                        <p>{{formData.applicantAddress}}</p>
                                    </div>

                                    <div class="mb-3">
                                        <h5 class="hs-subtitle">{{jobApplicationStrings.formFields.cv.label}}</h5>
                                        <a v-if="formData.fileUpload?.url" :href="formData.fileUpload?.url"
                                           target="_blank" class="profile-image-preview">
                                            <img :src="cvUrl(formData.fileUpload?.file.name)" alt="docx or word"
                                                 style="width: 100px">
                                        </a>
                                        <p v-else>N/A</p>
                                    </div>
                                    </div>
                                    <div v-else-if="selected_profile !== 'new profile' || selected_profile !== 'no profile'">
                                        <h5 class="hs-subtitle">Application profile</h5>
                                        <p>{{getProfileName(user_profiles,selected_profile)}}</p>
                                    </div>
                                    <div class="mb-2">
                                        <h5 class="hs-subtitle">
                                            {{jobApplicationStrings.formFields.motivation.label}}</h5>
                                        <p v-if="formData.motivation">{{formData.motivation}}</p>
                                        <p v-else>N/A</p>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div class="my-5">
                            <hr>
                        </div>

                        <div class="mt-5 d-flex"
                             :class="{'justify-content-end':!getObjectIndex(progressSteps,currentStep.value),'justify-content-between':getObjectIndex(progressSteps,currentStep.value) }">
                            <span v-if="getObjectIndex(progressSteps,currentStep.value)"
                                  class="fas fa-chevron-left cursor-pointer stepper-btn hs-round"
                                  @click="goToPrevious" :disabled="submitting"></span>
                            <div v-if="(getObjectIndex(progressSteps,currentStep.value) +1) === progressSteps.length">
                                <button type="submit"
                                        class="hs-btn hs-btn-primary" :disabled="submitting">
                                        <span v-if="payFee">{{jobApplicationStrings.formFields.buttons.apply}}</span>
                                        <span v-else>{{jobApplicationStrings.formFields.buttons.submit}}</span>
                                    
                                    <i
                                        v-if="submitting"
                                        class="fas fa-spinner fa-pulse ml-1"></i>
                                </button>
                            </div>

                            <span v-if="(getObjectIndex(progressSteps,currentStep.value)+1) !== progressSteps.length"
                                  class="fas fa-chevron-right cursor-pointer stepper-btn hs-round"
                                  @click="goToNext"></span>
                        </div>

                    </form>
                </div>
                <div class="col-md-2"></div>
            </div>

            <!-- MODAL WINDOW -->
        </div>

        <!--        Related data-->

        <div class="row mt-5" v-if="relatedJobs?.length && currentStep.value.toLowerCase() === 'platform fee'">

            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <h5 class="hs-title mb-4">{{jobApplicationStrings.relatedJobs}}</h5>

                        <job-card v-for="(job,index) in relatedJobs" :key="index" :job-data="[job]"></job-card>
                    </div>
                    <div class="col-md-1"></div>
                </div>

            </div>
        </div>
        <div id="jobs-modal" class="modal">
            <div class="modal-content">
                <span style="color:black;" id="close-modal" class="close">&times;</span>
                <div class="modal-inner-content text-center">
                    <h2 style="font-size:30px !important; color:#004475; padding-bottom: 20px;">
                        {{jobApplicationStrings.continueModal.heading}}</h2>
                    <p style="font-size:25px !important; color: #004475;">
                        <span style="font-weight: bold;">{{jobApplicationStrings.continueModal.subHeading}}</span>

                    </p>
                    <div class="buttons">
                        <button id="cancel-btn" type="button" class="hs-btn hs-btn-primary-outline mr-5">
                            {{jobApplicationStrings.continueModal.buttons.cancel}}
                        </button>
                        <button id="continue-btn" class="hs-btn hs-btn-primary">
                            {{jobApplicationStrings.continueModal.buttons.continue}}
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="jobs-application-modal" class="modal">
        <div class="modal-content">
            <span style="color:black;" id="close-application-modal" class="close">&times;</span>
            <div class="modal-inner-content text-center">
                <h2 style="font-size:30px !important; color:#004475; padding-bottom: 20px;">
                    {{jobApplicationStrings.applicationModal.heading}}</h2>
                <p style="font-size:25px !important; color: #004475;">
                    <span style="font-weight: bold;">{{jobApplicationStrings.applicationModal.subHeading}}</span>
                </p>
                <div class="buttons">
                    <button id="cancel-application-btn" type="button" class="hs-btn hs-btn-primary-outline mr-5">
                        {{jobApplicationStrings.applicationModal.buttons.cancel}}
                    </button>
                    <button id="continue-application-btn" class="hs-btn hs-btn-primary">
                        {{jobApplicationStrings.applicationModal.buttons.upgrade}}
                    </button>
                </div>
            </div>
        </div>
    </div>
</VLoader>
`,
};

export {JobApplication};
