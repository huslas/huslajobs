<?php
    global $wpdb, $user_ID;
    get_header();

?>

<div id="post-job-content" class="content-area primary vue-component"></div>
<div id="post-job-nonce">
    <?php wp_nonce_field( 'huslajobs-post-job', 'huslajobs-post-job-nonce' ); ?>
</div>
<div id="login-nonce">
    <?php wp_nonce_field( 'huslajobs-login', 'huslajobs-login-nonce' ); ?>
</div>
<script src="<?php echo 'https://www.google.com/recaptcha/api.js?render='.HUSLASJOBS_RECAPTCHA_SITE_KEY ?>" defer></script>
<script src="<?php echo HUSLA_JOBS_CLIENT_MODULE_URL ?>/post-job/resources/index.js" type="module"></script>
<?php get_footer(); ?>
