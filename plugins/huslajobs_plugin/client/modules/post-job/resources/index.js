import {PostJob} from "./components/add-job-form.js";

// VUE 3 initialization
const app = Vue.createApp({
    render() {
        return Vue.h(PostJob, {});
    },
});
app.mount("#post-job-content");