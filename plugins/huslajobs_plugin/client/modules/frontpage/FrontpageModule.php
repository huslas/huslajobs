<?php

namespace huslajobs_client;

use huslajobs\HuslaModule;

class FrontpageModule extends HuslaModule {
	public function __construct() {
//		$this->addActions();
		$this->parent_module = 'client';
		$this->module        = 'frontpage';

	}

}