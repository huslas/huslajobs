
// VUE 3 initialization
import {FrontPage} from "./components/frontpage.js";
//
const app = Vue.createApp({
    render() {
        return Vue.h(FrontPage, {});
    },
});

app.mount("#frontpage-component");