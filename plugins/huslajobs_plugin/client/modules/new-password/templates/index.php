<?php
    get_header();
    global $wpdb, $user_ID;

    $user_id = 0;
    $lost_password = 0;

    if ($user_ID) {
        $user_id = $user_ID;
    }

    if (isset($_REQUEST['reset_password']) && trim($_REQUEST['reset_password']) == 'reset_password') {
        $activation_key = isset($_REQUEST['activation_key']) ? sanitize_text_field($_REQUEST['activation_key']) : '';
        global $wpdb;
        if (is_multisite()) {
            $table = $wpdb->base_prefix . "usermeta";
        } else {
            $table = $wpdb->prefix . "usermeta";
        }
        $meta_data = $wpdb->get_row($wpdb->prepare("SELECT * FROM $table WHERE meta_value = %s", $activation_key));

        if (!empty($meta_data)) {
            $user_id = $meta_data->user_id;
            $lost_password = 1;
        }

    }

    if ($user_id){
        ?>

            <div id="login-content" class="content-area primary vue-component"></div>
            <div id="new-password-nonce">
                <?php wp_nonce_field('huslajobs-new-password', 'huslajobs-new-password-nonce'); ?>
                <input type="hidden" id="user-id" value="<?php echo $user_id ?>">
            </div>
        <script type="text/javascript">
            const lost_password = <?php  echo $lost_password; ?>;
            const current_user_id = <?php echo $user_id ?? 0; ?>;

        </script>
            <script src="<?php echo HUSLA_JOBS_CLIENT_MODULE_URL ?>/new-password/resources/index.js" type="module"></script>

<?php } else{ ?>
<script type="text/javascript">
    window.location.href = "<?php echo home_url("/login") ?>"
</script>
<?php } ?>

<?php get_footer(); ?>
