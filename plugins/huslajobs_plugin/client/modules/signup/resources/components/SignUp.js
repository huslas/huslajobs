
import {SignUpForm} from "./signup-form.js";
import {VLoader} from "../../../../../inc/global_components/loader/v-loader.js";
import {ReusableFunctions} from "../../../../../js/functions.js";
const SignUp = {
    data() {
        return {
            successMessage:undefined,
            homeUrl: home_url,
            imgUrl: THEME_URL,
            loading:false,
            sigUpStrings:VueUiStrings.clientFrontEnd.signUp
        };
    },
    mixins:[ReusableFunctions],
    emits:[''],
    components: {
        SignUpForm,
        VLoader

    },
    methods : {
        updateSuccessMessage(data){
            this.successMessage = data.message;
            window.scrollTo({
                top: 0,
                left: 0,
                behavior: 'smooth'
            });
        },
        updateLoading(loading){
            setTimeout(()=>{
                this.closeLoader();
            },300)
        }
    },
    template: `
       <section class="hs-section">
            <div class="row text-center">
                <div class="col-md-12">
                    <h1 class="page-title p-0 mb-0">
                        <span>{{sigUpStrings.heading}}</span>
                    </h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
    <!--                <img :src="imgUrl+'/hs-employee.png'" alt="">-->
                </div>
                <div class="col-md-8">
                    <div v-if="successMessage">
                        <p>
                            {{decodeHtml( successMessage)}}
                        </p>
                        <div class="mt-5 d-flex justify-content-center">
                            <a :href="homeUrl" class="hs-btn  hs-btn-primary-outline mr-5">{{sigUpStrings.buttons.home}}</a>
                            <a :href="homeUrl+'/my-account'" class="hs-btn hs-btn-primary">{{sigUpStrings.buttons.dashboard}}</a>
                        </div>
                    </div>
                    <div v-else>
                                    <sign-up-form  @success-message="updateSuccessMessage"
                                  @update-loading="updateLoading"></sign-up-form>
                    <p class="mt-5">
    
                        <a :href="homeUrl+'/login'" class="hs-primary-text-color-light">{{sigUpStrings.haveAccount}}</a>
    
                    </p>
    </div>
    
                </div>
                <div class="col-md-2">
    <!--                <img :src="imgUrl+'/hs-employee.png'" alt="">-->
                </div>
            </div>
        </section>
`,
};

export {SignUp};
