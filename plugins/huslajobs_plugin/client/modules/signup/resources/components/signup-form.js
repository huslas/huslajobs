import {world_countries as WorldCountries} from "../../../../../js/countries_data.js";
import {AnaVueSelect} from "../../../../../inc/global_components/ana_select/ana_vue_select.js";
import {CountriesStates} from "../../../../../js/states.js";
import {CountriesData} from "../../../../../js/countries_and_codes.js";
import {LoadingBar} from "../../../../../admin/modules/dashboard/resources/components/LoadingBar.js";
import {ReusableFunctions} from "../../../../../js/functions.js";
import {AnaVuePhoneNumber} from "../../../../../inc/global_components/phone-number/ana_vue_phone_number.js";
import {AnaVueFileUpload} from "../../../../../inc/global_components/ana_file_upload/ana_vue_fileupload.js";

const SignUpForm = {
    props: {
        currentPage: {
            type:String,
            required: false,
            default: 'signup'
        },
    },
    data() {
        return {
            form: {
                firstName: undefined,
                lastName: undefined,
                email: undefined,
                password: undefined,
                fileUpload: undefined,
                bio: undefined,
                phone: undefined,
                country: undefined,
                state: undefined,
                // dob: undefined,
                terms: false,
                user_id:this.$route?.params?.id,
            },
            registered: false,
            showPassword: false,
            countries: WorldCountries,
            states: [],
            successMessage: undefined,

            progressSteps: 4,
            imgUrl: THEME_URL,
            userAccountType: 'individual_job_seeker',
            accountTypes: [{
                label: 'I am a professional/freelancer looking to apply for jobs',
                value: 'individual_job_seeker'
            }, {label: 'I am an employer looking to hire an employee or post jobs', value: 'individual_recruiter'}],
            progressBarStyle: "width: 0%; margin-left: 5px",
            formErrors: {},
            submitting: false,
            currentStep: 1,
            selectedPackage: undefined,
            packages: [],
            recaptchaKey: recaptcha_key,
            jsUrl: THEME_JS_URL,
            homeUrl: home_url,
            loading: false,
            maxUpload:maximum_upload,
            signUpFormStrings: VueUiStrings.clientFrontEnd.signUpForm.formFields,
            buttonText:VueUiStrings.clientFrontEnd.signUpForm.formFields.buttonText.singUp,
            imageRequired:sign_up_image_required,
            signupImage:show_sign_up_image
        };
    },
    emits:['success-message','update-loading'],
    refs: ['signupForm'],

    mounted() {
        this.setUpPhoneNumber();
        if (this.$route?.params?.id){
            this.getUser();
            this.buttonText = this.signUpFormStrings.buttonText.save;
        }else{
            this.$emit('update-loading',true);
        }


    },
    watch: {
        'form.firstName': function (currentVal, oldVal) {
            this.validateInput("firstName", currentVal, {required: true},this.signUpFormStrings.firstName.label);
        },
        'form.secondName': function (currentVal, oldVal) {
            this.validateInput("lastName", currentVal, {required: true}, this.signUpFormStrings.lastName.label);
        },
        'form.password': function (currentVal, oldVal) {
            this.validateInput("password", currentVal, {required: true,min: 8}, this.signUpFormStrings.password.label);
        },
        'form.email': function (currentVal, oldVal) {
            this.validateInput("email", currentVal, {required: true}, this.signUpFormStrings.email.label);
        },
        'form.country':function (currentVal, oldVal) {
            this.validateInput("country", currentVal, {required: true}, this.signUpFormStrings.country.label);

            this.states = [];
            if(currentVal){
                const countriesData = CountriesData;
                let states = countriesData.filter((country)=>country.code.toLowerCase() === currentVal.toLowerCase())[0].states;
                if (states.length){
                    this.states = states.map((state)=>{
                        return{
                            label:state,
                            value:state
                        }
                    });
                }
            }else{
                // this.form.country =
                this.states = [];
                this.form.state = undefined
            }

        },
        'form.city':function (currentVal, oldVal) {
            this.validateInput("city", currentVal, {required: true}, this.signUpFormStrings.city.label);
        },
        'form.state': function (currentVal, oldVal) {
            this.validateInput("state", currentVal, {required: true}, this.signUpFormStrings.state.label);
        },
        // 'form.bio': function (currentVal, oldVal) {
        //     this.validateInput("bio", currentVal, {required: true,min:150,max:500}, this.signUpFormStrings.bio.label);
        // },
        // 'form.fileUpload': function (currentVal, oldVal) {
        //         this.validateInput("fileUpload", currentVal, {required: true}, this.signUpFormStrings.profileImage.label);
        // },

    },
    components: {
        AnaVueSelect,
        LoadingBar,
        AnaVuePhoneNumber,
        AnaVueFileUpload,
    },
    mixins:[ReusableFunctions],

    destroyed() {

        this.form = {
            firstName: undefined,
            lastName: undefined,
            email: undefined,
            password: undefined,
            fileUpload: undefined,
            bio: undefined,
            phone: undefined,
            country: undefined,
            state: undefined,
            // dob: undefined,
            terms: false,
            id: this.$route.params.id,
        }
    },
    methods: {
        phoneNumberError(err) {
            if (!err && this.formErrors){
                this.formErrors["phone"] = this.signUpFormStrings.phone.error;
            }
            else if (err && (this.formErrors && this.formErrors["phone"] ) ){
                delete this.formErrors["phone"]
            }
        },
        getCountries(searchText) {
            if (searchText) {
                const countries = WorldCountries
                this.countries = countries.filter((option) => option.label.toLowerCase().includes(searchText.toLowerCase()));
            } else {
                this.countries = WorldCountries;
            }
        },
        userRegistration() {
            this.submitting = true;
            //validate inputs
            this.validateInput("firstName", this.form.firstName, {required: true}, this.signUpFormStrings.firstName.label);
            this.validateInput("lastName", this.form.lastName, {required: true}, this.signUpFormStrings.lastName.label);
            if (!this.form.user_id){
                this.validateInput("password", this.form.password, {required: true}, this.signUpFormStrings.password.label);
                this.validateInput("email", this.form.email, {required: true}, this.signUpFormStrings.email.label);
            }
            // this.validateInput("bio", this.form.bio, {required: true}, this.signUpFormStrings.bio.label);
            this.validateInput("city", this.form.city, {required: true}, this.signUpFormStrings.city.label);
            this.validateInput("country", this.form.country, {required: true}, this.signUpFormStrings.country.label);
            this.validateInput("state", this.form.state, {required: true}, this.signUpFormStrings.state.label);
            // if (this.signupImage === 'yes'  && this.imageRequired === 'yes') {
            //     this.validateInput("fileUpload", this.form.fileUpload, {required: true}, this.signUpFormStrings.profileImage.label);
            // }
            const recaptcha = this.recaptchaKey;
            if (Object.keys(this.formErrors).length === 0) {
                const that = this;
                grecaptcha.ready(function () {
                    grecaptcha.execute(recaptcha, {action: 'edit'}).then(function (token) {

                        let input = document.createElement("input");
                        input.type = "hidden";
                        input.name = "g-recaptcha-response";
                        input.value = token;
                        that.$refs.signupForm.appendChild(input);
                        const data = new FormData();
                        data.append("first_name", that.form.firstName);
                        data.append("last_name", that.form.lastName);
                        data.append("password", that.form.password);
                        data.append('email', that.form.email);
                        data.append('token', token);
                        data.append("bio", that.form.bio??'');
                        data.append("city", that.form.city?? '');
                        data.append("country", that.form.country?? '');
                        data.append("state", that.form.state?? '');
                        data.append("phone", that.form.phone?? '');
                        data.append("profile_image", that.form.fileUpload?.file ? that.form.fileUpload?.file : that.form.fileUpload ?? '');


                        data.append('current_page',that.currentPage);
                        // data.append('register_nonce', document.getElementById("huslajobs-register-nonce").value);
                        data.append('user_id',that.form.user_id ??'');
                        data.append("action", "user_registration");
                        axios({
                            method: "post",
                            url: ajaxurl,
                            data: data,
                            headers: {"Content-Type": "multipart/form-data"},
                        })
                            .then(function (response) {
                                if (response.data) {
                                    toastr.success(response.data.message);
                                    if (response.data.user_data) {
                                            localStorage.setItem('user', JSON.stringify(response.data.user_data))
                                    }
                                    that.setUserLogout();
                                    that.$emit('success-message',response.data);
                                    that.submitting = false;
                                }
                            })
                            .catch(function (error) {
                                console.log(error)
                                if (error.response) {
                                    toastr.error(error.response.data.data);
                                } else {
                                    toastr.error("An error occurred");
                                }
                                that.submitting = false;

                            });
                    });
                });


            } else {
                this.submitting = false;
                setTimeout(()=>{
                    const firstErrorControl = document.querySelector('.validation-error')?.parentElement;
                    // Scroll to first error element
                    window.scrollTo(
                        firstErrorControl?.offsetLeft || 0,
                        (firstErrorControl?.offsetTop || 0) - 50 // Subtract 50 for better exposure
                    )
                },100)

            }
        },
        togglePassword() {
            const passwordInput = this.$refs.password;

            if (passwordInput.getAttribute('type') == 'password') {
                this.showPassword = true
                passwordInput.setAttribute('type', 'text')
            } else {
                this.showPassword = false
                passwordInput.setAttribute('type', 'password')
            }
        },
        getStates(searchText) {
            const countriesData =CountriesData;
            let states = countriesData.filter((country)=>country.code.toLowerCase() === currentVal.toLowerCase())[0].states;

            this.states = states.map((state)=>{
                return{
                    label:state,
                    value:state
                }
            });

            if (searchText) {
                this.states = states.filter((option) => option.label.toLowerCase().includes(searchText.toLowerCase()));
            } else {
                this.states = states;
            }
        },
        getUser() {
            this.$emit('update-loading',true);
            let user = localStorage.getItem('user')

            if (!user) {
                const data = new FormData();
                const that = this;
                data.append("user_id", this.$route.params.id)
                data.append("action", "get_user");
                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {

                        if (response.data) {
                            that.form.user_id = response.data.ID;
                            that.form.bio = response.data.bio;
                            that.form.state = response.data.state;
                            that.form.country = response.data.country;
                            that.form.city = response.data.city;
                            that.form.phone = response.data.phone_number;
                            that.form.eamil = response.data.email;
                            if (response.data.profile_image){
                                that.form.fileUpload = {
                                    url: response.data.profile_image
                                };
                            }

                            that.form.firstName = response.data.first_name;
                            that.form.lastName = response.data.last_name;
                            that.form.terms = true
                        }

                        setTimeout(() => {
                            that.$emit('update-loading', false);
                        }, 200)

                        // that.loading = false;
                    })
                    .catch(function (error) {
                        console.log(error)
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error("An error occurred");
                        }
                        this.$emit('update-loading', false);
                    });
            }
            else{
                user = JSON.parse(user);
                this.form.user_id = user.ID;
                this.form.bio = user.bio;
                this.form.state = user.state;
                this.form.country = user.country;
                this.form.city = user.city;
                this.form.phone = user.phone_number;
                this.form.eamil = user.email;
                if (user.profile_image){
                    this.form.fileUpload = {
                        url: user.profile_image
                    };
                }

                this.form.firstName = user.first_name;
                this.form.lastName = user.last_name;
                this.form.terms = true;
                this.$emit('update-loading',false);
            }
        },
        clearFile(){
            this.form.fileUpload = ''
            this.$refs.fileInput.value = '';
        }

    },

    template: `<form ref="signupForm" id="wp_signup_form" method="post" @submit.prevent="userRegistration">

    <div class="row">
        <div class="mb-3 col-md-6">
          <label for="fname" class="form-label">{{signUpFormStrings.firstName.label}}<span class="text-danger">*</span></label>
            <input type="text" class="form-control" :placeholder="signUpFormStrings.firstName.placeholder"
                   autocomplete="off" id="fname" name="name"
                   v-model="form.firstName">
            <span class="text-danger validation-error" v-if="formErrors.firstName">{{formErrors.firstName}}</span>
        </div>
        <div class="mb-3 col-md-6">
        <label for="lname" class="form-label">{{signUpFormStrings.lastName.label}}<span class="text-danger">*</span></label>
            <input type="text" class="form-control" :placeholder="signUpFormStrings.lastName.placeholder"
                   autocomplete="off" id="lname" name="name"
                   v-model="form.lastName">
            <span class="text-danger validation-error" v-if="formErrors.lastName">{{formErrors.lastName}}</span>
        </div>
        <div class="mb-3  col-sm-12" v-if="!form.user_id">
        <label for="email" class="form-label">{{signUpFormStrings.email.label}}<span class="text-danger">*</span></label>
            <input type="email" class="form-control" id="email" :placeholder="signUpFormStrings.email.placeholder"
                   name="email"
                   v-model="form.email">
            <span class="text-danger validation-error d-block" v-if="formErrors.email">{{formErrors.email}}</span>
        </div>
        <div class="col-md-6 mb-3">
        <label for="country" class="form-label">{{signUpFormStrings.country.label}}<span class="text-danger">*</span></label>
            <AnaVueSelect @search-change="getCountries" v-model="form.country" :options="countries"
                          :placeholder="signUpFormStrings.country.placeholder" id="country"></AnaVueSelect>
            <span class="text-danger validation-error" v-if="formErrors.country">{{formErrors.country}}</span>

        </div>
        <div class="col-md-6 mb-3">
        <label for="state" class="form-label">{{signUpFormStrings.state.label}}<span class="text-danger">*</span></label>
            <AnaVueSelect @search-change="getStates" v-model="form.state" :options="states"
                          :placeholder="signUpFormStrings.state.placeholder" :fieldName="signUpFormStrings.state.label"
                          :disabled="!form.country" id="state"></AnaVueSelect>
            <small>{{signUpFormStrings.state.tip}}</small>
            <span class="text-danger d-block validation-error" v-if="formErrors.state">{{formErrors.state}}</span>
        </div>

        <div class="col-md-12 mb-3">
        <label for="city" class="form-label">{{signUpFormStrings.city.label}}<span class="text-danger">*</span></label>
            <input type="text" name="city" id="city" v-model="form.city"
                   :placeholder="signUpFormStrings.city.placeholder"
                   class="form-control">
            <span class="text-danger validation-error" v-if="formErrors.city">{{formErrors.city}}</span>
        </div>
        <div class="col-md-12 mb-3">
        <label for="phone" class="form-label">{{signUpFormStrings.phone.label}}</label>
            <ana-vue-phone-number v-model="form.phone" :country-code="form.country"
                                  @phoneError="phoneNumberError" id="phone"></ana-vue-phone-number>
            <span class="text-danger validation-error" v-if="formErrors.phone">{{formErrors.phone}}</span>
        </div>

        <div class="col-md-12 mb-3">
        <label for="description" class="form-label">{{signUpFormStrings.bio.label}}</label>
                <textarea name="description" id="description" v-model="form.bio" rows="4"
                          :placeholder="signUpFormStrings.bio.placeholder" class="form-control"></textarea>
            <span class="text-danger validation-error" v-if="formErrors.bio">{{formErrors.bio}}</span>

        </div>
        <div class="mb-3 col-sm-12" v-if="!form.user_id">
                <label for="description" class="form-label">{{signUpFormStrings.password.label}}<span class="text-danger">*</span></label>
            <div class="input-group icon-right position-relative">
                <i v-if="showPassword" class="fas fa-eye icon cursor-pointer" @click="togglePassword"></i>
                <i v-else class="fas fa-eye-slash icon cursor-pointer" @click="togglePassword"></i>
                <input type="password" class="form-control" id="password" ref="password"
                       :placeholder="signUpFormStrings.password.placeholder"
                       name="password" v-model="form.password"
                >
            </div>
            <span class="text-danger validation-error" v-if="formErrors.password">{{formErrors.password}}</span>

        </div>
        <div v-if="signupImage=='yes'" class="col-md-12 mb-5">
             <label class="form-label">{{signUpFormStrings.profileImage.label}}<span v-if="imageRequired==='yes'" class="text-danger">*</span></label>
            <ana-vue-file-upload v-model="form.fileUpload"
                                 :maxSizeLabel="signUpFormStrings.profileImage.tips.maxSize"
                                 :maxUploadSize="maxUpload"
                                 :typesTitle="signUpFormStrings.profileImage.tips.imageTypes"
                                 fileTypes="png,jpeg,jpg"
                                 acceptedFiles="image/png,image/jpeg,image/jpg"
                                 :crop="true"
                                 :uploadButtonText="signUpFormStrings.profileImage.buttons.uploadImage"
                                 :changeButtonText="signUpFormStrings.profileImage.buttons.changeImage"
            ></ana-vue-file-upload>
            <span class="text-danger validation-error" v-if="formErrors.fileUpload">{{formErrors.fileUpload}}</span>
        </div>
        <div class="mb-5  col-sm-12" v-if="!form.user_id">
            <label for="terms" class="cursor-pointer">
                <input type="checkbox" name="terms" id="terms" v-model="form.terms">
                <span class="ml-4">{{signUpFormStrings.terms.agree}} <a :href="homeUrl+'/terms-of-usage'"
                                                                        class="hs-primary-text-color-light"> {{signUpFormStrings.terms.services}} </a> {{signUpFormStrings.terms.and}}  <a
                        :href="homeUrl+'/privacy-policy/'" class="hs-primary-text-color-light">{{signUpFormStrings.terms.privacyPolicy}} </a> </span>
            </label>
        </div>
        <div class="mt-4 col-md-12">
            <div class="d-flex align-items-center flex-wrap">
                <button type="submit" class="hs-btn hs-btn-signup hs-btn-primary"
                        :class="{'not-allowed':submitting || Object.keys(formErrors).length > 0 || !form.terms}"
                        :disabled="submitting || Object.keys(formErrors).length > 0 || !form.terms">{{buttonText}}<i
                        v-if="submitting" class="fas fa-spinner fa-pulse ml-2"></i></button>
            </div>
        </div>
    </div>

</form>

                        
`,
};

export {SignUpForm};
