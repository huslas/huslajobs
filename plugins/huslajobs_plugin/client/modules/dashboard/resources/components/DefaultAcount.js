import {LoadingBar} from "../../../../../admin/modules/dashboard/resources/components/LoadingBar.js";
import {ReusableFunctions} from "../../../../../js/functions.js";
import {AnaToolTip} from "../../../../../inc/global_components/tool-tip/ana-tool-tip.js";


// window.intlTelInput(phone, {});

const DefaultAccount = {
    data() {
        return {
            showProfile: 1,
            userAccountType: 'individual_job_seeker',
            accountTypes: [{
                label: 'I am a professional/freelancer looking for jobs',
                value: 'individual_job_seeker'
            }, {label: 'I am an employer looking to hire employees or post jobs', value: 'individual_recruiter'}],
            formErrors: {},
            submitting: false,
            jsUrl: THEME_JS_URL,
            homeUrl: home_url,
            loading: false,

        };
    },

    components: {
        LoadingBar,
        AnaToolTip
    },
    mixins:[ReusableFunctions],
    mounted() {
        this.startLoader();
        let active_account = localStorage.getItem('active_account')
        if (active_account){
            window.location.href = home_url + '/my-account';
        }else{
            this.closeLoader();
        }
        // this.logOut();
    },

    destroyed() {
        this.form = {
            name: undefined,
            email: undefined,
            password: undefined,
            country: undefined,
            state: undefined,
            terms: false
        }
    },
    methods: {
        updateAccountType(accountType) {
            if (accountType.value !== this.userAccountType) {
                this.userAccountType = accountType.value;
            }
        },
        setAccount(){
            this.submitting = true;
            const data = new FormData();
            data.append("show_profile", this.showProfile);
            data.append("account_type", this.userAccountType);
            data.append("action","set_default_account");
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: {"Content-Type": "multipart/form-data"},
            })
                .then(function (response) {
                    if (response.status < 300) {
                        toastr.success(response.data.message);
                        setTimeout(()=>{
                            window.location.href = home_url + '/my-account';
                        },200);
                    }
                })
                .catch(function (error) {
                    console.log(error)
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error(that.postJobStrings.errors.errorOccurred);
                    }
                    that.submitting = false;
                });
        }

    },

    template: `<div id="card" class="card">

    <div class="card-body p-0">
        <div class="row mb-5">
            <div class="col-md-12">
                <h2 class="section-title mb-1">
                    <span>Let's get you set up</span>
                </h2>
                   <p>What will you be doing on this platform</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-7 ">
                <form id="hs_account_form" method="post" @submit.prevent="setAccount">
                    <div class="row mb-4">
                        <div v-for="(accountType,index) in accountTypes"
                             class="col-md-6 d-flex justify-content-between flex-column" style="margin: 0 auto">
                            <div class="rounded activity-card cursor-pointer text-center p-4" :key="index"
                                 :class="{'active':userAccountType === accountType.value}"
                                 @click="updateAccountType(accountType)">
                                <p class="pt-4 font-weight-800">{{accountType.label.toString()}}</p>
                                <span><i class="fas fa-check-circle"></i></span>
                            </div>
                        </div>
                    </div>

                    <div v-if="userAccountType==='individual_job_seeker'" class="row">
                        <div class="col-md-12 mb-4">
                            <label for="country">Show my profile on job seekers page
                                <span >?</span>
                            </label>
                            <div class="d-flex align-items-center">
                                <label for="yes" class="form-label cursor-pointer d-inline-block mr-4">
                                    <input type="radio" class="mr-2" id="yes" name="show_profile" v-model="showProfile" value="1" />
                                           <span>Yes</span>
                               </label>
                               <label for="no" class="form-label cursor-pointer">
                                    <input type="radio" class="mr-2" id="no" name="show_profile" v-model="showProfile" value="0"/>
                                           <span>No</span>
                               </label>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-md-12 mb-4">
                            <button type="submit" class="hs-btn hs-btn-signup hs-btn-primary">Continue <i
                                        v-if="submitting"
                                        class="fas fa-spinner fa-pulse ml-1"></i></button>
                        </div>
                    </div>
                </form>
            </div>
            <!--                                <div class="col-md-5">                                      -->
            <!--                                    <img :src="imgUrl +'/demo-images/employee%20benefit.png'" alt="" class="stepper-image">                                     -->
            <!--                                </div>-->
        </div>


    </div>
</div>
`,
};

export {DefaultAccount};
