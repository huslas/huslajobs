import {AnaVueSelect} from "../../../../../../inc/global_components/ana_select/ana_vue_select.js";
import {LoadingBar} from "../../../../../../admin/modules/dashboard/resources/components/LoadingBar.js";
import {LoadingError} from "../../../../../../admin/modules/dashboard/resources/components/LoadingError.js";
import {ReusableFunctions} from "../../../../../../js/functions.js";
import {VLoader} from "../../../../../../inc/global_components/loader/v-loader.js";
import {AnaVuePhoneNumber} from "../../../../../../inc/global_components/phone-number/ana_vue_phone_number.js";
import {AnaVueFileUpload} from "../../../../../../inc/global_components/ana_file_upload/ana_vue_fileupload.js"

const AddProfile = {
    data() {
        return {
            id: 0,
            categories: [],
            router: {},
            formData: {
                profileName: undefined,
                profileDescription: undefined,
                fileUpload: undefined,
                profileCategories: undefined,
                profileEmail: undefined,
                jobTitle: undefined,
                phoneNumber: undefined,
                imageUpload: undefined,
                experience:undefined
            },
            loading: false,
            submitting:false,
            error:false,
            maxUpload:maximum_upload,
            formErrors: {},
            imgUrl:THEME_URL,
            profileCreateStrings:VueUiStrings.clientFrontEnd.dashboard.profiles.create,
            imageRequired:profile_image_required
        }
    },
    watch: {
        'formData.profileName': function (currentVal, oldVal) {
            this.validateInput("profileName", currentVal, {required: true}, this.profileCreateStrings.formFields.name.label);
        },
        'formData.jobTitle': function (currentVal, oldVal) {
            this.validateInput("jobTitle", currentVal, {required: true}, this.profileCreateStrings.formFields.jobTitle.label);
        },
        'formData.profileEmail': function (currentVal, oldVal) {
            this.validateInput("profileEmail", currentVal, {required: true,isEmail:true}, this.profileCreateStrings.formFields.email.label);
        },
        // 'formData.experience': function (currentVal, oldVal) {
        //     this.validateInput("experience", currentVal, {required: true}, this.profileCreateStrings.formFields.experience.label);
        // },
        'formData.phoneNumber': function (currentVal, oldVal) {
            this.validateInput("phoneNumber", currentVal, {required: true}, this.profileCreateStrings.formFields.phone.label);
        },
        'formData.profileDescription': function (currentVal, oldVal) {
            this.validateInput("profileDescription", currentVal, {required: true,min:150 ,max:500}, this.profileCreateStrings.formFields.description.label);
        },
        'formData.fileUpload': function (currentVal, oldVal) {
            this.validateInput("fileUpload", currentVal, {required: true}, this.profileCreateStrings.formFields.cv.label);
        },
        'formData.imageUpload': function (currentVal, oldVal) {
            this.validateInput("imageUpload", currentVal, {required: true}, this.profileCreateStrings.formFields.profileImage.label);
        },
        'formData.profileCategories': function (currentVal, oldVal) {
            this.validateInput("profileCategories", currentVal, {required: true}, this.profileCreateStrings.formFields.categories.label);
        },

    },
    components: {
        AnaVueSelect,
        LoadingBar,
        LoadingError,
        AnaVuePhoneNumber,
        VLoader,
        AnaVueFileUpload,
    },
    methods: {
        getActiveAccount() {
            let active_account = localStorage.getItem('active_account')
            active_account = JSON.parse(active_account)
            this.active_account = active_account
        },

        /**
         * function gets categories,job types, currencies, accounts
         */
        getCategories() {
            this.startLoader();
            const data = new FormData();
            data.append("action", "get_categories");
            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: {"Content-Type": "multipart/form-data"},
            })
                .then(function (response) {
                    if (response) {
                        that.categories = response.data.map(function (e) {
                            return {'label': e.name, 'value': e.id}
                        })
                        if (that.id){
                            that.getProfile()
                        }else{
                            setTimeout(()=>{
                                that.loading = false;
                                that.closeLoader();
                            },100)
                        }
                    }
                })
                .catch(function (error) {
                    console.log(error);
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error(that.profileCreateStrings.errors.errorOccurred);
                    }
                    that.loading = false;
                    that.closeLoader();
                });
        },
        submitForm() {
            this.submitting = true
            //validate inputs
            this.validateInput("profileName", this.formData.profileName, {required: true},this.profileCreateStrings.formFields.name.label);
            this.validateInput("profileDescription", this.formData.profileDescription, {required: true}, this.profileCreateStrings.formFields.description.label);
            this.validateInput("fileUpload", this.formData.fileUpload, {required: true}, this.profileCreateStrings.formFields.cv.label);
            this.validateInput("profileCategories", this.formData.profileCategories, {required: true}, this.profileCreateStrings.formFields.categories.label);
            this.validateInput("jobTitle", this.formData.jobTitle, {required: true},this.profileCreateStrings.formFields.jobTitle.label);
            this.validateInput("profileEmail", this.formData.profileEmail, {required: true,isEmail:true}, this.profileCreateStrings.formFields.email.label);
            // this.validateInput("experience", this.formData.experience, {required: true}, this.profileCreateStrings.formFields.experience.label);
            this.validateInput("phoneNumber", this.formData.phoneNumber, {required: true}, this.profileCreateStrings.formFields.phone.label);
            if (this.imageRequired === 'yes'){
                this.validateInput("imageUpload", this.formData.imageUpload, {required: true}, this.profileCreateStrings.formFields.profileImage.label);
            }


            if (Object.keys(this.formErrors).length === 0) {

                const data = new FormData();
                const that = this;
                const image = this.formData.imageUpload?.file ? this.formData.imageUpload?.file : this.formData.imageUpload?.url ?? '';
                const cv =  this.formData.fileUpload?.file ?this.formData.fileUpload?.file  : this.formData.fileUpload?.url ?? '';
                data.append("username", this.formData.profileName);
                data.append("description", this.formData.profileDescription);
                data.append("cv",cv);
                data.append("image",image );
                data.append("phone_number", this.formData.phoneNumber);
                data.append("years_of_experience", this.formData.experience);
                data.append("email", this.formData.profileEmail);
                data.append("title", this.formData.jobTitle);
                data.append("categories", this.formData.profileCategories);
                data.append("profile_id", this.id || 0);
                data.append('my_account_nonce', document.getElementById("huslajobs-my-account-nonce").value);
                data.append("action", "add_profile");

                // return;
                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {
                        if (response.data) {
                            toastr.success(response.data.message);
                            that.successMessage = response.data.message
                            localStorage.removeItem('user',response.data.user) // force getting new data from database;
                            localStorage.setItem('user',JSON.stringify(response.data.user));
                            const redirect_url =that.$route.query.redirect_url;
                            if (redirect_url){
                                window.location.href = redirect_url+'?selected_profile='+response.data.profile_id;
                            }else{
                                that.router.push('/');
                                localStorage.setItem('defaultHeader',JSON.stringify('profiles'));
                            }
                        }
                        that.submitting = false;

                    })
                    .catch(function (error) {
                        console.log(error)
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.profileCreateStrings.errors.errorOccurred);
                        }
                        that.submitting = false;

                    });

            } else {
                this.submitting = false
                const firstErrorControl = document.querySelector('.validation-error')?.parentElement;
                // Scroll to first error element
                window.scrollTo(
                    firstErrorControl?.offsetLeft || 0,
                    (firstErrorControl?.offsetTop || 0) - 50 // Subtract 50 for better exposure
                )
            }
        },
        clearFile(){
            this.formData.fileUpload = undefined
            this.$refs.fileInput.value = '';
        },
        getProfile() {
            this.startLoader();
            const data = new FormData();
            data.append("profile_id", this.id);
            data.append("action", "get_profile");
            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: {"Content-Type": "multipart/form-data"},
            })
                .then(function (response) {
                    if (response) {
                        that.formData.profileName = response.data.username;
                        that.formData.profileDescription = response.data.description;
                        that.formData.profileEmail = response.data.email;
                        that.formData.jobTitle = response.data.title;
                        that.formData.experience = response.data.years_of_experience;
                        that.formData.phoneNumber = response.data.phone_number;
                        that.formData.profileCategories = response.data.categories.map(e => parseInt(e.category_id));
                        that.formData.fileUpload = {
                            url: response.data.cv
                        }
                        that.formData.imageUpload = {
                            url: response.data.image
                        }
                        that.loading = false;
                        that.closeLoader();
                    }

                })
                .catch(function (error) {
                    console.log(error);
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error(that.profileEditStrings.errors.errorOccurred);
                    }
                    that.loading = false;
                    that.error = true;
                    that.closeLoader();
                });
        },
        phoneNumberError(err) {
            if (!err && this.formErrors){
                this.formErrors["phone"] = this.profileCreateStrings.formFields.phone.error;
            }
            else if (err && (this.formErrors && this.formErrors["phone"] ) ){
                delete this.formErrors["phone"]
            }
        },

    },
    mixins:[ReusableFunctions],
    mounted() {
        this.id = this.$route.params.id;
        this.router = VueRouter.useRouter();
        this.getCategories();
    },
    // language=HTML
    template:
        `<VLoader :active="loading" :translucent="true">
    <div v-if="error">
        <loading-error @update="getCategories"></loading-error>
    </div>
        <section v-else class="my-account-profile hs-section" >
            <div class="row mb-4 text-center">
                <div class="col-md-12">
                    <h1 class="page-title p-0 mb-0">
                     <span v-if="id">{{profileCreateStrings.headings.edit}}</span>
                        <span v-else>{{profileCreateStrings.headings.create}}</span>
                    </h1>
                    <router-link to="/" class="hs-primary-text-color-light">{{profileCreateStrings.goBack}}</router-link>
                </div>
            </div>
            <div class="row mb-1">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <form class="husla-form mt-4" @submit.prevent="submitForm">
                        <!--     Show this part when user is posting for the first time or is using a job seeker account                             -->
                        <div class="mb-3">
                            <label for="name" class="form-label">{{profileCreateStrings.formFields.name.label}}
                                <span  class="text-danger">*</span>
                            </label>
                            <input type="text" class="form-control" :placeholder="profileCreateStrings.formFields.name.placeholder"
                                   v-model="formData.profileName" id="name" />
                            <span class="text-danger validation-error"
                                  v-if="formErrors.profileName">{{formErrors.profileName}}</span>
                        </div>
                        <div class="mb-3">
                            <label for="jobTitle" class="form-label">{{profileCreateStrings.formFields.jobTitle.label}}
                                <span  class="text-danger">*</span>
                            </label>
                            <input type="text" class="form-control" :placeholder="profileCreateStrings.formFields.jobTitle.placeholder"
                                   v-model="formData.jobTitle" id="jobTitle"/>
                            <span class="text-danger validation-error"
                                  v-if="formErrors.jobTitle">{{formErrors.jobTitle}}</span>
                        </div>
                        <div class="mb-3">
                            <label for="description" class="form-label">{{profileCreateStrings.formFields.description.label}}
                                <span  class="text-danger">*</span>
                            </label>
                                <textarea class="form-control" :placeholder="profileCreateStrings.formFields.description.placeholder"
                                          v-model="formData.profileDescription" rows="5" id="description"></textarea>
                            <span class="text-danger validation-error"
                                  v-if="formErrors.profileDescription">{{formErrors.profileDescription}}</span>
                        </div>
                        <div class="mb-3">
                            <label for="categories" class="form-label">{{profileCreateStrings.formFields.categories.label}}
                                <span  class="text-danger">*</span>
                            </label>
                            <ana-vue-select v-model="formData.profileCategories"
                                            :options="categories" :placeholder="profileCreateStrings.formFields.categories.placeholder"
                                            mode="multiple" :searchable="true" id="categories"></ana-vue-select>
                            <span class="text-danger validation-error"
                                  v-if="formErrors.profileCategories">{{formErrors.profileCategories}}</span>
                        </div>
                        <div class="mb-3">
                            <label for="experience" class="form-label">{{profileCreateStrings.formFields.experience.label}}
                            </label>
                            <input type="number" min="0" class="form-control" :placeholder="profileCreateStrings.formFields.experience.placeholder"
                                   v-model="formData.experience" id="experience" />
                            <span class="text-danger validation-error"
                                  v-if="formErrors.experience">{{formErrors.experience}}</span>
                        </div>
                        <div class="mb-5">
                            <label for="cv" class="form-label">{{profileCreateStrings.formFields.cv.label}}
                                <span  class="text-danger">*</span>
                            </label>
                            <ana-vue-file-upload v-model="formData.fileUpload"
                                                 :maxSizeLabel="profileCreateStrings.formFields.cv.tips.maxSize"
                                                 :maxUploadSize="maxUpload"
                                                 :typesTitle="profileCreateStrings.formFields.cv.tips.fileTypes"
                                                 fileTypes="pdf,word"
                                                 :wordUrl="imgUrl + '/document-type/microsoft-word.png'"
                                                 :pdfUrl="imgUrl + '/document-type/pdf-logo.png'"
                                                 acceptedFiles="application/pdf,.doc,.docx,application/msword"
                                                 :uploadButtonText="profileCreateStrings.formFields.cv.buttons.uploadCv"
                                                 :changeButtonText="profileCreateStrings.formFields.cv.buttons.changeCv"
                                                 id="cv"
                            ></ana-vue-file-upload>
                            <span class="text-danger validation-error"
                                  v-if="formErrors.fileUpload">{{formErrors.fileUpload}}</span>
                        </div>
                        <div class="mb-3">
                            <label for="email" class="form-label">{{profileCreateStrings.formFields.email.label}}
                                <span  class="text-danger">*</span>
                            </label>
                            <input type="email" class="form-control" :placeholder="profileCreateStrings.formFields.email.label"
                                   v-model="formData.profileEmail" id="email"/>
                            <span class="text-danger validation-error"
                                  v-if="formErrors.profileEmail">{{formErrors.profileEmail}}</span>
                        </div>
                        <div class="mb-3">
                            <label for="phone" class="form-label">{{profileCreateStrings.formFields.phone.label}}
                                <span  class="text-danger">*</span>
                            </label>
                            <ana-vue-phone-number v-model="formData.phoneNumber"
                                                  @phoneError="phoneNumberError" id="phone"></ana-vue-phone-number>
                            <span class="text-danger validation-error"
                                  v-if="formErrors.phoneNumber">{{formErrors.phoneNumber}}</span>
                        </div>
                        <div class="mb-5">
                            <label for="profileImage" class="form-label">{{profileCreateStrings.formFields.profileImage.label}}
                                <span v-if="imageRequired==='yes'"  class="text-danger">*</span>
                            </label>
                            <ana-vue-file-upload v-model="formData.imageUpload" 
                                                 :maxSizeLabel="profileCreateStrings.formFields.profileImage.tips.maxSize"
                                                 :maxUploadSize="maxUpload"
                                                 :typesTitle="profileCreateStrings.formFields.profileImage.tips.imageTypes"
                                                 fileTypes="png,jpeg,jpg"
                                                 acceptedFiles="image/png,image/jpeg,image/jpg"
                                                 :crop="true"
                                                 :uploadButtonText="profileCreateStrings.formFields.profileImage.buttons.uploadImage"
                                                 :changeButtonText="profileCreateStrings.formFields.profileImage.buttons.changeImage"
                                                 id="profileImage"
                            ></ana-vue-file-upload>
                            <span class="text-danger validation-error" v-if="formErrors.imageUpload">{{formErrors.imageUpload}}</span>
                        </div>

                        <div class="mt-5 d-flex justify-content-between flex-wrap">

                            <router-link to="/" class="hs-btn hs-btn-signup hs-btn-gray-outline mb-4">
                                {{profileCreateStrings.formFields.buttons.cancel}}
                            </router-link>
                            <button type="submit" class="hs-btn hs-btn-signup hs-btn-primary mb-4" :class="{'not-allowed':submitting || Object.keys(formErrors).length > 0 }"
                                    :disabled="submitting || Object.keys(formErrors).length > 0 ">
                                
                                <span v-if="id">{{profileCreateStrings.formFields.buttons.editProfile}}</span>
                                <span v-else>{{profileCreateStrings.formFields.buttons.addProfile}}</span>
                               
                                <i v-if="submitting" class="fas fa-spinner fa-pulse ml-2"></i>
                            </button>
                        </div>
                    </form>
                </div>
                <div class="col-md-2"></div>
            </div>
        </section>

</VLoader>
            `
}

export {AddProfile}