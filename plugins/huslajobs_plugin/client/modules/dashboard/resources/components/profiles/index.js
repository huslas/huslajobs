import {Pagination} from "../../../../../../admin/modules/dashboard/resources/components/Pagination.js";
import {LoadingError} from "../../../../../../admin/modules/dashboard/resources/components/LoadingError.js";
import {LoadingBar} from "../../../../../../admin/modules/dashboard/resources/components/LoadingBar.js";
import {ReusableFunctions} from "../../../../../../js/functions.js";

const Profiles = {
    emits:['delete_profile'],
    data() {
        return {
            imageUrl: THEME_URL,
            profiles: [],
            active_account: {},
            user: {},
            loading: false,
            error: false,
            profileIndexStrings:VueUiStrings.clientFrontEnd.dashboard.profiles.index
        };
    },
    components: {
        LoadingBar,
        LoadingError,
        Pagination,
    },
    mounted() {
        //todo get all
        this.getProfiles();
    },
    mixins:[ReusableFunctions],

    watch: {
        searchText: function (current) {
            if (current === '') {
                this.search();
            }
        },
    },

    methods: {
        getProfiles() {
            let user = localStorage.getItem('user')
            // let active_account = localStorage.getItem('active_account')
            user = JSON.parse(user)
            // active_account = JSON.parse(active_account)
            this.profiles = user.profiles;//.filter(e => e.account_id == active_account.id)
            this.user = user;
            // this.active_account = active_account
        },
        deleteProfile(profile) {
            const confirm_delete = confirm(this.profileIndexStrings.confirm +" "+ profile.username + "?");
            if (confirm_delete) {
                this.startLoader();
                const data = new FormData();
                data.append("action", "delete_profile");
                data.append("profile_id", profile.id);
                const that = this;
                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {
                        // that.getAllJobs()
                        toastr.success(profile.username + " deleted");
                        localStorage.removeItem('user') // force getting new data from database
                        localStorage.setItem('defaultHeader',JSON.stringify('profiles'));
                        window.location.reload();

                    })
                    .catch(function (error) {
                        console.log(error);
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.profileIndexStrings.errors.errorOccurred);
                        }
                        that.closeLoader();
                    });
            }
        },
        goToPage(page) {
            this.page = page
            this.getProfiles();
        }
    },
    template: `<div class="module-content-wrapper">
    <loading-bar v-if="loading"></loading-bar>
    <div v-else>
        <div v-if="error">
            <loading-error @update="getProfiles"></loading-error>
        </div>
        <div v-else>
            <div v-if="profiles.length">
                <div class="row">
                    <div class="col-12">
                            <div class="card-body">
                                <router-link to="/add-profile" class="hs-btn hs-btn-primary">
                                    {{profileIndexStrings.addProfile}}
                                </router-link>
                            </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <table class="table table-striped">
                                    <tr>
                                        <th>{{profileIndexStrings.tableHeadings.profiles}}</th>
                                        <th>{{profileIndexStrings.tableHeadings.actions}}</th>
                                    </tr>
                                    <tr v-for="profile of profiles" :key="profile.id">
                                        <td>
                                            <div class="row">
                                                <div class="col-md-1">
                                                    <a :href="profile.cv" target="_blank">
                                                        <img :src="imageUrl+'/document-type/pdf-logo.png'" alt="cv">
                                                    </a>

                                                </div>
                                                <div class="col-md-11">
                                                    <h5 class="m-0 userdata-body-title">
                                                        {{decodeHtml(profile.title)}}</h5>
                                                    <p>
                                                        <i class="fas fa-eye"></i>
                                                        <span class="ml-2">{{profileIndexStrings.public}}</span>
                                                    </p>

                                                </div>
                                            </div>
                                        </td>

                                        <td>
                                            <i class="fa-solid fa-arrow-right-arrow-left"></i>
                                            <router-link :to="'/edit-profile/'+profile.id"
                                                         class="btn btn-info btn-sm mr-2 mb-2"><i
                                                    class="fa fa-pencil-alt fa-2x"></i></router-link>

                                            <button class="btn btn-danger btn-sm mb-2" @click="deleteProfile(profile)">
                                                <i
                                                        class="fa fa-trash fa-2x"></i></button>


                                        </td>
                                    </tr>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div v-else class="d-flex flex-column align-items-center justify-content-center">
                <p>{{profileIndexStrings.noProfile}}</p>
                <router-link to="/add-profile" class="hs-btn hs-btn-primary">
                    {{profileIndexStrings.addProfile}}
                </router-link>
            </div>
        </div>
    </div>
</div>
`,
};
export {Profiles};
