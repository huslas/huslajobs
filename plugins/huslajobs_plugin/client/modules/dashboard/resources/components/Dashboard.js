import {JobSearchForm} from "./job-search/job-search-form.js";
import {JobSearchResults} from "./job-search/job-search-results.js";
import {JobSeekerSearchForm} from "./job-seeker-search/job-seeker-search-form.js";
import {AccountJobs} from "./job/index.js";
import {Jobs} from "../../../../../admin/modules/dashboard/resources/components/jobs/index.js";
import {Accounts} from "./accounts/index.js";
import {Profiles} from "./profiles/index.js";
import {LoadingBar} from "../../../../../admin/modules/dashboard/resources/components/LoadingBar.js";
import {LoadingError} from "../../../../../admin/modules/dashboard/resources/components/LoadingError.js";
import {JobSearch} from "./job-search/job-search.js";
import {DefaultAccount} from "./DefaultAcount.js";
import {AccJobApplications} from './job-applications/index.js'
import {VLoader} from "../../../../../inc/global_components/loader/v-loader.js";
import {JobSeekerSearch} from "./job-seeker-search/job-seeker-search.js";
import {AnaToolTip} from "../../../../../inc/global_components/tool-tip/ana-tool-tip.js";
import {ReusableFunctions} from "../../../../../js/functions.js";
import {AccCompanies} from "./companies/index.js";

const Dashboard = {
    data() {
        return {
            imageUrl: THEME_URL,
            showResults: false,
            userId: user_id,
            user: undefined,
            homeUrl: home_url,
            active_account: undefined,
            accountJobs: 0,
            loading: false,
            error: false,
            loggingOut: false,
            activation_status: user_activation_status,
            userDataHeaders: [],
            defaultHeader: 'jobs',
            membership_plan: undefined,
            payment_complete,
            application_payment_complete,
            tooltip: 'upload resume',
            dashboardStrings: VueUiStrings.clientFrontEnd.dashboard.dashboard,
            router: {},
            profileNotice:profile_notice,
            hideProfileNotice: false
        }

    },
    components: {
        JobSearch,
        JobSeekerSearchForm,
        Jobs,
        Accounts,
        Profiles,
        LoadingBar,
        LoadingError,
        DefaultAccount,
        AccJobApplications,
        VLoader,
        JobSeekerSearch,
        AnaToolTip,
        AccCompanies

    },
    mounted() {
        this.router = VueRouter.useRouter();
        this.logOutUser();
        if (!this.loggingOut) {
            this.getUser();
        }

    },
    mixins: [ReusableFunctions],
    destroy() {
        this.payment_complete = undefined;
        this.application_payment_complete =undefined;
    },
    methods: {
        getSearchResult(searchResult) {
            this.showResults = searchResult;
        },
        updateProfileCount(total) {
            this.userDataHeaders = this.userDataHeaders.map((header) => {
                if (header.key === 'profiles') {
                    header.total = total
                }
                return header
            })
        },
        setUserData() {
            //set headers
            this.userDataHeaders.push(
                {
                    key: 'jobs',
                    value: this.dashboardStrings.dataHeaders.jobs,
                    total: this.user.jobs.length
                },
                {
                    key: 'job_applications',
                    value: this.dashboardStrings.dataHeaders.jobApplications,
                    total: this.user.job_applications.length
                },
                {
                    key: 'profiles',
                    value: this.dashboardStrings.dataHeaders.profiles,
                    total: this.user.profiles.length
                },
                {
                    key: 'companies',
                    value: this.dashboardStrings.dataHeaders.companies,
                    total: this.user.companies.length
                }
            );
            if (!parseInt(this.activation_status)) {
                toastr.warning(this.dashboardStrings.errors.emailUnverified)
            }
            if (this.payment_complete !== undefined) {
                toastr.success(this.dashboardStrings.successMessage.paymentSuccessful);
                this.payment_complete = undefined;
            }
            if (this.application_payment_complete !== undefined) {
                toastr.success(this.dashboardStrings.successMessage.applicationSent);
                this.application_payment_complete = undefined;
            }
            if (this.user.subscriptions.length) {
                for (const subscription of this.user.subscriptions) {
                    // const start_date = Date.parse(new Date(subscription.start_date));
                    if (parseInt(subscription.status) === 1) {
                        this.membership_plan = subscription
                    }
                }
            }

        },
        getUser() {
            // if we already have an active account in local storage, no need to fetch the database again
            // let active_account = localStorage.getItem('active_account')
            this.startLoader();
            let user = localStorage.getItem('user');
            let defaultHeader = localStorage.getItem('defaultHeader');
            if (defaultHeader){
                this.defaultHeader = JSON.parse(defaultHeader);
                localStorage.removeItem('defaultHeader');
            }

            if (user == null) {

                const data = new FormData();
                const that = this;
                data.append("action", "get_user_with_accounts");
                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {

                        if (response.data) {
                            that.user = response.data;
                            localStorage.setItem('user', JSON.stringify(that.user))
                            that.setUserData();

                        }
                        setTimeout(() => {
                            that.closeLoader();
                            return;
                        }, 200)
                    })
                    .catch(function (error) {
                        console.log(error)
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.dashboardStrings.errors.errorOccurred);
                        }
                        that.loading = false;
                        that.closeLoader();
                    });
            } else {
                // this.active_account = JSON.parse(active_account)
                this.user = JSON.parse(user);
                this.setUserData();
                setTimeout(() => {
                    this.loading = false
                    this.closeLoader();
                }, 400)
            }

        },
        getAccountJobs(total) {
            this.userDataHeaders = this.userDataHeaders.map((header) => {
                if (header.key === 'jobs') {
                    header.total = total;
                }
                return header
            })
        },
        getApplications(total) {
            this.userDataHeaders = this.userDataHeaders.map((header) => {
                if (header.key === 'job_applications') {
                    header.total = total;
                }
                return header
            })
        },
        getCompanies(total) {
            this.userDataHeaders = this.userDataHeaders.map((header) => {
                if (header.key === 'companies') {
                    header.total = total;
                }
                return header
            })
        },

        updateDefaultHeader(currentHeader) {
            this.defaultHeader = currentHeader;
        },
        updateLoading(value) {
            this.loading = value
        },
        resendVerification(email){
            this.startLoader();
                const data = new FormData();
                const that = this;
                data.append("email", email);
                data.append('wpnonce',document.getElementById("huslajobs-resend-verification-nonce").value);
                data.append("action", "resend_verification_code");
                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: { "Content-Type": "multipart/form-data" },
                })
                    .then(function (response) {
                        if (response.data) {
                            toastr.success(response.data, 'Success');
                                that.closeLoader();
                        }
                    })
                    .catch(function (error) {
                        console.log(error)
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error("An error occurred");
                        }
                        that.closeLoader();
                    });


        },
    },
    template:
        `<VLoader :active="loading" :translucent="true">
    <div v-if="error">
        <loading-error @update="getUser"></loading-error>
    </div>
    <div v-if="user">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <job-search @searchResult="getSearchResult" current-page="my-account"></job-search>
            </div>
            <div class="col-md-1"></div>
        </div>   
        <section id="user-profile" class="hs-section">
        <div v-if="!user?.profiles?.length" id="profile-notice"
             class="row mb-5">
            <div class="col-md-1"></div>
            <div class="col-md-10">

               <div id="profile-notice-content" class="p-3 px-4 position-relative">
                    <h5 class="profile-notice">{{profileNotice}}</h5>
                    <span id="close-profile-notice" @click="hideProfileNotice=true" class="fa fa-times cursor-pointer"></span>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-md-3 profile-image-wrapper">
                            <img v-if="user?.profile_image" :src="user?.profile_image" alt="" class="profile-image">
                            <div v-else
                                 class="round hs-bg-primary d-flex profile-image justify-content-center align-items-center">
                                <h2 class="m-0 hs-gray-text-color font-weight-800" style="font-size: 3.5rem">
                                    {{decodeHtml(user?.first_name[0] ?? user?.display_name[0] )}}{{decodeHtml(user?.last_name[0] ?? user?.display_name[0])}}</h2>
                            </div>
                        </div>
                        <div class="col-md-9 profile-description-wrapper">
                            <h3 class="mb-0 mt-5 font-weight-800">{{decodeHtml(user?.first_name)}} {{decodeHtml(user?.last_name)}}</h3>
                            <p class="p-0 mt-0 mb-2">
                                <span class="mr-2">{{user?.user_email}}</span> <span
                                    v-if="!parseInt(activation_status)" class="text-danger"><a
                                    href="#"
                                    @click.prevent="resendVerification(user?.user_email)"
                                    class="text-danger">{{dashboardStrings.links.verifyEmail}}</a> 
                                </span>
                            </p>
                            <p v-if="membership_plan" class="hs-secondary-text-color">
                                <span>{{membership_plan.package_name}} account  <router-link
                                        v-if="membership_plan.package_name.toLowerCase() !== 'premium' "
                                        to="/upgrade-account" class="hs-primary-text-color-light">{{dashboardStrings.links.upgradeAccount}}</router-link>  </span>
                            </p>
                            <div class="d-flex flex-wrap">
                                <router-link :to="'/account-details'"
                                             class="hs-btn hs-btn-gray-outline hs-btn-sm d-inline-block mr-3 mb-3 position-relative"
                                             @mouseover="tooltip='account details'"
                                             @mouseleave="tooltip=undefined"
                                >
                                    {{dashboardStrings.links.accountDetails}}
                                    <ana-tool-tip v-if="showToolTip(tooltip,'account details')"
                                                  :message="dashboardStrings.links.accountDetails"></ana-tool-tip>
                                </router-link>
                                                                <a :href="homeUrl+'/post-job'"
                                   class="hs-btn hs-btn-primary hs-btn-sm  d-inline-block mr-3 mb-3 position-relative"
                                   @mouseover="tooltip='post job'"
                                   @mouseleave="tooltip=undefined"
                                >
                                    {{dashboardStrings.links.postJob}}
                                    <ana-tool-tip v-if="showToolTip(tooltip,'post job')"
                                                  :message="dashboardStrings.links.postJob"></ana-tool-tip>
                                </a>
                                <router-link to="/add-profile"
                                             class="hs-btn hs-btn-gray hs-btn-sm  d-inline-block mr-3 mb-3 position-relative"
                                             @mouseover="tooltip='upload resume'"
                                             @mouseleave="tooltip=undefined"
                                             
                                >
                                    {{dashboardStrings.links.addProfile}}
                                    <ana-tool-tip v-if="showToolTip(tooltip,'upload resume')"
                                                  :message="dashboardStrings.links.addProfile"></ana-tool-tip>
                                </router-link>
                            </div>
                        </div>
                    </div>
                    <div id="user-data" class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-3 pr-md-0">
                                    <div class="userdata-menu">
                                        <div v-for="(header,index) in userDataHeaders" :key="index"
                                             @click="updateDefaultHeader(header.key)"
                                             class="userdata-menu-item cursor-pointer"
                                             :class="{'active':header.key === defaultHeader}">
                                            <span class="mr-1">{{header.value}}</span>
                                            <span class="total"
                                                  v-if="header.total===0 || header.total>0">({{header.total}})</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-9 pl-md-0">
                                    <div class="userdata-body d-flex">
                                        <!--        jobs                    -->
                                        <div :class="{'d-block active':defaultHeader==='jobs' ,'d-none':defaultHeader!=='jobs'}"
                                             class="userdata-body-item">
                                            <jobs v-if="userId" @account_jobs="getAccountJobs"
                                                  :active_account_id="userId"></jobs>
                                        </div>
                                        <!--    resume            -->
                                        <div :class="{'d-block active':defaultHeader==='profiles' ,'d-none':defaultHeader!=='profiles'}"
                                             class="userdata-body-item">
                                            <profiles v-if="!loading" @delete_profile="updateProfileCount"></profiles>
                                        </div>
                                        <!--                  job applications                  -->
                                        <div class="userdata-body-item"
                                             :class=" {'d-block active':defaultHeader==='job_applications' ,'d-none':defaultHeader!=='job_applications'} ">
                                            <acc-job-applications 
                                                                  @job_applications="getApplications"
                                                                  :active_account_id="user?.ID">
                                                                  
                                            </acc-job-applications>
                                        </div>
                                        <!--                  job applications                  -->
                                        <div class="userdata-body-item"
                                             :class=" {'d-block active':defaultHeader==='companies' ,'d-none':defaultHeader!=='companies'}">
                                            <acc-companies @companies="getCompanies" :account_id="user?.ID"></acc-companies>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-1"></div>
            </div>
        </section>
    </div>

</VLoader>


        `
}

export {Dashboard}