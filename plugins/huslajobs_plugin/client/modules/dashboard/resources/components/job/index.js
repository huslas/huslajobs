
import {Pagination} from "../../../../../../admin/modules/dashboard/resources/components/Pagination.js";
import {LoadingError} from "../../../../../../admin/modules/dashboard/resources/components/LoadingError.js";
import {LoadingBar} from "../../../../../../admin/modules/dashboard/resources/components/LoadingBar.js";

const AccountJobs = {
    props:{
        active_account_id:{
            type:Number,
            default:0,
        }
    },
    emits:['account_jobs'],
    data() {
        return {
            jobs: [],
            page: 1,
            perPage: 15,
            loading: true,
            pages: 0,
            account_id: this.active_account_id
        };
    },
    components: {
        LoadingBar,
        LoadingError,
        Pagination,
    },
    mounted() {
        //todo get all
        this.getAccountJobs();
    },

    watch: {
        searchText: function (current) {
            if (current === ''){
                this.search();
            }
        },
    },


    methods: {
        search() {
            this.page = 1;
            this.sortField = 'id';
            this.sort = 'desc';
            this.getAllJobs();
        },
        sortBy(field) {
            if (this.sortField == field) {
                this.sort = this.sort == 'desc' ? 'asc' : 'desc'
            } else {
                this.sort = 'asc';
                this.sortField = field;
            }
            this.page = 1;
            this.getAllJobs();
        },
        isSortedBy(field, sort = 'asc') {
            switch (sort) {
                case 'asc':
                    return this.sortField == field && sort == this.sort;
                case 'desc':
                    return this.sortField == field && sort == this.sort;
                default:
                    return true;
            }
        },
        getAccountJobs() {
            this.startLoader();
            const data = new FormData();
            data.append("action", "get_jobs");
            data.append("page", this.page);
            data.append("perPage", this.perPage);
            data.append("account_id",this.account_id)
            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: { "Content-Type": "multipart/form-data" },
            })
                .then(function (response) {
                    that.jobs = response.data.data;
                    that.pages = response.data.totalPages;
                    that.$emit('account_jobs',response.data.totalItems)
                    that.loading = false;
                    that.error = false;
                })
                .catch(function (error) {
                    console.log(error);
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error("An error occurred");
                    }
                    that.loading = false;
                    that.error = true;
                });
        },
        deleteJob(job) {
            const confirm_delete = confirm("Are you sure you want to delete " + job.name + "?");
            if (confirm_delete) {
                this.startLoader();
                const data = new FormData();
                data.append("action", "delete_job");
                data.append("job_id", job.id);
                const that = this;
                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: { "Content-Type": "multipart/form-data" },
                })
                    .then(function (response) {
                        that.getAllJobs()
                        toastr.success(job.name + " deleted");
                        that.loading = false;
                    })
                    .catch(function (error) {
                        console.log(error);
                        that.loading = false;

                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error("An error occurred");
                        }
                    });
            }
        },
        goToPage(page) {
            this.page = page
            this.getAllJobs();
        }
    },
    template: `<div class="module-content-wrapper">
    <loading-bar v-if="loading"></loading-bar>
    <div v-if="!loading">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div v-if="error">
                            <loading-error @update="getAccountJobs"></loading-error>
                        </div>
                        <div v-else>

                            <div v-if="jobs.length > 0">
                                <table class="table table-striped">
                                    <tr>
                                        <th @click="sortBy('id')" style="cursor:pointer">id
                                            <i class="fa fa-fw"
                                               v-bind:class="{'fa-sort-down':isSortedBy('id','desc'),'fa-sort-up':isSortedBy('id','asc'),'fa-sort':isSortedBy('id','normal')}"></i>
                                        </th>
                                        <th @click="sortBy('name')" style="cursor:pointer">
                                            name
                                            <i class="fa fa-fw"
                                               v-bind:class="{'fa-sort-down':isSortedBy('name','desc'),'fa-sort-up':isSortedBy('name','asc'),'fa-sort':isSortedBy('name','normal')}"></i>
                                        </th>
                                        <th>Job type</th>
                                        <th>description</th>
                                        <th>location</th>
                                        <th @click="sortBy('salary')" style="cursor:pointer">
                                            salary
                                            <i class="fa fa-fw"
                                               v-bind:class="{'fa-sort-down':isSortedBy('salary','desc'),'fa-sort-up':isSortedBy('salary','asc'),'fa-sort':isSortedBy('salary','normal')}"></i>
                                        </th>
                                        <th>actions</th>
                                    </tr>
                                    <tr v-for="job of jobs">
                                        <td>{{job.id}}</td>
                                        <td><a :href="'/jobs/'+job.id">{{job.name}}</a></td>
                                        <td><span>{{job.job_type_name}}</span></td>
                                        <td>{{job.description}}</td>
                                        <td>{{job.location}}</td>
                                        <td>
                                          <div v-if="job.salary !== 'undefined'">
                                            <span>{{job.salary}}</span>
                                            <span>{{job.currency_code}}</span>                  
                                          </div>
                                          <div v-else>-</div>
                                        </td>
                                        <td>
                                            <router-link :to="'/jobs/'+job.id+'/edit'"
                                                         class="btn mr-2 btn-primary btn-sm"><i
                                                    class="fas fa-pencil-alt "></i>
                                            </router-link>
                                            <button class="btn btn-danger btn-sm" @click="deleteJob(job)"><i
                                                    class="fa fa-trash"></i></button>
                                        </td>
                                    </tr>
                                </table>
                                <pagination v-if="pages > 1" :page="page" :pages="pages"
                                            @update="goToPage"></pagination>
                            </div>
                            <div v-else>
                                <p>No Job found</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
`,
};
export { AccountJobs };
