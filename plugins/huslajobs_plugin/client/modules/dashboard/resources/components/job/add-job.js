import {AnaVueSelect} from "../../../../../../inc/global_components/ana_select/ana_vue_select.js";
import {world_countries as WorldCountries} from "../../../../../../js/countries_data.js";
import {CountriesStates} from "../../../../../../js/states.js";
import {ReusableFunctions} from "../../../../../../js/functions.js";

const AddJob ={
    data(){
        return{
            imageUrl:THEME_URL,
            countries: WorldCountries,
            states:[],
            formData:{
                title:undefined,
                type:undefined,
                category:undefined,
                location:undefined,
                work:undefined,
                salary:undefined,
                description:undefined,
                currency:undefined,
                experience:undefined,
                companyName:undefined,
                companyEmail:undefined,
                companyWebsite:undefined,
                postingAs:'company',
            },
            loading:false,
            jobCategories:[],
            jobTypes:[],
            currencies:[],
            categories:[],
            formErrors: {},
            works:[],
            temporalWorks:[{label:'On site',value:'On site'},{label: 'Remote',value:'Remote'}],
            page: 1,
            perPage: 15,
            pages: 0,
            sortField: 'id',
            sort: 'desc',

        }
    },
    watch: {
        'formData.title': function (currentVal, oldVal) {
            this.validateInput("title", currentVal, {required: true}, "Job title");
        },
        'formData.type': function (currentVal, oldVal) {
            this.validateInput("type", currentVal, {required: true}, "Job type");
        },
        'formData.category': function (currentVal, oldVal) {
            this.validateInput("category", currentVal, {required: true}, "Job category");
        },
        'formData.location': function (currentVal, oldVal) {
            this.validateInput("location", currentVal, {required: true}, "Location");
        },
        'formData.description': function (currentVal, oldVal) {
            this.validateInput("description", currentVal, {required: true}, "Description");
        },
        'formData.work': function (currentVal, oldVal) {
            this.validateInput("work", currentVal, {required: true}, "Work");
        },
        'formData.companyName': function (currentVal, oldVal) {
            if (this.formData.postingAs === 'company') {
                this.validateInput("companyName", currentVal, {required: true}, "Company Name");
            }
        },
        'formData.companyEmail': function (currentVal, oldVal) {
            if (this.formData.postingAs === 'company'){
                this.validateInput("companyEmail", currentVal, {required: true}, "Company Email");
            }
        },
        'formData.companyWebsite': function (currentVal, oldVal) {
            if (this.formData.postingAs === 'company'){
                this.validateInput("companyWebsite", currentVal, {isUrl: true}, "Company Website");
            }
        }
    },
    components:{
        AnaVueSelect,
    },
    mixins:[ReusableFunctions],
    methods:{
        updatePostingAs(param){
            this.postingAs =param;
            this.formData.postingAs =param;
        },
        getWorks(searchText=undefined){
            if (searchText) {
                const works = this.temporalWorks;
                this.works = works.filter((option) => option.label.toLowerCase().includes(searchText.toLowerCase()));
            } else {
                this.works = this.temporalWorks;
            }
        },
        /**
         * function gets categories,job types, currencies, accounts
         */
        getJobDependencies() {
            this.startLoader();
            const data = new FormData();
            data.append("page", 1);
            data.append("perPage", -1);
            data.append('frontEnd',true);
            data.append("action", "get_categories_job_types_and_currencies");
            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: {"Content-Type": "multipart/form-data"},
            })
                .then(function (response) {
                    if (response) {
                        that.jobCategories = response.data.job_categories.data.map((category)=>{
                            return{
                                label:category.name,
                                value:category.id
                            }
                        });


                        that.jobTypes = response.data.job_types.data.map((jobType)=>{
                            return{
                                label:jobType.name,
                                value:jobType.id
                            }
                        });
                        that.currencies = response.data.currencies.data.map((currency)=>{
                            return{
                                label:currency.code,
                                value:currency.id
                            }
                        });
                        
                        that.loading = false;
                    }
                })
                .catch(function (error) {
                    console.log(error);
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error("An error occurred");
                    }
                    that.loading = false;
                });
        },
        getCategories(searchText=undefined) {

            this.startLoader();


                if (searchText !== undefined) {
                    const data = new FormData();
                    data.append("page", this.page);
                    data.append("perPage", this.perPage);
                    data.append("sortBy", this.sortField);
                    data.append("order", this.sort);
                    data.append("searchText", searchText);
                    data.append("searchFields",  "name");
                    data.append("action", "get_categories");

                    const that = this;
                    axios({
                        method: "post",
                        url: ajaxurl,
                        data: data,
                        headers: {"Content-Type": "multipart/form-data"},
                    })
                        .then(function (response) {
                            if(response.data.data.length){
                                that.jobCategories = response.data.data.map((category)=>{
                                    return{
                                        label:category.name,
                                        value:category
                                    }
                                });

                            }else{

                                that.jobCategories = response.data.data
                            }
                            that.loading = false;

                        })
                        .catch(function (error) {
                            console.log(error);
                            if (error.response) {
                                toastr.error(error.response.data.data);
                            } else {
                                toastr.error("An error occurred");
                            }
                            that.loading = false;
                        });
                }else{
                    this.categories = categories;
                }

        },
        getAllJobTypes(searchText= undefined) {

            this.startLoader();

            if(searchText !== undefined) {
                const data = new FormData();
                data.append("action", "get_job_types");
                data.append("page", this.page);
                data.append("perPage", this.perPage);
                data.append("sortBy", this.sortField);
                data.append("order", this.sort);
                data.append("searchText", searchText);
                data.append("searchFields", "name");

                const that = this;
                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},

                })
                    .then(function (response) {

                        if(response.data.data.length){
                            that.jobTypes =   response.data.data.map((jobType)=>{
                                return{
                                    label:jobType.name,
                                    value:jobType
                                }
                            });
                        }else {
                            that.jobTypes = response.data.data;
                        }
                        that.loading = false;
                    })
                    .catch(function (error) {
                        console.log(error)
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error("An error occurred");
                        }
                        that.loading = false;
                        that.error = true;
                    });
            }
        },
        submitForm() {
            this.submitting = true
            //validate inputs
            this.validateInput("title", this.formData.title, {required: true}, "Job title");
            this.validateInput("type", this.formData.type, {required: true}, "Job type");
            this.validateInput("category", this.formData.category, {required: true}, "Job category");
            this.validateInput("location", this.formData.location, {required: true}, "location");
            this.validateInput("description", this.formData.description, {required: true}, "Description");
            this.validateInput("work", this.formData.work, {required: true}, "Work");

            //
            if (this.formData.postingAs === 'company') {
                this.validateInput("companyName", this.formData.companyName, {required: true}, "Company Name");
                this.validateInput("companyEmail", this.formData.companyEmail, {required: true}, "Company Email");
            }

            if (Object.keys(this.formErrors).length === 0) {

                const data = new FormData();
                const that = this;
                data.append("name", this.formData.title);
                data.append("job_type_id", this.formData.type);
                data.append("category_id", this.formData.category);
                data.append("location", this.formData.location);
                data.append("description", this.formData.description);
                data.append("companyName", this.formData.companyName);
                data.append("companyEmail", this.formData.companyEmail);
                data.append("companyWebsite", this.formData.companyWebsite);
                data.append("postingAs", this.formData.postingAs);
                data.append("experience", this.formData.experience);
                data.append("salary", this.formData.salary);
                data.append("currency_id", this.formData.currency);
                data.append("work", this.formData.work);
                data.append("frontEnd", true);
                data.append('my_account_nonce', document.getElementById("huslajobs-my-account-nonce").value);
                data.append("action", "save_job");

                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {
                        if (response.data) {
                            toastr.success(response.data.message);
                            that.successMessage = response.data
                            window.location.href= response.data.redirect_url;

                        }
                        that.submitting = false;

                    })
                    .catch(function (error) {
                        console.log(error)
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error("An error occurred");
                        }
                        that.submitting = false;

                    });

            }else{
                this.submitting = false
            }
        },
    },
    mounted(){

        this.works = this.temporalWorks;
        this.getJobDependencies();
    },
    // language=HTML
    template:
        `
         <div class="user-profile">
            <section class="hs-section">
                      <div class="row mb-1">
                          <div class="col-md-8">
                              <h2 class="section-title mb-0">Post job</h2>
                              <router-link to="/" class="hs-primary-text-color-light">Go back</router-link>
                              <form class="husla-form mt-4" @submit.prevent="submitForm">
                                  <div class="mb-3">
                                      <h4 class="font-weight-800">Job Info</h4>
                                  </div>
                                  <div class="mb-4">
                                      <input type="text" class="form-control" id="name" placeholder="Job title" v-model="formData.title">
                                      <span class="text-danger" v-if="formErrors.title">{{formErrors.title}}</span>

                                  </div>
                                  <div class="mb-4">
                                      <div class="row">
                                          <div class="col-md-6">
                                              <AnaVueSelect @search-change="getAllJobTypes" :options="jobTypes" placeholder="Select job type" v-model="formData.type"></AnaVueSelect>
                                                  <span class="text-danger" v-if="formErrors.type">{{formErrors.type}}</span>
                                          </div>
                                          <div class="col-md-6">
                                              <AnaVueSelect @search-change="getCategories" :options="jobCategories" placeholder="Select job category" v-model="formData.category"></AnaVueSelect>
                                                  <span class="text-danger" v-if="formErrors.category">{{formErrors.category}}</span>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="mb-4">
                                      <input type="text" class="form-control" id="location"  placeholder="Job location" v-model="formData.location" />
                                      <span class="text-danger" v-if="formErrors.location">{{formErrors.location}}</span>
                                  </div>
                                  <div class="mb-4">
                                      <textarea id="description" class="form-control"  row="8"  placeholder="Job description" v-model="formData.description"></textarea>
                                      <span class="text-danger" v-if="formErrors.description">{{formErrors.description}}</span>
                                  </div>
        
                                  <div class="mb-4">
                                      <input type="number" class="form-control" id="experience" min="0"  placeholder="Employee experience in years" v-model="formData.experience"/>
                                  </div>
                                  <div class="mb-4">
                                      <div class="row">
                                          <div class="col-md-8">
                                              <input type="number" min="1" class="form-control" id="salary"  placeholder="Salary" v-model="formData.salary" />
                                              
                                          </div>
                                          <div class="col-md-4">
                                              <AnaVueSelect @search-change="getCurrencies" :options="currencies" placeholder="Select currency" v-model="formData.currency" ></AnaVueSelect>
                                          </div>
            
                                      </div>
                                  </div>
                                  <div class="mb-4">
                                      <AnaVueSelect @search-change="getWorks" :options="works" placeholder="Work" v-model="formData.work" ></AnaVueSelect>
                                      <span class="text-danger" v-if="formErrors.work">{{formErrors.work}}</span>
                                  </div>
                                  
                                  <div class="my-5"><hr></div>
                                <!--     Show this part when user is posting for the first time or is using a job seeker account                             -->
                                  <div class="mb-3"><h4 class="font-weight-800">User Info</h4></div>
    
                                  <div class="mb-4 row">
                                      <div class="col-md-4">
                                          <input type="radio" class="mr-2" id="individual-account" name="posting_as" @change="updatePostingAs('individual')" />
                                          <label for="individual-account" class="form-label" >Posting as individual</label>
                                      </div>
                                      <div class="col-md-4">
                                          <input type="radio" class="mr-2" id="motivation_required" name="posting_as"  @change="updatePostingAs('company')"  checked />
                                          <label for="motivation_required" class="form-label">Posting as company</label>
                                      </div>
                                      <div class="col-md-4"></div>
                                  </div>
                                  <div v-if=" formData.postingAs === 'company'">
                                      <div class="mb-4">
                                          <input type="text" class="form-control" id="experience"  placeholder="Company name" v-model="formData.companyName"/>
                                          <span class="text-danger" v-if="formErrors.companyName">{{formErrors.companyName}}</span>
                                      </div>
                                      <div class="mb-3">
                                          <input type="text" class="form-control" id="experience" placeholder="Company email" v-model="formData.companyEmail"/>
                                          <span class="text-danger" v-if="formErrors.companyEmail">{{formErrors.companyEmail}}</span>
                                      </div>
                                      <div class="mb-5">
                                          <input type="text" class="form-control" id="experience"  placeholder="Company website" v-model="formData.companyWebsite"/>
                                      </div>
                                  </div>
                                  <div class="mt-5 d-flex justify-content-between">
                                      
                                      <router-link to="/" class="hs-btn hs-btn-signup hs-btn-gray-outline" >Cancel</router-link>
                                      <button type="submit" class="hs-btn hs-btn-signup hs-btn-primary" >Post job</button>
                                  </div>
    
                              </form>
                          </div>
                          <div class="col-md-4"></div>
                      </div>
          
            </section>
                       
         </div>

        `
}

export  {AddJob}