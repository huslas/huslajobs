import {JobSeekerSearchResults} from "./job-seeker-search-results.js";
import {JobSeekerSearchForm} from "./job-seeker-search-form.js";

const JobSeekerSearch ={
    emits:['show-results','update-loading'],
    props: {
        currentPage: {
            required: false,
            default:'jobseekers'
        }
    },
    data(){
        return{
            searchResults:undefined,
            total:0,
            job_category:'',
            page:1,
            pages:0,
            clear_search:false
        }
    },
    watch: {
        searchResults:function(currentVal, oldVal){
            let show = currentVal? true : false
            this.$emit('show-results', show)
        },
    },
    components:{
        JobSeekerSearchForm,
        JobSeekerSearchResults,
    },
    methods:{
        showResults(result){
            this.searchResults = result.data;
            this.total = result.totalItems;
            this.pages = result.totalPages;
        },
        closeSearchResults(value){
            this.searchResults = undefined;
            this.total = 0;
            /**
             * update clear search to clear search form fields
             * This work around is to prevent anEmit to update clear search
             * after form fields have been cleared for the first time
             * @type {boolean|*}
             */
            this.clear_search = this.clear_search === value ? !this.clear_search : value;
        },
        updateJobCategory(value) {
            this.job_category = value
        },
        updatePageNumber(number){
            this.page = number;
        },
        updateLoading(value){
            this.$emit('update-loading',value)
        },
        resetSearchResults(value){
            this.searchResults =undefined;
        }
    },
    template:
        `
          <div class="search-results">
            <job-seeker-search-form @search-result="showResults" @get-category="updateJobCategory" :current-page="currentPage" :page-number="page" :clear-search="clear_search" @is-searching="resetSearchResults"></job-seeker-search-form>
            <job-seeker-search-results @clear-search="closeSearchResults"  @on-update-page="updatePageNumber" v-if="searchResults" :options="searchResults" :totalItems="total" :job_category="job_category" :total-pages="pages"></job-seeker-search-results>
          </div>

        `
}

export  {JobSeekerSearch}