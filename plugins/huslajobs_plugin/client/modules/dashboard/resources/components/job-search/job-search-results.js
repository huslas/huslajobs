import {PaginationComponent} from "../pagination/pagination.js";
import {JobCard} from "../../../../jobs/resources/components/job-card.js"
import {Pagination} from "../../../../../../admin/modules/dashboard/resources/components/Pagination.js";

const JobSearchResults = {
    emits:['clear-search','on-update-page'],
    props: {
        options: {
            required: true
        },
        totalItems: {
            required: true
        },
        totalPages: {
            required: true
        },
        jobLimit:{
            required: true
        },
        user:{
            required: true
        },
        availableJobs:{
            required:true
        }
    },
    data() {
        return {
            page:1,
            searchResultsStrings: VueUiStrings.clientFrontEnd.dashboard.jobSearch,
            homeUrl: home_url

        }
    },
    components: {
        Pagination,
        JobCard,
    },
    methods:{
        clearSearch(){
            this.$emit('clear-search',true);
        },
        goToPage(page) {
            this.page = page;
            this.$emit('on-update-page',this.page);
        },
    },
    template:
        `
                        <div class="search-results mt-5">
                       
                        <!--         search results                   -->
                            <div class="d-flex justify-content-end mb-3">
                                <span class="cancel-search" @click="clearSearch" title="close search results"><i class="fas fa-times"></i></span>
                            </div>
                            <div v-if="totalItems > 0">
                                <div class="row mb-1">
                                    <div class="col-md-12">
                                    <div class="d-flex justify-content-between">
                                    <p v-if="jobLimit &&  parseInt(jobLimit) !== -1 && (parseInt(totalItems) > parseInt(jobLimit) )" class="hs-title">{{searchResultsStrings.displaying}} {{jobLimit}} {{searchResultsStrings.of}} {{totalItems}}  {{searchResultsStrings.jobsFound}}. <a :href="homeUrl+ '/my-account/#/upgrade-account'" class="hs-secondary-text-color">
                                     <em v-if="user">{{searchResultsStrings.upgrade}}</em> 
                                     <em v-else>{{searchResultsStrings.signup}}</em> 
                                     </a> 
                                     </p>
                                    <p v-else-if="parseInt(totalItems)>1" class="hs-title">{{searchResultsStrings.displaying}} {{totalItems}} {{searchResultsStrings.of}} {{totalItems}}  {{searchResultsStrings.jobsFound}}.</p>
                                    <p v-else class="hs-title">{{searchResultsStrings.displaying}} {{totalItems}} {{searchResultsStrings.of}} {{totalItems}}  {{searchResultsStrings.jobFound}}.</p>
                                    <small v-if="parseInt(availableJobs) > 1">{{availableJobs}} {{searchResultsStrings.jobsAvailable}}</small>
                                    <small v-else>{{availableJobs}} {{searchResultsStrings.jobAvailable}}</small>
                          </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <job-card v-for="(job,index) in options" :key="index" :job-data="[job]"></job-card>
                                    </div>
                                </div>
                                <pagination v-if="totalPages>1" :page="page" :pages="totalPages"
                                            @update="goToPage">
                                </pagination>
                            </div>
                            <div v-else class="text-center">
                                <p class="hs-subtitle">{{searchResultsStrings.noJobs}}</p>
                            </div>
                       
                        </div>

        `
}

export {JobSearchResults}