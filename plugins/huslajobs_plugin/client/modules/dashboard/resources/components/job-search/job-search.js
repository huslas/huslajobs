import {JobSearchForm} from "./job-search-form.js";
import {JobSearchResults} from "./job-search-results.js";

const JobSearch ={
    emits:['show-results','update-loading'],
    props: {
        currentPage: {
            required: false,
            default:'jobs'
        },
        availableJobs: {
            required: false,
            default:0
        }
    },
    data(){
        return{
            searchResults:undefined,
            total:0,
            page:1,
            pages:1,
            job_limit:husla_jobs_limit,
            clear_search:false,
            user:undefined,
            perPage: jobs_per_page,
            isAdmin:parseInt(is_Admin)

        }
    },
    components:{
        JobSearchResults,
        JobSearchForm,
    },

    watch: {
        searchResults:function(currentVal, oldVal){
            let show = currentVal? true : false
            this.$emit('show-results', show)
        },
    },
    methods:{
        showResults(result){
            this.searchResults = result.data;
            this.total = result.totalItems;

            /**
             * Calculate pages if job limit is set
             */
            if (this.job_limit && parseInt(this.job_limit) !== -1 && (parseInt(this.total) > parseInt(this.job_limit))){
                this.pages = Math.ceil(parseInt(this.job_limit) / parseInt(result.perPage))
            }else{
                this.pages = result.totalPages;
            }
        },
        closeSearchResults(value){
            this.searchResults =undefined;
            this.total = 0;
            /**
             * update clear search to clear search form fields
             * This work around is to prevent anEmit to update clear search
             * after form fields have been cleared for the first time
             * @type {boolean|*}
             */
            this.clear_search = this.clear_search === value ? !this.clear_search : value;
        },
        resetSearchResults(val){
            this.searchResults = false;
        },
        updatePageNumber(number){
            this.page = number;
        },
        updateLoading(value){
            this.$emit('update-loading',value)

        }
    },
    mounted(){
        this.searchResults = undefined;
        this.user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : undefined;
        if (this.user && this.user.subscriptions){
            for (const subscription of this.user.subscriptions) {
                const start_date = Date.parse(new Date(subscription.start_date));
                this.job_limit = subscription.package_jobs;
            }
        }
        //Show all jobs for admins
        if (this.isAdmin){
            this.job_limit = -1;
        }

    },
    template:
        `
          <div class="search-results">
            <job-search-form @search-result="showResults" @is-searching="resetSearchResults" :current-page="currentPage" :page-number="page" :job-limit="job_limit" :clear-search="clear_search"  @is-loading="updateLoading" :pages="pages" ></job-search-form>
            <job-search-results @clear-search="closeSearchResults"  @on-update-page="updatePageNumber" v-if="currentPage==='jobs' && searchResults" :job-limit="job_limit" :options="searchResults" :totalItems="total" :total-pages="pages" :user="user" :availableJobs="availableJobs"></job-search-results>
          </div>
        `
}

export  {JobSearch}