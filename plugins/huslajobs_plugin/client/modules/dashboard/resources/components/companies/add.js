import {AnaVueSelect} from "../../../../../../inc/global_components/ana_select/ana_vue_select.js";
import {LoadingBar} from "../../../../../../admin/modules/dashboard/resources/components/LoadingBar.js";
import {LoadingError} from "../../../../../../admin/modules/dashboard/resources/components/LoadingError.js";
import {ReusableFunctions} from "../../../../../../js/functions.js";
import {VLoader} from "../../../../../../inc/global_components/loader/v-loader.js";
import {AnaVuePhoneNumber} from "../../../../../../inc/global_components/phone-number/ana_vue_phone_number.js";
import {AnaVueFileUpload} from "../../../../../../inc/global_components/ana_file_upload/ana_vue_fileupload.js"

const AddCompany = {
    data() {
        return {
            id: 0,
            categories: [],
            router: {},
            formData: {
                name: undefined,
                email: undefined,
                website: undefined,
                phone_number:undefined,
                address_1:undefined,
                address_2:undefined
            },
            loading: false,
            submitting:false,
            error:false,
            maxUpload:maximum_upload,
            formErrors: {},
            imgUrl:THEME_URL,
            companyCreateStrings:VueUiStrings.clientFrontEnd.dashboard.companies.create
        }
    },
    watch: {
        'formData.name': function (currentVal, oldVal) {
            this.validateInput("name", currentVal, {required: true}, this.companyCreateStrings.formFields.name.label);
        },
        'formData.email': function (currentVal, oldVal) {
            this.validateInput("email", currentVal, {required: true,isEmail:true}, this.companyCreateStrings.formFields.email.label);
        },
        'formData.address': function (currentVal, oldVal) {
            this.validateInput("address", currentVal, {required: true}, this.companyCreateStrings.formFields.address.label+' 1');
        }

    },
    components: {
        AnaVueSelect,
        LoadingBar,
        LoadingError,
        AnaVuePhoneNumber,
        VLoader,
    },
    methods: {
        submitForm() {
            this.submitting = true
            //validate inputs
            this.validateInput("name", this.formData.name, {required: true},this.companyCreateStrings.formFields.name.label);
            this.validateInput("email", this.formData.email, {required: true,isEmail:true}, this.companyCreateStrings.formFields.email.label);
            this.validateInput("address", this.formData.address_1, {required: true}, this.companyCreateStrings.formFields.address.label+' 1');


            if (Object.keys(this.formErrors).length === 0) {
                const data = new FormData();
                const that = this;
                data.append("name", this.formData.name);
                data.append("email", this.formData.email);
                data.append("website", this.formData.website ?? '');
                data.append("phone", this.formData.phone_number ?? '');
                data.append("address_1", this.formData.address_1 ?? '');
                data.append("address_2", this.formData.address_2 ?? '');
                data.append('my_account_nonce', document.getElementById("huslajobs-my-account-nonce").value);
                data.append("company_id", this.id || 0);
                data.append("action", "save_company");

                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {
                        if (response.data) {
                            toastr.success(response.data);
                            that.successMessage = response.data
                            localStorage.removeItem('user') // force getting new data from database
                            localStorage.setItem('defaultHeader',JSON.stringify('companies'));
                            that.router.push('/')
                        }
                        that.submitting = false;
                    })
                    .catch(function (error) {
                        console.log(error)
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.companyCreateStrings.errors.errorOccurred);
                        }
                        that.submitting = false;
                    });
            } else {
                this.submitting = false
                const firstErrorControl = document.querySelector('.validation-error')?.parentElement;
                // Scroll to first error element
                window.scrollTo(
                    firstErrorControl?.offsetLeft || 0,
                    (firstErrorControl?.offsetTop || 0) - 50 // Subtract 50 for better exposure
                )
            }
        },
        phoneNumberError(err) {
            if (!err && this.formErrors) {
                this.formErrors["phone"] = this.companyCreateStrings.formFields.phone.error;
            } else if (err && (this.formErrors && this.formErrors["phone"])) {
                delete this.formErrors["phone"]
            }
        },
        getCompany() {
            this.startLoader();
            const data = new FormData();
            data.append("company_id", this.id);
            data.append("action", "get_company");
            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: {"Content-Type": "multipart/form-data"},
            })
                .then(function (response) {
                    if (response) {
                        that.formData.name = response.data.name;
                        that.formData.email = response.data.email;
                        that.formData.website = response.data.website;
                        that.formData.phone_number= response.data.phone_number;
                            that.formData.address_1= response.data.address_1;
                            that.formData.address_2= response.data.address_2;
                        that.loading = false;
                        that.setUpPhoneNumber();
                        that.closeLoader();
                    }

                })
                .catch(function (error) {
                    console.log(error);
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error(that.companyCreateStrings.errors.errorOccurred);
                    }
                    that.loading = false;
                    that.error = true;
                    that.closeLoader();
                });
        },
    },
    mixins:[ReusableFunctions],
    mounted() {
        this.id = this.$route.params.id || 0;
        this.router = VueRouter.useRouter();
        if (this.id){
            this.getCompany();
        }else {
            this.setUpPhoneNumber();
            this.closeLoader();
        }

    },
    // language=HTML
    template:
        `<VLoader :active="loading" :translucent="true">
    <div v-if="error">
        <loading-error @update="getCompany"></loading-error>
    </div>
        <section v-else class="my-account-profile hs-section" >
            <div class="row mb-3 text-center">
                <div class="col-md-12">
                    <h1 v-if="id" class="page-title p-0 mb-0">
                        {{companyCreateStrings.headings.edit}}
                    </h1>
                    <h1 v-else class="page-title pt-0 mb-2">
                        {{companyCreateStrings.headings.create}}
                    </h1>
                    <router-link to="/" class="hs-primary-text-color-light">{{companyCreateStrings.goBack}}</router-link>
                </div>
            </div>
            <div class="row mb-1">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <form class="husla-form mt-4" @submit.prevent="submitForm">
                        <!--     Show this part when user is posting for the first time or is using a job seeker account                             -->
                        <div class="mb-3">
                            <label for="name" class="form-label">{{companyCreateStrings.formFields.name.label}}<span class="text-danger">*</span></label>
                            <input id="name" type="text" class="form-control" :placeholder="companyCreateStrings.formFields.name.label"
                                   v-model="formData.name"/>
                            <span class="text-danger validation-error"
                                  v-if="formErrors.name">{{formErrors.name}}</span>
                        </div>
                        <div class="row">
                            <div class="mb-3 col-md-6">

                                <label for="email" class="form-label">{{companyCreateStrings.formFields.email.label}}
                                    <span class="text-danger">*</span>
                                </label>

                                <input id="email" type="email" class="form-control" :placeholder="companyCreateStrings.formFields.email.placeholder"
                                       v-model="formData.email"/>
                                <span class="text-danger validation-error"
                                      v-if="formErrors.email">{{formErrors.email}}
                            </span>
                                <span class="text-danger validation-error" v-if="formErrors.applicantEmail">{{formErrors.applicantEmail.toString()}}</span>
                            </div>
                            <div class="mb-3 col-md-6">
                                <label class="form-label">{{companyCreateStrings.formFields.phone.label}}</label>
                                <ana-vue-phone-number v-model="formData.phone_number"
                                                      @phoneError="phoneNumberError"></ana-vue-phone-number>
                                <span class="text-danger validation-error" v-if="formErrors.phone">{{formErrors.phone}}</span>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="address-1" class="form-label">{{companyCreateStrings.formFields.address.label}}
                                <span>1</span><span class="text-danger">*</span>
                            </label>
                            <input type="text" class="form-control" :placeholder="companyCreateStrings.formFields.address.placeholder+' '+'1'"
                                   v-model="formData.address_1" id="address-1"/>
                            <span class="text-danger validation-error" v-if="formErrors.address">{{formErrors.address}}</span>

                        </div>
                        <div class="mb-3">
                            <label for="address-1" class="form-label">{{companyCreateStrings.formFields.address.label}}
                                <span>2</span>
                            </label>
                            <input type="text" class="form-control" :placeholder="companyCreateStrings.formFields.address.placeholder+ ' '+'2'"
                                   v-model="formData.address_2"/>
                        </div>                        
                        <div class="mb-3">
                            <label for="website" class="form-label">{{companyCreateStrings.formFields.website.label}}
                            </label>
                            <input type="text" class="form-control" :placeholder="companyCreateStrings.formFields.website.placeholder"
                                   v-model="formData.website"/>
                        </div>
                        <div class="mt-5 d-flex justify-content-between flex-wrap">
                            <router-link to="/" class="hs-btn hs-btn-signup hs-btn-gray-outline mb-3">
                                {{companyCreateStrings.formFields.buttons.cancel}}
                            </router-link>
                            <button type="submit" class="hs-btn hs-btn-signup hs-btn-primary mb-3" :class="{'not-allowed':submitting || Object.keys(formErrors).length > 0 }"
                                    :disabled="submitting || Object.keys(formErrors).length > 0 ">
                                <span v-if="id"> {{companyCreateStrings.formFields.buttons.editCompany}}</span>
                                <span v-else> {{companyCreateStrings.formFields.buttons.addCompany}}</span>
                                <i
                                        v-if="submitting" class="fas fa-spinner fa-pulse ml-2"></i>
                            </button>
                        </div>
                    </form>
                </div>
                <div class="col-md-2"></div>
            </div>

        </section>



</VLoader>
            `
}

export {AddCompany}