
import {Pagination} from "../../../../../../admin/modules/dashboard/resources/components/Pagination.js";
import {LoadingError} from "../../../../../../admin/modules/dashboard/resources/components/LoadingError.js";
import {LoadingBar} from "../../../../../../admin/modules/dashboard/resources/components/LoadingBar.js";
import {ReusableFunctions} from "../../../../../../js/functions.js";
const AccCompanies = {
    props:{
        account_id: {
            required:false
        }
    },
    emits:['companies'],
    data() {
        return {
            companies: [],
            page: 1,
            perPage: 15,
            loading: true,
            pages: 0,
            total:0,
            user_id: this.account_id,
            sortField: 'created_at',
            sort :'desc',
            companyStrings : VueUiStrings.clientFrontEnd.dashboard.companies.index,
            router: {}
        };
    },
    components: {
        LoadingBar,
        LoadingError,
        Pagination,
    },
    mounted() {
        //todo get all
        this.getCompanies();
        this.router = VueRouter.useRouter();
    },
    mixins:[ReusableFunctions],

    watch: {
        searchText: function (current) {
            if (current === ''){
                this.search();
            }
        },
    },


    methods: {
        sortBy(field) {
            if (this.sortField == field) {
                this.sort = this.sort == 'desc' ? 'asc' : 'desc'
            } else {
                this.sort = 'asc';
                this.sortField = field;
            }
            this.page = 1;
            this.getJobApplications();
        },
        isSortedBy(field, sort = 'asc') {
            switch (sort) {
                case 'asc':
                    return this.sortField == field && sort == this.sort;
                case 'desc':
                    return this.sortField == field && sort == this.sort;
                default:
                    return true;
            }
        },
        getCompanies() {
            // this.startLoader();
            let user = localStorage.getItem('user');

            const data = new FormData();
            data.append("action", "get_companies");
            data.append("page", this.page);
            data.append("perPage", this.perPage);
            data.append("user_id",this.user_id)
            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: { "Content-Type": "multipart/form-data" },
            })
                .then(function (response) {
                    that.companies = response.data.data;
                    that.pages = response.data.totalPages;

                    that.$emit('companies',response.data.totalItems);
                    that.loading = false;
                    that.error = false;
                })
                .catch(function (error) {
                    console.log(error);
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error("An error occurred");
                    }
                    that.loading = false;
                    that.error = true;
                });
        },
        deleteCompany(company) {
            const confirm_delete = confirm(this.companyStrings.confirm + " " + company.name + "?");
            if (confirm_delete) {
                this.startLoader();
                const data = new FormData();
                data.append("action", "delete_company");
                data.append("company_id", company.id);
                const that = this;
                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {
                        toastr.success(company.name + " deleted");
                        localStorage.removeItem('user') // force getting new data from database
                        localStorage.setItem('defaultHeader',JSON.stringify('companies'));
                        window.location.reload();
                    })
                    .catch(function (error) {
                        console.log(error);
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.companyStrings.errors.errorOccurred);
                        }
                        that.closeLoader();
                    });
            }
        },
        goToPage(page) {
            this.page = page
            this.getJobApplications();
        }
    },
    template: `<div class="module-content-wrapper">
    <loading-bar v-if="loading"></loading-bar>
    <div v-if="!loading">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div v-if="error">
                            <loading-error @update="getCompanies"></loading-error>
                        </div>
                        <div v-else>

                            <div v-if="companies.length > 0" class="hs-table-wrapper">
                                <div class="row">
                                    <div class="col-12">
                                            <div class="card-body">
                                                <router-link to="/add-company" class="hs-btn hs-btn-primary">{{companyStrings.addCompany}}</router-link>
                                            </div>
                                        
                                    </div>
                                </div>
                                <table class="table table-striped">
                                    <tr>
                                        <th>{{companyStrings.tableHeadings.name}}</th>
                                        <th>{{companyStrings.tableHeadings.email}}</th>
                                        <th>{{companyStrings.tableHeadings.website}}</th>
                                        <th>{{companyStrings.tableHeadings.actions}}</th>
                                    </tr>
                                    <tr v-for="company of companies">
                                        <td><span>{{company.name}}</span></td>
                                        <td>{{company.email}}</td>
                                        <td>{{company.website}}</td>
                                        <td>
                                            <i class="fa-solid fa-arrow-right-arrow-left"></i>
                                            <router-link :to="'/edit-company/'+company.id"
                                                         class="btn btn-info btn-sm mr-2 mb-2"><i
                                                    class="fa fa-pencil-alt fa-2x"></i></router-link>

                                            <button class="btn btn-danger btn-sm mb-2" @click="deleteCompany(company)">
                                                <i
                                                        class="fa fa-trash fa-2x"></i></button>


                                        </td>
                                    </tr>
                                </table>
                                <pagination v-if="pages > 1" :page="page" :pages="pages"
                                            @update="goToPage"></pagination>
                            </div>
                            <div v-else class="d-flex flex-column align-items-center justify-content-center">
                                <p>{{companyStrings.noCompany}}</p>
                              <router-link to="/add-company" class="hs-btn hs-btn-primary">{{companyStrings.addCompany}}</router-link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
`,
};
export { AccCompanies };
