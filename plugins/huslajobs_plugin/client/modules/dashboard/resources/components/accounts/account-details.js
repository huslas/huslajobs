import {LoadingError} from "../../../../../../admin/modules/dashboard/resources/components/LoadingError.js";
import {LoadingBar} from "../../../../../../admin/modules/dashboard/resources/components/LoadingBar.js";
import {AnaToolTip} from "../../../../../../inc/global_components/tool-tip/ana-tool-tip.js";
import {ReusableFunctions} from "../../../../../../js/functions.js";

const AccountDetails = {
    data() {
        return {
            user: undefined,
            accountDetailsStrings: VueUiStrings.clientFrontEnd.dashboard.accounts.accountDetails,
            membership_plan: undefined,
            tooltip: 'add profile',
            homeUrl: home_url,
            activation_status: user_activation_status,
            deleteNotice:delete_notice

        };
    },
    components: {
        LoadingBar,
        LoadingError,
        AnaToolTip
    },
    mixins:[ReusableFunctions],
    mounted() {
        //todo get all
        this.getUser();
    },
    methods: {
        getUser() {

            let user = localStorage.getItem('user')
            this.startLoader();

            if (user == null) {

                const data = new FormData();
                const that = this;
                data.append("action", "get_user_with_accounts");
                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {

                        if (response.data) {
                            that.user = response.data;
                            localStorage.setItem('user', JSON.stringify(that.user))
                            that.setUserData();

                        }
                        setTimeout(() => {
                            that.closeLoader();
                            return;
                        }, 200)
                    })
                    .catch(function (error) {
                        console.log(error)
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.accountDetailsStrings.errors.errorOccurred);
                        }
                        that.loading = false;
                        that.closeLoader();
                    });
            } else {
                this.user = JSON.parse(user);
                this.setUserData();
                setTimeout(() => {
                    this.loading = false
                    this.closeLoader();
                }, 400)
            }

        },

        deleteMyAccount(id) {
            const confirm_delete = confirm(this.accountDetailsStrings.confirm + "?");
            if (confirm_delete) {
                this.startLoader();
                const data = new FormData();
                const that = this;
                data.append("action", "delete_user_account");
                data.append("user_id", id);
                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {
                        if (response.data) {
                            toastr.success(response.data,'Success');
                            localStorage.removeItem('user');
                            localStorage.removeItem('active_account');
                            localStorage.setItem("userLogout", null);
                            localStorage.removeItem("userLogout");
                            localStorage.clear();
                            window.location.reload();

                        }
                        setTimeout(() => {
                            that.closeLoader();
                            return;
                        }, 200)
                    })
                    .catch(function (error) {
                        console.log(error)
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.accountDetailsStrings.errors.errorOccurred);
                        }
                        that.loading = false;
                        that.closeLoader();
                    });
            }
        },
        setUserData() {
            //set headers
            if (this.user.subscriptions.length) {
                for (const subscription of this.user.subscriptions) {
                    // const start_date = Date.parse(new Date(subscription.start_date));
                    if (parseInt(subscription.status) === 1) {
                        this.membership_plan = subscription
                    }
                }
            }
        },
    },
    template: `
    <div v-if="user" class="module-content-wrapper">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-10">
                <div class="row">
                    <div class="col-md-3 profile-image-wrapper">
                    <div class="d-flex flex-column align-items-center">
                        <img v-if="user?.profile_image" :src="user?.profile_image" alt="" class="profile-image">
                        <div v-else class="round hs-bg-primary d-flex profile-image justify-content-center align-items-center">
                            <h2 class="m-0 hs-gray-text-color font-weight-800" style="font-size: 3.5rem">
                                {{decodeHtml(user?.first_name[0] ?? user?.display_name[0] )}}{{decodeHtml(user?.last_name[0]
                                ?? user?.display_name[0])}}</h2>
                        </div>
                        <router-link to="/" class="hs-primary-text-color-light mt-4">{{accountDetailsStrings.goBack}}</router-link>
</div>
                    </div>
                    <div class="col-md-9 profile-description-wrapper">
                        <h3 class="mb-0 font-weight-800">{{decodeHtml(user?.first_name)}}
                            {{decodeHtml(user?.last_name)}}</h3>
                        <p class="p-0 mt-0 mb-2">
                            <span class="mr-2">{{user?.user_email}}</span>
                        </p>
                        <p v-if="membership_plan" class="hs-secondary-text-color">
                                    <span>{{membership_plan.package_name}} account  <router-link
                                            v-if="membership_plan.package_name.toLowerCase() !== 'premium' "
                                            to="/upgrade-account" class="hs-primary-text-color-light">{{accountDetailsStrings.links.upgradeAccount}}</router-link>  </span>
                        </p>
                        <div class="d-flex flex-wrap">
                            <router-link :to="'/edit-user-profile/'+ user?.ID"
                                         class="hs-btn hs-btn-gray-outline hs-btn-sm d-inline-block mr-3 mb-3 position-relative"
                                         @mouseover="tooltip='edit profile'"
                                         @mouseleave="tooltip=undefined"
                            >
                                {{accountDetailsStrings.links.editProfile}}
                                <ana-tool-tip v-if="showToolTip(tooltip,'edit profile')"
                                              :message="accountDetailsStrings.links.editProfile"></ana-tool-tip>
                            </router-link>
                            <a  :href="homeUrl+'/new-password'"
                               class="hs-btn hs-btn-gray-outline hs-btn-sm d-inline-block mr-3 mb-3 position-relative"
                               @mouseover="tooltip='change password'"
                               @mouseleave="tooltip=undefined"
                            >{{accountDetailsStrings.links.changePassword}}
                                <ana-tool-tip v-if="showToolTip(tooltip,'change password')"
                                              :message="accountDetailsStrings.links.changePassword"></ana-tool-tip>
                            </a>
                  
                            <a id="delete-account" href="#"
                               class="text-danger mb-3 align-self-end position-relative d-inline-block"
                               @mouseover="tooltip='delete account'"
                               @mouseleave="tooltip=undefined"
                               @click="deleteMyAccount(user?.ID)"
                            >
                                {{accountDetailsStrings.links.deleteAccount.label}}
                                <ana-tool-tip v-if="showToolTip(tooltip,'delete account')"
                                              :message="deleteNotice"></ana-tool-tip>
                            </a>
                        </div>
                        <hr>
                                    <div class="mb-2">
                                        <h5 class="hs-subtitle">{{accountDetailsStrings.country}}</h5>
                                        <p>{{selectedCountry(user?.country)}}</p>
                                    </div>

                                    <div class="mb-2">
                                        <h5 class="hs-subtitle">{{accountDetailsStrings.state}}</h5>
                                        <p>{{user?.state}}</p>
                                    </div>

                                    <div class="mb-2">
                                        <h5 class="hs-subtitle">{{accountDetailsStrings.city}}</h5>
                                        <p>{{user?.city}}</p>
                                    </div>
                                    <div class="mb-2">
                                        <h5 class="hs-subtitle">{{accountDetailsStrings.phone}}</h5>
                                        <p v-if="user?.phone_number">{{user?.phone_number}}</p>
                                        <p v-else>N/A</p>
                                    </div>
                                    <div class="mb-2">
                                        <h5 class="hs-subtitle">{{accountDetailsStrings.bio}}</h5>
                                        <p>{{user?.bio}}</p>
                                    </div>
                     
                    </div>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
`,
};
export {AccountDetails};
