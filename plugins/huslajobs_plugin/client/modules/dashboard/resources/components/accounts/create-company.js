import {ReusableFunctions} from "../../../../../../js/functions.js";
import {AnaVueSelect} from "../../../../../../inc/global_components/ana_select/ana_vue_select.js";
import {VLoader} from "../../../../../../inc/global_components/loader/v-loader.js";
import {world_countries as WorldCountries} from "../../../../../../js/countries_data.js";
import {CountriesData} from "../../../../../../js/countries_and_codes.js";
import {AnaVuePhoneNumber} from "../../../../../../inc/global_components/phone-number/ana_vue_phone_number.js";

const CreateCompany = {
    refs: ['jobsModal'],
    data() {
        return {
            form: {
                name: undefined,
                type: undefined,
                email: undefined,
                website: undefined,
                country: undefined,
                profile_image: undefined,
                state: undefined,
                city: undefined,
                phone_number: undefined,
                fax: undefined,
                bio: undefined,
                activity: undefined,
                fileUpload:undefined,

                // hasDefaultAccount:false,
            },
            registered: false,

            imgUrl: THEME_URL ?? '',
            userId: user_id ?? 0,
            progressBarStyle: "width: 0%; margin-left: 5px",
            formErrors: {},
            submitting: false,
            currentStep: {label:VueUiStrings.clientFrontEnd.dashboard.accounts.createAccount.steps.company,value:'company'},
            selectedPackage: undefined,
            packages: [],
            loading: false,
            jobCategories: [],
            jobTypes: [],
            currencies: [],
            categories: [],
            companyAccounts: undefined,
            maxUpload:maximum_upload,
            countries: WorldCountries,
            states: [],
            error: false,
            createCompanyStrings: VueUiStrings.clientFrontEnd.dashboard.accounts.createAccount,
            progressSteps: [
                {label:VueUiStrings.clientFrontEnd.dashboard.accounts.createAccount.steps.company,value:'company'},
                {label:VueUiStrings.clientFrontEnd.dashboard.accounts.createAccount.steps.activities,value:'activities'}
            ],
            accountTypes:[
                {label:VueUiStrings.clientFrontEnd.dashboard.accounts.createAccount.activities.applyForJobs,value:'seeker'},
                {label:VueUiStrings.clientFrontEnd.dashboard.accounts.createAccount.activities.postJobs,value:'recruiter'},
                {label:VueUiStrings.clientFrontEnd.dashboard.accounts.createAccount.activities.both,value:'both'}
            ],

        };
    },
    mixins: [ReusableFunctions],

    watch:{
        'form.name': function (currentVal, oldVal) {
            this.validateInput("name", currentVal, {required: true}, this.createCompanyStrings.formFields.name.label);
        },
        'form.email': function (currentVal, oldVal) {
            this.validateInput("email", currentVal, {required: true},this.createCompanyStrings.formFields.email.label);
        },
        'form.country':function (currentVal, oldVal) {
            this.validateInput("country", currentVal, {required: true}, this.createCompanyStrings.formFields.country.label);
            this.states = [];
            if(currentVal){
                const countriesData = CountriesData;
                let states = countriesData.filter((country)=>country.code.toLowerCase() === currentVal.toLowerCase())[0].states;
                if (states.length){
                    this.states = states.map((state)=>{
                        return{
                            label:state,
                            value:state
                        }
                    });
                }
            }else{
                // this.form.country =
                this.states = [];
                this.form.state = undefined
            }

        },
        'form.city':function (currentVal, oldVal) {
            this.validateInput("city", currentVal, {required: true}, this.createCompanyStrings.formFields.city.label);
        },
        'form.state': function (currentVal, oldVal) {
            this.validateInput("state", currentVal, {required: true}, this.createCompanyStrings.formFields.state.label);
        },
        'form.bio': function (currentVal, oldVal) {
            this.validateInput("bio", currentVal, {required: true,min:150,max:500}, this.createCompanyStrings.formFields.bio.label);
        },

    },
    components: {
        AnaVueSelect,
        VLoader,
        AnaVuePhoneNumber

    },

    destroyed() {
        this.formData = {
            title: undefined,
            type: undefined,
            category: undefined,
            location: undefined,
            work: undefined,
            salary: undefined,
            description: undefined,
            currency: undefined,
            experience: undefined,
            companyName: undefined,
            companyEmail: undefined,
            companyWebsite: undefined,
            postingAs: "company",
            createCompanyStrings: VueUiStrings.clientFrontEnd.dashboard.accounts.createAccount
        }
    },
    methods: {
        phoneNumberError(err) {
            if (!err && this.formErrors){
                this.formErrors["phone"] = "Phone number is incorrect";
            }
            else if (err && (this.formErrors && this.formErrors["phone"] ) ){
                delete this.formErrors["phone"]
            }
        },
        goToNext() {
            this.formErrors = {};
            let companyData = {
                company: {},
                activities: {},
            }
            if (this.currentStep.value.toLowerCase() === 'company') {
                this.validateInput("name", this.form.name, {required: true}, this.createCompanyStrings.formFields.name.label);
                this.validateInput("email", this.form.email, {required: true}, this.createCompanyStrings.formFields.email.label);
                this.validateInput("bio", this.form.bio, {required: true}, this.createCompanyStrings.formFields.bio.label);
                this.validateInput("city", this.form.city, {required: true}, this.createCompanyStrings.formFields.city.label);
                this.validateInput("country", this.form.country, {required: true}, this.createCompanyStrings.formFields.country.label);
                this.validateInput("state", this.form.state, {required: true}, this.createCompanyStrings.formFields.state.label);

            }
            if (Object.keys(this.formErrors).length === 0) {
                this.nextStep();
            }else{
                const firstErrorControl = document.querySelector('.validation-error')?.parentElement;
                // Scroll to first error element
                window.scrollTo(
                    firstErrorControl?.offsetLeft || 0,
                    (firstErrorControl?.offsetTop || 0) - 50 // Subtract 50 for better exposure
                )
            }
        },
        nextStep() {
            const currentStepIndex = this.getObjectIndex( this.progressSteps,this.currentStep.value)  + 1;

            this.currentStep = this.progressSteps[currentStepIndex];
            const style = this.progressSteps.length > 3 ? "margin-left: 10px;" : "margin-left: 8px;";

            let width = ((currentStepIndex) / (this.progressSteps.length - 1) * 100);
            let gap = ((width / 5) + 5) + 'px';
            width = width + '%'
            width = `width: calc(${width} - ${gap}) !important`;
            this.progressBarStyle = style + width;
            window.scrollTo({
                top: 0,
                left: 0,
                behavior: 'smooth'
            });
        },
        goToPrevious() {
            const currentStepIndex = this.getObjectIndex( this.progressSteps,this.currentStep.value)  - 1;
            this.currentStep = this.progressSteps[currentStepIndex];
            const style = "margin-left: 10px;"
            let width = ((currentStepIndex) / (this.progressSteps.length - 1) * 100);
            let gap = '5px';
            if (width > 0) {
                gap = ((width / 5) + 5) + 'px';
                width = width + '%'
                width = `width: calc(${width} - ${gap}) !important`;
            } else {
                width = "width: 0% !important";
            }

            this.progressBarStyle = style + width;
            window.scrollTo({
                top: 0,
                left: 0,
                behavior: 'smooth'
            });
        },
        updatePostingAs() {

            if (this.formData.postingAs === 'company') {
                this.formData.account_id = undefined;
            } else {
                if (this.individualAccount) {
                    this.formData.account_id = this.individualAccount.id;
                }else if(localStorage.getItem('signupAccountId')){
                    this.formData.account_id = JSON.parse(localStorage.getItem('signupAccountId')) ;
                }else{
                    //    do nothing
                }
            }
        },
        getWorks(searchText = undefined) {
            if (searchText) {
                const works = this.temporalWorks;
                this.works = works.filter((option) => option.label.toLowerCase().includes(searchText.toLowerCase()));
            } else {
                this.works = this.temporalWorks;
            }
        },
        updateAccountType(val){
            this.form.activity = val
        },
        submitForm() {

            this.submitting = true
            //validate inputs
            if (!this.form.activity){
                toastr.error(this.createCompanyStrings.errors.selectActivity);
                return;
            }
            if (Object.keys(this.formErrors).length === 0) {

                const data = new FormData();
                const that = this;
                data.append("name", this.form.name);
                data.append("email", this.form.email);
                data.append("website", this.form.website ?? '');
                data.append("country", this.form.country);
                data.append("profile_image", this.form.fileUpload?.file ? this.form.fileUpload?.file: this.form.fileUpload?.url ?? '');
                data.append("state", this.form.state);
                data.append("city", this.form.city);
                data.append("phone_number", this.form.phone_number ??'');
                data.append("fax", this.form.fax ?? '');
                data.append("bio", this.form.bio);
                data.append("activity_type", this.form.activity);
                data.append("action", 'save_company_account');
                // return
                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {
                        if (response.data) {
                            toastr.success(response.data);
                            localStorage.removeItem('active_account')
                            localStorage.removeItem('companyData');
                            localStorage.removeItem('user');
                            localStorage.setItem('active_account', JSON.stringify(response.data?.account))
                            that.$router.push('/');

                        }
                        that.submitting = false;
                    })
                    .catch(function (error) {
                        console.log(error)
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.createCompanyStrings.errors.errorOccurred);
                        }
                        that.submitting = false;
                    });
            } else {
                this.submitting = false
            }
        },


    },
    mounted() {
        this.setUpPhoneNumber();
        this.closeLoader();
    },
    template: `
    <div class="job-form">
        <div class="row mb-4 text-center">
            <div class="col-md-12">
                <h1 class="page-title pt-0 mb-2">
                    {{createCompanyStrings.header}}
                </h1>
                <router-link to="/" class="hs-primary-text-color-light">{{createCompanyStrings.goBack}}</router-link>
            </div>
        </div>
        <div class="row">
            <!--        progress bar                        -->
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="d-flex justify-content-between align-items-center sign-up-stepper-wrapper position-relative">
                    <div id="progress" :style="progressBarStyle"></div>
                    <div v-for="(step,index) in progressSteps" :key="index" :data-title="step"
                         class="sign-up-stepper d-flex justify-content-center align-items-center progress-step"
                         :class="{ 'current-step': step.value.toLowerCase()===currentStep.value.toLowerCase(), 'completed-step': progressSteps.indexOf(currentStep) > index }"
                    >
                        <span>{{step.label}}</span>
                    </div>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>

        <!--                                  steps             -->
        <div class="row mt-5">
            <div class="col-md-2"></div>
            <div class="col-md-8">

                <form class="husla-form" @submit.prevent="submitForm">

                    <!--step 1-->
                    <div v-if="currentStep.value.toLowerCase() === 'company'"
                         class="d-flex justify-content-between flex-column">
                        <div class="row">
                            <div class="mb-4 col-md-12">
                                <input type="text" class="form-control"
                                       :placeholder="createCompanyStrings.formFields.name.placeholder"
                                       autocomplete="off" id="name" name="name"
                                       v-model="form.name"
                                >
                                <span class="text-danger validation-error"
                                      v-if="formErrors.name">{{formErrors.name}}</span>
                            </div>
                            <div class="mb-4  col-md-12">
                                <input type="email" class="form-control" id="email"
                                       :placeholder="createCompanyStrings.formFields.email.placeholder" name="email"
                                       v-model="form.email"
                                >
                                <span class="text-danger d-block validation-error" v-if="formErrors.email">{{formErrors.email}}</span>
                            </div>
                            <div class="mb-4  col-md-12">
                                <input type="text" class="form-control" id="website"
                                       :placeholder="createCompanyStrings.formFields.website.placeholder" name="website"
                                       v-model="form.website">
                            </div>
                            <div class="col-md-6 mb-4">
                                <!--                                            {{countries}}-->
                                <AnaVueSelect
                                        v-model="form.country" :options="countries"
                                        :placeholder="createCompanyStrings.formFields.country.placeholder"></AnaVueSelect>
                                <span class="text-danger validation-error"
                                      v-if="formErrors.country">{{formErrors.country}}</span>

                            </div>
                            <div class="col-md-6 mb-4">
                                <AnaVueSelect v-model="form.state"
                                              :options="states"
                                              :placeholder="createCompanyStrings.formFields.state.placeholder"
                                              :fieldName="createCompanyStrings.formFields.state.label"
                                              :disabled="!form.country"></AnaVueSelect>
                                <small>{{createCompanyStrings.formFields.state.tip}}</small>
                                <span class="text-danger d-block validation-error" v-if="formErrors.state">{{formErrors.state}}</span>
                            </div>

                            <div class="col-md-12 mb-4">
                                <input type="text" name="city" id="city" v-model="form.city"
                                       :placeholder="createCompanyStrings.formFields.city.placeholder"
                                       class="form-control">

                                <span class="text-danger validation-error"
                                      v-if="formErrors.city">{{formErrors.city}}</span>
                            </div>
                            <div class="col-md-12 mb-4">
                                <!--                                <input type="tel" name="phone" v-model="form.phone_number"-->
                                <!--                                       id="phone-number" class="form-control">-->
                                <ana-vue-phone-number v-model="form.phone_number" :country-code="form.country"
                                                      @phoneError="phoneNumberError"></ana-vue-phone-number>
                                <span class="text-danger validation-error"
                                      v-if="formErrors.phone">{{formErrors.phone}}</span>
                            </div>
                            <div class="col-md-12 mb-4">
                                            <textarea name="description" id="description" v-model="form.bio" rows="5"
                                                      :placeholder="createCompanyStrings.formFields.bio.placeholder"
                                                      class="form-control">
                                            </textarea>
                                <span class="text-danger validation-error"
                                      v-if="formErrors.bio">{{formErrors.bio}}</span>
                            </div>
                            <div class="col-md-12 mb-4">
                                <input type="text" name="fax_number" v-model="form.fax"
                                       id="fax-number" class="form-control"
                                       :placeholder="createCompanyStrings.formFields.faxNumber.placeholder">
                            </div>
                            <div class="col-md-12 mb-5">
                                <div class="d-flex bg-light flex-wrap align-items-end py-5 px-3">

                                    <div class="profile-image-preview mr-5">
                                        <img v-if="form.fileUpload" ref="profile-image"
                                             :src="form.fileUpload.url"
                                             :alt="form.fileUpload.name">
                                        <div>
                                            <p class="m-0">
                                                {{createCompanyStrings.formFields.profileImage.tips.imageTypes}}:
                                                png,jpeg,jpg</p>
                                            <p class="m-0">
                                                {{createCompanyStrings.formFields.profileImage.tips.maxSize}}:{{maxUpload}}M</p>
                                        </div>

                                    </div>
                                    <div class="profile-image-btn">
                                        <label for="profile-image"
                                               class="hs-btn hs-btn-gray cursor-pointer">
                                            <input type="file" name="profile-image"
                                                   accept="image/png,image/jpeg,image/jpg"
                                                   id="profile-image" class="d-none"
                                                   @input="getFile">
                                            <span v-if="form.fileUpload">{{createCompanyStrings.formFields.profileImage.buttons.changeImage}}</span>
                                            <span v-else>{{createCompanyStrings.formFields.profileImage.buttons.uploadImage}}</span>
                                        </label>

                                    </div>
                                </div>
                                <span class="text-danger validation-error" v-if="formErrors.fileUpload">{{formErrors.fileUpload}}</span>
                            </div>
                        </div>
                    </div>
                    <div v-if="currentStep.value.toLowerCase() === 'activities'">
                        <div class="d-flex justify-content-between row">
                            <div v-for="(accountType,index) in accountTypes" :key="index" class="col-md-4">
                                <div class="rounded activity-card cursor-pointer text-center p-4"
                                     :class="{'active':form.activity === accountType.value}"
                                     @click="updateAccountType(accountType.value)">
                                    <p class="pt-4 font-weight-800">{{accountType.label}}</p>
                                    <span><i class="fas fa-check-circle"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="my-5">
                        <hr>
                    </div>

                    <div class="mt-5 d-flex"
                         :class="{'justify-content-end':!getObjectIndex(progressSteps,currentStep.value),'justify-content-between':getObjectIndex(progressSteps,currentStep.value) }">
                        <span v-if="getObjectIndex(progressSteps,currentStep.value)"
                              class="fas fa-chevron-left cursor-pointer stepper-btn hs-round"
                              @click="goToPrevious" :disabled="submitting"></span>

                        <div v-if="(getObjectIndex(progressSteps,currentStep.value) +1) === progressSteps.length">
                            <button type="submit" class="hs-btn hs-btn-primary" :disabled="submitting">
                                {{createCompanyStrings.formFields.button}}
                                <i
                                        v-if="submitting"
                                        class="fas fa-spinner fa-pulse ml-1"></i>
                            </button>

                        </div>

                        <span v-if="(getObjectIndex(progressSteps,currentStep.value)+1) !== progressSteps.length"
                              class="fas fa-chevron-right cursor-pointer stepper-btn hs-round"
                              :class="{'next':registered}" @click="goToNext"></span>
                    </div>

                </form>
            </div>
            <div class="col-md-2"></div>
        </div>

        <!-- MODAL WINDOW -->

    </div>

`,
};

export {CreateCompany};
