<?php

namespace huslajobs_client;

use huslajobs\HuslaModule;


class DashboardModule extends HuslaModule {
    public function __construct() {
        $this->parent_module = 'client';
        $this->module        = 'dashboard';
        $this->addActions();
        $this->addFilters();
    }

    public function addActions() {
        add_action( 'init', function () {
            add_rewrite_rule( 'my-account', 'index.php?my-account=$matches[0]', 'top' );
        } );

        add_action( 'template_include', function ( $template ) {
            if ( get_query_var( 'my-account' ) == false || get_query_var( 'my-account' ) == '' ) {
                return $template;
            }

            return HUSLA_JOBS_CLIENT_MODULE_DIR . '/dashboard/templates/index.php';
        } );

        add_action( 'show_user_profile', [$this,'userProfileFields'], 10, 1 );
        add_action( 'edit_user_profile', [$this,'userProfileFields'], 10, 1 );

    }

    public function addFilters() {

        add_filter( 'query_vars', function ( $query_vars ) {
            $query_vars[] = 'my-account';

            return $query_vars;
        } );

        add_filter( 'get_avatar_url', [$this,'getAvatarUrl'], 10, 3 );

    }
    public function userProfileFields( $profile_user ) {
        ?>
        <h3><?php echo __('Profile image', 'huslas'); ?></h3>
        <table class="form-table ayecode-avatar-upload-options">
            <tr>
                <th>
                    <label for="image"><?php echo __('Profile image', 'huslas'); ?></label>
                </th>
                <td>
                    <?php
                    // Check whether we saved the custom avatar, else return the default avatar.
                    $custom_avatar = get_the_author_meta( 'husla-user-avatar', $profile_user->ID );
                    if ( $custom_avatar == '' ){
                        $custom_avatar = get_avatar_url( $profile_user->ID );
                    }else{
                        $custom_avatar = esc_url_raw( $custom_avatar );
                    }
                    ?>
                    <img style="width: 96px; height: 96px; display: block; margin-bottom: 15px;" class="custom-avatar-preview" src="<?php echo $custom_avatar; ?>">
                    <input type="text" name="ayecode-custom-avatar" id="ayecode-custom-avatar" value="<?php echo esc_attr( esc_url_raw( get_the_author_meta( 'husla-user-avatar', $profile_user->ID )) ); ?>" class="regular-text" />
                    <input type='button' class="avatar-image-upload button-primary" value="<?php esc_attr_e("Upload Image","huslas");?>" id="uploadimage"/><br />
                    <span class="description">
					<?php echo __('Please upload a custom avatar for your profile, to remove the avatar simple delete the URL and click update.', 'huslas'); ?>
				</span>
                </td>
            </tr>
        </table>
        <?php
    }


    public function getAvatarUrl( $url, $id_or_email, $args ) {
        $id = '';
        if ( is_numeric( $id_or_email ) ) {
            $id = (int) $id_or_email;
        } elseif ( is_object( $id_or_email ) ) {
            if ( ! empty( $id_or_email->user_id ) ) {
                $id = (int) $id_or_email->user_id;
            }
        } else {
            $user = get_user_by( 'email', $id_or_email );
            $id = !empty( $user ) ?  $user->data->ID : '';
        }
        //Preparing for the launch.
        $custom_url = $id ?  get_user_meta( $id, 'husla-user-avatar', true ) : '';

        // If there is no custom avatar set, return the normal one.
        if( $custom_url == '' || !empty($args['force_default'])) {
            return $url;
//            return esc_url_raw( 'https://wpgd-jzgngzymm1v50s3e3fqotwtenpjxuqsmvkua.netdna-ssl.com/wp-content/uploads/sites/12/2020/07/blm-avatar.png' );
        }else{
            return esc_url_raw($custom_url);
        }
    }
}