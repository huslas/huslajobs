import {JobSearchForm} from "../../../dashboard/resources/components/job-search/job-search-form.js";
import {JobSearchResults} from "../../../dashboard/resources/components/job-search/job-search-results.js";
import {PaginationComponent} from "../../../dashboard/resources/components/pagination/pagination.js";
import {ReusableFunctions} from "../../../../../js/functions.js";
import {LoadingBar} from "../../../../../admin/modules/dashboard/resources/components/LoadingBar.js";
import {LoadingError} from "../../../../../admin/modules/dashboard/resources/components/LoadingError.js";
import {Pagination} from "../../../../../admin/modules/dashboard/resources/components/Pagination.js";
import {JobSeekerSearchForm} from "../../../dashboard/resources/components/job-seeker-search/job-seeker-search-form.js";
import {JobSeekerCard}  from "./job-seeker-card.js"
import {JobSeekerSearch} from "../../../dashboard/resources/components/job-seeker-search/job-seeker-search.js"
import {VLoader} from "../../../../../inc/global_components/loader/v-loader.js";
const JobSeekers ={
    emits:['account_jobs'],
    data() {
        return {
            jobs: undefined,
            page: 1,
            perPage: jobs_per_page,
            loading: false,
            error: false,
            deleting: false,
            pages: 0,
            sortField: 'profile_image',
            sort: 'desc',
            searchText:'',
            account_id: this.active_account_id,
            searchFields: "name,description,location,job_type",
            searchResults:undefined,
            total:0,
            homeUrl: THEME_URL,
            testUrl: '',
            job_seekers:undefined,
            show_results: false,
            jobSeekersStrings:VueUiStrings.clientFrontEnd.jobSeekers

        };
    },
    mixins:[ReusableFunctions],
    components: { LoadingBar, LoadingError, Pagination,JobSeekerSearch,JobSeekerCard, VLoader},
    mounted() {
        this.getJobSeekers();

    },
    methods:{
        goToPage(page) {
            this.page = page
            this.getJobSeekers();
        },
        showResults(result){
            this.show_results = result;
        },
        getJobSeekers(){
            this.startLoader()
            const data = new FormData();
            data.append("action", "search_job_seekers");
            data.append("page", this.page);
            data.append("perPage", this.perPage);
            data.append("sortBy", this.sortField);
            data.append("order", this.sort);
            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: {"Content-Type": "multipart/form-data"},
            })
                .then(function (response) {
                    that.job_seekers = response.data.data;
                    that.total = response.data.totalItems;
                    that.pages = response.data.totalPages;
                    setTimeout(()=>{
                        that.loading = false;
                        window.scrollTo({
                            top: 0,
                            left: 0,
                            behavior: 'smooth'
                        });
                        that.closeLoader();
                    },100)

                })
                .catch(function (error) {
                    console.log(error)
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error(that.jobSeekersStrings.errors.errorOccurred);
                    }
                    that.loading = false;
                    that.closeLoader();
                    that.error = true;
                });
        },
        updateLoading(value){
            this.loading = value
        }

    },
    created(){
        this.setJActiveLink('/job-seekers')
    },
    template:`<VLoader :active="loading" :translucent="true">
    <div class="all-jobs">
        <div v-if="error">
            <loading-error @update="getJobSeekers"></loading-error>
        </div>
        <section class="hs-section">
            <div class="row">
                <div class="col-md-10 hs-center-column">
                    <job-seeker-search @show-results="showResults"
                                       @update-loading="updateLoading"></job-seeker-search>
                </div>
            </div>
        </section>
        <div v-if="job_seekers">
            <section v-if="!show_results" class="hs-section">
                <div class="row">
                    <div class="col-md-1"></div>
                    <div v-if="job_seekers.length" class="col-md-10">
                        <div class="row mb-1">
                            <div class="col-md-12">
                                <p v-if="total>1" class="hs-title px-3">{{total}} {{jobSeekersStrings.jobSeekers}}</p>
                                <p v-else class="hs-title px-3">{{total}} {{jobSeekersStrings.jobSeeker}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <!--                     jobseekers card           -->
                            <div v-for="(job_seeker,index) in job_seekers" class="col-md-4 hs-seeker-card-wrapper"
                                 :key="index">
                                <job-seeker-card :job_seeker_data="[job_seeker]"></job-seeker-card>


                            </div>
                        </div>
                        <pagination :page="page" :pages="pages"
                                    @update="goToPage">

                        </pagination>
                    </div>
                    <div v-else class="col-md-10 text-center">
                        <p class="hs-subtitle">{{jobSeekersStrings.noJobSeekers}}</p>
                    </div>
                    <div class="col-md-1"></div>
                </div>
            </section>
        </div>
    </div>
</VLoader>
    `
}

export {JobSeekers}