import {LoadingError} from "../../../../../admin/modules/dashboard/resources/components/LoadingError.js";
import {LoadingBar} from "../../../../../admin/modules/dashboard/resources/components/LoadingBar.js";
import {ReusableFunctions} from "../../../../../js/functions.js";
import {VLoader} from "../../../../../inc/global_components/loader/v-loader.js";

const JobSeeker = {
    data() {
        return {
            form: {
                name: undefined,
                email: undefined,
                message: undefined
            },
            homeUrl: THEME_URL,
            userDataHeaders: [{key: 'about', value: 'About'}],
            defaultHeader: 'about',
            loading: false,
            id: job_seeker_id,
            account: undefined,
            profile: undefined,
            categories: undefined,
            error: false,
            submitting: false,
            formErrors: {},
            imgUrl: THEME_URL,
            jobSeekerStrings: VueUiStrings.clientFrontEnd.jobSeeker
        }
    },
    components: {LoadingError, LoadingBar, VLoader},
    mixins: [ReusableFunctions],
    methods: {
        updateDefaultHeader(currentHeader) {
            this.defaultHeader = currentHeader;
        },
        getAccount() {
            this.startLoader();
            const data = new FormData();
            data.append("action", "get_profile");
            data.append("profile_id", this.id);
            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: {"Content-Type": "multipart/form-data"},
            })
                .then(function (response) {
                    let profileCategories = [];
                    // that.account = response.data.account;
                    that.profile = response.data;
                    that.categories = that.profile.categories; //[...new Set(profileCategories)]
                    that.error = false;
                    setTimeout(() => {
                        that.loading = false;
                        that.closeLoader();
                    }, 100)
                })
                .catch(function (error) {
                    console.log(error);
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error(that.jobSeekerStrings.errors.errorOccurred);
                    }
                    that.loading = false;
                    that.closeLoader();
                    that.error = true;
                });
        },
        contactSeeker() {
            this.submitting = true;
            this.validateInput("name", this.form.name, {required: true}, this.jobSeekerStrings.contactForm.fields.name.label);
            this.validateInput("message", this.form.message, {required: true}, this.jobSeekerStrings.contactForm.fields.message.label);
            this.validateInput("email", this.form.email, {required: true}, this.jobSeekerStrings.contactForm.fields.email.label);
            if (Object.keys(this.formErrors).length === 0) {
                const data = new FormData();
                data.append("action", "contact_job_seeker");
                data.append("sender_email", this.form.email);
                data.append("sender_name", this.form.name);
                data.append("sender_message", this.form.message);
                data.append("job_seeker_email", this.profile.email);

                const that = this;
                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {
                        if (response.data) {
                            toastr.success(response.data.message)
                            that.form = {
                                name: undefined,
                                email: undefined,
                                message: undefined
                            }
                            window.scrollTo({
                                top: 0,
                                left: 0,
                                behavior: 'smooth'
                            });
                        }
                        that.submitting = false
                    })
                    .catch(function (error) {
                        console.log(error);
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.jobSeekerStrings.errors.errorOccurred);
                        }
                        that.submitting = false
                    });

            } else {
                this.submitting = false
            }
        },
        setBg(url){
            return `background-image:url('${url}')`;
        }
    },
    created(){
        this.setJActiveLink('/job-seekers')
    },
    watch: {
        'form.name': function (currentVal, oldVal) {
            if (currentVal !== undefined) {
                this.validateInput("name", currentVal, {required: true}, this.jobSeekerStrings.contactForm.fields.name.label);
            }

        },
        'form.message': function (currentVal, oldVal) {
            if (currentVal !== undefined) {
                this.validateInput("message", currentVal, {required: true}, this.jobSeekerStrings.contactForm.fields.message.label);
            }
        },
        'form.email': function (currentVal, oldVal) {
            if (currentVal !== undefined) {
                this.validateInput("email", currentVal, {required: true}, this.jobSeekerStrings.contactForm.fields.email.label);
            }
        }
    },
    mounted() {

        this.getAccount();


    },
    template: `<VLoader :active="loading" :translucent="true">
    <div id="job-seeker">
        <div v-if="error">
            <loading-error @update="getAccount"></loading-error>
        </div>
        <div v-else-if="profile">
            <!--            job seeker details -->
            <section class="hs-section">
                <div class="row">
                    <div class="col-md-8 mb-4">
                        <div class="d-flex flex-column">
                            <div>
                                <div class="row">
                                    <div class="col-md-5">
                                        <div class="hs-profile-img">
                                            <div class="hs-profile-img-inner">
                                                <img :src="setImage(profile.image,false)" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <h2 class="section_title">{{decodeHtml(profile.username)}}</h2>
                                        <p class="mb-0"><i class="fa fa-envelope mb-0"></i><span
                                                class="ml-2">{{profile.email}}</span>
                                        </p>
                                        <p><i class="fa fa-phone mb-0"></i><span
                                                class="ml-2">{{profile.phone_number}}</span>
                                        </p>
                                        <p class="hs-secondary-text-color" style="font-weight: bold;">
                                        <i class="fa fa-briefcase mr-1"></i>  
                                        <span>{{decodeHtml(profile.title)}}</span>
                                        
                                        </p>
                                   
                                    </div>
                                    <div v-if="categories.length" class="col-md-12">
                                        <div class="mt-5">
                                        <div class="hs-separator mb-4"></div>
                                            <p class="hs-subtitle">{{jobSeekerStrings.jobCategories}}:</p>
                                            <div class="d-flex flex-wrap">
                                                                                                <span v-for="(category,index) in categories"
                                                                                                      :key="index"
                                                                                                      class="hs-bg-gray-color hs-round hs-btn-sm mr-3 mb-3 d-inline-block text-capitalize hs-subtitle">{{decodeHtml(category.name)}}</span>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="mt-2">
                                            <p class="hs-subtitle">{{jobSeekerStrings.dataHeaders.about}}</p>
                                            <p>{{decodeHtml(profile.description)}}</p>
                                        </div>
                                    </div>
                                                                      
                                    <div v-if="profile.cv" class="col-md-12">
                                        <div class="mt-2">
                                            <p class="hs-subtitle">{{jobSeekerStrings.dataHeaders.cv}}</p>
                                            
                                            <a v-if="cvUrl(profile.cv,true)=='pdf'" :href="profile.cv" class="hs-btn hs-btn-primary" target="_blank">
                                                <span>{{jobSeekerStrings.resume.view}}</span>
                                                <span class="ml-1 mr-1">{{profile.username}}</span> 
                            
                                                <span>{{jobSeekerStrings.dataHeaders.cv}}</span>
                                            </a>
                                            <a v-else :href="profile.cv" class="hs-btn hs-btn-primary" target="_blank" download>
                                                <span>{{jobSeekerStrings.resume.view}}</span>
                                                <span class="ml-1 mr-1">{{profile.username}}</span> 
                            
                                                <span>{{jobSeekerStrings.dataHeaders.cv}}</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>


                            </div>

                        </div>
                    </div>
                    <div class="col-md-4 mb-4">

                        <div class="d-flex flex-column hs-border-1">
                            <div class="userdata-menu userdata-menu-item cursor-pointer active text-center py-3">
                                <h2 class="section-title mb-0">{{jobSeekerStrings.contactForm.heading}}</h2>
                            </div>
                            <div class="p-3">

                                <form id="wp_signup_form" @submit.prevent="contactSeeker">
                                    <div class="mb-4">
                                        <input type="text" class="form-control" id="username" name="name"
                                               :placeholder="jobSeekerStrings.contactForm.fields.name.placeholder"
                                               v-model="form.name"
                                        >
                                        <span class="text-danger"
                                              v-if="formErrors.name">{{formErrors.name}}</span>
                                    </div>
                                    <div class="mb-4">
                                        <input type="email" class="form-control" id="password" name="email"
                                               :placeholder="jobSeekerStrings.contactForm.fields.email.placeholder"
                                               v-model="form.email">
                                        <span class="text-danger d-block"
                                              v-if="formErrors.email">{{formErrors.email}}</span>
                                    </div>
                                    <div class="mb-4">
                                        <textarea name="message" v-model="form.message" rows="4"
                                                  :placeholder="jobSeekerStrings.contactForm.fields.message.placeholder"></textarea>
                                        <span class="text-danger"
                                              v-if="formErrors.message">{{formErrors.message}}</span>

                                    </div>
                                    <div class="mb-5">
                                        <button type="submit" class="hs-btn w-100 hs-btn-primary"
                                                :class="{'not-allowed':submitting || Object.keys(formErrors).length > 0}"
                                                :disabled="submitting || Object.keys(formErrors).length > 0"
                                        >
                                           
                                            {{jobSeekerStrings.contactForm.fields.button}}
                                             <i
                                                    v-if="submitting" class="fas fa-spinner fa-pulse ml-2"></i>
                                        </button>
                                    </div>
                                </form>

                            </div>
                        </div>

                    </div>


                </div>
            </section>
        </div>
    </div>
</VLoader>

    `,
}

export {JobSeeker}