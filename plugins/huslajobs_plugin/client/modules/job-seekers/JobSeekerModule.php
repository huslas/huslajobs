<?php
namespace huslajobs_client;

use huslajobs\HuslaModule;

class JobSeekerModule extends HuslaModule
{

    public function __construct() {
        $this->parent_module = 'client';
        $this->module        = 'job-seekers';
        $this->addActions();
        $this->addFilters();
    }

    public function addActions() {

        add_action( 'init', function () {
            add_rewrite_rule( 'job-seekers/([a-z0-9-]+)[/]?$', 'index.php?job-seeker=$matches[1]', 'top' );
        } );

        add_action( 'template_include', function ( $template ) {
            if ( get_query_var( 'job-seeker' ) == false || get_query_var( 'job-seeker' ) == '' ) {
                return $template;
            }
            return HUSLA_JOBS_CLIENT_MODULE_DIR . '/job-seekers/templates/single.php';
        } );

        add_action( 'init', function () {
            add_rewrite_rule( 'job-seekers', 'index.php?job-seekers=$matches[0]', 'top' );
        } );

        add_action( 'template_include', function ( $template ) {
            if ( get_query_var( 'job-seekers' ) == false || get_query_var( 'job-seekers' ) == '' ) {
                return $template;
            }
            return HUSLA_JOBS_CLIENT_MODULE_DIR . '/job-seekers/templates/index.php';
        } );




    }

    public function addFilters() {
        add_filter( 'query_vars', function ( $query_vars ) {
            $query_vars[] = 'job-seekers';
            return $query_vars;
        } );

        add_filter( 'query_vars', function ( $query_vars ) {
            $query_vars[] = 'job-seeker';

            return $query_vars;
        } );
    }


}