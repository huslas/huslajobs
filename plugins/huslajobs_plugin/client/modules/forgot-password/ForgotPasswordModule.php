<?php
    namespace huslajobs_client;

    use huslajobs\HuslaModule;
    use huslajobs\HuslajobsEmails;

    class ForgotPasswordModule extends HuslaModule
    {
        public function __construct() {
            $this->parent_module = 'client';
            $this->module        = 'forgot-password';
            $this->addActions();
            $this->addFilters();
        }
        public function addActions() {

            add_action( 'init', function () {
                add_rewrite_rule( 'forgot-password', 'index.php?forgot-password=$matches[0]', 'top' );
            } );

            add_action( 'template_include', function ( $template ) {
                if ( get_query_var( 'forgot-password' ) == false || get_query_var( 'forgot-password' ) == '' ) {
                    return $template;
                }
                return HUSLA_JOBS_CLIENT_MODULE_DIR . '/forgot-password/templates/index.php';
            } );

            add_action("wp_ajax_forgot_password", [$this, "forgotPassword"]);
            add_action("wp_ajax_nopriv_forgot_password", [$this, "forgotPassword"]);
        }

        public function addFilters() {
            add_filter( 'query_vars', function ( $query_vars ) {
                $query_vars[] = 'forgot-password';
                return $query_vars;
            } );
        }

        public function forgotPassword()
        {
            if (
                isset($_POST['forgot_password_nonce'])
                && wp_verify_nonce($_POST['forgot_password_nonce'], 'huslajobs-forgot-password')
            ) {
                $email = $_POST['email'];
                if (!is_email($email)) {
                    wp_send_json_error('Sorry the email address is invalid.', 400);
                }
                $user_data = get_user_by('email', $email);
                if ($user_data) {

                    $activation_key = get_password_reset_key($user_data);//;retrieve_password($user_data->user_login);
                    if (true == $activation_key) {
                        $lostPasswordEmails = new HuslajobsEmails();
                        $emailTemplatesData = $lostPasswordEmails->emailTemplatesData();
                        $email_templates_data = $emailTemplatesData['lost_password'];
                        $email_bcc = $email_templates_data['email_bcc'] ?? '';
                        $email_from = $email_templates_data['email_from'] ?? '';
                        $email_from_name = $email_templates_data['email_from_name'] ?? '';
                        $reply_to = $email_templates_data['reply_to'] ?? '';
                        $reply_to_name = $email_templates_data['reply_to_name'] ?? '';
                        $email_subject = $email_templates_data['subject'] ?? '';
                        $email_body = $email_templates_data['html'] ?? '';

                        $email_body = do_shortcode($email_body);
                        $email_body = wpautop($email_body);
                        $lost_password_url = home_url('/new-password');


                        $user_data = get_userdata($user_data->ID);
                        update_user_meta($user_data->ID, 'user_activation_key', $activation_key);
                        $lost_password_url = add_query_arg(
                            array(
                                'activation_key' => $activation_key,
                                'reset_password' => 'reset_password',
                            ),
                            $lost_password_url
                        );

                        $lost_password_url = wp_nonce_url($lost_password_url, 'reset_password');

                        $site_name = get_bloginfo('name');
                        $site_description = get_bloginfo('description');
                        $site_url = get_bloginfo('url');

                        $vars = array(
                            '{site_name}' => esc_html($site_name),
                            '{site_description}' => esc_html($site_description),
                            '{site_url}' => esc_url_raw($site_url),

                            '{first_name}' => esc_html($user_data->first_name),
                            '{last_name}' => esc_html($user_data->last_name),
                            '{user_display_name}' => esc_html($user_data->display_name),
                            '{user_email}' => esc_html($user_data->user_email),
                            '{user_name}' => esc_html($user_data->user_nicename),
                            '{user_avatar}' => get_avatar($user_data->user_email, 60),
                            '{activaton_url}' => esc_url_raw($lost_password_url),

                        );
                        $email_data['email_to'] = $user_data->user_email;
                        $email_data['email_bcc'] = $email_bcc;
                        $email_data['email_from'] = $email_from;
                        $email_data['email_from_name'] = $email_from_name;
                        $email_data['reply_to'] = $reply_to;
                        $email_data['reply_to_name'] = $reply_to_name;

                        $email_data['subject'] = strtr($email_subject, $vars);
                        $email_data['html'] = strtr($email_body, $vars);
                        $email_data['attachments'] = array();

                        $lostPasswordEmails->sendEmail($email_data);
                        echo json_encode(__("A password reset link has been sent to your email ."));
                    } else {
                        wp_send_json_error($activation_key->get_error_message(), 400);
                    }
                } else {
                    wp_send_json_error("There is no account with that username or email address", 400);
                }

                wp_die();
            } else {
                wp_send_json_error("Sorry,you are not allowed to reset password", 400);
            }
        }
    }