import {ForgotPassword} from "./components/ForgotPassword.js";

const app = Vue.createApp({
    render() {
        return Vue.h(ForgotPassword, {});
    },
});
app.mount("#forgot-password-content");