import {ReusableFunctions} from "../../../../../js/functions.js";
import {VLoader} from "../../../../../inc/global_components/loader/v-loader.js";

const ForgotPassword = {
    data() {
        return {
            form: {
                email: undefined,
            },
            successMessage: undefined,
            formErrors: {},
            submitting: false,
            loading:false,
            forgotPasswordStrings:VueUiStrings.clientFrontEnd.forgotPassword
        };
    },
    watch: {
        'form.email': function (currentVal, oldVal) {
            this.validateInput("email", currentVal, {required: true}, this.forgotPasswordStrings.formFields.email.label);
        },
    },
    components:{
        VLoader
    },
    mixins:[ReusableFunctions],
    mounted(){
        setTimeout(()=>{
            this.closeLoader();
        },200)
    },
    methods: {
        resetPassword() {
            this.submitting = true

            //validate inputs
            this.validateInput("email", this.form.email, {required: true}, this.forgotPasswordStrings.formFields.email.label);
            if (Object.keys(this.formErrors).length === 0) {
                const data = new FormData();
                const that = this;
                data.append("email", this.form.email);
                data.append('forgot_password_nonce', document.getElementById("huslajobs-forgot-password-nonce").value);
                data.append("action", "forgot_password");
                axios({
                    method: "post",
                    url: fp_ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {
                        if (response.data) {

                            toastr.success(response.data, 'Success');
                            that.successMessage = response.data
                            setTimeout(function () {
                                window.location.href = "/login"
                                that.submitting = false
                            }, 2000);

                        }
                    })
                    .catch(function (error) {
                        console.log(error)
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.forgotPasswordStrings.errors.errorOccurred);
                        }
                    });
            }else{
                this.submitting = false
                const firstErrorControl = document.querySelector('.validation-error')?.parentElement;
                // Scroll to first error element
                window.scrollTo(
                    firstErrorControl?.offsetLeft || 0,
                    (firstErrorControl?.offsetTop || 0) - 50 // Subtract 50 for better exposure
                )
            }


        },
    },

    template: `

<section class="hs-section">
        <div class="row text-center">
            <div class="col-md-12">
                <div class="hs-page-header">
                    <h1 class="page-title mb-5 mt-0 mx-0 p-0">
                        {{forgotPasswordStrings.heading}}
                    </h1>
                </div>
            </div>
        </div>
    <div class="row">
     <div class="col-md-4"></div>
        <div class="col-md-4">
            <div v-if="successMessage" class="text-center">
                <p class="mb-4">
                    {{successMessage}}
                </p>
                <a href="/login" class="hs-btn hs-btn-primary">{{forgotPasswordStrings.login}}</a>
            </div>
            <div v-else class="d-flex justify-content-between flex-column">

                <form id="wp_signup_form" method="post" @submit.prevent="resetPassword">
                    <div class="mb-4">
                        <input type="email" class="form-control" id="email" name="email" :placeholder="forgotPasswordStrings.formFields.email.placeholder"
                               v-model="form.email"
                               >
                        <span class="text-danger validation-error" v-if="formErrors.email">{{formErrors.email}}</span>
                    </div>
                    <div class="mb-4">
                        <button type="submit" class="hs-btn hs-btn-primary w-100"
                                :disabled="submitting || Object.keys(formErrors).length > 0">{{forgotPasswordStrings.formFields.button}}<i
                                v-if="submitting" class="fa fa-spinner fa-pulse ml-2"></i></button>
                    </div>
                </form>
                <div></div>

            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
    <div class="mb-5"></div>

</section>

   
`,
};

export {ForgotPassword};
