<?php
    global $wpdb, $user_ID;
    //Check whether the user is already logged in
    if ($user_ID) {
        ?>
        <script type="text/javascript">
            window.location.href = "<?php echo home_url("/my-account") ?>"
        </script>
        <?php
    }
    else{
        get_header();

        ?>

        <div id="forgot-password-content" class="content-area primary vue-component" ></div>
        <div id="forgot-password-nonce">
            <?php wp_nonce_field( 'huslajobs-forgot-password', 'huslajobs-forgot-password-nonce' ); ?>
        </div>
        <script type="text/javascript">
            const fp_ajaxurl = "<?php echo admin_url( 'admin-ajax.php' )?>"

        </script>
        <script src="<?php echo HUSLA_JOBS_CLIENT_MODULE_URL ?>/forgot-password/resources/index.js" type="module"></script>
        <?php get_footer(); ?>
    <?php } ?>