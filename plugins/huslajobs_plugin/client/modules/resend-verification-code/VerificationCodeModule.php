<?php
    namespace huslajobs_client;


    use huslajobs\HuslaModule;
    use huslajobs\HuslajobsEmails;

    class VerificationCodeModule extends HuslaModule
    {

        public function __construct() {
            $this->parent_module = 'client';
            $this->module        = 'resend-verification-code';
            $this->addActions();
            $this->addFilters();
        }

        public function addActions() {

            add_action( 'init', function () {
                add_rewrite_rule( 'resend-verification-code', 'index.php?resend-verification-code=$matches[0]', 'top' );

            } );

            add_action( 'template_include', function ( $template ) {
                if ( get_query_var( 'resend-verification-code' ) == false || get_query_var( 'resend-verification-code' ) == '' ) {
                    return $template;
                }
                return HUSLA_JOBS_CLIENT_MODULE_DIR . '/resend-verification-code/templates/index.php';
            } );
            add_action("wp_ajax_resend_verification_code",[$this,'resendVerificationCode']);
            add_action('wp_ajax_nopriv_resend_verification_code',[$this,'resendVerificationCode']);

        }

        public function addFilters() {
            add_filter( 'query_vars', function ( $query_vars ) {
                $query_vars[] = 'resend-verification-code';
                return $query_vars;
            } );
        }

        public function resendVerificationCode(){
            $email = $_POST['email'];
            $wpnonce = $_POST['wpnonce'];
            if (!is_email($email)) {
                wp_send_json_error(__('Sorry the email address is invalid.', 'huslajobs'), 400);
            }
            $user_data = get_user_by('email', $email);
            $activation_status = get_user_meta($user_data->ID,'user_activation_status',true);
            if ($user_data && !intval( $activation_status)) {

                if (wp_verify_nonce($wpnonce, 'huslajobs-resend-verification')) {

                    $resendVerificationEmails = new HuslajobsEmails();
                    $emailTemplatesData = $resendVerificationEmails->emailTemplatesData();
                    $exclude_user_roles = ['administrator'];
                    $emailTemplatesData = $emailTemplatesData['email_resend_key'];
                    $email_bcc = $emailTemplatesData['email_bcc'] ?? '';
                    $email_from = $emailTemplatesData['email_from'] ?? '';
                    $email_from_name = $emailTemplatesData['email_from_name'] ?? '';
                    $reply_to = $emailTemplatesData['reply_to'] ?? '';
                    $reply_to_name = $emailTemplatesData['reply_to_name'] ?? '';
                    $email_subject = $emailTemplatesData['subject'] ?? '';
                    $email_body = $emailTemplatesData['html'] ?? '';

                    $email_body = do_shortcode($email_body);
                    $email_body = wpautop($email_body);

                    $verification_page_url = home_url('/email-verification');
                    $user_activation_key = md5(uniqid('', true));

                    update_user_meta($user_data->ID, 'user_activation_key', $user_activation_key);
                    update_user_meta($user_data->ID, 'user_activation_status', 0);

                    $user_roles = !empty($user_data->roles) ? $user_data->roles : array();

                    if (!empty($exclude_user_roles)) {
                        foreach ($exclude_user_roles as $role):

                            if (in_array($role, $user_roles)) {
                                //update_option('uv_custom_option', $role);
                                update_user_meta($user_data->ID, 'user_activation_status', 1);
                                return;
                            }

                        endforeach;
                    }


                    $verification_url = add_query_arg(
                        array(
                            'activation_key' => $user_activation_key,
                            'user_verification_action' => 'email_verification',
                        ),
                        $verification_page_url
                    );

                    $verification_url = wp_nonce_url($verification_url, 'email_verification');

                    $site_name = get_bloginfo('name');
                    $site_description = get_bloginfo('description');
                    $site_url = get_bloginfo('url');

                    $vars = array(
                        '{site_name}' => esc_html($site_name),
                        '{site_description}' => esc_html($site_description),
                        '{site_url}' => esc_url_raw($site_url),

                        '{first_name}' => esc_html($user_data->first_name),
                        '{last_name}' => esc_html($user_data->last_name),
                        '{user_display_name}' => esc_html($user_data->display_name),
                        '{user_email}' => esc_html($user_data->user_email),
                        '{user_name}' => esc_html($user_data->user_nicename),
                        '{user_avatar}' => get_avatar($user_data->user_email, 60),

                        '{activaton_url}' => esc_url_raw($verification_url),

                    );
                    $email_data['email_to'] = $user_data->user_email;
                    $email_data['email_bcc'] = $email_bcc;
                    $email_data['email_from'] = $email_from;
                    $email_data['email_from_name'] = $email_from_name;
                    $email_data['reply_to'] = $reply_to;
                    $email_data['reply_to_name'] = $reply_to_name;

                    $email_data['subject'] = strtr($email_subject, $vars);
                    $email_data['html'] = strtr($email_body, $vars);
                    $email_data['attachments'] = array();

                    $resendVerificationEmails->sendEmail($email_data);

                    echo json_encode(__("A verification link has been sent to your email. Please verify your email account by clicking on the link","huslajobs"));
                } else {
                    wp_send_json_error(__("Sorry, you are not allowed. Please contact us if you think it is an issue","huslajobs"), 400);
                }
            }
            elseif ($user_data && intval( $activation_status)){
                echo json_encode(__("This Email has already been verified","huslajobs"));
            }
            else{
                wp_send_json_error(__("There is no account with that email address","huslajobs"), 400);
            }

            wp_die();
        }
    }