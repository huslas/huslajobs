import {VLoader} from "../../../../../inc/global_components/loader/v-loader.js";
import {ReusableFunctions} from "../../../../../js/functions.js";

const ResendCode = {
    data() {
        return {
            form:{
                email:undefined,
            },
            formErrors:{},
            submitting:false,
            loading:false,
            homeUrl:home_url,
            verificationStrings: VueUiStrings.clientFrontEnd.resendVerification

        };
    },
    watch: {
        'form.email':function(currentVal, oldVal){
            this.validateInput("email", currentVal,{required: true},"Email");
        },
    },
    mixins:[ReusableFunctions],
    components:{
        VLoader
    },
    mounted(){
        setTimeout(()=>{
            this.closeLoader();
        },200);
    },

    methods: {
        resendVerificationCode(){
            /**
             * todo
             * validate form on
             */
            this.submitting = true

            //validate inputs
            this.validateInput("email", this.form.email,{required: true},"Email");
            if (Object.keys(this.formErrors).length === 0){
                const data = new FormData();
                const that = this;
                data.append("email", this.form.email);
                data.append('wpnonce',document.getElementById("huslajobs-resend-verification-nonce").value);
                data.append("action", "resend_verification_code");
                axios({
                    method: "post",
                    url: verification_ajaxurl,
                    data: data,
                    headers: { "Content-Type": "multipart/form-data" },
                })
                    .then(function (response) {
                        if (response.data) {
                            /**
                             * todo take user to dashboard
                             */
                            toastr.success(response.data, 'Success');
                            // that.successMessage = response.data
                            setTimeout(function(){
                                window.location.href = that.homeUrl+"/login"
                                that.submitting = false

                            }, 2000);
                        }
                    })
                    .catch(function (error) {
                        console.log(error)
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error("An error occurred");
                        }
                        that.submitting = false;
                    });

            }
            else{
                this.submitting = false
                const firstErrorControl = document.querySelector('.validation-error')?.parentElement;
                // Scroll to first error element
                window.scrollTo(
                    firstErrorControl?.offsetLeft || 0,
                    (firstErrorControl?.offsetTop || 0) - 50 // Subtract 50 for better exposure
                )
            }


        },
    },

    template: `
    <section class="hs-section">

        <div class="row text-center">
            <div class="col-md-12">
                <div class="hs-page-header">
                    <h1 class="page-title p-0 mb-0">
                        {{verificationStrings.heading}}
                    </h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4"></div>
            <div class="col-md-4">
                <form id="wp_signup_form" method="post" @submit.prevent="resendVerificationCode">
                    <div class="mb-4">
                        <input type="email" class="form-control" id="email"
                               :placeholder="verificationStrings.formFields.email.placeholder" name="email"
                               v-model="form.email">
                        <span class="text-danger validation-error" v-if="formErrors.email">{{formErrors.email}}</span>
                    </div>

                    <div class="mb-4">
                        <button type="submit" class="hs-btn hs-btn-primary w-100"
                                :disabled="submitting || Object.keys(formErrors).length > 0">
                            {{verificationStrings.formFields.button}}<i v-if="submitting"
                                                                        class="fa fa-spinner fa-pulse ml-2"></i></button>
                    </div>
                </form>
            </div>
            <div class="col-md-4"></div>

        </div>

    </section>
`,
};

export {ResendCode};
