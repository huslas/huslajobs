<?php
    global $wpdb, $user_ID;
    //Check whether the user is already logged in
    if ($user_ID) {
        $user = get_user_by('id', $user_ID);
        if (in_array('administrator', (array)$user->roles)) {
            $redirect_to = home_url('/wp-admin');
            ?>
            <script type="text/javascript">
                window.location.href = "<?php echo home_url("/wp-admin") ?>"
            </script>
            <?php
        }
}
    get_header();

?>

<div id="verification-code-content" class="content-area primary vue-component"></div>
<div id="resend-verification-nonce">
    <?php wp_nonce_field( 'huslajobs-resend-verification', 'huslajobs-resend-verification-nonce' ); ?>
</div>
<script type="text/javascript">
    const verification_ajaxurl = "<?php echo admin_url( 'admin-ajax.php' )?>"

</script>
<script src="<?php echo HUSLA_JOBS_CLIENT_MODULE_URL ?>/resend-verification-code/resources/index.js" type="module"></script>

<?php get_footer(); ?>
