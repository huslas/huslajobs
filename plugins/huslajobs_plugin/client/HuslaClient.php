<?php

namespace huslajobs_client;

use huslajobs\HuslaModule;

class HuslaClient
{

    public function __construct()
    {
        $this->addActions();
        $this->addFilters();
        $this->initModules();

    }

    /**
     * @since v1.0
     * Adds actions
     */
    public function addActions(): void
    {
        add_action('phpmailer_init', [$this, 'setupMailer']);

        add_action('wp_enqueue_scripts', [$this, 'addScripts']);
        add_action('template_redirect', [$this, 'redirectUsers']);

        add_action('wp_head', [$this, 'huslaHeader'], 1);
        add_action('wp_footer', [$this, 'addFooterScripts']);

    }

    public function addFilters(): void
    {
        add_filter('script_loader_tag', [$this, 'addTypeAttribute'], 10, 3);
        add_filter('style_loader_tag', [$this, 'deferCss'], 10, 4);
    }

    /**
     * @since v1.0
     * Adds stylesheets and scripts on the client side
     */
    public function addScripts(): void
    {

        $plugin_mode = get_option('husla_jobs_plugin_mode');
        // insert styles and scripts here to be used on the client side
//        wp_enqueue_script('croppie-js', HUSLA_JOBS_JS_URL . '/croppie.js', [], 'v1.5.12', true);
        if ($plugin_mode === 'production' || $plugin_mode === 'testing') {
            wp_enqueue_script('vuejs', HUSLA_JOBS_JS_URL . '/vue.prod.js', [], '3.2.29', false);
            wp_enqueue_script('vue-router', HUSLA_JOBS_JS_URL . '/vue.router.js', [], '4.0.12', false);
        } else {
            wp_enqueue_script('vuejs', HUSLA_JOBS_JS_URL . '/vue.js', [], '3.2.29', false);
            wp_enqueue_script('vue-router', HUSLA_JOBS_JS_URL . '/vue-router.min.js', [], '4.0.12', false);
        }

        wp_enqueue_script('popper', HUSLA_JOBS_JS_URL . '/popper.js', [], '1.14.7', false);
//        wp_enqueue_script('bootstrap', HUSLA_JOBS_JS_URL . '/bootstrap.min.js', ['jquery'], '4.1.3', true);
        wp_enqueue_script('axios', HUSLA_JOBS_JS_URL . '/axios.min.js', [], '0.25.0', false);
        wp_enqueue_script('toastr', HUSLA_JOBS_JS_URL . '/toastr.min.js', [], '2.1.3', false);
        wp_enqueue_script('intlTelInput', HUSLA_JOBS_JS_URL . '/intlTelInput.js', [], '17.0.8', false);


        wp_enqueue_style('bootstrap', HUSLA_JOBS_CSS_URL . '/bootstrap.min.css', [], '4.1.3');
        wp_enqueue_style('app', HUSLA_JOBS_CSS_URL . '/app.min.css', [], '1.0.0');
        wp_enqueue_style('toastify', HUSLA_JOBS_CSS_URL . '/toastr.min.css', [], '2.1.3');
        wp_enqueue_style('font-awesome', HUSLA_JOBS_CSS_URL . '/font-awesome.css', [], '5.15.1');
        wp_enqueue_style('huslajobs-style', HUSLA_JOBS_CSS_URL . '/huslajobs-styles.css', [], '1.0');
        wp_enqueue_style('hs-loader', HUSLA_JOBS_CSS_URL . '/hs-loader.css', [], '1.0');
        wp_enqueue_style('intlTelInput-style', HUSLA_JOBS_CSS_URL . '/intlTelInput.css', [], '17.0.8');
//        wp_enqueue_style('croppie-css', HUSLA_JOBS_CSS_URL . '/croppie.css', [], '1.5.12');

    }

    /**
     * @since v1.0
     * Initialises class modules
     */
    public function initModules()
    {
        foreach (HuslaModule::getModules(HUSLA_JOBS_CLIENT_MODULE_DIR, false) as $dir) {
            $module = 'huslajobs_client\\' . rtrim($dir, ".php ");
            new $module();
        }
    }

    public function setupMailer($phpmailer)
    {
        $phpmailer->Host = 'mailhog';
        $phpmailer->Port = 1025;
        $phpmailer->IsSMTP();
    }

    public function redirectUsers()
    {

        if (is_cart()) {
            if (WC()->cart->is_empty()) {
                // If empty cart redirect to home
                wp_redirect(get_home_url(), 302);
            } else {
                // Else redirect to check out url
                wp_redirect(wc_get_checkout_url());
            }
        };

    }

    public function huslaHeader()
    {
        global $user_ID;

        require_once HUSLA_JOBS_INC_DIR . '/translate-script.php';
        ?>
        <script type="text/javascript">
            let is_Admin = <?php
                if ($user_ID){
                    $user = get_user_by('id', $user_ID);
                    if (in_array('administrator', (array)$user->roles)) {
                        echo 1;
                    }else{
                        echo 0;
                    }
                }else{
                    echo 0;
                }
            ?>;


            const user_id = parseInt("<?php echo $user_ID ?? 0 ?>");
            const THEME_URL = "<?php echo HUSLA_JOBS_IMAGE_URL ?>";
            const ajaxurl = "<?php echo admin_url('admin-ajax.php')?>";
            const home_url = "<?php echo home_url()?>";
            const THEME_JS_URL = "<?php echo HUSLA_JOBS_JS_URL ?>";
            const maximum_upload = <?php echo get_option('husla_jobs_profile_upload_size', 10) ?>;
            const recaptcha_key = "<?php echo HUSLASJOBS_RECAPTCHA_SITE_KEY ?? '' ?>";
            const price_per_paid_internship = <?php echo get_option('husla_jobs_price_per_internship', 100) ?>;
            const hs_price_per_job = <?php echo get_option('husla_jobs_price_per_job', 10) ?>;
            const woocommerce_currency = "<?php echo get_option('woocommerce_currency', 'XAF') ?>";
            const husla_jobs_limit = <?php echo get_option('husla_jobs_limit', 20) ?>;
            const yearly_discount = "<?php echo get_option('husla_jobs_yearly_discount_rate', 15) ?>";
            const biannual_discount = <?php echo get_option('husla_jobs_biannual_discount_rate', 10) ?>;
            const quarter_discount = <?php echo get_option('husla_jobs_quarter_discount_rate', 5) ?>;
            const jobs_per_page = <?php echo get_option('husla_jobs_per_page', 15) ?>;
            const login_session = <?php echo get_option('husla_jobs_login_session', 30) ?>;
            const profile_image_required = "<?php echo get_option('husla_jobs_profile_image') ?>";
            const sign_up_image_required = "<?php echo get_option('husla_jobs_signup_image_required') ?>";
            const show_sign_up_image = "<?php echo get_option('husla_jobs_signup_image') ?>";
            const profile_notice = "<?php echo get_option('husla_jobs_profile_notice') ?>";
            const application_fee_message = "<?php echo get_option('husla_jobs_application_fee_message') ?>";
            const application_fee_link_message = "<?php echo get_option('husla_jobs_application_fee_link_message') ?>";
            const application_limit_message = "<?php echo get_option('husla_jobs_application_limit_message') ?>";
            const application_limit_link_message = "<?php echo get_option('husla_jobs_application_limit_link_message') ?>";
            const job_notice = "<?php echo get_option('husla_jobs_job_notice') ?>";
            const delete_notice = "<?php echo get_option('husla_jobs_delete_notice') ?>";

        </script>
        <?php

    }

    public function astra_header_cart($output, $section, $section_type)
    {
        if ('edd' === $section_type && apply_filters('astra_edd_header_cart_icon', true)) {
            $output = $this->huslaHeader();
        }
        return $output;
    }


    public function addFooterScripts()
    {

        //    HUSLA_JOBS_JS_URL
        if (is_front_page()) {
            wp_enqueue_script('hs-front-page', HUSLA_JOBS_CLIENT_MODULE_URL . '/frontpage/resources/index.js', [], null, true);
        }
    }

    public function addTypeAttribute($tag, $handle, $src)
    {
        if (is_admin() || (FALSE === strpos($src, '.js')) || (strpos($src, 'jquery') || strpos($src, 'element'))) {
            return $tag;
        }
        elseif (!is_admin() && 'hs-front-page' == $handle) {
            return '<script type="module" src="' . esc_url($src) . '" defer></script>';
        }
        else {
            return '<script type="text/javascript" src="' . esc_url($src) . '" defer></script>';
        }
    }

    public function deferCss($html, $handle, $href, $media)
    {
        if (!is_admin()) {
            $html = '<link rel="preload" href="' . $href . '" as="style" id="' . $handle . '" media="' . $media . '" onload="this.onload=null;this.rel=\'stylesheet\'">'
                . '<noscript>' . $html . '</noscript>';
        }
        return $html;
    }

}