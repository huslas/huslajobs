<?php

/**
 * Add libraries to be included
 */


add_filter( 'husla_jobs_includes_filter', function ( $includes ) {
	$files = [
		HUSLA_JOBS_LIB_DIR . '/wordpress_tools/KMMenuPage.php', //
		HUSLA_JOBS_LIB_DIR . '/wordpress_tools/KMSubMenuPage.php', //
		HUSLA_JOBS_LIB_DIR . '/wordpress_tools/KMSetting.php', //
	];

	$includes = array_merge( $includes, $files );

	return $includes;
} );