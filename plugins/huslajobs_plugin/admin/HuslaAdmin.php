<?php

namespace huslajobs_admin;

use huslajobs\HuslaModule;
use KMMenuPage;

class HuslaAdmin {

	public function __construct() {
		// do something bro
		$this->addActions();
		$this->initModules();
		$this->addMenuPage();

	}

	/**
	 * @since v1.0
	 * Adds actions
	 */
	public function addActions(): void {
		add_action( 'admin_enqueue_scripts', [ $this, 'addScripts' ] );
        add_action('admin_head', [$this, 'huslaHeader'], 1);
	}

	/**
	 * @since v1.0
	 * Adds stylesheets and scripts on the admin side
	 */
	public function addScripts( $hook ): void {

		// insert styles and scripts here to be used on the admin side
		if ( $hook == 'toplevel_page_husla-jobs' ) {

//			wp_enqueue_script( 'vuejs', HUSLA_JOBS_JS_URL . '/vue.js', [], '3.2.29', false );
            $plugin_mode = get_option('husla_jobs_plugin_mode');
            // insert styles and scripts here to be used on the client side

            if ($plugin_mode ==='production' || $plugin_mode === 'testing'){
                wp_enqueue_script('vuejs', HUSLA_JOBS_JS_URL . '/vue.prod.js', [], '3.2.29', false);

            }else{
                wp_enqueue_script('vuejs', HUSLA_JOBS_JS_URL . '/vue.js', [], '3.2.29', false);
            }
			wp_enqueue_script( 'vue-router', HUSLA_JOBS_JS_URL . '/vue.router.js', [], '4.0.12', false );
			wp_enqueue_script( 'popper', HUSLA_JOBS_JS_URL . '/popper.js', [], '1.14.7', false );
			wp_enqueue_script( 'bootstrap', HUSLA_JOBS_JS_URL . '/bootstrap.min.js', [ 'jquery' ], '4.1.3', true );
			wp_enqueue_script( 'axios', HUSLA_JOBS_JS_URL . '/axios.min.js', [], '0.25.0', false );
//			wp_enqueue_script( 'app', HUSLA_JOBS_JS_URL . '/app.min.js', [ 'jquery' ], '1.0.0', true );
			wp_enqueue_script( 'vendor', HUSLA_JOBS_JS_URL . '/vendor.min.js', [ 'jquery' ], '1.0.0', true );
			wp_enqueue_script( 'toastr', HUSLA_JOBS_JS_URL . '/toastr.min.js', [], '2.1.3', false );
			wp_enqueue_script( 'apex-chart', HUSLA_JOBS_JS_URL . '/apex.chart.min.js', [], '3.33.0', false );


            wp_enqueue_style( 'bootstrap', HUSLA_JOBS_CSS_URL . '/bootstrap.min.css', [], '4.1.3' );
			wp_enqueue_style( 'app', HUSLA_JOBS_CSS_URL . '/app.min.css', [], '1.0.0' );
			wp_enqueue_style( 'toastify', HUSLA_JOBS_CSS_URL . '/toastr.min.css', [], '2.1.3' );
			wp_enqueue_style( 'font-awesome', HUSLA_JOBS_CSS_URL . '/font-awesome.css', [], '5.15.1' );
            wp_enqueue_style('huslajobs-style', HUSLA_JOBS_CSS_URL . '/huslajobs-styles.css', [], '1.0');


        }
	}

	/**
	 * @since v1.o
	 * Adds the admin menu page
	 */
	public function addMenuPage(): void {
		$menu_page      = new KMMenuPage( array(
			'page_title' => 'Husla Jobs',
			'menu_title' => 'Husla Jobs',
			'capability' => 'read',
			'menu_slug'  => 'husla-jobs',
			'icon_url'   => 'dashicons-filter',
			'position'   => null,
			'function'   => null
		) );
		$sub_menu_pages = apply_filters( 'husla_jobs_admin_sub_menu_pages_filter', [] );
		foreach ( $sub_menu_pages as $sub_menu_page ) {
			$menu_page->add_sub_menu_page( $sub_menu_page );
		}
		$menu_page->run();
	}

	/**
	 * @since v1.0
	 * Initialises class modules
	 */
	public function initModules() {
		foreach ( HuslaModule::getModules( HUSLA_JOBS_ADMIN_MODULE_DIR, false ) as $dir ) {
			$module = 'huslajobs_admin\\' . rtrim( $dir, ".php " );
			new $module();
		}
	}

    public function huslaHeader()
    {

        require_once HUSLA_JOBS_INC_DIR. '/translate-script.php';?>

        <script type="text/javascript">
            const jobs_per_page = <?php echo get_option('husla_jobs_per_page', 15) ?>;
        </script>
        <?php

    }
}