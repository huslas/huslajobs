<?php

namespace huslajobs_admin;

use huslajobs\HuslaModule;
use huslajobs\Package;
use KMSetting;
use KMSubMenuPage;

class SettingsModule extends HuslaModule {
	public function __construct() {
		$this->parent_module = 'admin';
		$this->module        = 'settings';
		$this->addActions();
		$this->addSettings();
		add_filter( 'husla_jobs_admin_sub_menu_pages_filter', [ $this, 'addSettingsSubMenuPage' ] );
	}

	/**
	 * @since v1.0
	 * Adds dashboard submenu page
	 */
	function addSettingsSubMenuPage( $sub_menu_pages ) {
		$dashboard_page = new KMSubMenuPage(
			array(
				'page_title' => 'Settings',
				'menu_title' => 'Settings',
				'capability' => 'manage_options',
				'menu_slug'  => 'husla-jobs-settings',
				'position'   => 1,
				'function'   => [ $this, 'settingsPageContent' ]
			) );
		array_push( $sub_menu_pages, $dashboard_page );

		return $sub_menu_pages;
	}

	/**
	 * @since v1.0
	 * Displays content on dashboard sub menu page
	 */
	function settingsPageContent() {
		$this->renderContent( 'index' );
	}

	public function addActions(): void {
		// add actions here
	}

	public function addSettings() {

		$settings = new KMSetting( 'husla-jobs-settings' );
		$settings->add_section( 'husla-jobs-settings' );
        $settings->add_field(
            array(
                'type'  => 'text',
                'id'    => 'husla_jobs_woo_product_id',
                'label' => 'Product Id',
                'tip'   => "<span class='text-primary'>The product id to be used for membership subscriptions</span>"
            )
        );
        $settings->add_field(
            array(
                'type'  => 'number',
                'id'    => 'husla_jobs_profile_upload_size',
                'label' => 'Profile upload size(MB)',
                'tip'   => '',

            )
        );

        $settings->add_field(
            array(
                'type'  => 'number',
                'id'    => 'husla_jobs_maximum_login',
                'label' => 'Maximum login',
                'tip'   => 'Number of times a user can login if they have not very their email'
            )
        );
        $settings->add_field(
            array(
                'type'  => 'number',
                'id'    => 'husla_jobs_login_session',
                'label' => 'Logged in Session',
                'default'=> 30,
                'tip'   => 'The time after which the user will be logged out'
            )
        );
        $settings->add_field(
            array(
                'type'  => 'number',
                'id'    => 'husla_jobs_quarter_discount_rate',
                'label' => '3 months discount rate(%)',
                'tip'   => 'Discount rate for users paying upfront for membership plans'
            )
        );
        $settings->add_field(
            array(
                'type'  => 'number',
                'id'    => 'husla_jobs_biannual_discount_rate',
                'label' => '6 months discount rate(%)',
                'tip'   => 'Discount rate for users paying upfront for membership plans'
            )
        );
        $settings->add_field(
            array(
                'type'  => 'number',
                'id'    => 'husla_jobs_yearly_discount_rate',
                'label' => '1 year discount rate(%)',
                'tip'   => 'Discount rate for users paying upfront for membership plans'
            )
        );
        $settings->add_field(
            array(
                'type'  => 'number',
                'id'    => 'husla_jobs_price_per_job',
                'label' => 'Price per job',
                'tip'   => 'The cost per job for guest members'
            )
        );
        $settings->add_field(
            array(
                'type'  => 'number',
                'id'    => 'husla_jobs_limit',
                'label' => 'Job limit',
                'default_option'=>20,
                'tip'   => 'The number of jobs non logged in users can view'
            )
        );
        $settings->add_field(
            array(
                'type'  => 'number',
                'id'    => 'husla_jobs_price_per_internship',
                'label' => 'Price per paid internship',
                'tip'   => 'The cost per paid internship for guest members'
            )
        );
        $settings->add_field(
            array(
                'type'  => 'number',
                'id'    => 'husla_jobs_delete_account_limit',
                'label' => 'Delete account limit',
                'tip'   => 'The number of days after which a user account will be deleted when they delete their account'
            )
        );
//		$settings->add_field(
//			array(
//				'type'    => 'select',
//				'id'      => 'husla_jobs_account_type',
//				'label'   => 'Default account type: ',
//				'options' => array(
//                    'Individual job seeker' =>'individual_job_seeker',
//                    'Individual recruiter' =>'individual_recruiter',
//
//				),
//				 'default_option' => 'individual_job_seeker'
//			)
//		);

        $settings->add_field(
            array(
                'type'    => 'select',
                'id'      => 'husla_jobs_per_page',
                'label'   => 'Items per page: ',
                'options' => array(
                    '15' =>15,
                    '25' =>25,
                    '50' =>50,
                ),
                'default_option' => 15
            )
        );
        $settings->add_field(
            array(
                'type'  => 'text',
                'id'    => 'husla_jobs_recaptcha_key',
                'label' => 'Google recaptcha key',
            )
        );
        $settings->add_field(
            array(
                'type'  => 'text',
                'id'    => 'husla_jobs_recaptcha_secret',
                'label' => 'Google recaptcha secret',
            )
        );
        $settings->add_field(
            array(
                'type'  => 'textarea',
                'id'    => 'husla_jobs_profile_notice',
                'label' => 'Profile notice',
                'default_option'=> 'Do you have a skill or trade? Are you unemployed and ready to work? Advertise your skills on our platform by adding your job profile(s)',
                'tip'=>'The message the user sees when they have not created a job profile'
            )
        );
        $settings->add_field(
            array(
                'type'  => 'textarea',
                'id'    => 'husla_jobs_application_fee_message',
                'label' => 'Application fee message',
                'default_option'=> 'Application fees only applies to guest members.',
                'tip'=>'The message the user sees when they are applying for a job and have not upgraded their account'
            )
        );
        $settings->add_field(
            array(
                'type'  => 'textarea',
                'id'    => 'husla_jobs_application_fee_link_message',
                'label' => 'Application fee link message',
                'default_option'=> 'Upgrade not to pay application fees',
                'tip'=>'The link message the user sees when they are applying for a job and have not upgraded their account'
            )
        );
        $settings->add_field(
            array(
                'type'  => 'textarea',
                'id'    => 'husla_jobs_application_limit_message',
                'label' => 'Application Exceeded message',
                'default'=> 'Application limit exceeded.',
                'tip'=>'The upgrade message users sees when they have exceeded their application limit'
            )
        );
        $settings->add_field(
            array(
                'type'  => 'textarea',
                'id'    => 'husla_jobs_application_limit_link_message',
                'label' => 'Application Exceeded link message',
                'default'=> 'Upgrade to apply',
                'tip'=>'The upgrade link message users sees when they have exceeded their application limit'
            )
        );
        $settings->add_field(
            array(
                'type'  => 'textarea',
                'id'    => 'husla_jobs_job_notice',
                'label' => 'Job notice',
                'default'=> 'Applications for this job will be sent to the company email',
                'tip'=>'The message users sees when they choose to post as a company'
            )
        );
        $settings->add_field(
            array(
                'type'  => 'textarea',
                'id'    => 'husla_jobs_delete_notice',
                'label' => 'Delete account notice',
                'default'=> "Please note that there’s no option to restore the account or its data nor reuse the email to create an account for a period of 30 days",
                'tip'=>'The message users sees when they want to delete their account'
            )
        );
        $settings->add_field(
            array(
                'type'    => 'select',
                'id'      => 'husla_jobs_plugin_mode',
                'label'   => 'Plugin mode: ',
                'options' => array(
                    'Development' =>'development',
                    'Testing' =>'testing',
                    'Production' =>'production',
                ),
                'default_option' => 'development'
            )
        );
        $settings->add_field(
            array(
                'type'    => 'select',
                'id'      => 'husla_jobs_profile_image',
                'label'   => 'Must users upload image when creating their profile?: ',
                'options' => array(
                    'Yes' =>'yes',
                    'No' =>'no',
                ),
                'default_option' => 'yes',
                'tip'   => "Select yes if you want users to upload an image when they are creating their profile. If No is selected they can upload as well as they can't"
            )
        );
        $settings->add_field(
            array(
                'type'    => 'select',
                'id'      => 'husla_jobs_signup_image',
                'label'   => 'Should users upload image during signup?: ',
                'options' => array(
                    'Yes' =>'yes',
                    'No' =>'no',
                ),
                'default_option' => 'no',
                'tip'   => 'Select yes if you want the users to have the option to upload their images during signup. To make it required select yes on the select input'
            )
        );
        $settings->add_field(
            array(
                'type'    => 'select',
                'id'      => 'husla_jobs_signup_image_required',
                'label'   => 'Is signup image required?:',
                'options' => array(
                    'Yes' =>'yes',
                    'No' =>'no',
                ),
                'default_option' => 'no',
                'tip'   => 'Select yes if you want it mandatory and no if optional. This only applies if the previous select is set to Yes'
            )
        );


        $settings->save();
	}
}