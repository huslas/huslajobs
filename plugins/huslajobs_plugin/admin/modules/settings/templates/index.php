<?php

?>
    <h2>Advanced Settings </h2>
<?php settings_errors(); ?>
    <form method="post" action="options.php">
		<?php

		settings_fields( 'husla-jobs-settings' );
		do_settings_sections( 'husla-jobs-settings' );

		submit_button();
		?>
    </form>
    <style>
        #husla_jobs_woo_product_id{
            cursor: not-allowed;
            pointer-events: none;
            background: grey;
        }
    </style>
<?php