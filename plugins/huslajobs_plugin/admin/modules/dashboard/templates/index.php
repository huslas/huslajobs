<?php

/**
 * packages template
 */
?>

<style>
    .card {
        max-width: 100% !important;
    }
</style>
<div id="hs-dashboard">
    <div class="hs-loader d-flex align-items-center">

        <div class="loading-content">
            <img src="<?php echo HUSLA_JOBS_IMAGE_URL ?>/logo.png" style="animation-name: km-img-loading;
          animation-duration: 2s;
          animation-iteration-count: infinite;
          width: 110px !important;
"/>
            <h5>Loading...</h5>
        </div>
</div>


<script>
    const THEME_URL = "<?php echo HUSLA_JOBS_IMAGE_URL?>";
    const home_url = "<?php echo home_url()?>";
</script>
<script src="<?php echo HUSLA_JOBS_ADMIN_MODULE_URL ?>/dashboard/resources/index.js" type="module"></script>
