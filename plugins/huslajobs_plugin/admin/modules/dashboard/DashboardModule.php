<?php

namespace huslajobs_admin;

use huslajobs\Account;
use huslajobs\HuslaModel;
use huslajobs\HuslaModule;
use huslajobs\HuslaValidator;
use huslajobs\JobApplication;
use huslajobs\Package;
use huslajobs\JobType;
use huslajobs\Job;
use huslajobs\PackageLimit;
use huslajobs\Category;
use huslajobs\Subscription;
use huslajobs\Currency;
use KMSubMenuPage;
use function Sodium\add;

class DashboardModule extends HuslaModule {
	public function __construct() {
		$this->parent_module = 'admin';
		$this->module        = 'dashboard';
		$this->addActions();
		add_filter( 'husla_jobs_admin_sub_menu_pages_filter', [ $this, 'addDashboardSubMenuPage' ] );
	}

	/**
	 * @since v1.0
	 * Adds dashboard submenu page
	 */
	function addDashboardSubMenuPage( $sub_menu_pages ) {
		$dashboard_page = new KMSubMenuPage(
			array(
				'page_title' => 'Husla Jobs',
				'menu_title' => 'Husla Jobs',
				'capability' => 'manage_options',
				'menu_slug'  => 'husla-jobs',
				'position'   => 1,
				'function'   => [ $this, 'dashboardPageContent' ]
			)
		);
		array_push( $sub_menu_pages, $dashboard_page );

		return $sub_menu_pages;
	}

	/**
	 * @since v1.0
	 * Displays content on dashboard sub menu page
	 */
	function dashboardPageContent() {
		$this->renderContent( 'index' );
	}

	public function addActions(): void {


	}
}
