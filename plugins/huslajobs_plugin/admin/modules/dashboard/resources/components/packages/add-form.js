import {LoadingBar} from "../LoadingBar.js";
import {LoadingError} from "../LoadingError.js";
import {ReusableFunctions} from "../../../../../../js/functions.js";

export const AddForm = {
    props: {
        id: {
            default: 0
        }
    },
    mixins:[ReusableFunctions],
    data() {
        return {
            package: {
                name: undefined,
                description: undefined,
                price: 0,
                duration: 1,
                currency_id: '',
                profiles: -1,
                benefits: undefined,
                jobs:-1,
                job_applications:-1,

            },
            formErrors: {},
            submitting: false,
            router: {},
            jobTypes: [],
            tempLimit: {
                jobType: '',
                limit: 1
            },
            loading: true,
            error: false,
            currencies: [],
            addFormStrings: VueUiStrings.adminBackend.packages.addForm
        };
    },
    mounted() {
        this.router = VueRouter.useRouter();
        this.loadContent()
    },
    components: {LoadingBar, LoadingError},
    watch: {
        'package.name': function (current) {
            this.validateInput("name", current, {required: true}, this.addFormStrings.formFields.name);
        },
        'package.description': function (current) {
            this.validateInput("description", current, {required: true}, this.addFormStrings.formFields.description.label);
        },
        'package.benefits': function (current) {
            this.validateInput("benefits", current, {required: true}, this.addFormStrings.formFields.benefits.label);
        },
        'package.price': function (current) {
            this.validateInput("price", current, {isNotNegative: true,isNumeric: true}, this.addFormStrings.formFields.price);
        },
    },
    methods: {
        loadContent() {
            if (this.id === 0) {
                this.getCurrencies();
            } else {
                this.getPackage();
            }
        },
        getCurrencies() {
            this.loading = true
            const data = new FormData();
            data.append("action", "get_currencies");
            data.append("page", 1);
            data.append("perPage", -1);
            const that = this
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: {"Content-Type": "multipart/form-data"},
            })
                .then(function (response) {
                    that.getJobTypes();
                    that.currencies = response.data.data
                })
                .catch(function (error) {
                    console.log(error);
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error(that.addFormStrings.errors.errorOccurred);
                    }
                    that.loading = false;
                    that.error = true;
                });
        },
        getPackage() {
            this.loading = true
            const data = new FormData();
            data.append("action", "get_package");
            data.append("package_id", this.id);
            const that = this
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: {"Content-Type": "multipart/form-data"},
            })
                .then(function (response) {
                    that.getCurrencies();
                    that.package = response.data
                })
                .catch(function (error) {
                    console.log(error);
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error(that.addFormStrings.errors.errorOccurred);
                    }
                    that.loading = false;
                    that.error = true;
                });
        },
        // addLimit() {
        //     const limit = this.package.limits.filter(e => e.jobType == this.tempLimit.jobType);
        //     if (this.tempLimit.jobType == '') {
        //         toastr.error("Please select a job type")
        //     } else if (limit.length > 0) {
        //         toastr.error("You have already set limit for this job type")
        //     } else {
        //         this.package.limits.push(this.tempLimit);
        //         this.tempLimit = {
        //             jobType: '',
        //             limit: 1
        //         };
        //     }
        // },
        removeLimit(index) {
            this.package.limits.splice(index, 1)
        },
        getJobTypeById(id) {
            const jobType = this.jobTypes.filter(e => e.id == id); // == was used just in case id is string or number
            return jobType[0].name;
        },
        getJobTypes() {
            const data = new FormData();
            data.append("action", "get_job_types");
            data.append("page", 1);
            data.append("perPage", -1);

            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: {"Content-Type": "multipart/form-data"},
            })
                .then(function (response) {
                    that.jobTypes = response.data.data;
                    that.loading = false;
                    that.error = false;
                })
                .catch(function (error) {
                    console.log(error);
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error(that.addFormStrings.errors.errorOccurred);
                    }
                    that.loading = false;
                    that.error = true;
                });
        },
        // validate() {
        //     if (this.package.name.trim() === '') {
        //         toastr.error(this.addFormStrings.errors.nameRequired);
        //         return false;
        //     }
        //     if (this.package.price === '') {
        //         toastr.error(this.addFormStrings.errors.priceRequired);
        //         return false
        //     }
        //     if (this.package.duration === '') {
        //         toastr.error(this.addFormStrings.errors.durationRequired);
        //         return false;
        //     }
        //     return true;
        // },
        submitForm() {
            this.submitting = true;
                this.validateInput("name", this.package.name, {required: true}, this.addFormStrings.formFields.name);
                this.validateInput("description", this.package.description, {required: true}, this.addFormStrings.formFields.description.label);
                this.validateInput("benefits", this.package.benefits, {required: true}, this.addFormStrings.formFields.benefits.label);
                this.validateInput("price", this.package.price, {isNotNegative: true,isNumeric:true}, this.addFormStrings.formFields.price);

            if (Object.keys(this.formErrors).length === 0) {
                const data = new FormData();

                const that = this;
                let limits = '';
                // this.package.limits.forEach(e => limits += (e.jobType + ":" + e.limit + ","));
                data.append("name", this.package.name);
                data.append("price", this.package.price);
                data.append("description", this.package.description);
                data.append("duration", this.package.duration);
                data.append("profiles", this.package.profiles);
                data.append("currency_id", this.package.currency_id);
                data.append("profiles", this.package.profiles);
                data.append("currency_id", this.package.currency_id);
                data.append("jobs", this.package.jobs);
                data.append("job_applications", this.package.job_applications);
                data.append("benefits",this.package.benefits);
                // data.append("limits", limits);
                if (this.id === 0)
                    data.append("action", "save_package");
                else {
                    data.append("package_id", this.id);
                    data.append("action", "update_package");
                }

                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {
                        if (response.data) {
                            that.package = {
                                name: undefined,
                                description: undefined,
                                price: 0,
                                duration: 1,
                            };
                            if (that.id === 0)
                                toastr.success(that.addFormStrings.successMessage.packageCreated, 'Success')
                            else
                                toastr.success(that.addFormStrings.successMessage.packageUpdated, 'Success')

                            that.submitting = false;
                            that.router.push('/packages')
                        }
                    })
                    .catch(function (error) {
                        console.log(error);
                        that.submitting = false;
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.addFormStrings.errors.errorOccurred);
                        }
                    });
            }else{
                this.submitting = false
                const firstErrorControl = document.querySelector('.validation-error')?.parentElement;
                // Scroll to first error element
                window.scrollTo(
                    firstErrorControl?.offsetLeft || 0,
                    (firstErrorControl?.offsetTop || 0) - 50 // Subtract 50 for better exposure
                )
            }
        }
    },
    template: `<div>
    <loading-bar v-if="loading"></loading-bar>
    <div v-if="!loading">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div v-if="error">
                            <loading-error @update="loadContent"></loading-error>
                        </div>
                        <div v-else>
                        <div>
                        <h5><strong>{{addFormStrings.tips.note}}</strong></h5>
                        <p>{{addFormStrings.tips.unlimitedNumber}}</p>
                         <p>{{addFormStrings.tips.packageCurrency}}</p>
</div>
                            <form @submit.prevent="submitForm">
                                <div class="mb-4">
                                    <label for="name" class="form-label">{{addFormStrings.formFields.name}}</label>
                                    <input type="text" class="form-control" id="name" v-model="package.name">
                                    <span class="text-danger validation-error" v-if="formErrors.name">{{formErrors.name}}</span>
                                </div>

                                <div class="mb-4">
                                    <label for="price" class="form-label">{{addFormStrings.formFields.price}}</label>
                                    <input type="number" class="form-control" id="price"
                                           v-model="package.price">
                                           <span class="text-danger validation-error" v-if="formErrors.price">{{formErrors.price}}</span>
                                </div>
                                <div class="mb-4">
                                    <label for="duration" class="form-label">{{addFormStrings.formFields.duration}}</label>
                                    <input type="number" class="form-control" min="1" max="1" id="duration"
                                           v-model="package.duration">
                                </div>
                                <div class="mb-4">
                                    <label for="profiles" class="form-label">{{addFormStrings.formFields.profiles}}</label>
                                    <input type="number" class="form-control" min="-1" id="profiles"
                                           v-model="package.profiles">
                                </div>
                                <div class="mb-4">
                                    <label for="jobs" class="form-label">{{addFormStrings.formFields.jobs}}</label>
                                    <input type="number" class="form-control" min="-1" id="jobs"
                                           v-model="package.jobs">
                                </div>
                                <div class="mb-4">
                                    <label for="job-applications" class="form-label">{{addFormStrings.formFields.jobApplications}}</label>
                                    <input type="number" class="form-control" min="-1" id="job-applications"
                                           v-model="package.job_applications">
                                </div>

                                <div class="mb-4">
                                    <label for="benefits" class="form-label">{{addFormStrings.formFields.benefits.label}}</label>
                                    <textarea class="form-control" id="benefits"
                                              v-model="package.benefits" :placeholder="addFormStrings.formFields.benefits.placeholder"></textarea>
                                              <span class="text-danger validation-error" v-if="formErrors.benefits">{{formErrors.benefits}}</span>
                                </div>
                                <div class="mb-4">
                                    <label for="description" class="form-label">{{addFormStrings.formFields.description.label}}</label>
                                    <textarea class="form-control" id="description"
                                              v-model="package.description" :placeholder="addFormStrings.formFields.description.placeholder"></textarea>
                                              <span class="text-danger validation-error" v-if="formErrors.description">{{formErrors.description}}</span>
                                </div>

                                <div class="mb-4">
                                    <button class="hs-btn hs-btn-primary hs-btn-signup" :disabled="submitting">{{addFormStrings.formFields.buttons.submit}} <i v-if="submitting"
                                                                                                     class="fa fa-spinner fa-pulse"></i>
                                    </button>
                                    <button class="hs-btn hs-btn-primary-outline ml-2 hs-btn-signup" type="button"
                                            @click='router.push("/packages")'>{{addFormStrings.formFields.buttons.cancel}}
                                    </button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>`
}
export default {AddForm}