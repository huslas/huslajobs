import {AddForm} from './add-form.js'

const Create = {
    data(){
        return{
            createPackageStrings: VueUiStrings.adminBackend.packages.create
        }
    },
    components: {AddForm},
    template: `<div>
    
        <div class="row page-title">
            <div class="col-md-12">
                <nav aria-label="breadcrumb" class="float-right mt-1">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <router-link to="/">{{createPackageStrings.dashboard}}</router-link>
                        </li>
                        <li class="breadcrumb-item">
                            <router-link to="/packages">{{createPackageStrings.packages}}</router-link>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{createPackageStrings.create}}</li>
                    </ol>
                </nav>
                <h4 class="mb-1 mt-0">{{createPackageStrings.createPackage}}</h4>
            </div>
        </div>
        <add-form></add-form>
    </div>`,
};

export {Create};
