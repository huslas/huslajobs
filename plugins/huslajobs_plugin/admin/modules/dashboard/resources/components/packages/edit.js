import {AddForm} from './add-form.js'


const Edit = {
    data() {
        return {
            id: 0,
            route: {},
            editPackageStrings:VueUiStrings.adminBackend.packages.edit
        }
    },
    mounted() {
        this.route = VueRouter.useRoute();
        this.id = this.route.params?.id ?? 0
    },
    components: {AddForm},
    template: `<div>
        <div class="row page-title">
            <div class="col-md-12">
                <nav aria-label="breadcrumb" class="float-right mt-1">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <router-link to="/">{{editPackageStrings.dashboard}}</router-link>
                        </li>
                        <li class="breadcrumb-item">
                            <router-link to="/packages">{{editPackageStrings.packages}}</router-link>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{editPackageStrings.edit}}</li>
                    </ol>
                </nav>
                <h4 class="mb-1 mt-0">{{editPackageStrings.editPackage}}</h4>
            </div>
        </div>
        <add-form :id="id" v-if="id!=0"></add-form>
    </div>
`,
};

export {Edit};
