import { CategoryForm } from "./add-form.js";

const CreateCategory = {
    props: {
        formParams: {
            type: Object,
            default: function () {
                return { id: undefined, name: undefined,
                    description: undefined,

                };
            },
        },
    },
    data(){
        return{
            categoryCreateStrings: VueUiStrings.adminBackend.categories.create
        }
    },
    components: {
        CategoryForm,
    },

    template: `

<div class="module-content-wrapper">
  <div class="row page-title">
    <div class="col-md-12">
        <nav  aria-label="breadcrumb" class="float-right mt-1">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><router-link to="/">{{categoryCreateStrings.dashboard}}</router-link></li>
                <li class="breadcrumb-item"><router-link to="/categories">{{categoryCreateStrings.categories}}</router-link></li>
                <li class="breadcrumb-item active" aria-current="page">{{categoryCreateStrings.addCategory}}</li>
            </ol>
        </nav>
        <h4 class="mb-1 mt-0">{{categoryCreateStrings.addCategory}}</h4>
    </div>
  </div>
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body"> 
         <category-form ></category-form >
        </div>
      </div>
    </div>
  </div>

</div>
`,
};
export { CreateCategory };
