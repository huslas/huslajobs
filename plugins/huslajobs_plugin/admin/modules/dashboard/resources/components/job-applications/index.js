import {LoadingBar} from "../LoadingBar.js";
import {LoadingError} from "../LoadingError.js";
import {Pagination} from "../Pagination.js";
import {ReusableFunctions} from "../../../../../../js/functions.js";

const Index = {
    data() {
        return {
            applications: [],
            loading: true,
            error: false,
            router: {},
            pages: 0,
            page: 1,
            homeUrl:home_url,
            applicationStrings:VueUiStrings.adminBackend.jobApplications
        };
    },
    components: {LoadingBar, LoadingError, Pagination},
    mounted() {
        console.log("Some what");
        console.log("Here we are ");
        this.router = VueRouter.useRouter()
        this.getApplications();
    },
    mixins:[ReusableFunctions],
    methods: {
        getApplications() {
            this.loading = true
            const data = new FormData();
            data.append("action", "get_applications");
            data.append("page", this.page);
            data.append("perPage", jobs_per_page);
            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: {"Content-Type": "multipart/form-data"},
            })
                .then(function (response) {
                    that.applications = response.data.data;
                    that.pages = response.data.totalPages;
                    that.loading = false
                    that.error = false;
                })
                .catch(function (error) {
                    console.log(error);
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error(that.applicationStrings.errors.errorOccurred);
                    }
                    that.loading = false
                    that.error = true;
                });
        },
        goToPage(page) {
            this.page = page
            this.getApplications();
        }
    },
    template: `<div>
    <loading-bar v-if="loading"></loading-bar>
    <div v-if="!loading">
        <div class="row page-title">
            <div class="col-md-12">
                <nav aria-label="breadcrumb" class="float-right mt-1">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <router-link to="/" href="#">{{applicationStrings.dashboard}}</router-link>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{applicationStrings.jobApplications}}</li>
                    </ol>
                </nav>
                <h4 class="mb-1 mt-0">{{applicationStrings.jobApplications}}</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div v-if="error">
                            <loading-error @update="getApplications"></loading-error>
                        </div>
                        <div v-else>

<div class="hs-table-wrapper">
                            <table class="table table-striped thead-dark mt-2">
                                <tr>
                                    <th>{{applicationStrings.tableHeaders.applicantName}}</th>
                                    <th>{{applicationStrings.tableHeaders.applicantEmail}}</th>
                                    <th>{{applicationStrings.tableHeaders.address}}</th>
                                    
                                    <th>{{applicationStrings.tableHeaders.job}}</th>
                                    <th>{{applicationStrings.tableHeaders.dateApplied}}</th>
                                </tr>
                                <tr v-for="application of applications">
                                    <td>{{application.applicant_name}}</td>
                                    <td>{{application.applicant_email}}</td>
                                    <td>{{application.address}}</td>
                                    <td><a :href="homeUrl+'/jobs/'+application.job_slug">{{homeUrl+'/jobs/'+application.job_slug}}{{decodeHtml(application.job_name)}} </a></td>
                                    <td>{{application.created_at}}</td>
                                </tr>
                            </table>
                            </div>
                            <pagination :page="page" :pages="pages" @update="goToPage"></pagination>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
`,
};

export {Index};
