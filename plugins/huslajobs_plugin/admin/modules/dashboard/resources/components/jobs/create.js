import { JobForm } from "./add-form.js";

const CreateJob = {
  props: {
    formParams: {
      type: Object,
      default: function () {
        return {
          id: undefined,
          name: undefined,
          description: undefined,

        };
      },
    },
  },
  data(){
    return{
      createJobStrings: VueUiStrings.adminBackend.jobs.create
    }
  },

  components: {
    JobForm,
  },

  template: `

<div class="module-content-wrapper">
 
    <div class="row page-title">
        <div class="col-md-12">
            <nav  aria-label="breadcrumb" class="float-right mt-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><router-link to="/">{{createJobStrings.dashboard.toString()}}</router-link></li>
                    <li class="breadcrumb-item"><router-link to="/jobs">{{createJobStrings.jobs.toString()}}</router-link></li>
                    <li class="breadcrumb-item active" aria-current="page">{{createJobStrings.addJob.toString()}}</li>
                </ol>
            </nav>
            <h4 class="mb-1 mt-0">{{createJobStrings.addJob.toString()}}</h4>
        </div>
    </div>
    <job-form></job-form>
  
</div>
`,
};
export { CreateJob };
