import { LoadingBar } from "../LoadingBar.js";
import { LoadingError } from "../LoadingError.js";
import { Pagination } from "../Pagination.js";
import {ReusableFunctions} from "../../../../../../js/functions.js";

const Jobs = {
  props:{
    active_account_id:{
      required:false,
    }
  },
  emits:['account_jobs'],
  data() {
    return {
      jobs: undefined,
      page: 1,
      perPage: jobs_per_page,
      loading: true,
      error: false,
      deleting: false,
      pages: 0,
      sortField: 'id',
      sort: 'desc',
      searchText:'',
      account_id: this.active_account_id,
      searchFields: "name,description,location,job_type",
      homeUrl:home_url,
      jobIndexStrings: VueUiStrings.adminBackend.jobs.index
    };
  },
  mixins:[ReusableFunctions],
  components: { LoadingBar, LoadingError, Pagination },
  mounted() {
    //todo get all
    this.getAllJobs();
  },

  watch: {
    searchText: function (current) {
      if (current === ''){
        this.search();
      }
    },
  },


  methods: {
    search() {
      this.page = 1;
      this.sortField = 'id';
      this.sort = 'desc';
      this.getAllJobs();
    },
    sortBy(field) {
      if (this.sortField == field) {
        this.sort = this.sort == 'desc' ? 'asc' : 'desc'
      } else {
        this.sort = 'asc';
        this.sortField = field;
      }
      this.page = 1;
      this.getAllJobs();
    },
    isSortedBy(field, sort = 'asc') {
      switch (sort) {
        case 'asc':
          return this.sortField == field && sort == this.sort;
        case 'desc':
          return this.sortField == field && sort == this.sort;
        default:
          return true;
      }
    },
    deleteJob(job) {
      const confirm_delete = confirm(this.jobIndexStrings.confirm +" " + job.name + "?");
      if (confirm_delete) {
        this.loading = true;
        const data = new FormData();
        data.append("action", "delete_job");
        data.append("job_id", job.id);
        const that = this;
        axios({
          method: "post",
          url: ajaxurl,
          data: data,
          headers: { "Content-Type": "multipart/form-data" },
        })
          .then(function (response) {
            toastr.success(job.name + " deleted");
            if (that.active_account_id){
              localStorage.setItem('defaultHeader',JSON.stringify('jobs'));
            }
            that.getAllJobs()

          })
          .catch(function (error) {
            console.log(error);
            that.loading = false;

            if (error.response) {
              toastr.error(error.response.data.data);
            } else {
              toastr.error(that.jobIndexStrings.errors.errorOccurred);
            }
          });
      }
    },
    goToPage(page) {
      this.page = page
      this.getAllJobs();
    }
  },
  template: `<div class="module-content-wrapper">
    <loading-bar v-if="loading" :class="{'relative-parent':active_account_id}"></loading-bar>
    <div v-if="!loading">
        <div v-if="!account_id" class="row page-title">
            <div class="col-md-12">
                <nav aria-label="breadcrumb" class="float-right mt-1">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <router-link to="/">{{jobIndexStrings.dashboard}}</router-link>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{jobIndexStrings.jobs}}</li>
                    </ol>
                </nav>
                <h4 class="mb-1 mt-0">{{jobIndexStrings.jobs}}</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div v-if="error">
                            <loading-error @update="getAllJobs"></loading-error>
                        </div>
                        <div v-else>
                            <div v-if="jobs?.length > 0">
                            <div class="d-flex justify-content-between align-items-center mb-4">
                                <form class="form-inline mt-2">
                                    <div class="form-group mb-2">
                                        <input type="text" class="form-control"
                                               :placeholder="jobIndexStrings.formFields.searchText.placeholder"
                                               v-model="searchText">
                                    </div>
                                    <button type="submit" class="hs-btn hs-btn-primary ml-2 mb-2 btn-sm"
                                            @click="search()">

                                        {{jobIndexStrings.formFields.button}}
                                    </button>
                                </form>
                                <a v-if="!account_id" :href="homeUrl+'/post-job'" class="hs-btn hs-btn-primary">
                                    {{jobIndexStrings.addJob}}
                                </a>
                            </div>
                                <div class="hs-table-wrapper">
                                    <table class="table table-striped">
                                        <tr>
                                            <th @click="sortBy('name')" style="cursor:pointer">
                                                {{jobIndexStrings.tableHeaders.name}}
                                                <i class="fa fa-fw"
                                                   v-bind:class="{'fa-sort-down':isSortedBy('name','desc'),'fa-sort-up':isSortedBy('name','asc'),'fa-sort':isSortedBy('name','normal')}"></i>
                                            </th>
                                            <th>{{jobIndexStrings.tableHeaders.jobType}}</th>
                                            <th>{{jobIndexStrings.tableHeaders.country}}</th>
                                            <th @click="sortBy('salary')" style="cursor:pointer">
                                                {{jobIndexStrings.tableHeaders.salary}}
                                                <i class="fa fa-fw"
                                                   v-bind:class="{'fa-sort-down':isSortedBy('salary','desc'),'fa-sort-up':isSortedBy('salary','asc'),'fa-sort':isSortedBy('salary','normal')}"></i>
                                            </th>
                                            <th>{{jobIndexStrings.tableHeaders.actions}}</th>
                                        </tr>
                                        <tr v-for="job of jobs">
                                            <td><a :href="'/jobs/'+job.slug"
                                                   target="_blank">{{decodeHtml(job.name)}}</a></td>
                                            <td><span>{{decodeHtml(job.job_type_name) ||job.crawled_job_types}}</span>
                                            </td>
                                            <td>{{selectedCountry(job.country || job.crawled_job_location)}}</td>
                                            <td>
                                                <div v-if="job.salary !== 'undefined'">
                                                    <span>{{job.salary}}</span>
                                                    <span>{{job.currency_code}}</span>
                                                </div>
                                                <div v-else>-</div>
                                            </td>
                                            <td>
                                                <router-link :to="'/jobs/'+job.slug+'/edit'"
                                                             class="btn mr-2 mb-2 btn-primary btn-sm"><i
                                                        class="fa fa-pencil-alt fa-2x"></i>
                                                </router-link>
                                                <button class="btn btn-danger btn-sm mb-2" @click="deleteJob(job)"><i
                                                        class="fa fa-trash fa-2x"></i></button>
                                            </td>
                                        </tr>
                                    </table>

                                </div>
                                <pagination v-if="pages > 1" :page="page" :pages="pages"
                                            @update="goToPage">

                                </pagination>
                            </div>
                            <div v-else-if="jobs?.length == 0" class="d-flex flex-column align-items-center justify-content-center">
                                <p>{{jobIndexStrings.noJobs}}</p>
                                <router-link v-if="!account_id" to="/jobs/create" class="hs-btn hs-btn-primary">
                                    {{jobIndexStrings.addJob}}
                                </router-link>
                                <a v-else :href="homeUrl+'/post-job'" class="hs-btn hs-btn-primary">
                                    {{jobIndexStrings.postJob}}
                                </a>
                            </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
`,
};
export { Jobs };
