import {LoadingBar} from "../LoadingBar.js";
import {LoadingError} from "../LoadingError.js";
import {Pagination} from "../Pagination.js";
import {ReusableFunctions} from "../../../../../../js/functions.js";

const Accounts = {
    props: {
        type: {
            required: true,
            default: 'seeker'
        }
    },
    data() {
        return {
            accounts: [],
            account_type: '',
            loading: true,
            error: false,
            router: {},
            pages: 0,
            page: 1,
            sortField: 'id',
            sort: 'desc',
            searchField: 'name',
            searchText: '',
            accountsStrings: VueUiStrings.adminBackend.accounts
        };
    },
    mixins:[ReusableFunctions],
    components: {LoadingBar, LoadingError, Pagination},
    mounted() {

        this.router = VueRouter.useRouter()
        this.account_type = this.type
        this.getAccounts();

    },
    methods: {
        search() {
            this.page = 1;
            this.sortField = 'id';
            this.sort = 'desc';
            this.getAccounts();
        },
        sortBy(field) {
            if (this.sortField == field) {
                this.sort = this.sort == 'desc' ? 'asc' : 'desc'
            } else {
                this.sort = 'asc';
                this.sortField = field;
            }
            this.page = 1;
            this.getAccounts();
        },
        isSortedBy(field, sort = 'asc') {
            switch (sort) {
                case 'asc':
                    return this.sortField == field && sort == this.sort;
                case 'desc':
                    return this.sortField == field && sort == this.sort;
                default:
                    return true;
            }
        },
        getAccounts() {
            this.loading = true
            const data = new FormData();
            data.append("action", "get_accounts");
            data.append("type", this.account_type);
            data.append("page", this.page);
            data.append("perPage", jobs_per_page);
            data.append("sortBy", this.sortField);
            data.append("order", this.sort);
            data.append("searchText", this.searchText);
            data.append("searchField", this.searchField);

            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: {"Content-Type": "multipart/form-data"},
            })
                .then(function (response) {
                    that.accounts = response.data.data;
                    that.pages = response.data.totalPages;
                    that.loading = false
                    that.error = false;
                })
                .catch(function (error) {
                    console.log(error);
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error(that.accountsStrings.errors.errorOccurred);
                    }
                    that.loading = false
                    that.error = true;
                });
        },
        deleteAccount(Account) {
            const confirm_delete = confirm(this.accountsStrings.confirm +" " + Account.first_name + " " + Account.last_name + "'s "+ this.accountsStrings.account +"?")
            if (confirm_delete) {
                this.loading = true
                const data = new FormData();
                data.append("action", "delete_account");
                data.append("account_id", Account.id);
                const that = this;
                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {
                        // console.log(response);
                        toastr.success(that.accountsStrings.successMessage.accountDeleted);
                        that.getAccounts();
                        that.loading = false
                    })
                    .catch(function (error) {
                        console.log(error);
                        that.loading = false

                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.accountsStrings.errors.errorOccurred);
                        }

                    });
            }
        },
        goToPage(page) {
            this.page = page
            this.getAccounts();
        }
    },
    template: `<div>
    <loading-bar v-if="loading"></loading-bar>
    <div v-if="!loading">
        <div class="row page-title">
            <div class="col-md-12">
                <nav aria-label="breadcrumb" class="float-right mt-1">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item">
                            <router-link to="/" href="#">{{accountsStrings.dashboard.toString()}}</router-link>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page">{{accountsStrings.accounts.toString()}}</li>
                    </ol>
                </nav>
                <h4 class="mb-1 mt-0">{{accountsStrings.accounts.toString()}}</h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div v-if="error">
                            <loading-error @update="getAccounts"></loading-error>
                        </div>
                        <div v-else>
                            <div class="form-inline">
                                <div class="input-group mb-2 mr-2">
                                    <select class="form-control" v-model="searchField">
                                        <option value="name">{{accountsStrings.formFields.name.label.toString()}}</option>
                                        <option value="email">{{accountsStrings.formFields.email.label.toString()}}</option>
                                    </select>
                                </div>
                                <div class="form-group mb-2 mr-2">
                                    <input type="text" class="form-control"
                                           :placeholder="accountsStrings.formFields.searchText.placeholder" v-model="searchText">
                                </div>
                                <button type="submit" class="hs-btn hs-btn-primary mb-2" @click="search()">{{accountsStrings.formFields.button.toString()}}
                                </button>
                            </div>
                            <div class="hs-table-wrapper">
                                <table class="table table-striped thead-dark mt-2">
                                    <tr>
                                        <th @click="sortBy('id')" style="cursor:pointer">
                                            {{accountsStrings.tableHeaders.id.toString()}}
                                            <i class="fa fa-fw"
                                               v-bind:class="{'fa-sort-down':isSortedBy('id','desc'),'fa-sort-up':isSortedBy('id','asc'),'fa-sort':isSortedBy('id','normal')}"></i>
                                        </th>
                                        <th @click="sortBy('first_name')" style="cursor:pointer">
                                            {{accountsStrings.tableHeaders.name.toString()}}
                                            <i class="fa fa-fw"
                                               v-bind:class="{'fa-sort-down':isSortedBy('first_name','desc'),'fa-sort-up':isSortedBy('first_name','asc'),'fa-sort':isSortedBy('first_name','normal')}"></i>
                                        </th>
                                        <th @click="sortBy('last_name')" style="cursor:pointer">
                                            {{accountsStrings.tableHeaders.email.toString()}}
                                            <i class="fa fa-fw"
                                               v-bind:class="{'fa-sort-down':isSortedBy('last_name','desc'),'fa-sort-up':isSortedBy('last_name','asc'),'fa-sort':isSortedBy('last_name','normal')}"></i>
                                        </th>
                                        <th @click="sortBy('email')" style="cursor:pointer">
                                            {{accountsStrings.tableHeaders.country.toString()}}
                                            <i class="fa fa-fw"
                                               v-bind:class="{'fa-sort-down':isSortedBy('email','desc'),'fa-sort-up':isSortedBy('email','asc'),'fa-sort':isSortedBy('email','normal')}"></i>
                                        </th>
                                        <th @click="sortBy('email')" style="cursor:pointer">
                                            {{accountsStrings.tableHeaders.state.toString()}}
                                            <i class="fa fa-fw"
                                               v-bind:class="{'fa-sort-down':isSortedBy('email','desc'),'fa-sort-up':isSortedBy('email','asc'),'fa-sort':isSortedBy('email','normal')}"></i>
                                        </th>
                                        <th @click="sortBy('email')" style="cursor:pointer">
                                           {{ accountsStrings.tableHeaders.phoneNumber}}
                                            <i class="fa fa-fw"
                                               v-bind:class="{'fa-sort-down':isSortedBy('email','desc'),'fa-sort-up':isSortedBy('email','asc'),'fa-sort':isSortedBy('email','normal')}"></i>
                                        </th>
                                        <th>{{accountsStrings.tableHeaders.actions}}</th>
                                    </tr>
                                    <tr v-for="account of accounts">
                                        <td>{{account.id}}</td>
                                        <td>{{account.name }}</td>
                                        <td>{{account.email}}</td>
                                        <td>{{selectedCountry(account.country)}}</td>
                                        <td>{{selectedCountry(account.state)}}</td>
                                          <td>{{selectedCountry(account.phone_number)}}</td>
                                        <td>
                                            <router-link :to="'/accounts/'+account.id"
                                                         class="btn mr-2 mb-2 btn-primary btn-sm">
                                                <i class="fa fa-eye fa-2x"></i>
                                            </router-link>
                                            <router-link :to="'/accounts/'+account.id+'/edit'"
                                                         class="btn mr-2 mb-2  btn-primary btn-sm"><i
                                                    class="fa fa-pencil-alt fa-2x"></i>
                                            </router-link>
                                            <button class="btn btn-danger mb-2 btn-sm" @click="deleteAccount(account)"><i
                                                    class="fa fa-trash fa-2x"></i></button>
                                        </td>

                                    </tr>
                                </table>
                            </div>
                            <pagination :page="page" :pages="pages" @update="goToPage"></pagination>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
`,
};

export {Accounts};
