import { LoadingBar } from "../LoadingBar.js";
import { LoadingError } from "../LoadingError.js";
import { Pagination } from "../Pagination.js";
import {ReusableFunctions} from "../../../../../../js/functions.js";

const Currencies = {
    data() {
        return {
            currencies:undefined,
            page: 1,
            perPage: jobs_per_page,
            pages: 0,
            loading: true,
            error: false,
            sortField: 'id',
            sort: 'desc',
            searchText:'',
            currencyIndexStrings: VueUiStrings.adminBackend.currency.index

        };
    },
    components: { LoadingBar, LoadingError, Pagination },
    mounted() {
        //todo get all
        this.getCurrencies();
    },
    mixins:[ReusableFunctions],
    watch: {
        searchText: function (current) {
            if (current === ''){
                this.search();
            }
        },
    },
    methods: {
        search() {
            this.page = 1;
            this.sortField = 'id';
            this.sort = 'desc';
            this.getCurrencies();
        },
        getCurrencies() {
            this.loading = true;
            const data = new FormData();
            data.append("searchText", this.searchText);
            data.append("action", "get_currencies");
            data.append("page", this.page);
            data.append("perPage", this.perPage);
            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: { "Content-Type": "multipart/form-data" },
            })
                .then(function (response) {
                    that.currencies = response.data.data;
                    that.pages = response.data.totalPages;
                    that.loading = false;
                    that.error = false;
                })
                .catch(function (error) {
                    console.log(error);
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error(that.currencyIndexStrings.errors.errorOccurred);
                    }
                    that.loading = false;
                    that.error = true;
                });
        },
        deleteCurrency(currency) {
            const confirm_delete = confirm(this.currencyIndexStrings.confirm+" " + currency.name + "?");
            if (confirm_delete) {
                this.loading = true;
                const data = new FormData();
                data.append("action", "delete_currency");
                data.append("currency_id", currency.id);
                const that = this;
                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: { "Content-Type": "multipart/form-data" },
                })
                    .then(function (response) {
                        toastr.success(currency.name + " " + that.currencyIndexStrings.deleted);
                        that.getCurrencies();
                        that.loading = false;
                    })
                    .catch(function (error) {
                        console.log(error);
                        that.loading = false;
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(that.currencyIndexStrings.errors.errorOccurred);
                        }
                    });
            }
        },
        goToPage(page) {
            this.page = page
            this.getCurrencies();
        }
    },
    template: `
  <div class="module-content-wrapper">
    <loading-bar v-if="loading"></loading-bar>
    <div v-if="!loading">
      <div class="row page-title">
        <div class="col-md-12">
            <nav  aria-label="breadcrumb" class="float-right mt-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><router-link to="/">{{currencyIndexStrings.dashboard}}</router-link></li>
                    <li class="breadcrumb-item active" aria-current="page">{{currencyIndexStrings.currencies}}</li>
                </ol>
            </nav>
            <h4 class="mb-1 mt-0">{{currencyIndexStrings.currencies}}</h4>
        </div>
      </div>
       <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body"> 
                  <div v-if="error">
                    <loading-error @update="getCurrencies"></loading-error>
                  </div>
                  <div v-else>
                         <div class="d-flex justify-content-between align-items-center mb-4">
                            <form class="form-inline mt-2">
                                <div class="form-group mb-2">
                                    <input type="text" class="form-control"
                                           :placeholder="currencyIndexStrings.formFields.searchText.placeholder" v-model="searchText">
                                </div>
                                <button type="submit" class="hs-btn hs-btn-primary ml-2 mb-2 " @click="search()">
                                    {{currencyIndexStrings.formFields.button}}
                                </button>
                            </form>
                    <router-link to="/currencies/create" class="hs-btn hs-btn-primary mb-4 float-right">{{currencyIndexStrings.addCurrency}}</router-link>

                        
                        </div>
                    <div v-if="currencies?.length > 0" class="hs-table-wrapper">
                        <table class="table table-striped">
                            <tr>
                                <th>{{currencyIndexStrings.tableHeaders.id}}</th>
                                <th>{{currencyIndexStrings.tableHeaders.name}}</th>
                                <th>{{currencyIndexStrings.tableHeaders.code}}</th>
                                <th>{{currencyIndexStrings.tableHeaders.actions}}</th>
                            </tr>
                            <tr v-for="(currency,index) of currencies" :key="index">
                                <td>{{currency.id}}</td>
                                <td>{{decodeHtml(currency.name)}}</td> 
                                <td>{{decodeHtml(currency.code)}}</td>
                                <td>
                                  <router-link :to="'/currencies/'+currency.id+'/edit'" class="btn mr-2 mb-2 btn-primary btn-sm"><i class="fa fa-pencil-alt fa-2x"></i>
                                  </router-link>
                                  <button class="btn btn-danger btn-sm"  @click="deleteCurrency(currency)" ><i class="fa fa-trash fa-2x"></i></button>
                                </td>
                            </tr>
                        </table>
                        
                    </div>
                           <div v-else-if="currencies?.length == 0">
                      <p>{{decodeHtml(currencyIndexStrings.noCurrencies)}}</p>
                    </div>
                    <pagination v-if="pages > 1" :page="page" :pages="pages" @update="goToPage"></pagination>

                  </div>
                </div>
            </div>
        </div>
      </div>  
    </div> 
</div>
`,
};
export { Currencies };
