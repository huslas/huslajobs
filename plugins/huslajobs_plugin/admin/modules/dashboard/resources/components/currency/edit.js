import { CurrencyForm } from "./add-form.js";
import { LoadingBar } from "../LoadingBar.js";
import { LoadingError } from "../LoadingError.js";

const EditCurrency = {
    data() {
        return {
            loading: true,
            error: false,
            form: {},
            currencyEditStrings: VueUiStrings.adminBackend.currency.edit
        };
    },
    components: {
        CurrencyForm,
        LoadingBar,
        LoadingError,

    },

    mounted() {
        //todo get all
        this.getCurrency();
    },
    methods: {
        getCurrency() {
            let currencyId = this.$route.params.id;
            const data = new FormData();
            data.append("currency_id", currencyId);
            data.append("action", "get_currency");

            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: { "Content-Type": "multipart/form-data" },
            })
                .then(function (response) {
                    that.form = response.data;
                    that.loading = false;
                    that.error = false;
                })
                .catch(function (error) {
                    console.log(error);
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error(that.currencyEditStrings.errors.errorOccurred);
                    }
                    that.loading = false;
                    that.error = true;
                });
        },
    },
    template: `
  <div class="module-content-wrapper">
      <loading-bar v-if="loading"></loading-bar>
      <div v-if="!loading">
        <div class="row page-title">
          <div class="col-md-12">
              <nav  aria-label="breadcrumb" class="float-right mt-1">
                  <ol class="breadcrumb">
                      <li class="breadcrumb-item"><router-link to="/">{{currencyEditStrings.dashboard}}</router-link></li>
                      <li class="breadcrumb-item"><router-link to="/currencies">{{currencyEditStrings.currencies}}</router-link></li>
                      <li class="breadcrumb-item active" aria-current="page">{{currencyEditStrings.editCurrency}}</li>
                  </ol>
              </nav>
              <h4 class="mb-1 mt-0">{{currencyEditStrings.editCurrency}}</h4>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-body"> 
                <div v-if="error">
                  <loading-error @update="getCurrency"></loading-error>
                </div>
                <div v-else>
                  <div v-if="Object.keys(form).length > 0">
                      <currency-form :formParams="form"></currency-form >
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
`,
};
export { EditCurrency };
