import { JobTypeForm } from "./add-form.js";

const CreateJobType = {
  props: {
    formParams: {
      type: Object,
      default: function () {
        return { id: undefined,
          name: undefined,
          description: undefined ,

        };
      },
    },
  },
  data(){
    return{
      createJobTypeStrings: VueUiStrings.adminBackend.jobTypes.create
    }
  },
  components: {
    JobTypeForm,
  },

  template: `

<div class="module-content-wrapper">
  <div class="row page-title">
    <div class="col-md-12">
        <nav  aria-label="breadcrumb" class="float-right mt-1">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><router-link to="/">{{createJobTypeStrings.dashboard}}</router-link></li>
                <li class="breadcrumb-item"><router-link to="/job-types">{{createJobTypeStrings.jobTypes}}</router-link></li>
                <li class="breadcrumb-item active" aria-current="page">{{createJobTypeStrings.addJobType}}</li>
            </ol>
        </nav>
        <h4 class="mb-1 mt-0">{{createJobTypeStrings.addJobType}}</h4>
    </div>
  </div>
  <div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-body"> 
         <job-type-form ></job-type-form >
        </div>
      </div>
    </div>
  </div>

</div>
`,
};
export { CreateJobType };
