import { JobTypeForm } from "./add-form.js";
import { LoadingBar } from "../LoadingBar.js";
import { LoadingError } from "../LoadingError.js";

const EditJobType = {
  data() {
    return {
      loading: true,
      error: false,
      form: {},
      editJobTypeStrings: VueUiStrings.adminBackend.jobTypes.edit
    };
  },
  components: {
    JobTypeForm,
    LoadingBar,
    LoadingError,
  },

  mounted() {
    //todo get all
    this.getJobType();
  },
  methods: {
    getJobType() {
      let jobTypeId = this.$route.params.id;
      const data = new FormData();
      data.append("id", jobTypeId);
      data.append("action", "get_job_type");

      const that = this;
      axios({
        method: "post",
        url: ajaxurl,
        data: data,
        headers: { "Content-Type": "multipart/form-data" },
      })
        .then(function (response) {
          that.form = response.data;
          that.loading = false;
          that.error = false;
        })
        .catch(function (error) {
          console.log(error);


          if (error.response) {
            toastr.error(error.response.data.data);
          } else {
            toastr.error(that.editJobTypeStrings.errors.errorOccurred);
          }
          that.loading = false;
          that.error = true;
        });
    },
  },
  template: `
  <div class="module-content-wrapper">
      <loading-bar v-if="loading"></loading-bar>
      <div v-if="!loading">
        <div class="row page-title">
          <div class="col-md-12">
              <nav  aria-label="breadcrumb" class="float-right mt-1">
                  <ol class="breadcrumb">
                      <li class="breadcrumb-item"><router-link to="/">{{editJobTypeStrings.dashboard}}</router-link></li>
                      <li class="breadcrumb-item"><router-link to="/job-types">{{editJobTypeStrings.jobTypes}}</router-link></li>
                      <li class="breadcrumb-item active" aria-current="page">{{editJobTypeStrings.editJobType}}</li>
                  </ol>
              </nav>
              <h4 class="mb-1 mt-0">{{editJobTypeStrings.editJobType}}</h4>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-body"> 
                <div v-if="error">
                  <loading-error @update="getAllJobTypes"></loading-error>
                </div>
                <div v-else>
                  <div v-if="form.name && form.description">
                      <job-type-form :formParams="form"></job-type-form >
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  </div>
`,
};
export { EditJobType };
