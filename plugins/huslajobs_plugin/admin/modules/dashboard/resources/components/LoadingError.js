const LoadingError = {
    data() {
        return {
            theme_url: THEME_URL,
            loadingErrorStrings: VueUiStrings.adminBackend.loadingError
        }
    },
    methods: {
        update() {
            this.$emit('update')
        }
    },
    emits: ['update'],
    template: `
<div class="min-vh-100 d-flex align-items-center ">
<div class="text-center alert alert-danger ">
    {{loadingErrorStrings.errorOccurred.toString()}}. <button class="btn btn-success btn-sm" @click="update">
      {{loadingErrorStrings.tryAgain.toString()}}
    </button>
  </div>
  
  </div>
`
}

export {LoadingError};