const LoadingBar = {
    data() {
        return {
            theme_url: THEME_URL
        }
    },
    template: `<div class="hs-loader d-flex align-items-center">

    <div class="loading-content">
        <img :src="theme_url+'/logo.png'" style="animation-name: km-img-loading;
          animation-duration: 2s;
          animation-iteration-count: infinite;
          width: 110px !important;
"/>
        <h5 class="hs-primary-text-color">Loading...</h5>

    </div>
</div>
`
}

export {LoadingBar};