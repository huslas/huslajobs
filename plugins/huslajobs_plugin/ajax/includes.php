<?php

/**
 * Add ajax functions to be included
 */


add_filter( 'husla_jobs_includes_filter', function ( $includes ) {
	$files = [
		HUSLA_JOBS_AJAX_DIR . '/modules/users.php', //
		HUSLA_JOBS_AJAX_DIR . '/modules/accounts.php', //
		HUSLA_JOBS_AJAX_DIR . '/modules/packages.php', //
		HUSLA_JOBS_AJAX_DIR . '/modules/subscriptions.php', //
		HUSLA_JOBS_AJAX_DIR . '/modules/jobs.php', //
		HUSLA_JOBS_AJAX_DIR . '/modules/currencies.php', //
		HUSLA_JOBS_AJAX_DIR . '/modules/categories.php', //
		HUSLA_JOBS_AJAX_DIR . '/modules/profiles.php', //
	];

	$includes = array_merge( $includes, $files );

	return $includes;
} );