<?php

namespace huslajobs;

// Actions
add_action( 'wp_ajax_get_subscriptions', 'huslajobs\getSubscriptions' );

// Methods
function getSubscriptions() {
    global $wpdb;
	$page          = intval( $_POST['page'] );
	$per_page      = intval( $_POST['perPage'] );
	$sort_by       = $_POST['sortBy'] ?? 'id';
	$order         = $_POST['order'] ?? 'desc';

    $users_table = $wpdb->prefix . 'users';
    $subscriptions = Subscription::paginate($per_page, $page)->orderBy($sort_by, $order )
        ->leftJoin('packages')
        ->on('package_id', 'id')
        ->leftJoin($users_table)
        ->on('wp_user_id', 'ID')
        ->get([
            Subscription::tableName() . '.*',
            $users_table . '.first_name',
            $users_table . '.last_name',
            Package::tableName() . '.name AS package_name',
            Package::tableName() . '.price AS package_price'
        ]);
//	$results       = Subscription::paginate( $per_page, $page )->orderBy( $sort_by, $order )->get();
//	$subscriptions = $results['data'];
//	for ( $i = 0; $i < sizeof( $subscriptions ); $i ++ ) {
//		$user                         = $subscriptions[ $i ]->user();
//		$package                      = $subscriptions[ $i ]->package();
//		$subscriptions[ $i ]->user    = $user;
//		$subscriptions[ $i ]->package = $package;
//	}
//	$results['data'] = $subscriptions;
	echo json_encode( $subscriptions );
	wp_die();
}