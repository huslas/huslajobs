<?php

namespace huslajobs;

// Actions
add_action( 'wp_ajax_get_currencies', 'huslajobs\getCurrencies' );
add_action( 'wp_ajax_nopriv_get_currencies', 'huslajobs\getCurrencies' );
add_action( 'wp_ajax_save_currency', 'huslajobs\saveCurrency' );
add_action( 'wp_ajax_get_currency', 'huslajobs\getCurrency' );
add_action( 'wp_ajax_update_currency', 'huslajobs\updateCurrency' );
add_action( 'wp_ajax_delete_currency', 'huslajobs\deleteCurrency' );


// Methods

function getCurrencies() {
	$page         = intval( $_POST['page'] );
	$search_text   = $_POST['searchText'];
	$per_page     = intval( $_POST['perPage'] );
	$sort_by      = $_POST['sortBy'] ?? 'id';
	$order        = $_POST['order'] ?? 'desc';

	$results = Currency::paginate( $per_page, $page )->orderBy( $sort_by, $order );
	if ( trim( $search_text ) != '') {
        $results->where( 'name', 'like', "'%" . $search_text . "%'" );
        $results->orWhere( 'code', 'like', "'%" . $search_text . "%'" );
	}
	echo json_encode( $results->get() );
	wp_die();
}

function saveCurrency() {

	if ( HuslaValidator::validate(
		[
			'name'   => 'required',
			'code'   => 'required',
		], $_POST ) ) {
		$code            = $_POST['code'];
		$currency_exists = Currency::where( 'code', 'like', "'" . $code . "'" )->get();

		if ( sizeof( $currency_exists ) > 0 ) {
			wp_send_json_error( __('A currency with this code already exists','huslajobs'), 400 );
		} else {
            $name             = stripslashes(esc_attr($_POST['name']));
            $code             = stripslashes(esc_attr($_POST['code']));
			$currency         = new Currency();
			$currency->code   = $code;
			$currency->name   = $name;
			echo json_encode( $currency->save() );
		}
	}
	wp_die();
}

function getCurrency() {
	$id       = intval( $_POST['currency_id'] );
	$currency = Currency::find( $id );
	if ( $currency ) {
		echo json_encode( $currency );
	} else {
		wp_send_json_error( __('Currency not found','huslajobs'), 400 );
	}
	wp_die();
}

function updateCurrency() {
	if ( HuslaValidator::validate( [
		'currency_id' => 'required|numeric',
		'name'        => 'required',
		'code'        => 'required'
	], $_POST ) ) {
		$id               = intval( $_POST['currency_id'] );
		$name             = stripslashes(esc_attr($_POST['name']));
		$code             = stripslashes(esc_attr($_POST['code']));
		$currency         = Currency::find( $id );
		$currency->code   = $code;
		$currency->name   = $name;

		if ( $currency->save() ) {
			echo "updated";
		} else {
			wp_send_json_error( __('An error occurred, please try again or contact the admin','huslajobs'), 400 );
		}
	}
	wp_die();
}

function deleteCurrency() {
	$id       = intval( $_POST['currency_id'] );
	$currency = Currency::find( $id );
	if ( sizeof( $currency->packages() ) > 0 ) {
		wp_send_json_error( __( 'Currency can not be deleted since it is used as a package price currency', 'huslajobs' ), 400 );
	} elseif ( sizeof( $currency->jobs() ) > 0 ) {
		wp_send_json_error( __( 'Currency can not be deleted since it is used as a job salary currency', 'huslajobs' ), 400 );
	} else {
		echo json_encode( $currency->delete() );
	}
	wp_die();
}