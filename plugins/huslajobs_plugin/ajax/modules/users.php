<?php

namespace huslajobs;

// Actions
use DateTime;
use WP_Locale_Switcher;

add_action('wp_ajax_hs_admin_dashboard_stats', 'huslajobs\adminDashboardStats');

add_action('wp_ajax_get_user_with_accounts', 'huslajobs\getUserWithAccount');
add_action('wp_ajax_nopriv_get_user_with_accounts', 'huslajobs\getUserWithAccount');

add_action('wp_ajax_get_user', 'huslajobs\getUser');
add_action('wp_ajax_nopriv_get_user', 'huslajobs\getUser');
add_action('wp_ajax_hs_log_out', 'huslajobs\hsLogout');
add_action('wp_ajax_nopriv_hs_log_out', 'huslajobs\hsLogout');

/**
 * =====================
 * LOGIN
 * =====================
 */
add_action("wp_ajax_user_login", 'huslajobs\userLogin');
add_action("wp_ajax_nopriv_user_login", 'huslajobs\userLogin');

add_action("wp_logout", 'huslajobs\redirectToLogin');
//add_action('wp_login', 'huslajobs\afterLogin', 99, 2);
add_action("init", 'huslajobs\redirectUsers');

/**
 * =====================
 * SIGNUP
 * =====================
 */
add_action("wp_ajax_user_registration", 'huslajobs\userRegistration');
add_action('wp_ajax_nopriv_user_registration', 'huslajobs\userRegistration');


add_action('user_register', 'huslajobs\userRegistered', 30, 1);

//            authenticate user
add_filter('authenticate', 'huslajobs\authenticateUser', 9999, 3);

/**
 * =====================
 * Language
 * =====================
 */

add_action("wp_ajax_hs_switch_language", 'huslajobs\switchLanguage');
add_action('wp_ajax_nopriv_hs_switch_language', 'huslajobs\switchLanguage');

/**
 * ====================
 * contact email
 * ====================
 *
 */
add_action("wp_ajax_contact_job_seeker", 'huslajobs\contactSeeker');
add_action('wp_ajax_nopriv_contact_job_seeker', 'huslajobs\contactSeeker');

add_action("wp_ajax_delete_user_account", 'huslajobs\deleteUserAccount');
add_action('wp_ajax_nopriv_delete_user_account', 'huslajobs\deleteUserAccount');


function getUserWithAccount()
{
    global $user_ID;
    $new_user = huslajobsGetUserInfo($user_ID);
    echo json_encode($new_user);
    wp_die();
}

function getUser()
{
    global $user_ID;
    $user_id = $_POST['user_id'] ? intval($_POST['user_id']) : $user_ID;
    $new_user =(array) huslajobsGetUser($user_id);
    echo json_encode($new_user);
    wp_die();
}


function redirectToLogin()
{
    wp_safe_redirect(home_url("/login"));
    exit;
}


function authenticateUser($errors, $username, $password)
{

    if (isset($errors->errors['incorrect_password'])) {
        return $errors;
    }
    if (!$username) {
        return $errors;
    }
    $user = get_user_by('email', $username);
    if (empty($user)) {
        $user = get_user_by('login', $username);
    }
    if (empty($user)) {
        return $errors;
    }

    $user_activation_status = get_user_meta($user->ID, 'user_activation_status', true);

    if ($user_activation_status == 0 && $user->ID != 1) {

        $number_of_logins = get_user_meta($user->ID, 'number_of_logins', true);
        if ($number_of_logins) {
            $login_limit = get_option('husla_jobs_maximum_login', true);
            if (intval($number_of_logins) == intval($login_limit)) {
                $message = apply_Filters(
                    'account_lock_message',
                    sprintf(
                        '<strong>%s</strong> %s',
                        __('Error:', 'huslajobs'),
                        __('Sorry you have exceeded your login limit,verify your email to login', 'huslajobs')
                    ),
                    $username
                );
                return new \WP_Error('authentication_failed', $message);
            } else {
                $number_of_logins = intval($number_of_logins) + 1;
                update_user_meta($user->ID, 'number_of_logins', $number_of_logins);
            }

        } else {
            update_user_meta($user->ID, 'number_of_logins', 1);
        }


    }

    $archive_user = ArchivedUser::where("wp_user_id","=", "'".$user->ID ."'")->get();
    if (sizeof($archive_user)){
        $message = apply_Filters(
            'account_lock_message',
            sprintf(
                '<strong>%s</strong> %s',
                __('Error:', 'huslajobs'),
                __("Sorry,you are not allowed to login", 'huslajobs')
            ),
            $username
        );
        return new \WP_Error('authentication_failed', $message);
    }

    return $errors;
}

function redirectUsers()
{
    global $pagenow;
    //check and delete user accounts
    $archived_users =  ArchivedUser::get();
    if (sizeof($archived_users)){
        $today = new DateTime('now');
        foreach ($archived_users as $archived_user) {
            $delete_date = new DateTime($archived_user->delete_date);
            if ($today > $delete_date) {
                $user_id = $archived_user->wp_user_id;
                $user= \huslajobs\huslajobsGetUser($user_id);
                if ($user){
                    $user->delete();
                    wp_delete_user($user_id);
                }
                $archived_user->delete();
            }
        }
    }
        //redirect users
    if (HUSLASJOBS_RECAPTCHA_SITE_KEY && HUSLASJOBS_RECAPTCHA_SECRETE_KEY) {
        if ($pagenow === "wp-login.php" && (!isset($_GET['action']) || ($_GET['action'] != 'logout'))) {
            wp_safe_redirect(home_url("/login"));
            exit;
        }

    }
}

function userLogin()
{
    include_once HUSLA_JOBS_INC_DIR . '/_google_recaptcha.inc';
    if (
        isset($_POST['login_nonce'])
        && wp_verify_nonce($_POST['login_nonce'], 'huslajobs-login')
    ) {

        $captcha = filter_input(INPUT_POST, 'token');
        $response = \huslajobs\verify_captcha($captcha);
        if ($response) {
            $password = esc_attr($_POST['password']);
            $email = esc_attr($_POST['email']);

            $creds = array(
                'user_login' => $email,
                'user_password' => $password,
                'remember' => true
            );
            $user_signon = wp_signon($creds, false);
            if (is_wp_error($user_signon)) {
                if ($user_signon->get_error_code() == 'incorrect_password') {
                    wp_send_json_error($user_signon->get_error_message('incorrect_password'), 400);
                } elseif ($user_signon->get_error_code() == 'invalid_username') {
                    wp_send_json_error(__('Wrong email', 'huslajobs'), 400);
                } else {
                    wp_send_json_error($user_signon->get_error_message(), 400);
                }

            } else {

                $response = ["message" => __('Successful, redirecting...', 'huslajobs')];
                $user = get_user_by('id', $user_signon->ID);
                wp_set_current_user($user_signon->ID);
                wp_set_auth_cookie($user_signon->ID);

                if (in_array('administrator', (array)$user->roles)) {
                    $response['redirect_to'] = home_url('/wp-admin');
                } else {
                    $response['redirect_to'] = home_url('/my-account');
                }
                //  get user_info
                $response['user_data'] = \huslajobs\huslajobsGetUserInfo($user->ID);
//          check if subscription has expired
                \huslajobs\afterLogin($user);

                echo json_encode($response);
            }
        }else{
            wp_send_json_error(__("Sorry,you are not allowed to login", 'huslajobs'), 400);

        }

    } else {
        wp_send_json_error(__("Sorry,you are not allowed to login", 'huslajobs'), 400);
    }

    wp_die();


}


function userRegistration()
{
    include_once HUSLA_JOBS_INC_DIR . '/_google_recaptcha.inc';

    if (get_option('users_can_register')) {
        $captcha = filter_input(INPUT_POST, 'token');
        $response = \huslajobs\verify_captcha($captcha);

        if ($response) {
            $first_name = stripslashes(sanitize_text_field($_POST['first_name']));
            $last_name = stripslashes(sanitize_text_field($_POST['last_name']));
            $profile_image = $_FILES['profile_image'];
            $email = stripslashes(sanitize_email($_POST['email']));
            $password = $_POST['password'];
            $profile_image = $profile_image ? \huslajobs\saveFile($profile_image)['url'] : $_POST['profile_image'];
            $bio = stripslashes(sanitize_text_field($_POST['bio']));
            $city = stripslashes(sanitize_text_field($_POST['city']));
            $country = stripslashes(sanitize_text_field($_POST['country']));
            $state = stripslashes(sanitize_text_field($_POST['state']));
            $date_of_birth = stripslashes(sanitize_text_field($_POST['dob']));
            $phone_number = stripslashes(sanitize_text_field($_POST['phone']));
//            $current_page = $_POST['current_page'];
//            $account_type = $current_page == 'post job' ? 'individual_recruiter' : get_option('husla_jobs_account_type');

            if ($_POST['user_id']) {

                $user_id = intval($_POST['user_id']);
                update_user_meta($user_id, 'first_name', $first_name);
                update_user_meta($user_id, 'last_name', $last_name);
                \huslajobs\updateUserTable($user_id, $bio, $city, $country, $state, $date_of_birth, $phone_number, $profile_image, $first_name, $last_name);
                echo json_encode(['message' => __('Updated successfully', 'huslajobs')]);

            } else {
                if (!is_email($email)) {
                    wp_send_json_error(__('Sorry the email address is invalid.', 'huslajobs'), 400);
                }
                elseif (email_exists($email)) {
                    wp_send_json_error(__('This email address already registered. You may want to login instead.', 'huslajobs'), 400);
                }
                elseif (strlen($password) < 8) {
                    wp_send_json_error(__('The password length must be 8 characters and above.', 'huslajobs'), 400);
                }
                $guest_package = Package::where('wp_user_id', '=', 0)->andWhere('price', '=', 0)->get();
                if (!sizeof($guest_package)) {
                    wp_send_json_error('Registration is currently disabled. Please try again later.', 400);
                }

                $query_args = [
                    'user_login' => sanitize_user($email),
                    'user_email' => sanitize_email($email),
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'user_pass' => $password,
                    'display_name' => trim($_POST['first_name']),
                ];
                $user_id = wp_insert_user($query_args);

                if (is_wp_error($user_id)) {
                    wp_send_json_error($user_id->get_error_message(), 400);
                } else {
                    //update user table
                    \huslajobs\updateUserTable($user_id, $bio, $city, $country, $state, $date_of_birth, $phone_number, $profile_image, $first_name, $last_name);

                    //  save account
//                    $user_name = $first_name . ' ' . $last_name;
//                    $account_id = \huslajobs\saveAccount($user_id, $account_type, $email, $bio, $city, $country, $state, $profile_image, $phone_number, 0, $user_name, 1);

//                       send response
                    $site_name = get_bloginfo('name');
                    $message = sprintf(
                        __('Thank you for registering on %s.Please verify your email account by clicking on the link that has been sent to your email.This might take 2-3 minutes to get to your account. If you do not find the email in your inbox after this time, please check your spam folder.', 'huslajobs'),
                        $site_name
                    );
                    wp_set_current_user($user_id);
                    wp_set_auth_cookie($user_id);


                    /**
                     * subscribe user to guest package
                     */

                    $guest_package = Package::where('wp_user_id', '=', 0)->andWhere('price', '=', 0)->get();
                    if (sizeof($guest_package) > 0) {
                        $subscription = new Subscription();
                        $subscription->wp_user_id = $user_id;
                        $subscription->package_id = $guest_package[0]->id;
                        $subscription->save();
                    }
                    /**
                     * get user data
                     */
                    $user_data = \huslajobs\huslajobsGetUserInfo($user_id);
                    $response = ['message' => $message,'user_data' => $user_data];
                    echo json_encode($response);
                }
            }

        } else {
            wp_send_json_error('Sorry,you are not allowed to register', 400);
        }

    } else {
        wp_send_json_error('Registration is currently disabled. Please try again later.', 400);
    }
    wp_die();

}

/**
 * @param $user_id
 *
 * @return void
 */
function userRegistered($user_id)
{
    $user_data = get_userdata($user_id);
    if (! strpos( $user_data->user_email, 'crawler' ) ) {
        $userVerificationEmails = new HuslajobsEmails();
        $emailTemplatesData = $userVerificationEmails->emailTemplatesData();
        $email_templates_data = $emailTemplatesData['user_registered'];
        $email_bcc = $email_templates_data['email_bcc'] ?? '';
        $email_from = $email_templates_data['email_from'] ?? '';
        $email_from_name = $email_templates_data['email_from_name'] ?? '';
        $reply_to = $email_templates_data['reply_to'] ?? '';
        $reply_to_name = $email_templates_data['reply_to_name'] ?? '';
        $email_subject = $email_templates_data['subject'] ?? '';
        $email_body = $email_templates_data['html'] ?? '';

        $email_body = do_shortcode($email_body);
        $email_body = wpautop($email_body);
        $verification_page_url = home_url('/email-verification');

        $activation_key = md5(uniqid('', true));

        update_user_meta($user_id, 'user_activation_key', $activation_key);
        update_user_meta($user_id, 'user_activation_status', 0);

        $user_roles = !empty($user_data->roles) ? $user_data->roles : array();
        $exclude_user_roles = ["administrator"];
        if (!empty($exclude_user_roles)) {
            foreach ($exclude_user_roles as $role) :

                if (in_array($role, $user_roles)) {
                    update_user_meta($user_id, 'user_activation_status', 1);

                    return;
                }
            endforeach;
        }


        $verification_url = add_query_arg(
            array(
                'activation_key' => $activation_key,
                'user_verification_action' => 'email_verification',
            ),
            $verification_page_url
        );

        $verification_url = wp_nonce_url($verification_url, 'email_verification');

        $site_name = get_bloginfo('name');
        $site_description = get_bloginfo('description');
        $site_url = get_bloginfo('url');
//        $site_logo_url = wp_get_attachment_url($logo_id);

        $vars = array(
            '{site_name}' => esc_html($site_name),
            '{site_description}' => esc_html($site_description),
            '{site_url}' => esc_url_raw($site_url),
//            'site_logo_url' => esc_url_raw($site_logo_url),

            '{first_name}' => esc_html($user_data->first_name),
            '{last_name}' => esc_html($user_data->last_name),
            '{user_display_name}' => esc_html($user_data->display_name),
            '{user_email}' => $user_data->user_email,
            '{user_name}' => esc_html($user_data->first_name),
            '{user_avatar}' => get_avatar($user_data->user_email, 60),
            '{activaton_url}' => esc_url_raw($verification_url),

        );
        $email_data['email_to'] = $user_data->user_email;
        $email_data['email_bcc'] = $email_bcc;
        $email_data['email_from'] = $email_from;
        $email_data['email_from_name'] = $email_from_name;
        $email_data['reply_to'] = $reply_to;
        $email_data['reply_to_name'] = $reply_to_name;

        $email_data['subject'] = strtr($email_subject, $vars);
        $email_data['html'] = strtr($email_body, $vars);
        $email_data['attachments'] = array();

        $userVerificationEmails->sendEmail($email_data);
    }
}

function saveAccount($user_id, $account_type, $email, $bio, $city, $country, $state, $profile_image, $phone_number, $account_id, $user_name, $default_account = 0,$show_profile=0, $fax = '', $website = '')
{

    $id = intval($account_id);
    $account = $id ? Account::find($id) : new Account();
    $account->wp_user_id = $user_id;
    $account->account_type = $account_type;
    $account->email = sanitize_email($email);
    $account->bio = $bio;
    $account->name = $user_name;
    $account->city = $city;
    $account->country = $country;
    $account->state = $state;
    $account->fax = $fax;
    $account->website = $website;
    $account->phone_number = $phone_number;
    $account->profile_image = $profile_image;
    $account->default_account = $default_account;
    $account->show_profile = $show_profile;
    $account->save();
    return $account->id;
}

function updateUserTable($user_id, $bio, $city, $country, $state, $date_of_birth, $phone_number, $profile_image, $first_name, $last_name)
{
    global $wpdb;
    $user_data = [
        'bio' => $bio,
        'city' => $city,
        'country' => $country,
        'state' => $state,
        'date_of_birth' => $date_of_birth,
        'phone_number' => $phone_number,
        'profile_image' => $profile_image,
        'first_name' => $first_name,
        'last_name' => $last_name

    ];

    $wpdb->update(
        $wpdb->prefix . 'users',
        $user_data,
        [
            'ID' => $user_id,
        ]
    );

}

function huslajobsGetUserInfo($userid = 0): array
{
    global $wpdb;

    /**
     * todo
     * get user accounts, user profiles, user qualifications
     */

    $users_table = $wpdb->prefix . 'users';
//    $user_accounts = Account::where('wp_user_id', '=', "'" . $userid . "'")->get();
//    $user= WpUser::where("ID", '=', "'" . $userid . "'")->get();
    $user_subscriptions = Subscription::leftJoin('packages')
        ->on('package_id', 'id')
        ->leftJoin($users_table)
        ->on('wp_user_id', 'ID')
        ->where('wp_user_id', '=', "'" . $userid . "'")
        ->andWhere('status', '=', 1)
        ->get([
            Subscription::tableName() . '.*',
            $users_table . '.first_name',
            $users_table . '.last_name',
            Package::tableName() . '.name AS package_name',
            Package::tableName() . '.jobs AS package_jobs',
            Package::tableName() . '.price AS package_price',
            Package::tableName() . '.job_applications AS package_job_applications',

        ]);

    $languages = get_user_meta($userid, 'user_languages', true);
    $user= \huslajobs\huslajobsGetUser($userid);
        $user_array = (array) $user;
    return array_merge(
        $user_array,
        [
            'profiles' => $user->profiles(),
            'subscriptions' => $user_subscriptions,
            'languages' => $languages ?: '',
            'job_applications'=> $user->applications(),
            'jobs'=> $user->jobs(),
            'companies'=>$user->companies()
        ]

    );
}


function huslajobsGetUser($userid = 0)
{
   $user = WpUser::where("ID", '=', "'" . $userid . "'")->first();
   if ($user){
       $user->first_name = $user->first_name ?: get_user_meta($userid, 'first_name', true);
       $user->last_name = $user->last_name ?: get_user_meta($userid, 'first_name', true);
   }
   return $user;
}

/**
 * function to check and update subscription status
 * @param $user
 * @return void
 * @throws \Exception
 */
function afterLogin($user)
{
//    die('code entered her');
    $subscriptions = Subscription::where('wp_user_id', '=', "'" . $user->ID . "'")->get();

    if (sizeof($subscriptions) > 0) {
        foreach ($subscriptions as $subscription) {
            if (intval($subscription->status) > 0) {
                $today = new DateTime('now');
                $exp_date = new DateTime($subscription->end_date);
                if (\huslajobs\validateDate($exp_date) && $today > $exp_date) {
                    $subscription->status = 0;
                    $subscription->save();
                    $package = Package::find($subscription->package_id);
//  build and send  emails
                    $site_name = get_bloginfo('name');
                    $site_description = get_bloginfo('description');
                    $site_url = get_bloginfo('url');
                    $vars = array(
                        '{site_name}' => esc_html($site_name),
                        '{site_description}' => esc_html($site_description),
                        '{site_url}' => esc_url_raw($site_url),
                        '{user_name}' => esc_html(get_user_meta($user->ID, 'first_name', true)),
                        '{package_name}' => esc_html($package->name),
                    );
//    send  emails
                    $expired_email = new HuslajobsEmails();
                    $email_data = \huslajobs\getTemplateData('expired_subscription_email', $vars, $user->user_email, []);
                    $expired_email->sendEmail($email_data);
                    break;
                }
            }
        }
    }

}

function switchLanguage()
{
    $locale = $_POST['locale'];
    $this_class = new WP_Locale_Switcher();
    wp_die();
}

function validateDate($date, $format = 'Y-m-d H:i:s')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}


/**
 * dashboard statistics
 */

function adminDashboardStats()
{
    global $wpdb;

    $users_table = $wpdb->prefix . 'users';
    $per_page = $_POST['perPage'];
    $page = $_POST['page'];
    $jobs = Job::paginate($per_page, $page)->get();
    $subscriptions = Subscription::paginate($per_page, $page)->orderBy('id', 'desc')
        ->leftJoin('packages')
        ->on('package_id', 'id')
        ->leftJoin($users_table)
        ->on('wp_user_id', 'ID')
        ->get([
            Subscription::tableName() . '.*',
            $users_table . '.first_name',
            $users_table . '.last_name',
            Package::tableName() . '.name AS package_name',
            Package::tableName() . '.price AS package_price'
        ]);
    $recruiters = Account::paginate($per_page, $page)->where('account_type', 'like', "'%" . 'recruiter' . "%'")->get();
    $seekers = Account::paginate($per_page, $page)->where('account_type', 'like', "'%" . 'seeker' . "%'")->get();
    $job_types = JobType::paginate($per_page, $page)->get();
    $categories = Category::paginate($per_page, $page)->get();
    $packages = Package::paginate($per_page, $page)->get();
    $job_applications = JobApplication::paginate($per_page, $page)->get();

    echo json_encode([
            'jobs' => $jobs,
            'job_applications' => $job_applications,
            'job_categories' => $categories,
            'packages' => $packages,
            'job_types' => $job_types,
            'subscriptions' => $subscriptions,
            'recruiters' => $recruiters,
            'seekers' => $seekers
        ]

    );
    wp_die();

}

/**
 * contact job seeker
 */
function contactSeeker(){


    $sender_email = $_POST['sender_email'];
    $sender_name = $_POST['sender_name'];
    $sender_message = $_POST['sender_message'];
    $job_seeker_email = $_POST['job_seeker_email'];

    //  build and send  emails
    $site_name        = get_bloginfo( 'name' );
    $site_description = get_bloginfo( 'description' );
    $site_url         = get_bloginfo( 'url' );
    $user = get_user_by('email' ,$job_seeker_email);
    $name = get_user_meta($user->ID,'first_name',true) ?? $user->first_name;

    $vars                          = [
        '{site_name}'        => esc_html( $site_name ),
        '{site_description}' => esc_html( $site_description ),
        '{site_url}'         => esc_url_raw( $site_url ),

        '{sender_email}'        => esc_html( $sender_email),
        '{sender_name}'         => esc_html( $sender_name),
        '{user_name}'=> esc_html($name),
        '{job_seeker_email}' => esc_html( $job_seeker_email),
        '{sender_message}'        => esc_html( $sender_message ),

    ];

//    send  emails
    $contactEmail = new HuslajobsEmails();
    $contact_email_data = \huslajobs\getTemplateData('contact_me_email',$vars,$job_seeker_email,[]); //company email
    $contactEmail->sendEmail( $contact_email_data ); //
    echo json_encode(['message'=>__('Your message has been sent','huslajobs')]);
    wp_die();

}
function hsLogout(){
    global $user_ID;
    if (isset($_POST['logout']) && ($_POST['logout'] && $user_ID)){
        //logout user
        if (wp_logout()){
            echo json_encode(['loggedOut'=>true]);
        }else{
            echo json_encode(['loggedOut'=>false]);
        }
    }
    wp_die();
}
function deleteUserAccount(){
    /**
     * logout user and delete their account
     *
     */
    $validator = HuslaValidator::validator(['user_id' => 'required'], $_POST);
    if ($validator->validate()) {
        $user_id = $_POST['user_id'];
        //archive user
        $today = date("Y-m-d H:i:s");
        $limit = intval(get_option('husla_jobs_delete_account_limit'));
        $delete_date = date('Y-m-d H:i:s', strtotime("+$limit days", strtotime($today)));
        $archived_user = new ArchivedUser();
        $archived_user->wp_user_id = $user_id;
        $archived_user->delete_date = $delete_date;
        $archived_user->save();
        wp_logout();
        echo json_encode(__('Account deleted','huslajobs'));
    }
    wp_die();

}