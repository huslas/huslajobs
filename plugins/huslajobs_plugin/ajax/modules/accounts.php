<?php

namespace huslajobs;

// Actions
add_action('wp_ajax_get_accounts', 'huslajobs\getAccounts');
add_action('wp_ajax_nopriv_get_accounts', "huslajobs\getAccounts");
add_action('wp_ajax_get_account', 'huslajobs\getAccount');
add_action('wp_ajax_update_account', 'huslajobs\updateAccount');
add_action('wp_ajax_delete_account', 'huslajobs\deleteAccount');
add_action('wp_ajax_user_account_type', 'huslajobs\userAccountType');
add_action('wp_ajax_nopriv_user_account_type', 'huslajobs\userAccountType');

add_action('wp_ajax_search_job_seekers', 'huslajobs\searchJobSeekers');
add_action('wp_ajax_nopriv_search_job_seekers', 'huslajobs\searchJobSeekers');

add_action('wp_ajax_job_seeker_search_options', 'huslajobs\jobSeekerSearchOptions');
add_action('wp_ajax_nopriv_job_seeker_search_options', 'huslajobs\jobSeekerSearchOptions');

add_action('wp_ajax_get_account', 'huslajobs\getAccount');
add_action('wp_ajax_nopriv_get_account', 'huslajobs\getAccount');

add_action('wp_ajax_save_company_account', 'huslajobs\saveCompanyAccount');
add_action('wp_ajax_nopriv_save_company_account', 'huslajobs\saveCompanyAccount');

add_action('wp_ajax_set_default_account', 'huslajobs\setDefaultAccount');
add_action('wp_ajax_nopriv_set_default_account', 'huslajobs\setDefaultAccount');

add_action('wp_ajax_get_companies', 'huslajobs\getCompanies');
add_action('wp_ajax_nopriv_get_companies', 'huslajobs\getCompanies');

add_action('wp_ajax_delete_company', 'huslajobs\deleteCompany');
add_action('wp_ajax_nopriv_delete_company', 'huslajobs\deleteCompany');

add_action('wp_ajax_get_company', 'huslajobs\getCompany');
add_action('wp_ajax_nopriv_get_company', 'huslajobs\getCompany');

add_action('wp_ajax_edit_company', 'huslajobs\editCompany');
add_action('wp_ajax_nopriv_edit_company', 'huslajobs\editCompany');

add_action('wp_ajax_save_company', 'huslajobs\saveCompany');
add_action('wp_ajax_nopriv_save_company', 'huslajobs\saveCompany');


// Methods

/**todo refactor this function
 *
 * @param $account
 *
 * @return mixed
 */
function getUserName($account)
{
    $user_id = intval($account->wp_user_id);
    $fname = get_user_meta($user_id, 'first_name', true);
    $lname = get_user_meta($user_id, 'last_name', true);
    $account->username = $fname . ' ' . $lname;
    return $account;
}

function getAccounts()
{

    $page = intval($_POST['page']);
    $per_page = intval($_POST['perPage']);
    $type = $_POST['type'];
    $sort_by = $_POST['sortBy'] ?? 'id';
    $order = $_POST['order'] ?? 'desc';
    $search_field = $_POST['searchField'] ?? '';
    $searchText = $_POST['searchText'] ?? '';

    $accounts = Account::paginate($per_page, $page)->orderBy($sort_by, $order);
    if ($type) {
        $accounts->where('account_type', 'like', "'%" . $type . "%'");
    }
    if (trim($search_field) != '' && trim($searchText) != '') {
        if ($type) {
            $accounts->andWhere($search_field, 'like', "'%" . $searchText . "%'");
        } else {
            $accounts->where($search_field, 'like', "'%" . $searchText . "%'");
        }
    } elseif (trim($searchText) != '') {
        if ($type) {
            $accounts->andWhere('name', 'like', "'%" . $searchText . "%'");
        } else {
            $accounts->where('name', 'like', "'%" . $searchText . "%'");
        }
    }

    $accounts = $accounts->get();;
    echo json_encode($accounts);

    wp_die();
}

function getAccount()
{
    if (HuslaValidator::validate(['account_id' => 'required|numeric'], $_POST)) {
        $account_id = intval($_POST['account_id']);
        $account_type = $_POST['account_type'];

        $account = Account::find($account_id);
        if ($account_type && strpos($account_type, "seeker") !== false) {
            $profiles = $account->profiles();
            if (sizeof($profiles) > 0) {
                $profiles = array_map('huslajobs\getProfileCategories', $profiles);
            }
            echo json_encode(['account' => $account, 'profiles' => $profiles]);
        } else {
            echo json_encode($account);
        }
    }
    wp_die();
}

function getCompanies(){
//    $user= \huslajobs\huslajobsGetUser($_POST['user_id']);

    $page = intval($_POST['page']);
    $per_page = intval($_POST['perPage']);
    $sort_by = $_POST['sortBy'] ?? 'created_at';
    $order = $_POST['order'] ?? 'desc';
    $user_id = intval($_POST['user_id']);
    $companies = Company::paginate($per_page, $page)->orderBy($sort_by, $order);
    $companies->where("wp_user_id","=" ,"'". $user_id ."'");
    $companies =  $companies->get();
    echo json_encode($companies);
    wp_die();
}

function updateAccount()
{

    $account_id = intval($_POST['account_id']);
    $account = Account::find($account_id);
    $profile_image = $_FILES['profile_image'];
    $email = trim(esc_attr($_POST['email']));
    $profile_image = $profile_image ? \huslajobs\saveFile($profile_image)['url'] : $_POST['profile_image'];
    $account->name = $_POST['name'];
    $account->city = $_POST['city'];
    $account->state = $_POST['state'];
    $account->country = $_POST['country'];
    $account->email = $email;
    $account->website = $_POST['website'];
    $account->fax = $_POST['fax'];
    $account->phone_number = $_POST['phone_number'];
    $account->bio = $_POST['bio'];
    $account->profile_image = $profile_image;

    $account->save();
    echo json_encode(__('Account updated', 'huslajobs'));

    wp_die();
}

function saveCompanyAccount()
{
    global $user_ID;
    /**
     * check if user already created company
     */
    $name = $_POST['name'];
    $activity_type = $_POST['activity_type'];

    $account = Account::where('wp_user_id', '=', intval($user_ID))
        ->andWhere('name', '=', "'" . $name . "'")
        ->andWhere('account_type', 'like', "'%" . 'company' . "%'")->get();
    if (sizeof($account) > 1) {
        wp_send_json_error('Recruiter and seeker accounts already exist with that name', 400);

    } elseif (sizeof($account) == 1) {

        if (strpos($activity_type, "seeker") !== false && strpos($account[0]->account_type, "seeker") !== false) {
            wp_send_json_error('Job seeker account already exist with that name', 400);
        } elseif (strpos($activity_type, "recruiter") !== false && strpos($account[0]->account_type, "recruiter") !== false) {
            wp_send_json_error('Job recruiter account already exist with that name', 400);
        }
    }
    $profile_image = $_FILES['profile_image'];
    $email = sanitize_email( $_POST['email']);
    $profile_image = $profile_image ? \huslajobs\saveFile($profile_image)['url'] : $_POST['profile_image'];
    $city = esc_attr($_POST['city']);
    $state = $_POST['state'];
    $country = $_POST['country'];
    $website = stripslashes(sanitize_url(esc_url($_POST['website'])));
    $fax = stripslashes(sanitize_text_field($_POST['fax']));
    $phone_number = stripslashes(sanitize_text_field($_POST['phone_number']));
    $bio = stripslashes(sanitize_text_field($_POST['bio'])) ;


    /**
     * create company accounts
     */
    if (strpos($activity_type, "seeker") !== false) {
        $account_type = 'company_job_seeker';
        \huslajobs\saveAccount($user_ID, $account_type, $email, $bio, $city, $country, $state, $profile_image, $phone_number, 0, $name,0, $fax, $website);

    } elseif (strpos($activity_type, "recruiter") !== false) {
        $account_type = 'company_recruiter';
        \huslajobs\saveAccount($user_ID, $account_type, $email, $bio, $city, $country, $state, $profile_image, $phone_number, 0, $name, 0,$fax, $website);

    } else {
        \huslajobs\saveAccount($user_ID, 'company_job_seeker', $email, $bio, $city, $country, $state, $profile_image, $phone_number, 0, $name,0, $fax, $website);
        \huslajobs\saveAccount($user_ID, 'company_recruiter', $email, $bio, $city, $country, $state, $profile_image, $phone_number, 0, $name,0, $fax, $website);

    }
    echo json_encode('Company created');
    wp_die();

}

function deleteAccount()
{

    if (HuslaValidator::validate(['account_id' => 'required|numeric'], $_POST)) {
        $id = intval($_POST['account_id']);
        $account = Account::find($id);
        echo json_encode($account->delete());
    }
    wp_die();
}

function userAccountType()
{
    global $user_ID, $wpdb;
    $user = get_user_by('id', $user_ID);
    $account_type = $_POST['account_type'];
    $account = new Account();
    if (strpos($account_type, "seeker") !== false) {
        $profile_image = $_FILES['profile_image'];

        $profile_image = \huslajobs\saveFile($profile_image);
//        update user table
        $wpdb->update(
            $wpdb->prefix . 'users',
            [
                'description' => $_POST['bio'],
                'city' => $_POST['city'],
                'country' => $_POST['country'],
                'state' => $_POST['state'],
                'date_of_birth' => $_POST['dob'],
                'phone' => $_POST['phone'],
                'avatar' => $profile_image['url'] ?? ''
            ],
            [
                'ID' => $user_ID,
            ]
        );
    }
    $account->wp_user_id = $user_ID;
    $account->account_type = $account_type;
//    $account->email = $user->user_email;
    $account->default_account = 1;
//    $account->active_account = 1;
    echo json_encode($account->save());
    wp_die();
}

function saveFile($file)
{
    // removing white space
    $file_name = preg_replace('/\s+/', '-', $file["name"]);

    // removing special character but keep . character because . seprate to extantion of file
    $file_name = preg_replace('/[^A-Za-z0-9.\-]/', '', $file_name);

    // rename file using time
    $file_name = time() . '-' . $file_name;
    return wp_upload_bits($file_name, null, file_get_contents($file["tmp_name"]));
//    return  wp_handle_upload($file, ['test_form' => false]);

}

function searchJobSeekers()
{
    $accounts = \huslajobs\searchSeekers();
    echo json_encode($accounts);
    wp_die();
}
function jobSeekerSearchOptions(){
    $categories = Category::paginate(-1);
    $categories = $categories->get();
    $results = ['categories'=> $categories];
    if (isset($_POST['searchSeekers'])){
        $accounts = \huslajobs\searchSeekers();
        $results['accounts'] = $accounts;
    }
    echo json_encode($results);
    wp_die();
}

function searchSeekers()
{
global $wpdb;
    /**
     * TODo build combine AND/OR operators to restrict search only on job seekers
     */

    $state = $_POST['state'];
    $country = $_POST['country'];
    $search_text = $_POST['searchText'];
    $category_id = $_POST['category'];
    $users_table = $wpdb->prefix . 'users';
//    $has_where = false;
    $page = intval($_POST['page']);
    $per_page = intval($_POST['perPage']);
    $accounts = Profile::paginate($per_page, $page)->orderBy('image', 'desc')->orderBy('id', 'desc');
    $accounts->leftJoin($users_table)
        ->on('wp_user_id', 'ID')
        ->leftJoin('profile_categories')
        ->onJoin('id', 'profile_id', Profile::tableName(), ProfileCategory::tableName())
        ->leftJoin('categories')
        ->onJoin('category_id', 'id', ProfileCategory::tableName(), Category::tableName())
        ->where('completed', '=', 1);
    if (trim($search_text) != '') {

        $accounts->addConditions("AND");
        $accounts->where('username', 'like', "'%" . $search_text . "%'");
        $accounts->orWhere('title', 'like', "'%" . $search_text . "%'");
        $accounts->orWhere('description', 'like', "'%" . $search_text . "%'");
        $accounts->orWhere('address', 'like', "'%" . $search_text . "%'");

        $accounts->orWhereJoin('city', 'like', "'%" . $search_text . "%'", $users_table);
        $accounts->orWhereJoin('state', 'like', "'%" . $search_text . "%'", $users_table);
        $accounts->orWhereJoin('country', 'like', "'%" . $search_text . "%'", $users_table);
        $accounts->orWhereJoin('name', 'like', "'%" . $search_text . "%'", Category::tableName());
        $accounts->closeConditions();
    }

    if ($country) {
        $accounts->addConditions("AND");
        $accounts->where('address', 'like', "'%" . $search_text . "%'");
        $accounts->orWhereJoin('country', '=', "'" . $country . "'",$users_table);
        $accounts->closeConditions();
    }

    if ($state) {
        $accounts->addConditions("AND");
        $accounts->where('address', 'like', "'%" . $search_text . "%'");
        $accounts->orWhereJoin('state', '=', "'" . $state . "'",$users_table);
        $accounts->closeConditions();
     }
    if ($category_id) {
        $accounts->andWhereJoin('id', '=', intval($category_id), Category::tableName());
    }
    $select_fields = [
        Profile::tableName() . '.*',
    ];
    return $accounts->get($select_fields, true);
}
function setDefaultAccount(){
    global $user_ID;
    $account_type = $_POST['account_type'];
    $show_profile = intval($_POST['show_profile']);
    $show_profile =  $account_type == 'individual_recruiter' ? 0 : $show_profile;
    $user = \huslajobs\huslajobsGetUser($user_ID);
    $user_name = $user['fname'] . " ". $user['lname'];
    $account_id = \huslajobs\saveAccount($user['id'], $account_type,$user['email'], $user['bio'], $user['city'], $user['country'], $user['state'], $user['avatar_id'], $user['phone_number'], 0, $user_name, 1,$show_profile);
    echo json_encode(['message'=>__('Updated successfully', 'huslajobs'),'user'=>$user,'account_id'=>$account_id]);
    wp_die();
}
function deleteCompany()
{
    $validator = HuslaValidator::validator(['company_id' => 'required'], $_POST);
    if ($validator->validate()) {
        $company_id = $_POST['company_id'];
        $company = Company::find($company_id);
        $company->delete();
        echo json_encode(__('Company deleted','huslajobs'));
    }
    wp_die();
}
function getCompany()
{
    $validator = HuslaValidator::validator(['company_id' => "required"], $_POST);
    if ($validator->validate()) {
        $id = $_POST['company_id'];
        $company = Company::find($id);
        echo json_encode($company);
    }
    wp_die();
}

function editCompany(){
        $data = array_merge($_POST, $_FILES);
        $validator = HuslaValidator::validator([
            'name' => 'required',
            'email' => 'required',
        ], $data);

        if ($validator->validate()) {
            $id = $_POST['company_id'];
            $company = Company::find($id);
            $name = stripslashes(sanitize_text_field($_POST['name']));
            $website = stripslashes(sanitize_text_field($_POST['website']));
            $company->website = $website;
            $company->name = $name;
            $company->email = stripslashes(sanitize_text_field($_POST['email']));
            $company->save();
            echo json_encode();
        }
        wp_die();

}

function saveCompany(){
    global $user_ID;
        $data = array_merge($_POST, $_FILES);
        $validator = HuslaValidator::validator([
            'name' => 'required',
            'email' => 'required',
            'address_1' => 'required',
        ], $data);

        if ($validator->validate()) {
            $company = new Company();
            $message = __('Company created','huslajobs');
            $id = $_POST['company_id'];
            if (intval($id)){
                $company = Company::find($id);
                $message = __('Company updated','huslajobs');
            }
            $name = stripslashes(sanitize_text_field($_POST['name']));
            $website = stripslashes(sanitize_text_field($_POST['website']));
            $address_1 = stripslashes(sanitize_text_field($_POST['address_1']));
            $address_2 = stripslashes(sanitize_text_field($_POST['address_2']));
            $phone_number = stripslashes(sanitize_text_field($_POST['phone_number']));
            $company->website = $website;
            $company->name = $name;
            $company->wp_user_id = $user_ID;
            $company->email = stripslashes(sanitize_text_field($_POST['email']));
            $company->address_1 = $address_1;
            $company->address_2 = $address_2;
            $phone_number->phone_number = $phone_number;
            $company->save();
            echo json_encode($message);
        }
        wp_die();

}