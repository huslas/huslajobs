<?php

namespace huslajobs;
// Actions
add_action('wp_ajax_add_profile', 'huslajobs\addProfile');
add_action('wp_ajax_update_profile', 'huslajobs\updateProfile');
add_action('wp_ajax_delete_profile', 'huslajobs\deleteProfile');

//
add_action('wp_ajax_get_user_profiles', 'huslajobs\getUserProfiles');
add_action('wp_ajax_nopriv_get_user_profiles', 'huslajobs\getUserProfiles');

add_action('wp_ajax_get_profile', 'huslajobs\getProfile');
add_action('wp_ajax_nopriv_get_profile', 'huslajobs\getProfile');

// Methods

function deleteProfile()
{
    $validator = HuslaValidator::validator(['profile_id' => 'required'], $_POST);
    if ($validator->validate()) {
        $profile_id = $_POST['profile_id'];
        $profile = Profile::find($profile_id);
        $profile->delete();
        echo json_encode(__('Profile deleted','huslajobs'));
    }
    wp_die();
}

function assignProfileCategories(HuslaModel $profile, array $categories)
{
    foreach ($categories as $category) {
        $profile_category = new ProfileCategory();
        $profile_category->profile_id = $profile->id;
        $profile_category->category_id = $category;
        $profile_category->save();
    }
}

function getProfile()
{
    $validator = HuslaValidator::validator(['profile_id' => "required"], $_POST);
    if ($validator->validate()) {
        $id = $_POST['profile_id'];
        $profile = Profile::find($id);
        $profile->categories = $profile->getCategories();

        echo json_encode($profile);
    }
    wp_die();
}
function addProfile()
{
    global $user_ID;
    $data = array_merge($_POST, $_FILES);
    $validation = [
        'username' => 'required',
        'description' => 'required',
        'categories' => 'required',
        'cv' => 'required',
        'years_of_experience' => 'required',
        'title' => 'required',
        'email' => 'required',
        'phone_number' => 'required',
    ];
    if (get_option('husla_jobs_profile_image') == 'yes'){
        $validation['image']  ='required';
    }
    $validator = HuslaValidator::validator($validation, $data);

    if ($validator->validate()) {

        $profile = new Profile();
        $message = __('Profile created','huslajobs');

        $id = $_POST['profile_id'];
        if ($id){
            $profile = Profile::find($id);
            $message = __('Profile updated','huslajobs');
        }
        $cv = $_FILES['cv'];
        $image = $_FILES['image'];
        $cv = $cv ? \huslajobs\saveFile($cv)['url'] : $_POST['cv'];
        $image =  $image ? \huslajobs\saveFile($image)['url'] : $_POST['image'];

        $name = stripslashes(sanitize_text_field($_POST['username']));
        $description = stripslashes(sanitize_text_field($_POST['description']));
        $categories = explode(',', $_POST['categories']);
        $profile->wp_user_id = $user_ID;
        $profile->description = $description;
        $profile->username = $name;
        $profile->cv = $cv;
        $profile->image = $image;
        $profile->completed = 1;
        $profile->title = stripslashes(sanitize_text_field($_POST['title']));
        $profile->email = stripslashes(sanitize_text_field($_POST['email']));
        $profile->phone_number = stripslashes(sanitize_text_field($_POST['phone_number']));
        $profile->years_of_experience = $_POST['years_of_experience'] ?  intval($_POST['years_of_experience'] ) : 0;
        $profile->save();

        assignProfileCategories($profile, $categories);
        $user = \huslajobs\huslajobsGetUserInfo($user_ID);
        echo json_encode(['message'=>$message,'user'=>$user,'profile_id'=>$profile->id]);
    }
    wp_die();
}

function getProfileCategories($profile)
{
    $categories = $profile->getCategories();
    $profile->categories = $categories;
    return $profile;
}
function getUserProfiles(){
    global $user_ID;
    $profiles = Profile::where('wp_user_id', '=', "'" . $user_ID . "'")->get();
    echo json_encode($profiles);
    wp_die();
}