<?php

namespace huslajobs;
// Actions
// job applications
add_action('wp_ajax_get_applications', 'huslajobs\getApplications');

// job types
add_action('wp_ajax_get_job_types', 'huslajobs\getJobTypes');
add_action('wp_ajax_nopriv_get_job_types', 'huslajobs\getJobTypes');
add_action('wp_ajax_save_job_types', 'huslajobs\saveJobTypes');
add_action('wp_ajax_get_job_type', 'huslajobs\getJobType');
add_action('wp_ajax_update_job_type', 'huslajobs\updateJobType');
add_action('wp_ajax_delete_job_type', 'huslajobs\deleteJobType');

// jobs
add_action('wp_ajax_get_jobs', 'huslajobs\getJobs');
add_action('wp_ajax_nopriv_get_jobs', 'huslajobs\getJobs');
add_action('wp_ajax_save_job', 'huslajobs\saveJob');
add_action('wp_ajax_nopriv_save_job', 'huslajobs\saveJob');
add_action('wp_ajax_update_job', 'huslajobs\saveJob');
add_action('wp_ajax_get_job', 'huslajobs\getJob');
add_action('wp_ajax_delete_job', 'huslajobs\deleteJob');
add_action('wp_ajax_job_application', 'huslajobs\jobApplication');
add_action('wp_ajax_nopriv_job_application', 'huslajobs\jobApplication');


add_action('wp_ajax_get_categories_job_types_and_currencies',
    'huslajobs\getJobDependencies'
);
add_action('wp_ajax_nopriv_get_categories_job_types_and_currencies', 'huslajobs\getJobDependencies');

add_action("wp_ajax_get_job_search_options", "huslajobs\getJobSearchOptions");
add_action("wp_ajax_nopriv_get_job_search_options", "huslajobs\getJobSearchOptions");

add_action("wp_ajax_search_jobs", "huslajobs\searchJobs");
add_action('wp_ajax_nopriv_search_jobs', 'huslajobs\searchJobs');

add_action('wp_ajax_get_job_and_related_jobs', 'huslajobs\getJobAndRelatedJobs');
add_action('wp_ajax_nopriv_get_job_and_related_jobs', 'huslajobs\getJobAndRelatedJobs');


// Methods
function getApplications()
{
    $page = intval($_POST['page']);
    $per_page = intval($_POST['perPage']);
    $sort_by = $_POST['sortBy'] ?? 'created_at';
    $order = $_POST['order'] ?? 'desc';
    $account_id = intval($_POST['account_id']);
    $applications = JobApplication::paginate($per_page, $page)->orderBy($sort_by, $order);

    $applications->leftJoin('jobs')
        ->on('job_id', 'id');
    $applications->leftJoin('profiles')
        ->on('profile_id', 'id');
    if ($account_id) {
        $applications->where('wp_user_id', '=', $account_id);
    }
    $select_fields = [
        JobApplication::tableName() . '.*',
        Job::tableName() . '.name AS job_name',
        Job::tableName() . '.slug AS job_slug',
        Profile::tableName().'.username AS applicant_name',
        Profile::tableName().'.address AS address',
        Profile::tableName().'.email AS applicant_email',
    ];
    $results = $applications->get($select_fields);

    echo json_encode($results);
    wp_die();
}


//job types

function getJobTypes()
{

    $page = intval($_POST['page']);
    $per_page = intval($_POST['perPage']);
    $sort_by = $_POST['sortBy'] ?? 'id';
    $order = $_POST['order'] ?? 'desc';
    $search_fields = $_POST['searchFields'] ?? false;
    $search_text = $_POST['searchText'];
    $results = JobType::paginate($per_page, $page)->orderBy($sort_by, $order);
    if (trim($search_text) != '' && $search_fields) {
        $search_fields = explode(",", $search_fields);
        for ($i = 0; $i < sizeof($search_fields); $i++) {
            if ($i == 0) {
                $results->where($search_fields[$i], 'like', "'%" . $search_text . "%'");
            } else {
                $results->orWhere($search_fields[$i], 'like', "'%" . $search_text . "%'");
            }
        }
    }
    echo json_encode($results->get());
    wp_die();

}

function getJobType()
{
    $id = intval($_POST['id']);
    $job_type = JobType::find($id);
    if ($job_type) {
        echo json_encode($job_type);
    } else {
        wp_send_json_error(__('Job type not found', 'huslajobs'), 400);
    }
    wp_die();
}

function saveJobTypes()
{

    if (HuslaValidator::validate(['name' => 'required', 'description' => 'required'], $_POST)) {
        $name = stripslashes(sanitize_text_field($_POST['name']));
        if (sizeof(JobType::name($name))) {
            wp_send_json_error(__('A job type with this name already exists', 'huslajobs'), 400);
        } else {
            $description = stripslashes(sanitize_text_field($_POST['description']));
            $job_type = new JobType();
            $job_type->description = $description;
            $job_type->name = $name;
            echo json_encode($job_type->save());
        }
    }
    wp_die();
}

function updateJobType()
{
    if (HuslaValidator::validate([
        'job_type_id' => 'required|numeric',
        'name' => 'required',
        'description' => 'required'
    ], $_POST)) {
        $id = intval($_POST['job_type_id']);
        $name = stripslashes(sanitize_text_field($_POST['name']));
        $description = stripslashes(sanitize_text_field($_POST['description']));
        $job_type = JobType::find($id);

        $job_type->description = $description;
        $job_type->name = $name;
        $results = $job_type->save();

        if ($results) {
            echo json_encode($results);
        } else {
            wp_send_json_error(__('An error occurred, please try again or contact the admin', 'huslajobs'), 400);
        }
    }
    wp_die();
}

function deleteJobType()
{
    $id = intval($_POST['job_type_id']);
    $job_type = JobType::find($id);
    if ( sizeof( $job_type->packages() ) > 0) {
        wp_send_json_error(__('Job type can not be deleted since it has a package limit', 'huslajobs'), 400);
    } elseif (sizeof($job_type->jobs()) > 0) {
        wp_send_json_error(__('Job type can not be deleted since it has been assigned to a job', 'huslajobs'), 400);
    } else {
        echo json_encode($job_type->delete());
    }
    wp_die();
}


//jobs

function getJobs()
{
    $results = \huslajobs\huslaSearchJobs();
    echo json_encode($results);
    wp_die();
}

function getJobAndRelatedJobs()
{
    $slug = $_POST['job_slug'];
    $job = \huslajobs\getJobData($slug);
    $related_jobs = [];
    if (sizeof($job)) {
        $job = $job[0];
        $related_jobs = \huslajobs\getRelatedJobs($job->id, $job->job_type_id, $job->category_id);
    } else {
        $job = null;
    }

    echo json_encode(['job' => $job, 'related_jobs' => $related_jobs]);
    wp_die();

}

function getRelatedJobs($jobId, $jobTypeId, $categroy_id)
{
    $page = 1;
    $per_page = 4;
    $sort_by = 'id';
    $order = 'desc';
    $results = Job::paginate($per_page, $page)->orderBy($sort_by, $order);
    $results->leftJoin('job_types')
        ->on('job_type_id', 'id')
        ->leftJoin('currencies')
        ->on('currency_id', 'id')
        ->leftJoin('categories')
        ->on('category_id', 'id')
        ->where('id', '!=', "'" . $jobId . "'")
        ->addConditions('AND')
        ->where('category_id', '=', "'" . $categroy_id . "'")
        ->orWhere('job_type_id', '=', "'" . $jobTypeId . "'")
        ->closeConditions();
    $select_fields = [
        Job::tableName() . '.*',
        Currency::tableName() . '.code AS currency_code',
        JobType::tableName() . '.name AS job_type_name ',
        Category::tableName() . '.name AS category_name'
    ];
    return $results->get($select_fields);
}

function saveJob()
{

    global $user_ID;
    $message = __('Job created successfully', 'huslajobs');

    $validator = HuslaValidator::validator([
        'description' => 'required',
        'name' => 'required',
        'category_id' => 'required',
        'job_type_id' => 'required',
    ], $_POST);

    if ($validator->validate()) {

        $id = $_POST['job_id'] ?? '';
        $job = $id ? Job::find($id) : new Job();
        $user = \huslajobs\huslajobsGetUser($user_ID);
        $application_email = $user->user_email;
        $company_id = 0;

        // check if user is posting as a company or as an individual
        if ($_POST['postingAs'] == 'company') {


            if (intval($_POST['companyAccount'])) {
                $application_email = $_POST['companyEmail'];
                $company_id = $_POST['companyAccount'];
            } else {
                /**
                 * check if user already has company with
                 * thesame name
                 */
                $company_account = Company::where('wp_user_id', '=', intval($user_ID))->andWhere('name', '=', "'" . $_POST['companyName'] . "'")
                    ->get();
                if (sizeof($company_account)) {
                    wp_send_json_error(__('You already have a company with that name', 'huslajobs'), 400);
                } else {
                    $new_account = new Company();
                    // set account user
                    $new_account->wp_user_id = $user_ID;
                    $new_account->email = stripslashes(sanitize_email($_POST['companyEmail']));
                    $new_account->name = stripslashes(sanitize_text_field($_POST['companyName']));
                    $new_account->website = stripslashes(sanitize_url($_POST['companyWebsite']));
                    $new_account->phone_number = stripslashes(sanitize_text_field($_POST['companyPhone']));
                    $new_account->address_1 = stripslashes(sanitize_url($_POST['companyAddress_1']));
                    $new_account->address_2 = stripslashes(sanitize_url($_POST['companyAddress_2']));
                    //create account
                    if ($new_account->save()) {
                        $company = Company::find($new_account->id);
                        $application_email = $company->email;
                        $company_id = $company->id;
                    } else {
                        // 			        throw an error
                        wp_send_json_error(__('An error occurred, please try again or contact the admin', 'huslajobs'), 400);
                    }
                }
            }

        }

        if ($user_ID) {
            // save job
            $name = stripslashes(sanitize_text_field($_POST['name']));
            $job->description = stripslashes(sanitize_text_field($_POST['description']));
            $job->name = $name;
            $job->work = $_POST['work'];
            $job->category_id = $_POST['category_id'];
            $job->experience = intval($_POST['experience']);
            $job->job_type_id = intval($_POST['job_type_id']);
            if (isset($_POST['currency_id']) && $_POST['currency_id']) {
                $job->currency_id = $_POST['currency_id'];
            }
            $job->country = $_POST['jobCountry'];
            $job->city = stripslashes(sanitize_text_field($_POST['jobCity']));
            $job->state = stripslashes(sanitize_text_field($_POST['jobState']));
            $job->salary = $_POST['salary'];
            $job->country_name = $_POST['country_name'];
            $job->cv_required = intval($_POST['cv_required']);
            $job->motivation_required = intval($_POST['motivation_required']);
            $job->company_id = $company_id;
            $job->wp_user_id = $user_ID;
            $job->application_email = $application_email;

            //generate slug
            $jobs = Job::where('name', '=', "'" . $name . "'")->get();

            if (sizeof($jobs) == 1) {
                $job->slug = \huslajobs\createSlug($name, 1);
            } elseif (sizeof($jobs) > 1) {
                $last_job = array_pop($jobs);
                $last_job_slug = $last_job->slug;
                $position = explode('-', $last_job_slug);
                $position = intval(array_pop($position));
                $job->slug = \huslajobs\createSlug($name, ($position + 1));
            } else {
                $job->slug = \huslajobs\createSlug($name);
            }
            if ($job->save()) {
                $message = $id ? 'updated' : $message;
                $user_data = \huslajobs\huslajobsGetUserInfo($user_ID);
                echo json_encode(['message' => $message, 'redirect_url' => home_url('/my-account'), 'user_data' => $user_data]);
            } else {
                wp_send_json_error(__('An error occurred, please try again or contact the admin', 'huslajobs'), 400);
            }
        } else {
            wp_send_json_error(__('An error occurred, please try again or contact the admin', 'huslajobs'), 400);
        }
    }
    wp_die();
}

function getJob()
{

    $slug = $_POST['job_slug'];
    $results = \huslajobs\getJobData($slug);
    $response = null;
    if (sizeof($results)) {
        $response = $results[0];
    }
    echo json_encode($response);
    wp_die();

}

function getJobData($jobSlug)
{
    global $wpdb;
    $users_table = $wpdb->prefix . 'users';

    $job = Job::leftJoin('job_types')
        ->on('job_type_id', 'id')
        ->leftJoin('currencies')
        ->on('currency_id', 'id')
        ->leftJoin('categories')
        ->on('category_id', 'id')
        ->leftJoin($users_table)
        ->on('wp_user_id', 'ID')
        ->leftJoin('companies')
        ->on('company_id', 'id')
        ->where('slug', '=', "'" . $jobSlug . "'");


    $select_fields = [
        Job::tableName() . '.*',
        Currency::tableName() . '.code AS currency_code',
        JobType::tableName() . '.name AS job_type_name ',
        Category::tableName() . '.name AS category_name',
        $users_table . '.first_name',
        $users_table . '.last_name',
        $users_table . '.state AS user_state',
        $users_table . '.country AS user_country',
        $users_table . '.city AS user_city',
        $users_table.'.user_email as user_email',
        Company::tableName().'.name AS company_name',
        Company::tableName().'.address_1 AS company_address',
    ];
    return $job->get($select_fields);
}

function deleteJob()
{
    $id = intval($_POST['job_id']);
    $job = Job::find($id);
    if (sizeof($job->applications()) > 0) {
        echo json_encode($job->softDelete());
    } else {
        echo json_encode($job->delete());
    }
    wp_die();
}

function getJobDependencies()
{

    $page = intval($_POST['page']);
    $per_page = intval($_POST['perPage']);
    $front_end = $_POST['frontEnd'];


    // get jobtypes
    $job_types = JobType::paginate($per_page, $page)->orderBy('id', 'desc')->get();
    // get jobcategories
    $job_categories = Category::paginate($per_page, $page)->orderBy('id', 'desc')->get();
    // get currencies
    $currencies = Currency::paginate($per_page, $page)->orderBy('id', 'desc')->get();

    // get accounts
    $account = '';
    if (!$front_end) {
        $account = Account::paginate($per_page, $page)->orderBy('id', 'desc')->get();
    }

    echo json_encode([
        'job_types' => $job_types,
        'job_categories' => $job_categories,
        'currencies' => $currencies,
        'accounts' => $account
    ]);
    wp_die();
}

function getJobSearchOptions()
{
    global $wpdb;
//    $users_table = $wpdb->prefix . 'users';
    $page = intval($_POST['page']);
    $per_page = intval($_POST['perPage']);
    $sort_by = $_POST['sortBy'] ?? 'id';
    $order = $_POST['order'] ?? 'desc';

    $job_types = JobType::paginate($per_page, $page)->orderBy('id', 'desc')->get();
    $categories = Category::paginate($per_page, $page)->orderBy('id', 'desc')->get();
    $accounts = Account::paginate(100, $page)->orderBy($sort_by, $order);
    $accounts->where('account_type', 'like', "'%" . 'recruiter' . "%'");
    $accounts = $accounts->get();
    $results = [
        'job_types' => $job_types,
        'recruiters' => $accounts,
        'categories' => $categories
    ];
    if (isset($_POST['searchJobs'])) {
        $search_results = \huslajobs\huslaSearchJobs();
        $results['search_results'] = $search_results;
    }


    echo json_encode($results);
    wp_die();
}

function searchJobs()
{
    $results = \huslajobs\huslaSearchJobs();

    echo json_encode($results);
    wp_die();

}

function huslaSearchJobs()
{
    $page = intval($_POST['page']);
    $per_page = intval($_POST['perPage']);
    $sort_by = $_POST['sortBy'] ?? 'id';
    $order = $_POST['order'] ?? 'desc';
    $search_text = $_POST['searchText'];
    $limit_per_page = $_POST['limitPerPage'] ? intval($_POST['limitPerPage']) : 0;
    $search_location = $_POST['searchLocation'];
    $job_type = $_POST['jobType'] ? intval($_POST['jobType']) : 0;
    $experience_level = intval($_POST['experienceLevel']);
    $country = $_POST['country'];
    $category_id = $_POST['category'] ? intval($_POST['category']) : 0;
    $work_type = $_POST['workType'];
    $category_name = $_POST['categoryName'];
    $country_name = $_POST['countryName'];
    $job_type_name = $_POST['jobTypeName'];
    $results = Job::paginate($per_page, $page, $limit_per_page)->orderBy($sort_by, $order);
    $results->leftJoin('job_types')
        ->on('job_type_id', 'id')
        ->leftJoin('currencies')
        ->on('currency_id', 'id')
        ->leftJoin('categories')
        ->on('category_id', 'id');
    $results->where('name', '!=', "''");

//    echo json_encode([$_POST['account_id']]);
//    wp_die();
    if ($_POST['account_id']) {
        $results->andWhere('wp_user_id', '=', "'" . $_POST['account_id'] . "'");
    }

    if (trim($search_text) != '') {
        $results->addConditions('AND');
        $results->where('name', 'like', "'%" . $search_text . "%'");
        $results->orWhere('description', 'like', "'%" . $search_text . "%'");
        $results->orWhere('state', 'like', "'%" . $search_text . "%'");
        $results->orWhere('city', 'like', "'%" . $search_text . "%'");
        $results->orWhere('country_name', 'like', "'%" . $search_text . "%'");
        $results->orWhere('crawled_job_location', 'like', "'%" . $search_text . "%'");
        $results->orWhere('crawled_job_types', 'like', "'%" . $search_text . "%'");
        $results->orWhere('crawled_categories', 'like', "'%" . $search_text . "%'");
        $results->orWhereJoin('name', 'like', "'%" . $search_text . "%'", JobType::tableName());
        $results->orWhereJoin('code', 'like', "'%" . $search_text . "%'", Currency::tableName());
        $results->orWhereJoin('name', 'like', "'%" . $search_text . "%'", Category::tableName());
        $results->closeConditions();
    }
    if ($job_type) {
        $results->addConditions('AND');
        $results->where('job_type_id', '=', $job_type);
        $results->orWhere('crawled_job_types', 'like', "'%" . $job_type_name . "%'");
        $results->closeConditions();

    }
    if ($experience_level) {
        $results->addConditions('AND');
        $results->where('experience', '>=', $experience_level);
        $results->closeConditions();
    }

    if ($country) {
        $results->addConditions('AND');
        $results->where('country', '=', "'" . $country . "'");
        $results->orWhere('crawled_job_location', 'like', "'%" . $country_name . "%'");
        $results->closeConditions();

    }
    if ($work_type) {
        $results->addConditions('AND');
        $results->where('work', '=', "'" . $work_type . "'");
        $results->closeConditions();
    }


    if ($search_location) {
        $results->addConditions('AND');
        $results->where('state', 'like', "'%" . $search_location . "%'");
        $results->orWhere('city', 'like', "'%" . $search_location . "%'");
        $results->orWhere('country_name', 'like', "'%" . $search_location . "%'");
        $results->orWhere('crawled_job_location', 'like', "'%" . $search_location . "%'");
        $results->closeConditions();
    }
    if ($category_id) {
        $results->addConditions('AND')
            ->where('category_id', '=', $category_id)
            ->orWhere('crawled_categories', 'like', "'%" . $category_name . "%'")
            ->closeConditions();
    }


    $select_fields = [
        Job::tableName() . '.*',
        Currency::tableName() . '.code AS currency_code',
        JobType::tableName() . '.name AS job_type_name ',
        Category::tableName() . '.name AS category_name'
    ];

    return $results->get($select_fields);
}

function saveJobApplication($user_ID,$job_id,$applicant_name,$applicant_email,$address,$applicant_phone='',$motivation,$cv,$profile_id,$application_email,$action="no_payment"){

    if (!$profile_id) {
        //    save profile
        $profile = new Profile();
        $profile->wp_user_id = $user_ID;
        $profile->username = $applicant_name;
        $profile->email = $applicant_email;
        $profile->phone_number = $applicant_phone;
        $profile->cv = $cv;
        $profile->address = $address;
        if ($profile->save()) {
            $profile_id = $profile->id;
        } else {
            wp_send_json_error(__('An error occurred, please try again or contact the admin', 'huslajobs'), 400);

        }
    }else{
        $user_profile =  Profile::find(intval($profile_id));
        if ($user_profile){
            $applicant_name = $user_profile->username;
            $applicant_email = $user_profile->email;
            $address = $user_profile->address;
            $applicant_phone =$user_profile->phone_number;
            $cv = $user_profile->cv;
        }else{
            wp_send_json_error(__('An error occurred, please try again or contact the admin', 'huslajobs'), 400);
        }
    }
//   save application
    $job_applications = new JobApplication();
    $job_applications->job_id = $job_id;
    $job_applications->motivation = $motivation;
    $job_applications->wp_user_id = $user_ID;
    $job_applications->profile_id = $profile_id;

    if ($job_applications->save()) {
//  build and send  emails
        $site_name = get_bloginfo('name');
        $site_description = get_bloginfo('description');
        $site_url = get_bloginfo('url');

        $vars = array(
            '{site_name}' => esc_html($site_name),
            '{site_description}' => esc_html($site_description),
            '{site_url}' => esc_url_raw($site_url),
            '{applicant_name}' => esc_html($applicant_name),
            '{applicant_email}' => esc_html($applicant_email),
            '{applicant_address}' => esc_html($address),
            '{applicant_phone}' => esc_html($applicant_phone),
            '{applicant_motivation}' => esc_html($motivation),
            '{attachments}' => esc_html($cv),

        );
        $attachments = [];
        $attachments[] = $cv;

//    send  emails
        $company_email = new HuslajobsEmails();
        $job_seeker_email = new HuslajobsEmails();
        $company_email_data = \huslajobs\getTemplateData('company_job_application', $vars, $application_email, $attachments); //company email
        $jobseeker_email_data = \huslajobs\getTemplateData('job_seeker_application_email', $vars, $applicant_email, $attachments); // job seeker email

        $company_email->sendEmail($company_email_data); //
        $job_seeker_email->sendEmail($jobseeker_email_data); //
        /**
         * update user applications
         */
        $user = get_user_by('id', $user_ID);
        if (!in_array('administrator', (array)$user->roles)) {
            $user_applications = get_user_meta(intval($user_ID), 'job_applications', true) ?? 0;
            update_user_meta(intval($user_ID), 'job_applications', $user_applications + 1);
        }
        if ($action == 'no_payment'){
            $user_data = \huslajobs\huslajobsGetUserInfo($user_ID);
            echo json_encode(['message' => __('Your application has been sent', 'huslajobs'), 'user_data' => $user_data]);
        }

    } else {
        wp_send_json_error(__('An error occurred, please try again or contact the admin', 'huslajobs'), 400);
    }
}
function jobApplication()
{
    global $user_ID;
    /**
     * TODO Update job application table,send emails to user and company,save cv
     */

    $job_id = intval($_POST['jobId']);
    $applicant_name = esc_attr($_POST['applicantName']);
    $applicant_email = trim(esc_attr($_POST['applicantEmail']));
    $address = trim(esc_attr($_POST['applicantAddress']));
    $applicant_phone = esc_attr($_POST['applicantPhone']);
    $motivation = esc_attr($_POST['motivation']);
    $profile_id = intval($_POST['profileId']);
    $cv = $_FILES['cv'];
    $cv = $cv ? \huslajobs\saveFile($cv)['url'] : $_POST['cv'];
    $application_email = trim(esc_attr($_POST['jobApplicationEmail']));
   \huslajobs\saveJobApplication($user_ID,$job_id,$applicant_name,$applicant_email,$address,$applicant_phone,$motivation,$cv,$profile_id,$application_email);
    wp_die();
}

function getTemplateData($email_template, $vars, $email_to, $attachments)
{
    $user_verification_emails = new HuslajobsEmails();
    $email_templates_data = $user_verification_emails->emailTemplatesData();
    $email_templates_data = $email_templates_data[$email_template];
    $email_bcc = $email_templates_data['email_bcc'] ?? '';
    $email_from = $email_templates_data['email_from'] ?? '';
    $email_from_name = $email_templates_data['email_from_name'] ?? '';
    $reply_to = $email_templates_data['reply_to'] ?? '';
    $reply_to_name = $email_templates_data['reply_to_name'] ?? '';
    $email_subject = $email_templates_data['subject'] ?? '';
    $email_body = $email_templates_data['html'] ?? '';
    $email_body = do_shortcode($email_body);
    $email_data['email_to'] = $email_to;
    $email_data['email_bcc'] = $email_bcc;
    $email_data['email_from'] = $email_from;
    $email_data['email_from_name'] = $email_from_name;
    $email_data['reply_to'] = $reply_to;
    $email_data['reply_to_name'] = $reply_to_name;

    $email_data['subject'] = strtr($email_subject, $vars);
    $email_data['html'] = strtr($email_body, $vars);
    $email_data['attachments'] = $attachments;
    return $email_data;
}