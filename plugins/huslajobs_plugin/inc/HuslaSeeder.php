<?php

namespace huslajobs;

use Faker\Factory;

class HuslaSeeder {
	private static array $seeders = [];


	public function __construct( callable $function, int $times = 1 ) {
		array_push( self::$seeders, [ 'function' => $function, 'frequency' => $times ] );
	}

	public static function seed() {
		$faker = Factory::create();
		foreach ( self::$seeders as $seeder ) {
			$function  = $seeder['function'];
			$frequency = $seeder['frequency'];
			for ( $i = 0; $i < $frequency; $i ++ ) {
				$function( $faker );
			}
		}

		die( 'Seeding done. Please comment out seeder function' );
	}

}