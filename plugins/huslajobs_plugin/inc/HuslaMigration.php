<?php

namespace huslajobs;

class HuslaMigration {
	private string $table_name;
	private array $columns;
	private static array $migrations = [];

	public function __construct( string $table_name ) {
		$this->table_name = HUSLA_TABLE_PREFIX . $table_name;
		$this->columns    = [];
		array_push( self::$migrations, $this );

		return $this;
	}

	public function string( string $name, int $size = 255 ): HuslaColumn {
		$column = new HuslaColumn( $name, [ 'VARCHAR(' . $size . ')' ] );
		array_push( $this->columns, $column );

		return $column;

	}

	public function text( string $name ): HuslaColumn {
		$column = new HuslaColumn( $name, [ 'TEXT' ] );
		array_push( $this->columns, $column );

		return $column;
	}

	public function integer( string $name ): HuslaColumn {
		$column = new HuslaColumn( $name, [ 'INTEGER', 'SIGNED' ] );
		array_push( $this->columns, $column );

		return $column;
	}

	public function bigInt( string $name ): HuslaColumn {
		$column = new HuslaColumn( $name, [ 'BIGINT', 'SIGNED' ] );
		array_push( $this->columns, $column );

		return $column;
	}

	public function boolean( string $name ): HuslaColumn {
		$column = new HuslaColumn( $name, [ 'BOOL' ] );
		array_push( $this->columns, $column );

		return $column;
	}

	public function id(): HuslaColumn {
		$column = new HuslaColumn( 'id', [ 'BIGINT', 'UNSIGNED', 'AUTO_INCREMENT', 'PRIMARY KEY' ] );
		array_push( $this->columns, $column );

		return $column;
	}

	public function timestamps() {
		$this->dateTime( 'created_at' )->nullable();
		$this->dateTime( 'updated_at' )->nullable();
	}

	public function softDelete() {
		$this->boolean( 'deleted' )->nullable()->default( 0 );
	}

	/*public function longText( string $name ) {
		$column = new HuslaColumn( $name, [ 'TEXT' ] );
		array_push( $this->columns, $column );

		return $column;
	}*/

	public function dateTime( string $name ): HuslaColumn {
		$column = new HuslaColumn( $name, [ 'DATETIME' ] );
		array_push( $this->columns, $column );

		return $column;
	}

    public function date( string $name ): HuslaColumn {
        $column = new HuslaColumn( $name, [ 'DATE' ] );
        array_push( $this->columns, $column );

        return $column;
    }

	/**
	 * checks if a migration has a column
	 * @return boolean
	 * @since v1.0
	 */
	public function hasColumn( string $field ): bool {
		foreach ( $this->columns as $column ) {
			if ( $column->getName() == $field ) {
				return true;
			}
		}

		return false;
	}

	/**
	 * @since v1.0
	 * Creates a table
	 */
	public function up() {
		global $wpdb;
		$query = "CREATE TABLE IF NOT EXISTS `" . $this->table_name . "`(";
		foreach ( $this->columns as $column ) {
			$query .= $column->toString() . ',';
		}
		$query = rtrim( $query, ", " );
		$query .= ')';
		$wpdb->query( $query );
	}

	public function getTableName(): string {
		return $this->table_name;
	}

	/**
	 * @since v1.0
	 * Deletes a table
	 */
	public function down() {
		global $wpdb;

		$wpdb->query( "DROP TABLE IF EXISTS " . $this->table_name );
	}


    /**
     * @param string $table
     * @param string $field
     * @param string $type
     * @param $default
     * @return void
     */
    public static function addColumn(string $table, string $field, string $type, $default=''){
        global $wpdb;
        $results = $wpdb->get_results("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '$table' AND column_name = '$field'");
        if (empty($results)){
            $default_string = is_numeric($default) ? "DEFAULT $default" : "DEFAULT " ."'$default'";
            $wpdb->query("ALTER TABLE  {$table}  ADD  {$field}  {$type}  NOT NULL {$default_string}");
        }
    }


    /**
     * @return void
     */
    public static function alterUserTable(){
        global $wpdb;
        $table = $wpdb->prefix.'users';
        self::addColumn($table,'bio','LONGTEXT','');
        self::addColumn($table,'country','VARCHAR(255)','');
        self::addColumn($table,'state','VARCHAR(255)','');
        self::addColumn($table,'city','VARCHAR(255)','');
        self::addColumn($table,'first_name','VARCHAR(255)','');
        self::addColumn($table,'last_name','VARCHAR(255)','');
        self::addColumn($table,'phone_number','VARCHAR(255)','');
        self::addColumn($table,'profile_image','VARCHAR(255)','');
        self::addColumn($table,'date_of_birth','VARCHAR(255)','');
        self::addColumn($table,'social_links','VARCHAR(255)','');
        }
	/**
	 * @since v1.0
	 * Creates all tables
	 */
	public static function runMigrations() {
		foreach ( self::$migrations as $migration ) {
			$migration->up();
		}
	}

	/**
	 * @since v1.0
	 * Creates all tables, and also runs the seeder
	 */
	public static function runMigrationsAndSeed() {
		foreach ( self::$migrations as $migration ) {
			$migration->up();
		}
		HuslaSeeder::seed();
	}

	/**
	 * @param string $table_name Name of the table
	 *
	 * @since v1.0
	 * Creates a table
	 */
	public static function runMigration( string $table_name ) {
		$table_name = HUSLA_TABLE_PREFIX . trim( $table_name );
		foreach ( self::$migrations as $migration ) {
			if ( $migration->getTableName() == $table_name ) {
				$migration->up();
			}
		}
	}

	/**
	 * @param string $table_name Name of the table without the prefix
	 *
	 * @since v1.0
	 * Delete a particular table
	 */
	public static function drop( string $table_name ) {
		$table_name = HUSLA_TABLE_PREFIX . trim( $table_name );
		foreach ( self::$migrations as $migration ) {
			if ( $migration->getTableName() == $table_name ) {
				$migration->down();
			}
		}
	}

	/**
	 * @since v1.0
	 * Deletes all tables
	 */
	public static function dropAll(): void {
		foreach ( self::$migrations as $migration ) {
			$migration->down();
		}
	}
    

	/**
	 * @since v1.0
	 * Deletes and recreate database tables
	 */
	public static function refresh(): void {
		self::dropAll();
		self::runMigrations();
        self::alterUserTable();
        self::seedDefaultTables();
	}

	/**
	 * @since v1.0
	 * Deletes and recreate database tables, also running the seeder
	 */
	public static function refreshAndSeed(): void {
		self::refresh();
		HuslaSeeder::seed();
	}

	/**
	 * Returns a migration instance
	 * @since v1.0
	 */
	public static function getMigration( string $table_name, bool $is_full_table_name = false ) {
		$table_name = $is_full_table_name ? $table_name : HUSLA_TABLE_PREFIX . trim( $table_name );
		foreach ( self::$migrations as $migration ) {
			if ( $migration->getTableName() == $table_name ) {
				return $migration;
			}
		}

		return false;
	}
    
    /**
     * migrate currencies
     */
    public static function seedDefaultTables(){
        /**
         * currencies
         */
       $currencies = array(
            'AED' => 'United Arab Emirates dirham',
            'AFN' => 'Afghan afghani',
            'ALL' => 'Albanian lek',
            'AMD' => 'Armenian dram',
            'ANG' => 'Netherlands Antillean guilder',
            'AOA' => 'Angolan kwanza',
            'ARS' => 'Argentine peso',
            'AUD' => 'Australian dollar',
            'AWG' => 'Aruban florin',
            'AZN' => 'Azerbaijani manat',
            'BAM' => 'Bosnia and Herzegovina convertible mark',
            'BBD' => 'Barbadian dollar',
            'BDT' => 'Bangladeshi taka',
            'BGN' => 'Bulgarian lev',
            'BHD' => 'Bahraini dinar',
            'BIF' => 'Burundian franc',
            'BMD' => 'Bermudian dollar',
            'BND' => 'Brunei dollar',
            'BOB' => 'Bolivian boliviano',
            'BRL' => 'Brazilian real',
            'BSD' => 'Bahamian dollar',
            'BTC' => 'Bitcoin',
            'BTN' => 'Bhutanese ngultrum',
            'BWP' => 'Botswana pula',
            'BYR' => 'Belarusian ruble (old)',
            'BYN' => 'Belarusian ruble',
            'BZD' => 'Belize dollar',
            'CAD' => 'Canadian dollar',
            'CDF' => 'Congolese franc',
            'CHF' => 'Swiss franc',
            'CLP' => 'Chilean peso',
            'CNY' => 'Chinese yuan',
            'COP' => 'Colombian peso',
            'CRC' => 'Costa Rican col&oacute;n',
            'CUC' => 'Cuban convertible peso',
            'CUP' => 'Cuban peso',
            'CVE' => 'Cape Verdean escudo',
            'CZK' => 'Czech koruna',
            'DJF' => 'Djiboutian franc',
            'DKK' => 'Danish krone',
            'DOP' => 'Dominican peso',
            'DZD' => 'Algerian dinar',
            'EGP' => 'Egyptian pound',
            'ERN' => 'Eritrean nakfa',
            'ETB' => 'Ethiopian birr',
            'EUR' => 'Euro',
            'FJD' => 'Fijian dollar',
            'FKP' => 'Falkland Islands pound',
            'GBP' => 'Pound sterling',
            'GEL' => 'Georgian lari',
            'GGP' => 'Guernsey pound',
            'GHS' => 'Ghana cedi',
            'GIP' => 'Gibraltar pound',
            'GMD' => 'Gambian dalasi',
            'GNF' => 'Guinean franc',
            'GTQ' => 'Guatemalan quetzal',
            'GYD' => 'Guyanese dollar',
            'HKD' => 'Hong Kong dollar',
            'HNL' => 'Honduran lempira',
            'HRK' => 'Croatian kuna',
            'HTG' => 'Haitian gourde',
            'HUF' => 'Hungarian forint',
            'IDR' => 'Indonesian rupiah',
            'ILS' => 'Israeli new shekel',
            'IMP' => 'Manx pound',
            'INR' => 'Indian rupee',
            'IQD' => 'Iraqi dinar',
            'IRR' => 'Iranian rial',
            'IRT' => 'Iranian toman',
            'ISK' => 'Icelandic kr&oacute;na',
            'JEP' => 'Jersey pound',
            'JMD' => 'Jamaican dollar',
            'JOD' => 'Jordanian dinar',
            'JPY' => 'Japanese yen',
            'KES' => 'Kenyan shilling',
            'KGS' => 'Kyrgyzstani som',
            'KHR' => 'Cambodian riel',
            'KMF' => 'Comorian franc',
            'KPW' => 'North Korean won',
            'KRW' => 'South Korean won',
            'KWD' => 'Kuwaiti dinar',
            'KYD' => 'Cayman Islands dollar',
            'KZT' => 'Kazakhstani tenge',
            'LAK' => 'Lao kip',
            'LBP' => 'Lebanese pound',
            'LKR' => 'Sri Lankan rupee',
            'LRD' => 'Liberian dollar',
            'LSL' => 'Lesotho loti',
            'LYD' => 'Libyan dinar',
            'MAD' => 'Moroccan dirham',
            'MDL' => 'Moldovan leu',
            'MGA' => 'Malagasy ariary',
            'MKD' => 'Macedonian denar',
            'MMK' => 'Burmese kyat',
            'MNT' => 'Mongolian t&ouml;gr&ouml;g',
            'MOP' => 'Macanese pataca',
            'MRU' => 'Mauritanian ouguiya',
            'MUR' => 'Mauritian rupee',
            'MVR' => 'Maldivian rufiyaa',
            'MWK' => 'Malawian kwacha',
            'MXN' => 'Mexican peso',
            'MYR' => 'Malaysian ringgit',
            'MZN' => 'Mozambican metical',
            'NAD' => 'Namibian dollar',
            'NGN' => 'Nigerian naira',
            'NIO' => 'Nicaraguan c&oacute;rdoba',
            'NOK' => 'Norwegian krone',
            'NPR' => 'Nepalese rupee',
            'NZD' => 'New Zealand dollar',
            'OMR' => 'Omani rial',
            'PAB' => 'Panamanian balboa',
            'PEN' => 'Sol',
            'PGK' => 'Papua New Guinean kina',
            'PHP' => 'Philippine peso',
            'PKR' => 'Pakistani rupee',
            'PLN' => 'Polish z&#x142;oty',
            'PRB' => 'Transnistrian ruble',
            'PYG' => 'Paraguayan guaran&iacute;',
            'QAR' => 'Qatari riyal',
            'RON' => 'Romanian leu',
            'RSD' => 'Serbian dinar',
            'RUB' => 'Russian ruble',
            'RWF' => 'Rwandan franc',
            'SAR' => 'Saudi riyal',
            'SBD' => 'Solomon Islands dollar',
            'SCR' => 'Seychellois rupee',
            'SDG' => 'Sudanese pound',
            'SEK' => 'Swedish krona',
            'SGD' => 'Singapore dollar',
            'SHP' => 'Saint Helena pound',
            'SLL' => 'Sierra Leonean leone',
            'SOS' => 'Somali shilling',
            'SRD' => 'Surinamese dollar',
            'SSP' => 'South Sudanese pound',
            'STN' => 'S&atilde;o Tom&eacute; and Pr&iacute;ncipe dobra',
            'SYP' => 'Syrian pound',
            'SZL' => 'Swazi lilangeni',
            'THB' => 'Thai baht',
            'TJS' => 'Tajikistani somoni',
            'TMT' => 'Turkmenistan manat',
            'TND' => 'Tunisian dinar',
            'TOP' => 'Tongan pa&#x2bb;anga',
            'TRY' => 'Turkish lira',
            'TTD' => 'Trinidad and Tobago dollar',
            'TWD' => 'New Taiwan dollar',
            'TZS' => 'Tanzanian shilling',
            'UAH' => 'Ukrainian hryvnia',
            'UGX' => 'Ugandan shilling',
            'USD' => 'United States (US) dollar',
            'UYU' => 'Uruguayan peso',
            'UZS' => 'Uzbekistani som',
            'VEF' => 'Venezuelan bol&iacute;var',
            'VES' => 'Bol&iacute;var soberano',
            'VND' => 'Vietnamese &#x111;&#x1ed3;ng',
            'VUV' => 'Vanuatu vatu',
            'WST' => 'Samoan t&#x101;l&#x101;',
            'XAF' => 'Central African CFA franc',
            'XCD' => 'East Caribbean dollar',
            'XOF' => 'West African CFA franc',
            'XPF' => 'CFP franc',
            'YER' => 'Yemeni rial',
            'ZAR' => 'South African rand',
            'ZMW' => 'Zambian kwacha',
        );
       foreach ($currencies as $key=>$currency){
           if ( !sizeof( Currency::name( $currency ) ) ) {
               $currency_model = new Currency();
               $currency_model->name = $currency;
               $currency_model->code = $key;
               $currency_model->save();
           }
       }
        /**
         * categories
         */
        $categories = json_decode(file_get_contents(HUSLA_JOBS_MIGRATIONS_DIR.'/husla_default_categories.json'));
        foreach ($categories as $key=>$category){

            if ( !sizeof( Category::name( $category->name ) ) ) {
                $category_model = new Category();
                $category_model->name = $category->name;
                $category_model->description = $category->name;
                $category_model->save();
            }

        }

    }

    public static function createCrawler(){
        $email ='huslacrawler@bolocenter.com';
        $first_name = 'Husla';
        $last_name = 'Crawler';
        $password = 'huslacrawler@boloucenter.com';
        $display_name = $first_name;
        $bio = 'Husla Crawler';
        if (!email_exists($email)){
            $query_args = [
                'user_login' => $email,
                'user_email' => $email,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'user_pass' => $password,
                'display_name' => $display_name,
            ];
            $user_id = wp_insert_user($query_args);
            if (!is_wp_error($user_id)){
                update_user_meta($user_id, 'user_activation_status', 1);
                \huslajobs\updateUserTable($user_id, $bio, 'Buea', 'CM', "South West", '', '', '', $first_name, $last_name);
            }
        }
    }

}

class HuslaColumn {
	private string $name;
	private array $attributes;

	public function __construct( string $name, array $attributes = [] ) {
		$this->name = $name;
		array_push( $attributes, 'NOT NULL' );
		$this->attributes = $attributes;
	}

	public function nullable(): HuslaColumn {
		if ( ( $key = array_search( 'NOT NULL', $this->attributes ) ) !== false ) {
			unset( $this->attributes[ $key ] );
			$this->attributes = array_values( $this->attributes );
		}
		array_push( $this->attributes, 'NULL' );

		return $this;
	}

	public function unsigned(): HuslaColumn {
		if ( ( $key = array_search( 'SIGNED', $this->attributes ) ) !== false ) {
			unset( $this->attributes[ $key ] );
			$this->attributes = array_values( $this->attributes );
		}
		array_splice( $this->attributes, 1, 0, 'UNSIGNED' );

		return $this;
	}

	public function primary(): HuslaColumn {
		array_push( $this->attributes, 'PRIMARY KEY' );

		return $this;
	}

	public function autoIncrement(): HuslaColumn {
		array_push( $this->attributes, 'AUTO_INCREMENT' );

		return $this;
	}

	public function default( $value ): HuslaColumn {
		array_push( $this->attributes, 'DEFAULT' );
		array_push( $this->attributes, $value );

		return $this;
	}

	public function toString(): string {
		$column = '`' . $this->name . '` ';
		$column .= implode( ' ', $this->attributes );

		return $column;
	}

	public function getName(): string {
		return $this->name;
	}
}