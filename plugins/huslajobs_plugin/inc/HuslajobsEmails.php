<?php

    namespace huslajobs;

    class HuslajobsEmails
    {

        public function __construct() {
            // do something here
        }
        public function sendEmail($email_data){
            
            $email_to = $email_data['email_to'] ?? '';
            $email_bcc = $email_data['email_bcc'] ?? '';

            $email_from = $email_data['email_from'] ?? get_option('admin_email');
            $email_from_name = $email_data['email_from_name'] ?? get_bloginfo('name');

            $reply_to = $email_data['reply_to'] ?? get_option('admin_email');
            $reply_to_name = $email_data['reply_to_name'] ?? get_bloginfo('name');

            $subject = $email_data['subject'] ?? '';
            $email_body = $email_data['html'] ?? '';
            $attachments = $email_data['attachments'] ?? [];
            $headers = [];
            $headers[] = "From: ".$email_from_name." <".$email_from.">";

            if(!empty($reply_to)){
                $headers[] = "Reply-To: ".$reply_to_name." <".$reply_to.">";
            }

//            $headers[] = "MIME-Version: 1.0";
            $headers[] = "Content-Type: text/html";
            if(!empty($email_bcc)){
                $headers[] = "Bcc: ".$email_bcc;
            }

            return wp_mail($email_to, $subject, $email_body, $headers, $attachments);

        }

        public function emailTemplatesData(){

            $templates_data_html = array();

            include HUSLA_JOBS_TEMPLATES_DIR. 'emails/user_registered.php';
            include HUSLA_JOBS_TEMPLATES_DIR . 'emails/email_confirmed.php';
            include HUSLA_JOBS_TEMPLATES_DIR . 'emails/email_resend_key.php';
            include HUSLA_JOBS_TEMPLATES_DIR . 'emails/lost_password.php';
            include HUSLA_JOBS_TEMPLATES_DIR . 'emails/send_mail_otp.php';
            include HUSLA_JOBS_TEMPLATES_DIR . 'emails/package_subscription_email.php';
            include HUSLA_JOBS_TEMPLATES_DIR . 'emails/company-job-application.php';
            include HUSLA_JOBS_TEMPLATES_DIR . 'emails/jobseeker-application-email.php';
            include HUSLA_JOBS_TEMPLATES_DIR . 'emails/expired-subscription.php';
            include HUSLA_JOBS_TEMPLATES_DIR . 'emails/contact_me_email.php';


            $templates_data = array(
                'user_registered'=>array(
                    'name'=>__('New user registered','huslajobs'),
                    'description'=>__('Notification email for admin when a new user is registered.','huslajobs'),
                    'subject'=>__('New user submitted - {site_url}','huslajobs'),
                    'html'=>$templates_data_html['user_registered'],
                    'email_to'=>get_option('admin_email'),
                    'email_from'=>get_option('admin_email'),
                    'email_from_name'=> get_bloginfo('name'),
                    'enable'=> 'yes',
                ),
                'lost_password'=>array(
                    'name'=>__('Password Recovery','huslajobs'),
                    'description'=>__('Notification email for admin when a user recover their password.','huslajobs'),
                    'subject'=>__('Password Recovery - {site_url}','huslajobs'),
                    'html'=>$templates_data_html['lost_password'],
                    'email_to'=>get_option('admin_email'),
                    'email_from'=>get_option('admin_email'),
                    'email_from_name'=> get_bloginfo('name'),
                    'enable'=> 'yes',
                ),
                'email_confirmed'=>array(
                    'name'=>__('New user confirmed','huslajobs'),
                    'description'=>__('Notification email for confirming a new user.','huslajobs'),
                    'subject'=>__('New user confirmed - {site_url}','huslajobs'),
                    'html'=>$templates_data_html['email_confirmed'],
                    'email_to'=>get_option('admin_email'),
                    'email_from'=>get_option('admin_email'),
                    'email_from_name'=> get_bloginfo('name'),
                    'enable'=> 'yes',
                ),
                'email_resend_key'=>array(
                    'name'=>__('Resend activation key','huslajobs'),
                    'description'=>__('Notification email for resend activation key.','huslajobs'),
                    'subject'=>__('Please verify account - {site_url}','huslajobs'),
                    'html'=>$templates_data_html['email_resend_key'],
                    'email_to'=>get_option('admin_email'),
                    'email_from'=>get_option('admin_email'),
                    'email_from_name'=> get_bloginfo('name'),
                    'enable'=> 'yes',
                ),
                'send_mail_otp'=>array(
                    'name'=>__('Send mail OTP','huslajobs'),
                    'description'=>__('Notification email for sending mail OTP.','huslajobs'),
                    'subject'=>__('OTP - {site_url}','huslajobs'),
                    'html'=>$templates_data_html['send_mail_otp'],
                    'email_to'=>get_option('admin_email'),
                    'email_from'=>get_option('admin_email'),
                    'email_from_name'=> get_bloginfo('name'),
                    'enable'=> 'yes',
                ),
                'package_subscription_email'=>array(
                    'name'=>__('Package Subscription','huslajobs'),
                    'description'=>__('Notification email for package subscription.','huslajobs'),
                    'subject'=>__('Package Subscription - {site_url}','huslajobs'),
                    'html'=>$templates_data_html['package_subscription_email'],
                    'email_to'=>get_option('admin_email'),
                    'email_from'=>get_option('admin_email'),
                    'email_from_name'=> get_bloginfo('name'),
                    'enable'=> 'yes',
                ),
                'company_job_application'=>array(
                    'name'=>__('Job Application','huslajobs'),
                    'description'=>__('Notification email for job application.','huslajobs'),
                    'subject'=>__('Job Application - {site_url}','huslajobs'),
                    'html'=>$templates_data_html['company_job_application'],
                    'email_to'=>get_option('admin_email'),
                    'email_from'=>get_option('admin_email'),
                    'email_from_name'=> get_bloginfo('name'),
                    'enable'=> 'yes',
                ),
                'job_seeker_application_email'=>array(
                    'name'=>__('Job Seeker Application','huslajobs'),
                    'description'=>__('Notification email for job application.','huslajobs'),
                    'subject'=>__('Job Seeker Application - {site_url}','huslajobs'),
                    'html'=>$templates_data_html['job_seeker_application_email'],
                    'email_to'=>get_option('admin_email'),
                    'email_from'=>get_option('admin_email'),
                    'email_from_name'=> get_bloginfo('name'),
                    'enable'=> 'yes',
                ),
                'expired_subscription_email'=>array(
                    'name'=>__('Expired Package Subscription','huslajobs'),
                    'description'=>__('Notification email for expired package subscription.','huslajobs'),
                    'subject'=>__('Expired Package Subscription - {site_url}','huslajobs'),
                    'html'=>$templates_data_html['expired_subscription_email'],
                    'email_to'=>get_option('admin_email'),
                    'email_from'=>get_option('admin_email'),
                    'email_from_name'=> get_bloginfo('name'),
                    'enable'=> 'yes',
                ),
                'contact_me_email'=>array(
                    'name'=>__('Request For Service','huslajobs'),
                    'description'=>__('Notification email for request service.','huslajobs'),
                    'subject'=>__('Request For Service - {site_url}','huslajobs'),
                    'html'=>$templates_data_html['contact_me_email'],
                    'email_to'=>get_option('admin_email'),
                    'email_from'=>get_option('admin_email'),
                    'email_from_name'=> get_bloginfo('name'),
                    'enable'=> 'yes',
                ),
            );
            
            return $templates_data;

        }
    }