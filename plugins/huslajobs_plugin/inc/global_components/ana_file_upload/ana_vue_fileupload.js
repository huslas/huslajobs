/**
 * A simple Vuejs select/multiselect component
 * Author: Akombo Neville Akwo
 *
 */

const AnaVueFileUpload = {
    emits: ['search-change', 'update:modelValue', 'open'],
    props: {
        value: {
            required: false,
        },
        modelValue: {
            required: true,
        },
        uploadButtonText: {
            type: String,
            required: true,
        },
        fileTypes: {
            type: String,
            required: true,
        },
        acceptedFiles: {
            type: String,
            required: true,
        },
        typesTitle: {
            type: String,
            required: true,
        },
        maxUploadSize: {
            type: Number,
            required: true,
        },
        wordUrl: {
            type: String,
            required: false,
        },
        pdfUrl: {
            type: String,
            required: false,
        },
        changeButtonText: {
            type: String,
            required: true,
        },
        maxSizeLabel: {
            type: String,
            required: true,
        },
        crop: {
            required: false,
            default: false,
        }

    },
    data() {
        return {

            showCroppieModal: false,
            cropper: undefined,
            avatar: this.$refs.profileImage,
            input: this.$refs.fileInput,
            image: this.$refs.modalImage,
            modal: this.$refs.croppieModal,
            fileName: undefined,
            imageUrl:THEME_URL

        };
    },
    watch: {
        'showCroppieModal': function (currentVal, oldVal) {
            if (currentVal) {
                this.cropper = new Cropper(this.$refs.modalImage, {
                    aspectRatio: 2 / 1.5,
                    viewMode: 0,
                    enableZoom: true,
                    showZoomer: true,
                    ready: function () {
                        let clone = this.cloneNode();
                        clone.className = '';
                        clone.style.cssText = (
                            'display: block;' +
                            'width: 400px;'
                        );
                    },

                });
            } else {
                if (this.cropper) {
                    this.cropper.destroy();
                    this.cropper = undefined
                }
            }
        }
    },
    methods: {
        updateModel(modelVal) {
            this.$emit('update:modelValue', modelVal);
        },
        /**
         * get file Url
         * @param file:array
         * @returns file base64url
         */
        async getDataURL(file) {
            return new Promise((resolve, reject) => {
                const reader = new FileReader()
                reader.onload = () => {
                    resolve(reader.result)
                }
                reader.onerror = reject

                reader.readAsDataURL(file)
            })
        },
        dataURLtoFile(fileData) {
            let base64 = fileData.base64;
            let arr = base64.split(','),
                mime = arr[0].match(/:(.*?);/)[1],
                bstr = atob(arr[1]),
                n = bstr.length,
                u8arr = new Uint8Array(n);

            while (n--) {
                u8arr[n] = bstr.charCodeAt(n);
            }
            const file = new File([u8arr], fileData.name, {type: mime});
            const results = {file: file, url: URL.createObjectURL(file)}
            return results;
        },
        async getFile(e) {
            const files = e.target.files || e.dataTransfer.files
            if (!files.length) return
            const file = files[0];
            const maxSize = parseInt(this.maxUploadSize);
            const file_size = (file.size / 1024 / 1024).toFixed(2) //in megabytes

            if (parseInt(file_size) <= maximum_upload) {
                // file.name = file.name.replace(/[^a-zA-Z0-9.-]/g, '-')
                if (this.crop) {
                    this.resizeImage(file);
                }else{
                    const imageFile = {
                        url: '',
                        file: file,
                        name: file.name,
                        type: file.type,
                        size: file.size,
                        lastModified: file.lastModified,
                        ...file,
                        base64: '',
                    }
                    imageFile.url = URL.createObjectURL(file);

                    imageFile.base64 = await this.getDataURL(file);
                    this.updateModel(imageFile);
                }


            } else {
                toastr.error(`${file.name},file size exceeded maximum upload size ${maximum_upload}MB`);
                e.target.value = ''
            }
        },
        clearFile() {
            this.updateModel(undefined)
            this.$refs.fileInput.value = '';
        },
        cvUrl(url) {
            let extension = url.split('.')[1];
            return extension == 'pdf' ? this.pdfUrl : this.wordUrl
        },
        preViewCrop(files) {
            const that = this;
            let done = function (url) {
                that.$refs.fileInput.value = '';
                that.$refs.modalImage.setAttribute('src', url);
                that.showCroppieModal = true;
            };

            let reader;
            let file;
            let url;
            let r;

            if (files && files.length > 0) {
                file = files[0];

                this.fileName = file.name;
                if (URL) {
                    done(URL.createObjectURL(file));
                    r = new FileReader();
                    r.readAsDataURL(file);
                } else if (FileReader) {
                    reader = new FileReader();
                    reader.onload = function (e) {
                        done(reader.result);
                    };
                    reader.readAsDataURL(file);
                }
            }

        },
        deleteContent(j) {
            $('#image-' + j).remove();
        },
        cropImage() {
            let initialAvatarURL;
            let canvas;
            if (this.cropper) {
                canvas = this.cropper.getCroppedCanvas({
                    width: 800,
                    height: 600,
                });
                // this.avatar.setAttribute('src', canvas.toDataURL());
                const that = this;
                canvas.toBlob(function (blob) {
                    let reader = new FileReader();
                    reader.readAsDataURL(blob);
                    reader.onloadend = function () {
                        let base64data = reader.result;
                        const convertedData = that.dataURLtoFile({"name": that.fileName, "base64": base64data});
                        that.updateModel(convertedData);
                        that.closeModal();
                    }
                });
            }
        },
        resizeImage(file) {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            const that = this;
            reader.onload = function (event) {
                const imgElement = document.createElement("img");
                imgElement.src = event.target.result;
                imgElement.onload = function (e) {
                    const canvas = document.createElement("canvas");
                    const MAX_WIDTH = 600;
                    const scaleSize = MAX_WIDTH / e.target.width;
                    canvas.width = MAX_WIDTH;
                    canvas.height = e.target.height * scaleSize;
                    const ctx = canvas.getContext("2d");
                    ctx.drawImage(e.target, 0, 0, canvas.width, canvas.height);
                    const srcEncoded = ctx.canvas.toDataURL(e.target, "image/jpeg");
                    const convertedData = that.dataURLtoFile({"name": file.name, "base64": srcEncoded});
                    that.updateModel(convertedData);
                }
            }
        },
        closeModal() {
            this.showCroppieModal = false;
        }

    },
    destroyed() {
        // important to clean up!!
        document.removeEventListener('click', this.deactivate)
    },
    template: `<div
        ref="anaVueFileUpload"
        class="ana-file-container p-0 position-relative"
>
    <div class="d-flex flex-wrap align-items-end py-5 px-3">
        <div class="profile-image-preview mr-5">
            <div class="image">
                <div v-if="modelValue">
                    <img v-if="wordUrl || pdfUrl || !crop" ref="profile-image"
                         :src="cvUrl(modelValue.file?.name ?? modelValue.url)"
                         :alt="modelValue.name">

                    <img v-else ref="profileImage" :src="modelValue.url"
                         :alt="modelValue.name" :url="modelValue.url" >
                    <a @click="clearFile" class="clear-preview"><span class="fa fa-trash"></span></a>
                </div>
               
            </div>
            <div>
                <p class="m-0">{{typesTitle}}: {{fileTypes}}</p>
                <p class="m-0">{{maxSizeLabel}}:{{maxUploadSize}}M</p>
            </div>
        </div>
        <div class="profile-image-btn">
            <label class="hs-btn hs-btn-gray cursor-pointer">
<!--                <input v-if="crop" type="file" ref="fileInput" name="profile-image" :accept="acceptedFiles"-->
<!--                       class="d-none" @input="preViewCrop">-->
                <input type="file" ref="fileInput" name="profile-image" :accept="acceptedFiles"
                       class="d-none" @input="getFile">
                <span v-if="modelValue">{{changeButtonText}}</span>
                <span v-else>{{uploadButtonText}}</span>
            </label>
        </div>
    </div>
    <!--  croppie modal  -->
    <div v-show="showCroppieModal" class="modal show" :class="{'d-block':showCroppieModal}" id="modal-croppie" ref="croppieModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
         data-backdrop="static" data-keyboard="false" aria-hidden="true" >
        <div class="" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Crop image</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" @click="closeModal">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="img-container">
                        <img id="ddimage" ref="modalImage" style="max-height : 300px; width : auto; object-fit: contain;">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="hs-btn hs-secondary-text-color hs-btn-gray-outline" data-dismiss="modal" @click="closeModal">Cancel</button>
                    <button  type="button" class="hs-btn hs-btn-primary" id="crop-image" @click="cropImage">Crop</button>
                </div>
            </div>
        </div>
    </div>
</div>
        
`
}
export {AnaVueFileUpload}