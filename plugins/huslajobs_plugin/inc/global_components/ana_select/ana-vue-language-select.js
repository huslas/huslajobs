/**
 *
 */


const AnaVueLanguageSelect ={
    emits: ['search-change','update:modelValue','open'],
    props: {
        value: {
            required: false,
        },
        modelValue: {
            required: false,
        },
        imageUrl:{
            required: false,
        },
        /**
         * options: an array of objects [{label:'Akombo Neville',value:'Akombo Neville'},]
         * label will be the option text and value the option value
         */
        trackBy: {
            type: String,
            required: false,
            default: 'label',
        },
        noOptionsText: {
            type: String,
            required: false,
            default: 'The list is empty',
        },
        noResultsText: {
            type: String,
            required: false,
            default: 'No results found',
        },
        disabled:{
            type: Boolean,
            required: false,
            default: false,
        }
    },
    data() {
        return {
            search:'',
            active:false,
            selectedOption:  {
                label: "United States",
                value: "US"},
            searchText:'',
            imageUrl:THEME_URL,
            options:[  {
                label: "United States",
                value: "US"},
                {
                    label: "France",
                    value: "FR"
                }
            ],
            defaultCountry:undefined,
            selectOptions:this.options
        };
    },

    refs:['searchInput'],
    methods:{
        updateModel(option){
            // this.selectedOption = option.label;
            // this.$emit('update:modelValue', option.value);
            // this.active = false;
            // this.$refs.searchInput.blur()
        },
        deactivate (event) {
            let el = this.$refs.anaVueSelect;
            let target = event.target
            if ( el && (el !== target) && !el.contains(target)) {
                this.active = false
                setTimeout(() => {
                    if (!this.active) {
                        // close()
                        this.searchText = '';
                        if (!this.selectedOption && this.selectedOption !== undefined){
                            this.$emit('update:modelValue', '');
                        }
                    }
                }, 1)
            }


        },
        clear(){
            this.selectedOption = '';
            this.searchText = '';
            this.$emit('update:modelValue', '');
        },
        activate(){
            if (!this.disabled){
                this.active = true;
                this.showInput();
                this.$emit('open')
            }

        },
        // options(countriesCodes=['us','fr']){
        //     const countries = world_countries;
        //     const site_Languages =  countries.filter((country)=> countriesCodes.indexOf(country.value.toLowerCase()) !== -1);
        //     if (site_Languages[0] !== undefined){
        //         this.selectedOption = site_Languages[0];
        //     }
        //
        // }
    },
    watch: {
        searchText :function(currentVal, oldVal){
            this.$emit('search-change',currentVal);
        },
    },
    created () {
        document.addEventListener('click', this.deactivate)
    },
    destroyed () {
        // important to clean up!!
        document.removeEventListener('click', this.deactivate)
    },
    computed:{
        watchModelVal(){
            return this.options;
        }
    },
    mounted(){
        const that = this;
        fetch('https://api.ipregistry.co/?key=tryout')
            .then(function (response) {
                return response.json();
            })
            .then(function (payload) {
                const countries = world_countries;
               const user_country = countries.filter((option)=>option.value.code.toLowerCase() === payload.location.country.code.toLowerCase() );
                that.selectedOption = user_country.length ? user_country[0].value : that.selectedOption;
            });
    },
    template: `

        <div
         
        ref="anaVueSelect"
        class="ana-custom-select custom-select-container d-flex flex-column flex-wrap p-0 position-relative"
        :class="{'not-allowed':disabled}"  
        :disabled="disabled"
    >
                <div class="selected position-relative" @click="activate" :disabled="disabled">
                    <p class="form-control selected-text pr-2 mb-0 ">
                         <img :src="imageUrl+'/flags/'+ selectedOption.value.toLowerCase()+'.png'" alt="country flag">  
                    </p>
                    <div class="selected-icons position-absolute top-50 end-0 translate-middle-y">
                        <i v-if="active" class="fas fa-chevron-up"></i>
                        <i v-else class="fas fa-chevron-down"></i>   
                     </div>
                </div>
                
            <div  class="options-container w-100" :class="active?'':'is-hidden'" tabindex="-1" >
                <ul class="options m-0 p-0" v-if="options.length">
                    <li v-for="(option,key) in options" class="option m-0 p-3" :key="key" :class="{'active':selectedOption==option || modelValue===option.value}" value="option.value" @click.stop.prevent="updateModel(option)">
                        <img :src="imageUrl+'/flags/'+ option.value.toLowerCase()+'.png'" class="mr-1" alt="country flag">
                        <span class="mr-1">{{option.label}}</span>                        
                    </li>
                </ul>
                <p v-else>
                <span v-if="!searchText">{{noOptionsText}}</span>
                <span v-else>{{noResultsText}}</span>
                </p>
            </div>
        </div>
`
}
export {AnaVueLanguageSelect}