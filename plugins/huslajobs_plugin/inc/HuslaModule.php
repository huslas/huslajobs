<?php

namespace huslajobs;

use Exception;


class HuslaModule {
	/** Render a template file. */
	protected string $parent_module;
	protected string $module;

	protected function renderContent( string $template = '', bool $echo = true ) {

		$parent_module_folder = $this->parent_module == 'client' ? HUSLA_JOBS_CLIENT_MODULE_DIR : HUSLA_JOBS_ADMIN_MODULE_DIR;
		// Start output buffering.
		ob_start();
		ob_implicit_flush( 0 );
		try {
			include $parent_module_folder . '/' . $this->module . '/templates/' . $template . '.php';
		} catch ( Exception $e ) {
			ob_end_clean();
			throw $e;
		}

		if ( $echo ) {
			echo ob_get_clean();
		} else {
			return ob_get_clean();
		}
	}

	public static function getModules( string $dir, bool $show_folder_name = true ): array {
		$ffs   = scandir( $dir );
		$files = [];
		unset( $ffs[ array_search( '.', $ffs, true ) ] );
		unset( $ffs[ array_search( '..', $ffs, true ) ] );

		// prevent empty ordered elements


		foreach ( $ffs as $ff ) {
			if ( is_dir( $dir . '/' . $ff ) ) {
				$files = array_merge( $files, self::getModules( $dir . '/' . $ff, $show_folder_name ) );
			} else {
				if ( strpos( $ff, 'Module' ) > 0 ) {
					if ( $show_folder_name ) {
						array_push( $files, $dir . '/' . $ff );
					} else {
						array_push( $files, $ff );
					}
				}
			}
		}

		return $files;
	}
}