<?php

namespace huslajobs;

class WooPayment
{
    private $checkout_info = array();
    private $product_id;


    public function __construct()
    {
        $this->product_id = get_option('husla_jobs_woo_product_id') ? esc_html(get_option('husla_jobs_woo_product_id')) : '';

        $this->addActions();
        $this->addFilters();
    }

    public function addActions()
    {
        add_action('wp_ajax_hs_add_to_cart', [$this, 'addToCart']);
        add_action('wp_ajax_nopriv_hs_add_to_cart', [$this, 'addToCart']);

        // woocommerce actions
        add_action('woocommerce_add_order_item_meta', [$this, 'addBookingMetaData'], 10, 3);
        add_action('woocommerce_after_order_itemmeta', [$this, 'bookingMetaData'], 10, 1);
        add_action('woocommerce_before_calculate_totals', [$this, 'beforeCalculatePrice'], 10, 1);
        add_action('woocommerce_order_item_meta_end', [$this, 'bookingMetaData'], 10, 1);
        add_action('woocommerce_checkout_update_customer', [$this, 'checkoutUpdateCustomer'], 10, 2);
        add_action('woocommerce_thankyou', array($this, 'redirectAfterPayment'), 50, 2);
        add_action('woocommerce_order_status_cancelled', array($this, 'cancelOrder'), 10, 1);
        add_action('woocommerce_order_status_completed', array($this, 'paymentComplete'), 10, 1);
        add_action('woocommerce_order_status_on-hold', array($this, 'paymentComplete'), 10, 1);
        add_action('woocommerce_order_status_processing', array($this, 'paymentComplete'), 10, 1);
        add_action('woocommerce_order_status_refunded', array($this, 'cancelOrder'), 10, 1);
        add_action('woocommerce_deleted_order_items', array($this, 'deletedOrder'), 10, 1);
    }

    public function addFilters()
    {
//            add_filter('woocommerce_get_price_html', 'alterPriceDisplay', 9999, 2);
        add_filter('woocommerce_checkout_get_value', array($this, 'update_billing_info'), 10, 2);
        add_filter('woocommerce_get_item_data', array($this, 'updateCartMetaItem'), 10, 2);
        add_filter('woocommerce_quantity_input_args', array($this, 'removeQuantityField'), 10, 2);
//
        add_filter('woocommerce_available_payment_gateways', array($this, 'disableAvailablePaymentGateways'), 10, 2);
        add_filter('woocommerce_checkout_fields', [$this, 'removeCheckoutFields']);
        // Removes Order Notes Title - Additional Information & Notes Field
        add_filter('woocommerce_enable_order_notes_field', '__return_false', 9999);
        add_filter('woocommerce_order_button_text', [$this, 'customButtonText'], 70);
        add_filter('woocommerce_add_to_cart_redirect', [$this, 'skipCartRedirectCheckout']);


    }

    public function alterPriceDisplay($price_html, $product)
    {
        // ONLY ON FRONTEND
        if (is_admin()) return $price_html;
        // ONLY IF PRICE NOT NULL
        if ('' === $product->get_price()) return $price_html;
        $orig_price = wc_get_price_to_display($product);
        $price_html = wc_price($orig_price * 0.80);
        return $price_html;

    }

    public function addToCart(): void
    {
        global $user_ID;

        $extra_data = array_merge($_REQUEST, ['user_id' => $user_ID]);
        if ($_FILES['cv']){
            $cv = $_FILES['cv'];
            $user_cv = $cv ? \huslajobs\saveFile($cv)['url'] : $_POST['cv'];
            $extra_data = array_merge($extra_data,['cv' => $user_cv]);
        }
        WC()->cart->empty_cart();
        WC()->cart->add_to_cart($this->product_id, 1, '', array(), array('wooextradata' => $extra_data));
        echo json_encode(['success' => true, 'checkout_url' => wc_get_checkout_url(), 'product_id' => $this->product_id, 'wooextradata' => $extra_data, 'user_by_id' => get_user_by('id', $user_ID)]);
        wp_die();
    }

    public function addBookingMetaData($item_id, $values, $wc_key)
    {
        if (isset ($values['wooextradata'])) {
            wc_update_order_item_meta($item_id, 'wooextradata', $values['wooextradata']);
        }
    }

    public function bookingMetaData($item_id)
    {
        $data = wc_get_order_item_meta($item_id, 'wooextradata');
        if ($data) {
            $other_data = $this->updateCartMetaItem(array(), array('wooextradata' => $data));
            if (!empty($other_data)) {
                echo '<br/>' . $other_data[0]['name'] . '<br/>' . nl2br($other_data[0]['value']);
            }
        }
    }

    public function beforeCalculatePrice($cart_object)
    {
        foreach ($cart_object->cart_contents as $wc_key => $wc_item) {
            if (isset ($wc_item['wooextradata'])) {
                $wc_item['data']->set_price(intval($wc_item['wooextradata']['package_price']));
            }
        }
    }

    public function checkoutUpdateCustomer($customer, $data)
    {

        if (!is_user_logged_in() || is_admin()) return;

        // Get the user ID
        $user_id = $customer->get_id();

        // Get the default wordpress first name and last name (if they exist)
        $user_first_name = get_user_meta($user_id, 'first_name', true);
        $user_last_name = get_user_meta($user_id, 'last_name', true);

        if (empty($user_first_name) || empty($user_last_name)) return;

        // We set the values by defaul worpress ones, before it's saved to DB
        $customer->set_first_name($user_first_name);
        $customer->set_last_name($user_last_name);
    }

    public function cancelOrder($order_id)
    {
        global $wpdb, $service_finder_Tables;
        $order = new \WC_Order($order_id);
        if ($order->get_status() != 'failed') {
            foreach ($order->get_items() as $item_id => $order_item) {
                $wooextradata = wc_get_order_item_meta($item_id, 'wooextradata');
                if ($wooextradata && !isset ($wooextradata['completed'])) {

                    $row = $wpdb->get_row($wpdb->prepare('SELECT `user_id` FROM ' . $wpdb->prefix . 'usermeta WHERE `meta_value` = %d AND `meta_key` = "order_id"', $order_id));

                    if (!empty($row)) {
//                            $userId = $row->user_id;
//                            update_user_meta($userId, 'upgrade_request_status','cancelled');

                        $wooextradata['cancelled'] = true;
                        wc_update_order_item_meta($item_id, 'wooextradata', $wooextradata);
                    }

                }
            }
        }
    }

    public function paymentComplete($order_id)
    {
        global $wpdb;
        $order = new \WC_Order($order_id);


        if ($order->get_status() != 'failed') {
            foreach ($order->get_items() as $item_id => $order_item) {
                $wooextradata = wc_get_order_item_meta($item_id, 'wooextradata');
                $pro_id = $order_item->get_product_id();
                if ($pro_id == $this->product_id) {
                    $user_id = $wooextradata['user_id'];
                    $this->afterPayment($user_id, $wooextradata, $order_id, $item_id);
                    break;
                }

            }
        }
    }

    public function afterPayment($userId, $wooextradata, $order_id, $item_id)
    {
        global $wpdb;
        $order = new \WC_Order($order_id);
        /**
         * update subscription table
         * compose email and send to user
         *
         */

        $payment_method = $order->get_payment_method();
        $order_status = $order->get_status();
        if (($order_status == 'completed' || $order_status == 'processing') && $wooextradata && !isset ($wooextradata['completed']) && !isset ($wooextradata['cancelled'])) {
            if ($wooextradata['application_type'] === 'package_subscription') {
                $subscriptions = Subscription::where('wp_user_id', '=', "'" . $userId . "'")->andWhere('status', '=', 1)->get();
                if (sizeof($subscriptions) > 0) {
                    $subscription = $subscriptions [0];
                    $subscription->status = 0;
                    $subscription->save();
                }

                //save subscription
                $today = date("Y-m-d H:i:s");
                $package = Package::find($wooextradata['package_id']);
                $limit = intval($wooextradata['package_duration']); //limit in months
                $end_date = date('Y-m-d H:i:s', strtotime("+$limit months", strtotime($today)));
                $subscription = new Subscription();
                $subscription->wp_user_id = $userId;
                $subscription->package_id = $package->id;
                $subscription->start_date = $today;
                $subscription->end_date = $end_date;
                $subscription->save();

                /**
                 * reset number of job applications to 0
                 */
                update_user_meta(intval($userId), 'job_applications', 0);

                // todo save transactions
                //   build email and sent

                $userVerificationEmails = new HuslajobsEmails();
                $emailTemplatesData = $userVerificationEmails->emailTemplatesData();
                $email_templates_data = $emailTemplatesData['package_subscription_email'];
                $email_bcc = $email_templates_data['email_bcc'] ?? '';
                $email_from = $email_templates_data['email_from'] ?? '';
                $email_from_name = $email_templates_data['email_from_name'] ?? '';
                $reply_to = $email_templates_data['reply_to'] ?? '';
                $reply_to_name = $email_templates_data['reply_to_name'] ?? '';
                $email_subject = $email_templates_data['subject'] ?? '';
                $email_body = $email_templates_data['html'] ?? '';
                $email_body = do_shortcode($email_body);
                $email_body = wpautop($email_body);
                $site_name = get_bloginfo('name');
                $site_description = get_bloginfo('description');
                $site_url = get_bloginfo('url');
                $user_data = get_userdata($userId);
                $vars = array(
                    '{site_name}' => esc_html($site_name),
                    '{site_description}' => esc_html($site_description),
                    '{site_url}' => esc_url_raw($site_url),
                    '{user_name}' => esc_html(get_user_meta($userId, 'first_name', true)),
                    '{user_display_name}' => esc_html($user_data->display_name),
                    '{user_email}' => esc_html($user_data->user_email),
                    '{package_name}' => esc_html($wooextradata['package_name']),
                );
                $email_data['email_to'] = $user_data->user_email;
                $email_data['email_bcc'] = $email_bcc;
                $email_data['email_from'] = $email_from;
                $email_data['email_from_name'] = $email_from_name;
                $email_data['reply_to'] = $reply_to;
                $email_data['reply_to_name'] = $reply_to_name;
                $email_data['subject'] = strtr($email_subject, $vars);
                $email_data['html'] = strtr($email_body, $vars);
                $email_data['attachments'] = array();
                $userVerificationEmails->sendEmail($email_data);
                $wooextradata['completed'] = true;

            } elseif ($wooextradata['application_type'] === 'job_application_fee') {
                //save profile
                $job_id = intval($wooextradata['jobId']);
                $applicant_name = esc_attr($wooextradata['applicantName']);
                $applicant_email = trim(esc_attr($wooextradata['applicantEmail']));
                $address = trim(esc_attr($wooextradata['applicantAddress']));
                $applicant_phone = esc_attr($wooextradata['applicantPhone']);
                $motivation = esc_attr($wooextradata['motivation']);
                $profile_id = intval($wooextradata['profileId']);
                $user_id = $wooextradata['user_id'];
                $cv = $wooextradata['cv'];
                $application_email = trim(esc_attr($wooextradata['jobApplicationEmail']));
                //save application and send email
                \huslajobs\saveJobApplication($user_id, $job_id, $applicant_name, $applicant_email, $address, $applicant_phone, $motivation, $cv, $profile_id, $application_email,'make_payment');
                $wooextradata['completed'] = true;
            }
            wc_update_order_item_meta($item_id, 'wooextradata', $wooextradata);
        }
        elseif ($order_status == 'failed'){
            if ($wooextradata['cv']){
                unlink($wooextradata['cv']);
            }

        }
    }


    public function deletedOrder($order_id)
    {
        global $wpdb;

        $order = new \WC_Order($order_id);
        if ($order->get_status() != 'failed') {
            foreach ($order->get_items() as $item_id => $order_item) {
                $wooextradata = wc_get_order_item_meta($item_id, 'wooextradata');
                $row = $wpdb->get_row($wpdb->prepare('SELECT * FROM ' . $wpdb->prefix . 'usermeta WHERE `meta_value` = %d AND `meta_key` = "order_id"', $order_id));

                if (!empty($row)) {
                    $userid = $row->user_id;
                    delete_user_meta($userid, 'order_id');
                }

            }
        }
    }

    public function update_billing_info($null, $field_name)
    {

        if (empty($this->checkout_info)) {
            foreach (WC()->cart->get_cart() as $wc_key => $wc_item) {
                if (array_key_exists('wooextradata', $wc_item)) {
                    $application_type = $wc_item['wooextradata']['application_type'];
                    if ($application_type === 'package_subscription' || $application_type === 'job_application_fee') {
                        $userInfo = (array)\huslajobs\huslajobsGetUser($wc_item['wooextradata']['user_id']);
                        $this->checkout_info = array(
                            'billing_first_name' => $userInfo['first_name'],
                            'billing_last_name' => $userInfo['last_name'],
                            'billing_email' => $userInfo['user_email'],
                        );
                    }
                }
            }
        }
        if (array_key_exists($field_name, $this->checkout_info)) {
            return $this->checkout_info[$field_name];
        }

        return null;
    }

    public function updateCartMetaItem($other_data, $wc_item)
    {
        if (isset($wc_item['wooextradata'])) {
            $other_data[] = array('name' => esc_html__('Package', 'service-finder'), 'value' => $wc_item['wooextradata']['package_name']);
        }

        return $other_data;
    }

    public function removeQuantityField($args, $product)
    {
        if ($product->get_id() == $this->product_id) {
            $args['max_value'] = $args['input_value'];
            $args['min_value'] = $args['input_value'];
        }
        return $args;
    }

    public function redirectAfterPayment($order_id)
    {
        if (is_checkout()) {
            $order = new \WC_Order($order_id);
            $order_status = $order->get_status();
            if ($order_status == 'completed' || $order_status == 'processing') {
                foreach ($order->get_items() as $item_id => $order_item) {
                    $pro_id = $order_item->get_product_id();
                    if ($pro_id == $this->product_id) {
                        $wooextradata = wc_get_order_item_meta($item_id, 'wooextradata');
                        $application_type = $wooextradata['application_type'];
                        if ($order_status == 'processing') {
                            $order->update_status('completed');
                        }
                        switch ($application_type) {
                            case 'job_application_fee':
                                $job_slug = $wooextradata['job_slug'];
                                $link = '/jobs/' . $job_slug . '/apply';
//                                $url = esc_url(add_query_arg(array('payment_complete' => 'true'), home_url($link)));
                                $url = esc_url(add_query_arg(array('application_payment_complete' => 'true'), home_url('/my-account')));

                                wp_redirect($url);
                                break;
                            case 'package_subscription':
                                $url = esc_url(add_query_arg(array('payment_complete' => 'true'), home_url('/my-account')));
                                wp_redirect($url);
                                break;
                            default:
                                $url = home_url('/');
                                wp_redirect($url);
                                break;

                        }
                    }
                }
            }

        }

    }

    public function disableAvailablePaymentGateways($gateways)
    {
        global $woocommerce;

        if (WC()->cart && sizeof(WC()->cart->get_cart()) != 0) {
            if ($woocommerce->cart->total < 500) {
                unset($gateways['stripe']);
            }

//                unset($gateways['paypal']);
        }

        return $gateways;
    }

    public function removeCheckoutFields($fields)
    {

        unset($fields['billing']['billing_company']);
        unset($fields['billing']['billing_address_1']);
        unset($fields['billing']['billing_address_2']);
        unset($fields['billing']['billing_city']);
        unset($fields['billing']['billing_postcode']);
        unset($fields['billing']['billing_country']);
        unset($fields['billing']['billing_state']);
        unset($fields['billing']['billing_phone']);

        return $fields;
    }

    public function customButtonText($button_text)
    {
        return __('Make payment', 'huslajobs');

    }

    function skipCartRedirectCheckout($url)
    {

        if (WC()->cart->get_cart_contents_count() > 0) {
            $url = wc_get_checkout_url();
        } else {
            $url = home_url();
        }
        return $url;
    }

}