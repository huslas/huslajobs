<?php
/**
 * This file contains strings translated in php to be used
 * in vue components.
 */
?>
<script>
    const VueUiStrings = {
        adminBackend: {
            loadingError: {
                errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                tryAgain: "<?php echo __("Click to try again", "huslajobs") ?>"
            },
            layout: {
                menuOptions: {
                    dashboard: "<?php echo __("Dashboard", "huslajobs"); ?>",
                    jobs: "<?php echo __("Jobs", "huslajobs"); ?>",
                    jobTypes: "<?php echo __("Job types", "huslajobs"); ?>",
                    jobApplications: "<?php echo __("Job applications", "huslajobs"); ?>",
                    categories: "<?php echo __("Categories", "huslajobs"); ?>",
                    recruiters: "<?php echo __("Recruiters", "huslajobs"); ?>",
                    jobSeekers: "<?php echo __("Job Seekers", "huslajobs"); ?>",
                    packages: "<?php echo __("Packages", "huslajobs"); ?>",
                    subscriptions: "<?php echo __("Subscriptions", "huslajobs"); ?>",
                    currencies: "<?php echo __("Currencies", "huslajobs"); ?>",
                }
            },
            dashboard: {
                dashboard: "<?php echo __("Dashboard", "huslajobs"); ?>",
                jobs: "<?php echo __("Jobs", "huslajobs"); ?>",
                jobTypes: "<?php echo __("Job types", "huslajobs"); ?>",
                jobApplications: "<?php echo __("Job applications", "huslajobs"); ?>",
                categories: "<?php echo __("Categories", "huslajobs"); ?>",
                recruiters: "<?php echo __("Recruiters", "huslajobs"); ?>",
                jobSeekers: "<?php echo __("Job Seekers", "huslajobs"); ?>",
                packages: "<?php echo __("Packages", "huslajobs"); ?>",
                subscriptions: "<?php echo __("Subscriptions", "huslajobs"); ?>",
                currencies: "<?php echo __("Currencies", "huslajobs"); ?>",
                errors: {
                    errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                },
                recentSubscriptions: "<?php echo __("Recent Subscriptions", "huslajobs"); ?>",
                tableHeaders: {
                    package: "<?php echo __("Package", "huslajobs"); ?>",
                    subscriber: "<?php echo __("Subscriber", "huslajobs"); ?>",
                    cost: "<?php echo __("Cost", "huslajobs"); ?>",
                    status: "<?php echo __("Status", "huslajobs"); ?>",
                },
                active: "<?php echo __("Active", "huslajobs"); ?>",
                expired: "<?php echo __("Expired", "huslajobs"); ?>",
                noSubscription: "<?php echo __("No recent subscription", "huslajobs"); ?>",

            },
            subscriptions: {
                dashboard: "<?php echo __("Dashboard", "huslajobs"); ?>",
                subscriptions: "<?php echo __("Subscriptions", "huslajobs"); ?>",
                errors: {
                    errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                },
                tableHeaders: {
                    id: "<?php echo __("Id", "huslajobs"); ?>",
                    user: "<?php echo __("User", "huslajobs"); ?>",
                    package: "<?php echo __("Package", "huslajobs"); ?>",
                    startDate: "<?php echo __("Start date", "huslajobs"); ?>",
                    endDate: "<?php echo __("End date", "huslajobs"); ?>",
                    //endDate: "<?php //echo __("End date", "huslajobs"); ?>//",
                },
                noSubscription: "<?php echo __("No recent subscription", "huslajobs"); ?>",
            },
            packages: {
                index: {
                    dashboard: "<?php echo __("Dashboard", "huslajobs"); ?>",
                    packages: "<?php echo __("Packages", "huslajobs"); ?>",
                    errors: {
                        errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                    },
                    successMessage: {
                        packageDeleted: "<?php echo __("Package deleted", "huslajobs"); ?>",
                    },
                    confirm: "<?php echo __("Are you sure you want to delete", "huslajobs"); ?>",
                    buttons: {
                        addPackage: "<?php echo __("Add Package", "huslajobs"); ?>",
                        search: "<?php echo __("Search", "huslajobs"); ?>",
                    },
                    name: "<?php echo __("Name", "huslajobs"); ?>",
                    duration: "<?php echo __("Duration", "huslajobs"); ?>",
                    price: "<?php echo __("Price", "huslajobs"); ?>",
                    id: "<?php echo __("Id", "huslajobs"); ?>",
                    actions: "<?php echo __("Actions", "huslajobs"); ?>",

                },
                edit: {
                    dashboard: "<?php echo __("Dashboard", "huslajobs"); ?>",
                    packages: "<?php echo __("Packages", "huslajobs"); ?>",
                    editPackage: "<?php echo __("Edit package", "huslajobs"); ?>",
                    edit: "<?php echo __("Edit", "huslajobs"); ?>",
                },
                create: {
                    dashboard: "<?php echo __("Dashboard", "huslajobs"); ?>",
                    packages: "<?php echo __("Packages", "huslajobs"); ?>",
                    createPackage: "<?php echo __("Create package", "huslajobs"); ?>",
                    create: "<?php echo __("Create", "huslajobs"); ?>",
                },
                addForm: {
                    errors: {
                        errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                        nameRequired: "<?php echo __("Name is required", "huslajobs"); ?>",
                        priceRequired: "<?php echo __("Price is required", "huslajobs"); ?>",
                        durationRequired: "<?php echo __("Duration is required", "huslajobs"); ?>",
                    },
                    successMessage: {
                        packageDeleted: "<?php echo __("Package deleted", "huslajobs"); ?>",
                        packageCreated: "<?php echo __("Package created successfully", "huslajobs"); ?>",
                        packageUpdated: "<?php echo __("Package updated successfully", "huslajobs"); ?>",
                    },
                    tips: {
                        note: "<?php echo __("Note", "huslajobs"); ?>",
                        unlimitedNumber: "<?php echo __("-1 means unlimited number", "huslajobs"); ?>",
                        packageCurrency: "<?php echo __("Package currency will be set using woocommerce", "huslajobs"); ?>",

                    },
                    formFields: {
                        name: "<?php echo __("Name", "huslajobs"); ?>",
                        price: "<?php echo __("Price", "huslajobs"); ?>",
                        profiles: "<?php echo __("Profiles", "huslajobs"); ?>",
                        duration: "<?php echo __("Duration", "huslajobs"); ?>",
                        jobs: "<?php echo __("Jobs", "huslajobs"); ?>",
                        jobApplications: "<?php echo __("Job applications", "huslajobs"); ?>",
                        benefits: {
                            label: "<?php echo __("Benefits", "huslajobs"); ?>",
                            placeholder: "<?php echo __("A comma(,) seperated lists of benefits", "huslajobs"); ?>",
                        },
                        description: {
                            label: "<?php echo __("Description", "huslajobs"); ?>",
                            placeholder: "<?php echo __("A short summary about the package", "huslajobs"); ?>",
                        },

                        buttons: {
                            submit: "<?php echo __("Submit", "huslajobs"); ?>",
                            cancel: "<?php echo __("Cancel", "huslajobs"); ?>",
                        }
                    }
                },


            },
            jobs: {
                index: {
                    dashboard: "<?php echo __("Dashboard", "huslajobs"); ?>",
                    jobs: "<?php echo __("Jobs", "huslajobs"); ?>",
                    noJobs: "<?php echo __("No jobs available", "huslajobs"); ?>",
                    deleted: "<?php echo __("deleted", "huslajobs"); ?>",
                    addJob: "<?php echo __("Add job", "huslajobs"); ?>",
                    postJob: "<?php echo __("Post job", "huslajobs"); ?>",
                    confirm: "<?php echo __("Are you sure you want to delete", "huslajobs"); ?>",
                    formFields: {
                        searchText: {
                            placeholder: "<?php echo __("Search text ...", "huslajobs"); ?>",
                        },
                        button: "<?php echo __("Search", "huslajobs"); ?>",
                    },
                    tableHeaders: {
                        id: "<?php echo __("Id", "huslajobs"); ?>",
                        name: "<?php echo __("Name", "huslajobs"); ?>",
                        jobType: "<?php echo __("Job type", "huslajobs"); ?>",
                        description: "<?php echo __("Description", "huslajobs"); ?>",
                        country: "<?php echo __("Country", "huslajobs"); ?>",
                        salary: "<?php echo __("Salary", "huslajobs"); ?>",
                        actions: "<?php echo __("Actions", "huslajobs"); ?>",
                    },
                    errors: {
                        errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                    },
                },
                edit: {
                    dashboard: "<?php echo __("Dashboard", "huslajobs"); ?>",
                    jobs: "<?php echo __("Jobs", "huslajobs"); ?>",
                    editJob: "<?php echo __("Edit job", "huslajobs"); ?>",
                    errors: {
                        errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                    },
                },
                create: {
                    dashboard: "<?php echo __("Dashboard", "huslajobs"); ?>",
                    jobs: "<?php echo __("Jobs", "huslajobs"); ?>",
                    addJob: "<?php echo __("Add job", "huslajobs"); ?>",
                },
                addForm: {
                    formFields: {
                        jobTitle: {
                            placeholder: "<?php echo __("Enter job title", "huslajobs"); ?>",
                            label: "<?php echo __("Job title", "huslajobs"); ?>"
                        },
                        jobType: {
                            placeholder: "<?php echo __("Select job type", "huslajobs"); ?>",
                            label: "<?php echo __("Job type", "huslajobs"); ?>"
                        },
                        jobCategory: {
                            placeholder: "<?php echo __("Select job category", "huslajobs"); ?>",
                            label: "<?php echo __("Job category", "huslajobs"); ?>"
                        },
                        description: {
                            placeholder: "<?php echo __("Job description", "huslajobs"); ?>",
                            label: "<?php echo __("Job description", "huslajobs"); ?>"
                        },
                        experience: {
                            placeholder: "<?php echo __("Employee experience in years", "huslajobs"); ?>",
                            label: "<?php echo __("Experience", "huslajobs"); ?>"
                        },
                        salary: {
                            placeholder: "<?php echo __("Salary", "huslajobs"); ?>",
                            label: "<?php echo __("Salary", "huslajobs"); ?>"
                        },
                        currency: {
                            placeholder: "<?php echo __("Select currency", "huslajobs"); ?>",
                            label: "<?php echo __("Currency", "huslajobs"); ?>"
                        },
                        work: {
                            placeholder: "<?php echo __("Work location", "huslajobs"); ?>",
                            label: "<?php echo __("Work location", "huslajobs"); ?>"
                        },
                        cv: {
                            placeholder: "<?php echo __("Is CV required?", "huslajobs"); ?>",
                            label: "<?php echo __("CV", "huslajobs"); ?>",
                        },
                        motivation: {
                            placeholder: "<?php echo __("Is motivation/cover letter required?", "huslajobs"); ?>",
                            label: "<?php echo __("Motivation", "huslajobs"); ?>",
                        },
                        account: {
                            placeholder: "<?php echo __("Select account", "huslajobs"); ?>",
                            label: "<?php echo __("Account", "huslajobs"); ?>"
                        },
                        country: {
                            placeholder: "<?php echo __("Select country", "huslajobs"); ?>",
                            label: "<?php echo __("Country", "huslajobs"); ?>"
                        },
                        state: {
                            placeholder: "<?php echo __("Select state/region", "huslajobs"); ?>",
                            label: "<?php echo __("State/Region", "huslajobs"); ?>",
                            tip: "<?php echo __("Select country to select state", "huslajobs"); ?>",
                        },
                        city: {
                            placeholder: "<?php echo __("City/Town", "huslajobs"); ?>",
                            label: "<?php echo __("City/Town", "huslajobs"); ?>",
                        },
                        button: {
                            post: "<?php echo __("Post job", "huslajobs"); ?>",
                            update: "<?php echo __("Update", "huslajobs") ?>"
                        },


                    },
                    no: "<?php echo __("NO", "huslajobs"); ?>",
                    yes: "<?php echo __("YES", "huslajobs"); ?>",
                    currency: "<?php echo __("Currency", "huslajobs"); ?>",
                    jobTypes: "<?php echo __("Job types", "huslajobs"); ?>",
                    jobCategories: "<?php echo __("Job categories", "huslajobs"); ?>",
                    noAccounts: "<?php echo __("No registered account", "huslajobs"); ?>",
                    noAvailable: "<?php echo __("Some job dependencies are not available ,please contact admin to add", "huslajobs"); ?>",
                    errors: {
                        errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                    },
                    successMessage: {
                        jobCreated: "<?php echo __("Job created successfully", "huslajobs"); ?>",
                        jobUpdated: "<?php echo __("Job updated successfully", "huslajobs"); ?>",
                    },
                }
            },
            jobTypes: {
                index: {
                    dashboard: "<?php echo __("Dashboard", "huslajobs"); ?>",
                    jobTypes: "<?php echo __("Job types", "huslajobs"); ?>",
                    noJobTypes: "<?php echo __("No job types available", "huslajobs"); ?>",
                    deleted: "<?php echo __("deleted", "huslajobs"); ?>",
                    errors: {
                        errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                    },
                    addJobType: "<?php echo __("Add job type", "huslajobs"); ?>",
                    confirm: "<?php echo __("Are you sure you want to delete", "huslajobs"); ?>",
                    formFields: {
                        searchText: {
                            placeholder: "<?php echo __("Search text ...", "huslajobs"); ?>",
                        },
                        button: "<?php echo __("Search", "huslajobs"); ?>",
                    },
                    tableHeaders: {
                        id: "<?php echo __("Id", "huslajobs"); ?>",
                        name: "<?php echo __("Name", "huslajobs"); ?>",
                        description: "<?php echo __("Description", "huslajobs"); ?>",
                        actions: "<?php echo __("Actions", "huslajobs"); ?>",
                    },
                },
                edit: {
                    dashboard: "<?php echo __("Dashboard", "huslajobs"); ?>",
                    jobTypes: "<?php echo __("Job types", "huslajobs"); ?>",
                    editJobType: "<?php echo __("Edit job type", "huslajobs"); ?>",
                    errors: {
                        errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                    },
                },
                create: {
                    dashboard: "<?php echo __("Dashboard", "huslajobs"); ?>",
                    jobTypes: "<?php echo __("Job types", "huslajobs"); ?>",
                    addJobType: "<?php echo __("Add job type", "huslajobs"); ?>",
                    errors: {
                        errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                    },
                },
                addForm: {
                    formFields: {
                        name: {
                            placeholder: "<?php echo __("Enter job type", "huslajobs"); ?>",
                            label: "<?php echo __("Name", "huslajobs"); ?>"
                        },
                        description: {
                            placeholder: "<?php echo __("Job type description", "huslajobs"); ?>",
                            label: "<?php echo __("Description", "huslajobs"); ?>"
                        },
                        button: "<?php echo __("Submit", "huslajobs"); ?>",

                    },
                    errors: {
                        errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                    },
                    successMessage: {
                        jobTypeCreated: "<?php echo __("Job type created successfully", "huslajobs"); ?>",
                        jobTypeUpdated: "<?php echo __("Job type updated successfully", "huslajobs"); ?>",
                    },
                }
            },
            jobApplications: {
                dashboard: "<?php echo __("Dashboard", "huslajobs"); ?>",
                jobApplications: "<?php echo __("Job applications", "huslajobs"); ?>",
                errors: {
                    errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                },
                tableHeaders: {
                    id: "<?php echo __("Id", "huslajobs"); ?>",
                    applicantName: "<?php echo __("Applicant name", "huslajobs"); ?>",
                    applicantEmail: "<?php echo __("Applicant email", "huslajobs"); ?>",
                    country: "<?php echo __("Country", "huslajobs"); ?>",
                    userAccount: "<?php echo __("User account", "huslajobs"); ?>",
                    job: "<?php echo __("Job", "huslajobs"); ?>",
                    dateApplied: "<?php echo __("Date applied", "huslajobs"); ?>",
                    address: "<?php echo __("Address", "huslajobs"); ?>"
                },
            },
            currency: {
                index: {
                    dashboard: "<?php echo __("Dashboard", "huslajobs"); ?>",
                    currencies: "<?php echo __("Currencies", "huslajobs"); ?>",
                    noCurrencies: "<?php echo __("No currency available", "huslajobs"); ?>",
                    deleted: "<?php echo __("deleted", "huslajobs"); ?>",
                    errors: {
                        errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                    },
                    addCurrency: "<?php echo __("Add currency", "huslajobs"); ?>",
                    confirm: "<?php echo __("Are you sure you want to delete", "huslajobs"); ?>",
                    formFields: {
                        searchText: {
                            placeholder: "<?php echo __("Search text ...", "huslajobs"); ?>",
                        },
                        button: "<?php echo __("Search", "huslajobs"); ?>",
                    },
                    tableHeaders: {
                        id: "<?php echo __("Id", "huslajobs"); ?>",
                        name: "<?php echo __("Name", "huslajobs"); ?>",
                        code: "<?php echo __("Code", "huslajobs"); ?>",
                        actions: "<?php echo __("Actions", "huslajobs"); ?>",
                    },
                },
                edit: {
                    dashboard: "<?php echo __("Dashboard", "huslajobs"); ?>",
                    currencies: "<?php echo __("Currencies", "huslajobs"); ?>",
                    editCurrency: "<?php echo __("Edit currency", "huslajobs"); ?>",
                    errors: {
                        errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                    },
                },
                create: {
                    dashboard: "<?php echo __("Dashboard", "huslajobs"); ?>",
                    currencies: "<?php echo __("Currencies", "huslajobs"); ?>",
                    addCurrency: "<?php echo __("Add currency", "huslajobs"); ?>",
                    errors: {
                        errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                    },
                },
                addForm: {
                    formFields: {
                        name: {
                            placeholder: "<?php echo __("Currency name", "huslajobs"); ?>",
                            label: "<?php echo __("Name", "huslajobs"); ?>"
                        },
                        code: {
                            placeholder: "<?php echo __("Currency code", "huslajobs"); ?>",
                            label: "<?php echo __("Code", "huslajobs"); ?>"
                        },
                        button: "<?php echo __("Submit", "huslajobs"); ?>",

                    },
                    errors: {
                        errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                    },
                    successMessage: {
                        currencyCreated: "<?php echo __("Currency created successfully", "huslajobs"); ?>",
                        currencyUpdated: "<?php echo __("Currency updated successfully", "huslajobs"); ?>",
                    },
                }
            },
            categories: {
                index: {
                    dashboard: "<?php echo __("Dashboard", "huslajobs"); ?>",
                    categories: "<?php echo __("Categories", "huslajobs"); ?>",
                    noCategories: "<?php echo __("No category available", "huslajobs"); ?>",
                    deleted: "<?php echo __("deleted", "huslajobs"); ?>",
                    errors: {
                        errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                    },
                    addCategory: "<?php echo __("Add category", "huslajobs"); ?>",
                    confirm: "<?php echo __("Are you sure you want to delete", "huslajobs"); ?>",
                    formFields: {
                        searchText: {
                            placeholder: "<?php echo __("Search text ...", "huslajobs"); ?>",
                        },
                        button: "<?php echo __("Search", "huslajobs"); ?>",
                    },
                    tableHeaders: {
                        id: "<?php echo __("Id", "huslajobs"); ?>",
                        name: "<?php echo __("Name", "huslajobs"); ?>",
                        description: "<?php echo __("Description", "huslajobs"); ?>",
                        actions: "<?php echo __("Actions", "huslajobs"); ?>",
                    },
                },
                edit: {
                    dashboard: "<?php echo __("Dashboard", "huslajobs"); ?>",
                    categories: "<?php echo __("Categories", "huslajobs"); ?>",
                    editCategory: "<?php echo __("Edit category", "huslajobs"); ?>",
                    errors: {
                        errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                    },
                },
                create: {
                    dashboard: "<?php echo __("Dashboard", "huslajobs"); ?>",
                    categories: "<?php echo __("Categories", "huslajobs"); ?>",
                    addCategory: "<?php echo __("Add category", "huslajobs"); ?>",
                    errors: {
                        errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                    },
                },
                addForm: {
                    formFields: {
                        name: {
                            placeholder: "<?php echo __("Name", "huslajobs"); ?>",
                            label: "<?php echo __("Name", "huslajobs"); ?>"
                        },
                        description: {
                            placeholder: "<?php echo __("Description", "huslajobs"); ?>",
                            label: "<?php echo __("Description", "huslajobs"); ?>"
                        },
                        button: "<?php echo __("Submit", "huslajobs"); ?>",

                    },
                    errors: {
                        errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                    },
                    successMessage: {
                        categoryCreated: "<?php echo __("Category created successfully", "huslajobs"); ?>",
                        categoryUpdated: "<?php echo __("Category updated successfully", "huslajobs"); ?>",
                    },
                }
            },
            accounts: {
                dashboard: "<?php echo __("Dashboard", "huslajobs"); ?>",
                accounts: "<?php echo __("Accounts", "huslajobs"); ?>",
                account: "<?php echo __("account", "huslajobs"); ?>",
                deleted: "<?php echo __("deleted", "huslajobs"); ?>",
                errors: {
                    errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                },
                confirm: "<?php echo __("Are you sure you want to delete", "huslajobs"); ?>",
                formFields: {
                    searchText: {
                        placeholder: "<?php echo __("Search text ...", "huslajobs"); ?>",
                    },
                    name: {
                        label: "<?php echo __("Name", "huslajobs"); ?>",
                    },
                    email: {
                        label: "<?php echo __("Email", "huslajobs"); ?>",
                    },
                    button: "<?php echo __("Search", "huslajobs"); ?>",
                },
                tableHeaders: {
                    id: "<?php echo __("Id", "huslajobs"); ?>",
                    name: "<?php echo __("Name", "huslajobs"); ?>",
                    email: "<?php echo __("Email", "huslajobs"); ?>",
                    country: "<?php echo __("Country", "huslajobs"); ?>",
                    state: "<?php echo __("State", "huslajobs"); ?>",
                    phoneNumber: "<?php echo __("Phone number", "huslajobs"); ?>",
                    actions: "<?php echo __("Actions", "huslajobs"); ?>",
                },
                successMessage: {
                    accountDeleted: "<?php echo __("Account deleted", "huslajobs"); ?>",
                },

            }
        },
        clientFrontEnd: {
            signUp: {
                heading: "<?php echo __("Sign up", "huslajobs"); ?>",
                buttons: {
                    home: "<?php echo __("Home", "huslajobs"); ?>",
                    dashboard: "<?php echo __("Dashboard", "huslajobs"); ?>",
                },
                haveAccount: "<?php echo __("Already have an account?", "huslajobs"); ?>",
            },
            signUpForm: {
                formFields: {
                    firstName: {
                        placeholder: "<?php echo __("First name", "huslajobs"); ?>",
                        label: "<?php echo __("First name", "huslajobs"); ?>",
                    },
                    lastName: {
                        placeholder: "<?php echo __("Last name", "huslajobs"); ?>",
                        label: " <?php echo __("Last name", "huslajobs"); ?>",
                    },
                    email: {
                        placeholder: "<?php echo __("Email", "huslajobs"); ?>",
                        label: "<?php echo __("Email", "huslajobs"); ?>"
                    },
                    country: {
                        placeholder: "<?php echo __("Select country", "huslajobs"); ?>",
                        label: "<?php echo __("Country", "huslajobs"); ?>"
                    },
                    state: {
                        placeholder: "<?php echo __("Select state/region", "huslajobs"); ?>",
                        label: "<?php echo __("State/Region", "huslajobs"); ?>",
                        tip: "<?php echo __("Select country to select state", "huslajobs"); ?>",
                    },
                    city: {
                        placeholder: "<?php echo __("City/Town", "huslajobs"); ?>",
                        label: "<?php echo __("City/Town", "huslajobs"); ?>",
                    },
                    dob: {
                        placeholder: "<?php echo __("Date of birth", "huslajobs"); ?>",
                        label: "<?php echo __("Date of birth", "huslajobs"); ?>",
                    },
                    phone: {
                        placeholder: "<?php echo __("Phone number", "huslajobs"); ?>",
                        label: "<?php echo __("Phone number", "huslajobs"); ?>",
                        error: "<?php echo __("Phone number is incorrect", "huslajobs"); ?>",
                    },
                    bio: {
                        placeholder: "<?php echo __("Tell us about yourself", "huslajobs"); ?>",
                        label: "<?php echo __("About", "huslajobs"); ?>",
                    },
                    password: {
                        placeholder: "<?php echo __("Password", "huslajobs"); ?>",
                        label: "<?php echo __("Password", "huslajobs"); ?>",
                    },
                    profileImage: {
                        buttons: {
                            changeImage: "<?php echo __("Change profile image", "huslajobs"); ?>",
                            uploadImage: "<?php echo __("Upload profile image", "huslajobs"); ?>",
                        },
                        tips: {
                            imageTypes: "<?php echo __("Image types", "huslajobs"); ?>",
                            maxSize: "<?php echo __("Max size", "huslajobs"); ?>",
                        },
                        label: "<?php echo __("Profile image", "huslajobs"); ?>",
                    },
                    terms: {
                        agree: "<?php echo __("I agree to", "huslajobs"); ?>",
                        services: "<?php echo __("Terms of services", "huslajobs"); ?>",
                        and: "<?php echo __("and", "huslajobs"); ?>",
                        privacyPolicy: "<?php echo __("Privacy policy", "huslajobs"); ?>",
                    },
                    buttonText: {
                        singUp: "<?php echo __("Sign Up", "huslajobs"); ?>",
                        save: "<?php echo __('Save', 'huslajobs'); ?>",
                    }

                }
            },
            resendVerification: {
                heading: "<?php echo __("Resend Verification", "huslajobs"); ?>",
                formFields: {
                    email: {
                        placeholder: "<?php echo __("Email", "huslajobs"); ?>",
                        label: "<?php echo __("Email", "huslajobs"); ?>"
                    },
                    button: "<?php echo __("Send", "huslajobs"); ?>"
                }

            },
            postJob: {
                headings: {
                    normal: "<?php echo __("Let's get your job post ready", "huslajobs"); ?>",
                    review: "<?php echo __("Review your job post", "huslajobs"); ?>",
                },
                steps: {
                    job: "<?php echo __("Job", "huslajobs"); ?>",
                    login: "<?php echo __("Login/Signup", "huslajobs"); ?>",
                    postingAs: "<?php echo __("Post job as", "huslajobs"); ?>",
                    review: "<?php echo __("Review", "huslajobs"); ?>",
                },
                formFields: {
                    jobTitle: {
                        placeholder: "<?php echo __("Enter job title", "huslajobs"); ?>",
                        label: "<?php echo __("Job title", "huslajobs"); ?>"
                    },
                    jobType: {
                        placeholder: "<?php echo __("Select job type", "huslajobs"); ?>",
                        label: "<?php echo __("Job type", "huslajobs"); ?>"
                    },
                    jobCategory: {
                        placeholder: "<?php echo __("Select job category", "huslajobs"); ?>",
                        label: "<?php echo __("Job category", "huslajobs"); ?>"
                    },
                    location: {
                        placeholder: "<?php echo __("Enter job location", "huslajobs"); ?>",
                        label: "<?php echo __("Location", "huslajobs"); ?>"
                    },

                    description: {
                        placeholder: "<?php echo __("Job description of atleast 100 characters", "huslajobs"); ?>",
                        label: "<?php echo __("Job description", "huslajobs"); ?>"
                    },
                    experience: {
                        placeholder: "<?php echo __("Employee experience in years", "huslajobs"); ?>",
                        label: "<?php echo __("Experience", "huslajobs"); ?>"
                    },
                    salary: {
                        placeholder: "<?php echo __("Salary", "huslajobs"); ?>",
                        label: "<?php echo __("Salary", "huslajobs"); ?>"
                    },
                    currency: {
                        placeholder: "<?php echo __("Select currency", "huslajobs"); ?>",
                        label: "<?php echo __("Currency", "huslajobs"); ?>"
                    },
                    work: {
                        placeholder: "<?php echo __("Work location", "huslajobs"); ?>",
                        label: "<?php echo __("Work location", "huslajobs"); ?>"
                    },
                    cv: {
                        placeholder: "<?php echo __("Is CV required?", "huslajobs"); ?>",
                        label: "<?php echo __("CV", "huslajobs"); ?>",
                    },
                    motivation: {
                        placeholder: "<?php echo __("Is motivation/cover letter required?", "huslajobs"); ?>",
                        label: "<?php echo __("Motivation", "huslajobs"); ?>",
                    },
                    companyAccount: {
                        label: "<?php echo __("Company accounts", "huslajobs"); ?>",
                        placeholder: "<?php echo __("Select account", "huslajobs") ?>"
                    },
                    companyName: {
                        label: "<?php echo __("Name", "huslajobs"); ?>",
                        placeholder: "<?php echo __("Company name", "huslajobs") ?>"
                    },
                    companyEmail: {
                        label: "<?php echo __("Email", "huslajobs"); ?>",
                        placeholder: "<?php echo __("Company email", "huslajobs") ?>"
                    },
                    companyWebsite: {
                        label: "<?php echo __("Website", "huslajobs"); ?>",
                        placeholder: "<?php echo __("Company website", "huslajobs") ?>"
                    },
                    companyPhone: {
                        placeholder: "<?php echo __("Phone number", "huslajobs"); ?>",
                        label: "<?php echo __("Phone number", "huslajobs"); ?>",
                        error: "<?php echo __("Phone number is incorrect", "huslajobs"); ?>",
                    },
                    companyAddress: {
                        placeholder: "<?php echo __("Company address", "huslajobs"); ?>",
                        label: "<?php echo __("Address", "huslajobs"); ?>",
                    },
                    country: {
                        placeholder: "<?php echo __("Select country", "huslajobs"); ?>",
                        label: "<?php echo __("Country", "huslajobs"); ?>"
                    },
                    state: {
                        placeholder: "<?php echo __("Select state/region", "huslajobs"); ?>",
                        label: "<?php echo __("State/Region", "huslajobs"); ?>",
                        tip: "<?php echo __("Select country to select state", "huslajobs"); ?>",
                    },
                    city: {
                        placeholder: "<?php echo __("City/Town", "huslajobs"); ?>",
                        label: "<?php echo __("City/Town", "huslajobs"); ?>",
                    },
                    button: "<?php echo __("Post job", "huslajobs"); ?>",

                },
                notice: "<?php echo __("Applications for this job will be sent to the company email", "huslajobs")?>",
                login: "<?php echo __("Login", "huslajobs"); ?>",
                signUp: "<?php echo __("Sign up", "huslajobs"); ?>",
                company: "<?php echo __("Company", "huslajobs"); ?>",
                individual: "<?php echo __("Individual", "huslajobs"); ?>",
                or: "<?php echo __("OR", "huslajobs"); ?>",
                no: "<?php echo __("NO", "huslajobs"); ?>",
                yes: "<?php echo __("YES", "huslajobs"); ?>",
                errors: {
                    errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                    login: "<?php echo __("Sign up or login to proceed", "huslajobs"); ?>",
                },
                continueModal: {
                    heading: "<?php echo __("Welcome back", "huslajobs"); ?>",
                    subHeading: "<?php echo __("Continue were you left off", "huslajobs"); ?>",
                    buttons: {
                        cancel: "<?php echo __("Cancel", "huslajobs"); ?>",
                        continue: "<?php echo __("Continue", "huslajobs"); ?>",

                    }

                },

            },
            newPassword: {
                heading: "<?php echo __('New password', 'huslajobs'); ?>",
                formFields: {
                    currentPassword: {
                        placeholder: "<?php echo __("Current password", "huslajobs"); ?>",
                        label: "<?php echo __("Current password", "huslajobs"); ?>",
                    },
                    newPassword: {
                        placeholder: "<?php echo __("New password", "huslajobs"); ?>",
                        label: "<?php echo __("New password", "huslajobs"); ?>",

                    },

                    button: "<?php echo __("Set New Password", "huslajobs");?>"
                },
                login: "<?php echo __("Login", "huslajobs"); ?>",

            },
            login: {
                heading: "<?php echo __("Login", "huslajobs"); ?>",
                links: {
                    haveAccount: "<?php echo __("Don't have an account? Sign up", "huslajobs"); ?>",
                    forgotPassword: "<?php echo __("Forgot password", "huslajobs"); ?>",
                    resendCode: "<?php echo __("Resend code", "huslajobs"); ?>",
                }
            },
            loginForm: {
                formFields: {
                    email: {
                        placeholder: "<?php echo __("Email", "huslajobs"); ?>",
                        label: "<?php echo __("Email", "huslajobs"); ?>"
                    },
                    password: {
                        placeholder: "<?php echo __("Password", "huslajobs"); ?>",
                        label: "<?php echo __("Password", "huslajobs"); ?>",
                    },
                    button: "<?php echo __("Login", "huslajobs"); ?>",
                },
                notAllowed: "<?php echo __("Sorry,you are not allowed to login", 'huslajobs') ?>"
            },
            jobs: {
                heading: "<?php echo __("Search and Apply", "huslajobs"); ?>",
                subHeading: "<?php echo __("For dream jobs", "huslajobs"); ?>",
                jobs: "<?php echo __("Jobs", "huslajobs"); ?>",
                jobsAvailable: "<?php echo __("Jobs available", "huslajobs"); ?>",
                jobAvailable: "<?php echo __("Job available", "huslajobs"); ?>",
                jobsFor: "<?php echo __("jobs for", "huslajobs"); ?>",
                login: "<?php echo __("Login to view job details", "huslajobs")?>",
                results: {
                    message: "<?php echo __("You are currently viewing", "huslajobs"); ?>",
                    guestUpgrade: "<?php echo __("Signup for more jobs", "huslajobs"); ?>",
                    loginUpgrade: "<?php echo __("Upgrade for more jobs", "huslajobs"); ?>",
                },
                noJobs: "<?php echo __("No jobs available", "huslajobs"); ?>",
                of: "<?php echo __("of", "huslajobs"); ?>",

                viewJob: "<?php echo __("View job", "huslajobs"); ?>",
                jobCategory: "<?php echo __("Job category", "huslajobs"); ?>",
                workLocation: "<?php echo __("Work location", "huslajobs"); ?>",
                jobType: "<?php echo __("Job type", "huslajobs"); ?>",
            },
            singleJob: {
                headings: {
                    posted: "<?php echo __("Posted", "huslajobs"); ?>",
                    jobLocation: "<?php echo __("Job location", "huslajobs"); ?>",
                    workLocation: "<?php echo __("Work location", "huslajobs"); ?>",
                    cv: "<?php echo __("CV", "huslajobs"); ?>",
                    motivation: "<?php echo __("Motivation/Cover letter", "huslajobs"); ?>",
                    jobType: "<?php echo __("Job type", "huslajobs"); ?>",
                    jobCategory: "<?php echo __("Job category", "huslajobs"); ?>",
                    experience: "<?php echo __("Experience", "huslajobs"); ?>",
                    noExperience: "<?php echo __("No experience needed", "huslajobs"); ?>",
                    year: "<?php echo __("Year", "huslajobs"); ?>",
                    years: "<?php echo __("Years", "huslajobs"); ?>",
                    salary: "<?php echo __("Salary", "huslajobs"); ?>",
                    jobDescription: "<?php echo __("Full job description", "huslajobs"); ?>",
                    country: "<?php echo __("Country", "huslajobs"); ?>",
                    state: "<?php echo __("State/Region", "huslajobs"); ?>",
                    city: "<?php echo __("City/Town", "huslajobs"); ?>",


                },
                links: {
                    apply: "<?php echo __("Apply", "huslajobs"); ?>",
                    upgrade: "<?php echo __("Application limit exceeded,upgrade to apply", "huslajobs"); ?>",
                },
                notFound: "<?php echo __("Job not found", "huslajobs"); ?>",
                relatedJobs: "<?php echo __("Related jobs", "huslajobs"); ?>",
                required: "<?php echo __("Required", "huslajobs"); ?>",
                notRequired: "<?php echo __("Not required", "huslajobs"); ?>",
            },
            jobApplication: {
                headings: {
                    normal: "<?php echo __("Let's get your application ready", "huslajobs"); ?>",
                    review: "<?php echo __("Review your job application", "huslajobs"); ?>",
                },
                steps: {
                    application: "<?php echo __("Application", "huslajobs"); ?>",
                    login: "<?php echo __("Login/Signup", "huslajobs"); ?>",
                    platformFee: "<?php echo __("Application fee", "huslajobs"); ?>",
                    review: "<?php echo __("Review", "huslajobs"); ?>",
                    fee: "<?php echo __("Fee", "huslajobs") ?>",
                },
                formFields: {
                    name: {
                        placeholder: "<?php echo __("Name", "huslajobs"); ?>",
                        label: "<?php echo __("Name", "huslajobs"); ?>"
                    },
                    phone: {
                        placeholder: "<?php echo __("Phone number", "huslajobs"); ?>",
                        label: "<?php echo __("Phone number", "huslajobs"); ?>",
                        error: "<?php echo __("Phone number is incorrect", "huslajobs"); ?>",
                    },
                    profile: {
                        placeholder: "<?php echo __("Select profile", "huslajobs"); ?>",
                        label: "<?php echo __("Choose a profile to apply", "huslajobs"); ?>",
                        errorlabel: "<?php echo __("Profile", "huslajobs"); ?>",
                        options: {
                            'newProfile': "<?php echo __("Create a profile", "huslajobs"); ?>",
                            'noProfile': "<?php echo __("Use no profile", "huslajobs")?>"
                        }
                    },
                    address: {
                        placeholder: "<?php echo __("Enter your address", "huslajobs"); ?>",
                        label: "<?php echo __("Address", "huslajobs"); ?>",
                    },
                    email: {
                        placeholder: "<?php echo __("Email", "huslajobs"); ?>",
                        label: "<?php echo __("Email", "huslajobs"); ?>"
                    },
                    country: {
                        placeholder: "<?php echo __("Select country", "huslajobs"); ?>",
                        label: "<?php echo __("Country", "huslajobs"); ?>"
                    },
                    city: {
                        placeholder: "<?php echo __("City/Town", "huslajobs"); ?>",
                        label: "<?php echo __("City/Town", "huslajobs"); ?>",
                    },
                    motivation: {
                        placeholder: "<?php echo __("Your motivation/cover letter,between 150 to 500 characters", "huslajobs"); ?>",
                        label: "<?php echo __("Motivation", "huslajobs"); ?>"
                    },
                    cv: {
                        buttons: {
                            changeCv: "<?php echo __("Change CV", "huslajobs"); ?>",
                            uploadCv: "<?php echo __("Upload CV", "huslajobs"); ?>",
                        },
                        tips: {
                            fileTypes: "<?php echo __("File types", "huslajobs"); ?>",
                            maxSize: "<?php echo __("Max size", "huslajobs"); ?>",
                        },
                        placeholder: "<?php echo __("Select CV", "huslajobs"); ?>",
                        label: "<?php echo __("CV", "huslajobs"); ?>",
                    },
                    buttons: {
                        submit: "<?php echo __("Submit", "huslajobs"); ?>",
                        apply: "<?php echo __("Pay & apply", "huslajobs") ?>",
                    }

                },
                login: "<?php echo __("Login", "huslajobs"); ?>",
                signUp: "<?php echo __("Sign up", "huslajobs"); ?>",
                platformFee: {
                    heading: "<?php echo __("Application fee", "huslajobs"); ?>",
                    madePayments: "<?php echo __("You have already made payments proceed to apply", "huslajobs"); ?>",
                    button: "<?php echo __("Proceed to payment", "huslajobs"); ?>",
                    tip: "<?php echo __("Application fees only applies to guest members ,upgrade not to pay application fees", "huslajobs"); ?>",
                },
                or: "<?php echo __("OR", "huslajobs"); ?>",
                errors: {
                    errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                    login: "<?php echo __("Sign up or login to proceed", "huslajobs"); ?>",
                    makePayments: "<?php echo __("Make payments to continue", "huslajobs");?>"
                },
                continueModal: {
                    heading: "<?php echo __("Welcome back", "huslajobs"); ?>",
                    subHeading: "<?php echo __("Continue were you left off", "huslajobs"); ?>",
                    buttons: {
                        cancel: "<?php echo __("Cancel", "huslajobs"); ?>",
                        continue: "<?php echo __("Continue", "huslajobs"); ?>",

                    }

                },
                applicationModal: {
                    heading: "<?php echo __("Application limit exceeded", "huslajobs"); ?>",
                    subHeading: "<?php echo __("Upgrade to apply", "huslajobs"); ?>",
                    buttons: {
                        cancel: "<?php echo __("Cancel", "huslajobs"); ?>",
                        upgrade: "<?php echo __("Upgrade", "huslajobs"); ?>",
                    },
                },
                relatedJobs: "<?php echo __("Related jobs", "huslajobs"); ?>",
            },
            jobSeekers: {
                heading: "<?php echo __("Search for talents", "huslajobs"); ?>",
                subHeading: "<?php echo __("Job seekers available", "huslajobs"); ?>",
                jobSeekers: "<?php echo __("Job seekers", "huslajobs"); ?>",
                jobSeeker: "<?php echo __("Job seeker", "huslajobs"); ?>",

                noJobSeekers: "<?php echo __("No job seeker found", "huslajobs"); ?>",
                errors: {
                    errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                },

            },
            jobSeeker: {
                jobCategories: "<?php echo __("Job categories", "huslajobs"); ?>",
                resume: {
                    tip: "<?php echo __("Click on file to view CV", "huslajobs"); ?>",
                    view: "<?php echo __("View", "huslajobs"); ?>"

                },
                contactForm: {
                    heading: "<?php echo __("Contact me", "huslajobs"); ?>",
                    fields: {
                        name: {
                            placeholder: "<?php echo __("Name", "huslajobs"); ?>",
                            label: "<?php echo __("Name", "huslajobs"); ?>"
                        },
                        message: {
                            placeholder: "<?php echo __("Your message", "huslajobs"); ?>",
                            label: "<?php echo __("Message", "huslajobs"); ?>"
                        },
                        email: {
                            placeholder: "<?php echo __("Email", "huslajobs"); ?>",
                            label: "<?php echo __("Email", "huslajobs"); ?>"
                        },
                        button: "<?php echo __("Send", "huslajobs"); ?>"

                    }
                }
                ,
                dataHeaders: {
                    about: "<?php echo __("About", "huslajobs"); ?>",
                    resume: "<?php echo __("Resume", "huslajobs"); ?>",
                    cv:"<?php echo __("CV", "huslajobs"); ?>"
                },
                errors: {
                    errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                },
            },
            frontPage: {
                heading: "<?php echo __("A window to limitless opportunities for job seekers and employers", "huslajobs"); ?>",
                subHeading: "<?php echo __("Let's get you started on your path to getting your dream job or that perfect employee", "huslajobs"); ?>",
                button: "<?php echo __("Find workers", "huslajobs"); ?>",
                or: "<?php echo __("OR", "huslajobs"); ?>",
            },
            forgotPassword: {
                heading: "<?php echo __("Forgot Password", "huslajobs"); ?>",
                formFields: {
                    email: {
                        placeholder: "<?php echo __("Email", "huslajobs"); ?>",
                        label: "<?php echo __("Email", "huslajobs"); ?>"
                    },
                    button: "<?php echo __("Reset Password", "huslajobs"); ?>"
                }, errors: {
                    errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                },
                login: "<?php echo __("Login", "huslajobs"); ?>",

            },
            dashboard: {
                dashboard: {
                    dataHeaders: {
                        jobApplications: "<?php echo __("Job applications", "huslajobs"); ?>",
                        profiles: "<?php echo __("Profiles", "huslajobs"); ?>",
                        subAccounts: "<?php echo __("Sub accounts", "huslajobs"); ?>",
                        jobs: "<?php echo __("Jobs", "huslajobs"); ?>",
                        companies: "<?php echo __("Companies", "huslajobs"); ?>",
                    },
                    errors: {
                        errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                        emailUnverified: "<?php echo __("Email unverified , please verify your email or you won't be able to access your dashboard after a certain number of logins", "huslajobs"); ?>",
                    },
                    successMessage: {
                        paymentSuccessful: "<?php echo __("Payment successfull", "huslajobs"); ?>",
                        applicationSent: "<?php echo __("Your application has been sent", "huslajobs"); ?>",
                    },
                    links: {
                        verifyEmail: "<?php echo __("Verify email", "huslajobs"); ?>",
                        upgradeAccount: "<?php echo __("Upgrade account", "huslajobs"); ?>",
                        createCompany: "<?php echo __("Create company", "huslajobs"); ?>",
                        accountDetails: "<?php echo __("Account details", "huslajobs"); ?>",
                        editProfile: "<?php echo __("Edit", "huslajobs"); ?>",
                        changePassword: "<?php echo __("Change password", "huslajobs"); ?>",
                        uploadResume: "<?php echo __("Upload resume", "huslajobs"); ?>",
                        postJob: "<?php echo __("Post job", "huslajobs"); ?>",
                        addProfile: "<?php echo __("Add profile", "huslajobs"); ?>",
                        deleteAccount: {
                            label: "<?php echo __("Delete Account", "huslajobs"); ?>",
                            tip: "<?php echo __("Please note that there’s no option to restore the account or its data nor reuse the email to create an account for a period of 30 days") ?>"
                        }
                    },
                    confirm: "<?php echo __("Are you sure you want to delete", "huslajobs"); ?>",
                    profileNotice: "<?php echo __("Do you have a skill or trade? Are you unemployed and ready to work? Advertise your skills on our platform by adding your job profile(s)", "huslajobs")?>"


                },
                profiles: {
                    index: {
                        confirm: "<?php echo __("Are you sure you want to delete", "huslajobs"); ?>",
                        addProfile: "<?php echo __("Add profile", "huslajobs"); ?>",
                        tableHeadings: {
                            profiles: "<?php echo __("Profiles", "huslajobs"); ?>",
                            actions: "<?php echo __("Actions", "huslajobs"); ?>",
                        },
                        public: "<?php echo __("Public", "huslajobs"); ?>",
                        errors: {
                            errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                        },
                        noProfile: "<?php echo __("No profile created yet", "huslajobs")?>",
                    },
                    create: {
                        headings: {
                            create: "<?php echo __("Add profile", "huslajobs"); ?>",
                            edit: "<?php echo __("Edit profile", "huslajobs"); ?>",
                        },
                        goBack: "<?php echo __("Go back", "huslajobs"); ?>",

                        formFields: {
                            name: {
                                placeholder: "<?php echo __("Profile name", "huslajobs"); ?>",
                                label: "<?php echo __("Profile name", "huslajobs"); ?>",
                            },
                            description: {
                                placeholder: "<?php echo __("Profile description between 150 to 500 characters", "huslajobs"); ?>",
                                label: "<?php echo __("Profile description", "huslajobs"); ?>"
                            },
                            categories: {
                                placeholder: "<?php echo __("Profile categories", "huslajobs"); ?>",
                                label: "<?php echo __("Profile categories", "huslajobs"); ?>"
                            },
                            email: {
                                placeholder: "<?php echo __("Email", "huslajobs"); ?>",
                                label: "<?php echo __("Email", "huslajobs"); ?>"
                            },
                            jobTitle: {
                                placeholder: "<?php echo __("Job title e.g Software Developer", "huslajobs"); ?>",
                                label: "<?php echo __("Job title", "huslajobs"); ?>"
                            },
                            phone: {
                                placeholder: "<?php echo __("Phone number", "huslajobs"); ?>",
                                label: "<?php echo __("Phone number", "huslajobs"); ?>",
                                error: "<?php echo __("Phone number is incorrect", "huslajobs"); ?>",
                            },
                            cv: {
                                buttons: {
                                    changeCv: "<?php echo __("Change CV", "huslajobs"); ?>",
                                    uploadCv: "<?php echo __("Upload CV", "huslajobs"); ?>",
                                },
                                tips: {
                                    fileTypes: "<?php echo __("File types", "huslajobs"); ?>",
                                    maxSize: "<?php echo __("Max size", "huslajobs"); ?>",
                                },
                                placeholder: "<?php echo __("Select CV", "huslajobs"); ?>",
                                label: "<?php echo __("Profile CV", "huslajobs"); ?>",
                            },
                            profileImage: {
                                buttons: {
                                    changeImage: "<?php echo __("Change profile image", "huslajobs"); ?>",
                                    uploadImage: "<?php echo __("Upload profile image", "huslajobs"); ?>",
                                },
                                tips: {
                                    imageTypes: "<?php echo __("Image types", "huslajobs"); ?>",
                                    maxSize: "<?php echo __("Max size", "huslajobs"); ?>",
                                },
                                label: "<?php echo __("Profile image", "huslajobs"); ?>",
                            },
                            experience: {
                                placeholder: "<?php echo __("Years of experience", "huslajobs"); ?>",
                                label: "<?php echo __("Experience", "huslajobs"); ?>"
                            },
                            buttons: {
                                addProfile: "<?php echo __("Add Profile", "huslajobs"); ?>",
                                cancel: "<?php echo __("Cancel", "huslajobs"); ?>",
                                editProfile: "<?php echo __("Edit profile", "huslajobs"); ?>",
                            }

                        },
                        errors: {
                            errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                        },
                    }
                },
                companies: {
                    index: {
                        confirm: "<?php echo __("Are you sure you want to delete", "huslajobs"); ?>",
                        addCompany: "<?php echo __("Add company", "huslajobs"); ?>",
                        tableHeadings: {
                            name: "<?php echo __("Name", "huslajobs"); ?>",
                            email: "<?php echo __("Email", "huslajobs"); ?>",
                            website: "<?php echo __("Website", "huslajobs"); ?>",
                            actions: "<?php echo __("Actions", "huslajobs"); ?>",
                        },
                        public: "<?php echo __("Public", "huslajobs"); ?>",
                        errors: {
                            errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                        },
                        noCompany: "<?php echo __("No company created yet", "huslajobs"); ?>",
                    },
                    create: {
                        headings: {
                            create: "<?php echo __("Add company", "huslajobs"); ?>",
                            edit: "<?php echo __("Edit company", "huslajobs"); ?>",
                        },
                        goBack: "<?php echo __("Go back", "huslajobs"); ?>",
                        formFields: {
                            name: {
                                placeholder: "<?php echo __("Name", "huslajobs"); ?>",
                                label: "<?php echo __("Name", "huslajobs"); ?>",
                            },
                            email: {
                                placeholder: "<?php echo __("Email", "huslajobs"); ?>",
                                label: "<?php echo __("Email", "huslajobs"); ?>"
                            },
                            website: {
                                placeholder: "<?php echo __("Company website", "huslajobs"); ?>",
                                label: " <?php echo __("Website", "huslajobs"); ?>",
                            },
                            phone: {
                                placeholder: "<?php echo __("Phone number", "huslajobs"); ?>",
                                label: "<?php echo __("Phone number", "huslajobs"); ?>",
                                error: "<?php echo __("Phone number is incorrect", "huslajobs"); ?>",
                            },
                            address: {
                                placeholder: "<?php echo __("Company address", "huslajobs"); ?>",
                                label: "<?php echo __("Address", "huslajobs"); ?>",
                            },
                            buttons: {
                                addCompany: "<?php echo __("Add company", "huslajobs"); ?>",
                                cancel: "<?php echo __("Cancel", "huslajobs"); ?>",
                                editCompany: "<?php echo __("Edit company", "huslajobs"); ?>",
                            }

                        },
                        updated: "<?php echo __("Company updated", "huslajobs") ?>",
                        errors: {
                            errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                        },
                    }
                },
                packages: {
                    heading: "<?php echo __("Membership plans", "huslajobs"); ?>",
                    subHeading: "<?php echo __("Choose the plan that suits you best.Pay upfront and get discounts", "huslajobs"); ?>",
                    goBack: "<?php echo __("Go back", "huslajobs"); ?>",

                    packagePeriod: {
                        month: "<?php echo __("Per month", "huslajobs"); ?>",
                        year: "<?php echo __("Per year", "huslajobs"); ?>",
                        threeMonths: "<?php echo __("Per 3 months", "huslajobs"); ?>",
                        sixMonths: "<?php echo __("Per 6 months", "huslajobs"); ?>",
                    },
                    button: "<?php echo __("Get started", "huslajobs"); ?>",
                    errors: {
                        errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                    },
                    subscribed: "<?php echo __("You are currently subscribed to this package.", "huslajobs") ?>"
                },
                jobSeekerSearch: {
                    formFields: {
                        searchText: {
                            placeholder: "<?php echo __("Search text ...", "huslajobs"); ?>",
                        },
                        category: {
                            placeholder: "<?php echo __("Select category", "huslajobs"); ?>",
                        },
                        state: {
                            placeholder: "<?php echo __("Select state", "huslajobs"); ?>",
                        },
                        country: {
                            placeholder: "<?php echo __("Select country", "huslajobs"); ?>",
                        },

                        button: "<?php echo __("Find workers", "huslajobs"); ?>"

                    },
                    errors: {
                        errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                        noFilter: "<?php echo __("Fill or select at least one filter to search", "huslajobs"); ?>",

                    },
                    noJobSeekers: "<?php echo __("No job seeker found", "huslajobs"); ?>",
                    jobSeekersFound: "<?php echo __("Job seekers found", "huslajobs"); ?>",
                    jobSeekerFound: "<?php echo __("Job seeker found", "huslajobs"); ?>",
                    tip: "<?php echo __("Clear search results", "huslajobs") ?>"
                },
                jobSearch: {
                    formFields: {
                        searchText: {
                            placeholder: "<?php echo __("Search text ...", "huslajobs"); ?>",
                        },
                        category: {
                            placeholder: "<?php echo __("Job category", "huslajobs"); ?>",
                        },
                        work: {
                            placeholder: "<?php echo __("Work location", "huslajobs"); ?>",
                        },
                        experience: {
                            placeholder: "<?php echo __("Experience level", "huslajobs"); ?>",
                        },
                        recruiter: {
                            placeholder: "<?php echo __("Recruiter", "huslajobs"); ?>",
                        },
                        jobType: {
                            placeholder: "<?php echo __("Job type", "huslajobs"); ?>",
                        },
                        location: {
                            placeholder: "<?php echo __("Location: state/region, city/town", "huslajobs"); ?>",
                        },
                        country: {
                            placeholder: "<?php echo __("Select country", "huslajobs"); ?>",
                        },

                        button: "<?php echo __("Find jobs", "huslajobs"); ?>"

                    },
                    errors: {
                        errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                        noFilter: "<?php echo __("Fill or select at least one filter to search", "huslajobs"); ?>",

                    },
                    noJobs: "<?php echo __("No job found", "huslajobs"); ?>",
                    jobs: "<?php echo __("Jobs", "huslajobs"); ?>",
                    tip: "<?php echo __("Clear search results", "huslajobs") ?>",
                    jobs: "<?php echo __("Jobs", "huslajobs"); ?>",
                    job: "<?php echo __("Job", "huslajobs"); ?>",
                    jobsFound: "<?php echo __("Jobs found", "huslajobs"); ?>",
                    jobFound: "<?php echo __("Job found", "huslajobs"); ?>",
                    of: "<?php echo __("of", "huslajobs"); ?>",
                    jobsFor: "<?php echo __("jobs for", "huslajobs"); ?>",
                    displaying: "<?php echo __("Displaying", "huslajobs"); ?>",
                    signup: "<?php echo __("Signup for more jobs", "huslajobs"); ?>",
                    upgrade: "<?php echo __("Upgrade for more jobs", "huslajobs"); ?>",
                    jobsAvailable: "<?php echo __("Jobs available", "huslajobs"); ?>",
                    jobAvailable: "<?php echo __("Job available", "huslajobs"); ?>",
                },
                job: {
                    edit: {

                        header: "<?php echo __("Edit job", "huslajobs"); ?>",
                        goBack: "<?php echo __("Go back", "huslajobs"); ?>",
                        formFields: {
                            jobTitle: {
                                placeholder: "<?php echo __("Enter job title", "huslajobs"); ?>",
                                label: "<?php echo __("Job title", "huslajobs"); ?>"
                            },
                            jobType: {
                                placeholder: "<?php echo __("Select job type", "huslajobs"); ?>",
                                label: "<?php echo __("Job type", "huslajobs"); ?>"
                            },
                            jobCategory: {
                                placeholder: "<?php echo __("Select job category", "huslajobs"); ?>",
                                label: "<?php echo __("Job category", "huslajobs"); ?>"
                            },
                            location: {
                                placeholder: "<?php echo __("Enter job location", "huslajobs"); ?>",
                                label: "<?php echo __("Location", "huslajobs"); ?>"
                            },
                            description: {
                                placeholder: "<?php echo __("Job description", "huslajobs"); ?>",
                                label: "<?php echo __("Job description", "huslajobs"); ?>"
                            },
                            experience: {
                                placeholder: "<?php echo __("Employee experience in years", "huslajobs"); ?>",
                                label: "<?php echo __("Experience", "huslajobs"); ?>"
                            },
                            salary: {
                                placeholder: "<?php echo __("Salary", "huslajobs"); ?>",
                                label: "<?php echo __("Salary", "huslajobs"); ?>"
                            },
                            currency: {
                                placeholder: "<?php echo __("Select currency", "huslajobs"); ?>",
                                label: "<?php echo __("Currency", "huslajobs"); ?>"
                            },
                            work: {
                                placeholder: "<?php echo __("Work location", "huslajobs"); ?>",
                                label: "<?php echo __("Work location", "huslajobs"); ?>"
                            },
                            cv: {
                                placeholder: "<?php echo __("Is CV required?", "huslajobs"); ?>",
                                label: "<?php echo __("CV", "huslajobs"); ?>",
                            },
                            motivation: {
                                placeholder: "<?php echo __("Is motivation/cover letter required?", "huslajobs"); ?>",
                                label: "<?php echo __("Motivation", "huslajobs"); ?>",
                            },
                            companyAccount: {
                                label: "<?php echo __("Company accounts", "huslajobs"); ?>",
                                placeholder: "<?php echo __("Select account", "huslajobs") ?>"
                            },
                            country: {
                                placeholder: "<?php echo __("Select country", "huslajobs"); ?>",
                                label: "<?php echo __("Country", "huslajobs"); ?>"
                            },
                            state: {
                                placeholder: "<?php echo __("Select state/region", "huslajobs"); ?>",
                                label: "<?php echo __("State/Region", "huslajobs"); ?>",
                                tip: "<?php echo __("Select country to select state", "huslajobs"); ?>",
                            },
                            city: {
                                placeholder: "<?php echo __("City/Town", "huslajobs"); ?>",
                                label: "<?php echo __("City/Town", "huslajobs"); ?>",
                            },
                            buttons: {
                                cancel: "<?php echo __("Cancel", "huslajobs"); ?>",
                                updateJob: "<?php echo __("Update job", "huslajobs"); ?>",
                            }

                        },
                        errors: {
                            errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                            login: "<?php echo __("Sign up or login to proceed", "huslajobs"); ?>",
                        }, no: "<?php echo __("NO", "huslajobs"); ?>",
                        yes: "<?php echo __("YES", "huslajobs"); ?>",

                    }
                },
                jobApplication: {
                    name: "<?php echo __("Name", "huslajobs"); ?>",
                    job: "<?php echo __("Job", "huslajobs") ?>",
                    motivation: "<?php echo __("Motivation", "huslajobs"); ?>",
                    city: "<?php echo __("City", "huslajobs") ?>",
                    country: "<?php echo __("Country", "huslajobs") ?>",
                    email: "<?php echo __("Email", "huslajobs") ?>",
                    address: "<?php echo __("Address", "huslajobs") ?>",
                    noApplication: "<?php echo __("No application yet", "huslajobs") ?>",
                    apply:"<?php echo __("Apply","huslajobs") ?>"
                },
                accounts: {
                    index: {
                        errors: {
                            errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                        },
                        confirm: "<?php echo __("Are you sure you want to delete", "huslajobs") ?>",
                        current: "<?php echo __("Current", "huslajobs") ?>",
                        default: "<?php echo __("Default", "huslajobs") ?>",
                        tableHeadings: {
                            actions: "<?php echo __("Actions", "huslajobs") ?>",
                            account: "<?php echo __("Account", "huslajobs") ?>",
                        }

                    },
                    editUser: {
                        header: "<?php echo __("Edit user account", "huslajobs") ?>",
                        goBack: "<?php echo __("Go back", "huslajobs") ?>",
                    },
                    edit: {
                        header: "<?php echo __("Edit account", "huslajobs") ?>",
                        goBack: "<?php echo __("Go back", "huslajobs") ?>",
                        formFields: {
                            name: {
                                placeholder: "<?php echo __("Name", "huslajobs"); ?>",
                                label: "<?php echo __("Name", "huslajobs"); ?>",
                            },
                            website: {
                                placeholder: "<?php echo __("Company website", "huslajobs"); ?>",
                                label: " <?php echo __("Website", "huslajobs"); ?>",
                            },
                            phone: {
                                placeholder: "<?php echo __("Phone number", "huslajobs"); ?>",
                                label: "<?php echo __("Phone number", "huslajobs"); ?>",
                                error: "<?php echo __("Phone number is incorrect", "huslajobs"); ?>",
                            },
                            faxNumber: {
                                placeholder: "<?php echo __("Fax number", "huslajobs"); ?>",
                                label: " <?php echo __("Fax number", "huslajobs"); ?>",
                            },
                            email: {
                                placeholder: "<?php echo __("Email", "huslajobs"); ?>",
                                label: "<?php echo __("Email", "huslajobs"); ?>"
                            },
                            country: {
                                placeholder: "<?php echo __("Select country", "huslajobs"); ?>",
                                label: "<?php echo __("Country", "huslajobs"); ?>"
                            },
                            state: {
                                placeholder: "<?php echo __("Select state/region", "huslajobs"); ?>",
                                label: "<?php echo __("State/Region", "huslajobs"); ?>",
                                tip: "<?php echo __("Select country to select state", "huslajobs"); ?>",
                            },
                            city: {
                                placeholder: "<?php echo __("City/Town", "huslajobs"); ?>",
                                label: "<?php echo __("City/Town", "huslajobs"); ?>",
                            },
                            bio: {
                                placeholders: {
                                    individual: "<?php echo __("Tell us about yourself", "huslajobs"); ?>",
                                    company: "<?php echo __("Tell us about company", "huslajobs"); ?>",
                                },
                                label: "<?php echo __("About", "huslajobs"); ?>",
                            },
                            profileImage: {
                                buttons: {
                                    company: {
                                        changeImage: "<?php echo __("Change company logo", "huslajobs"); ?>",
                                        uploadImage: "<?php echo __("Company logo", "huslajobs"); ?>",
                                    },
                                    individual: {
                                        changeImage: "<?php echo __("Change profile image", "huslajobs"); ?>",
                                        uploadImage: "<?php echo __("Upload profile image", "huslajobs"); ?>",
                                    }

                                },
                                tips: {
                                    imageTypes: "<?php echo __("Image types", "huslajobs"); ?>",
                                    maxSize: "<?php echo __("Max size", "huslajobs"); ?>",
                                },
                                label: "<?php echo __("Profile image", "huslajobs"); ?>",
                            },
                            button: "<?php echo __('Save', 'huslajobs'); ?>",


                        }
                        , errors: {
                            errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",

                        },

                    },
                    createAccount: {
                        header: "<?php echo __("Create company", "huslajobs") ?>",
                        goBack: "<?php echo __("Go back", "huslajobs") ?>",
                        steps: {
                            company: "<?php echo __("Company", "huslajobs") ?>",
                            activities: "<?php echo __("Activities", "huslajobs") ?>",
                        },
                        formFields: {
                            name: {
                                placeholder: "<?php echo __("Name", "huslajobs"); ?>",
                                label: "<?php echo __("Name", "huslajobs"); ?>",
                            },
                            website: {
                                placeholder: "<?php echo __("Company website", "huslajobs"); ?>",
                                label: " <?php echo __("Website", "huslajobs"); ?>",
                            },
                            faxNumber: {
                                placeholder: "<?php echo __("Fax number", "huslajobs"); ?>",
                                label: " <?php echo __("Fax number", "huslajobs"); ?>",
                            },
                            email: {
                                placeholder: "<?php echo __("Email", "huslajobs"); ?>",
                                label: "<?php echo __("Email", "huslajobs"); ?>"
                            },
                            country: {
                                placeholder: "<?php echo __("Select country", "huslajobs"); ?>",
                                label: "<?php echo __("Country", "huslajobs"); ?>"
                            },
                            phone: {
                                placeholder: "<?php echo __("Phone number", "huslajobs"); ?>",
                                label: "<?php echo __("Phone number", "huslajobs"); ?>",
                                error: "<?php echo __("Phone number is incorrect", "huslajobs"); ?>",
                            },
                            state: {
                                placeholder: "<?php echo __("Select state/region", "huslajobs"); ?>",
                                label: "<?php echo __("State/Region", "huslajobs"); ?>",
                                tip: "<?php echo __("Select country to select state", "huslajobs"); ?>",
                            },
                            city: {
                                placeholder: "<?php echo __("City/Town", "huslajobs"); ?>",
                                label: "<?php echo __("City/Town", "huslajobs"); ?>",
                            },
                            bio: {
                                placeholder: "<?php echo __("Tell us about your company,between 150 to 500 characters", "huslajobs"); ?>",
                                label: "<?php echo __("About", "huslajobs"); ?>",
                            },
                            profileImage: {
                                buttons: {
                                    changeImage: "<?php echo __("Change company logo", "huslajobs"); ?>",
                                    uploadImage: "<?php echo __("Company logo", "huslajobs"); ?>",
                                },
                                tips: {
                                    imageTypes: "<?php echo __("Image types", "huslajobs"); ?>",
                                    maxSize: "<?php echo __("Max size", "huslajobs"); ?>",
                                },
                                label: "<?php echo __("Company logo", "huslajobs"); ?>",
                            },
                            button: "<?php echo __('Save', 'huslajobs'); ?>",


                        },
                        errors: {
                            errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                            selectActivity: "<?php echo __("Please select an activity", "huslajobs"); ?>",
                        },
                        activities: {
                            'applyForJobs': "<?php echo __("Looking to apply for jobs", "huslajobs") ?>",
                            'postJobs': "<?php echo __("Looking to hire an employee/post jobs", "huslajobs") ?>",
                            'both': "<?php echo __("Both", "huslajobs") ?>"
                        }

                    },
                    accountDetails: {
                        firstName: "<?php echo __("First name", "huslajobs"); ?>",
                        lastName: " <?php echo __("Last name", "huslajobs"); ?>",
                        email: "<?php echo __("Email", "huslajobs"); ?>",
                        country: "<?php echo __("Country", "huslajobs"); ?>",
                        state: "<?php echo __("State/Region", "huslajobs"); ?>",
                        city: "<?php echo __("City/Town", "huslajobs"); ?>",
                        dob: "<?php echo __("Date of birth", "huslajobs"); ?>",
                        phone: "<?php echo __("Phone number", "huslajobs"); ?>",
                        bio: "<?php echo __("About", "huslajobs"); ?>",
                        goBack: "<?php echo __("Go back", "huslajobs"); ?>",
                        confirm: "<?php echo __("Are you sure you want to delete", "huslajobs"); ?>",
                        links: {
                            verifyEmail: "<?php echo __("Verify email", "huslajobs"); ?>",
                            upgradeAccount: "<?php echo __("Upgrade account", "huslajobs"); ?>",
                            createCompany: "<?php echo __("Create company", "huslajobs"); ?>",
                            accountDetails: "<?php echo __("Account details", "huslajobs"); ?>",
                            editProfile: "<?php echo __("Edit", "huslajobs"); ?>",
                            changePassword: "<?php echo __("Change password", "huslajobs"); ?>",
                            uploadResume: "<?php echo __("Upload resume", "huslajobs"); ?>",
                            postJob: "<?php echo __("Post job", "huslajobs"); ?>",
                            addProfile: "<?php echo __("Add profile", "huslajobs"); ?>",
                            deleteAccount: {
                                label: "<?php echo __("Delete Account", "huslajobs"); ?>",
                                tip: "<?php echo __("Please note that there’s no option to restore the account or its data nor reuse the email to create an account for a period of 30 days") ?>"
                            }
                        },
                        errors: {
                            errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
                            emailUnverified: "<?php echo __("Email unverified , please verify your email or you won't be able to access your dashboard after a certain number of logins", "huslajobs"); ?>",
                        },
                    }

                }

            }


        },
        pagination: {
            page: "<?php echo __("Page", "huslajobs"); ?>",
            of: "<?php echo __("of", "huslajobs"); ?>",
        },
        functionsJs: {
            validationErrors: {
                isRequired: "<?php echo __("is required", "huslajobs"); ?>",
                atleast: "<?php echo __("must be at least", "huslajobs"); ?>",
                atmost: "<?php echo __("must be at most", "huslajobs"); ?>",
                characters: "<?php echo __("characters", "huslajobs"); ?>",
                validEmail: "<?php echo __("must be a valid email e.g husla@gmail.com", "huslajobs"); ?>",
                isNumber: "<?php echo __("must be a number", "huslajobs"); ?>",
                isNotNegative: "<?php echo __("must not be a negative number", "huslajobs"); ?>",
                password: "<?php echo __("Password do not match", "huslajobs"); ?>",
                noSpace: "<?php echo __("must not contain space", "huslajobs"); ?>",
                validUrl: "<?php echo __("must be a valid url eg https://huslajobs.com", "huslajobs"); ?>",
            },
            errorOccurred: "<?php echo __("An error occurred, please try again or contact the admin", "huslajobs"); ?>",
            exceedLimit: "<?php echo __("file size exceeded maximum upload size", "huslajobs"); ?>",
            sessionExpired: "<?php echo __("Session has expired", "huslajobs"); ?>",


        }
    };
    // console.log('VueUiStrings', VueUiStrings);
</script>