import { CountriesData } from "./countries_and_codes.js";

const world_countries = [];
for (const country of CountriesData) {
    world_countries.push( {value:country.code,label:country.name})
}
export { world_countries }