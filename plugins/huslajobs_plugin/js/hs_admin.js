(function ($) {
    'use strict';

    $(document).ready(function() {
        $("#husla-logout").click(e => {
            localStorage.clear();
        });
        const url = new URL(window.location.href);
        const action = url.searchParams.get("action");
        if (action  && action== 'logout'){
            localStorage.clear();
        }
    });

}(jQuery))