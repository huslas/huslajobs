import {world_countries as WorldCountries} from "./countries_data.js";

/**
 * Reusable Functions are functions that repeat over components
 * To use it
 * -import the function in your component
 * -you can add it as a mixins or add it to the method object of your component
 * e.g
 * import {ReusableFunctions} from "../../../../../js/functions.js";
 *
 * const SignUp = {
 *     mixins:[ReusableFunctions],
 * }
 *
 * or
 * const SignUp = {
 *     methods:{
 *         ...ReusableFunctions.methods,
 *     }
 * }
 *
 * @type {{methods: {validateInput(*, *, *=, *): void, testMethod(): void}}}
 */



const ReusableFunctions = {

    methods: {
        /**
         *            To use this function create formErrors:{} inside your component
         *            ie
         * data{
         *       return{
         *          formErrors:{}
         *       }
         *   }
         * @param fieldKey
         * @param fieldValue
         * @param validation
         * @param fieldName
         */
        validateInput(fieldKey, fieldValue, validation = {
            required: false,
            isEmail: false,
            min: 0,
            max: 0,
            terms: true,
            isPassword: false,
            isUsername: false,
            isUrl: false,
            isNumeric: false,
            isNotNegative: false,
        }, fieldName) {
            let trimmedValue = fieldValue ? fieldValue.toString().replace(/^\s+|\s+$/gm, '').replace(/(\r\n|\n|\r)/gm, "") : undefined ;
            if (validation.required && !fieldValue) {
                this.formErrors[fieldKey] = `${fieldName} ${VueUiStrings.functionsJs.validationErrors.isRequired}`;
            } else if (validation.min && (trimmedValue  && trimmedValue.length < parseInt(validation.min))) {
                this.formErrors[
                    fieldKey
                    ] = `${fieldName} ${VueUiStrings.functionsJs.validationErrors.atleast}  ${validation.min} ${VueUiStrings.functionsJs.validationErrors.characters}`;
            } else if (validation.max && (trimmedValue  && trimmedValue.length > parseInt(validation.max))) {
                this.formErrors[
                    fieldKey
                    ] = `${fieldName} ${VueUiStrings.functionsJs.validationErrors.atmost}  ${validation.max} ${VueUiStrings.functionsJs.validationErrors.characters}`;
            } else if (validation.isEmail && !fieldValue.match(/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g)) {
                this.formErrors[
                    fieldKey
                    ] = `${fieldName} ${VueUiStrings.functionsJs.validationErrors.validEmail}`;
            } else if (validation.isNumeric && isNaN(parseInt(fieldValue))) {
                this.formErrors[
                    fieldKey
                    ] = `${fieldName} ${VueUiStrings.functionsJs.validationErrors.isNumber}`;
            } else if (validation.isNotNegative && (parseInt(fieldValue) < 0)) {
                this.formErrors[
                    fieldKey
                    ] = `${fieldName} ${VueUiStrings.functionsJs.validationErrors.isNotNegative}`;
            } else if (validation.isPassword && fieldValue !== this.form.password) {
                this.formErrors[
                    fieldKey
                    ] = `${VueUiStrings.functionsJs.validationErrors.password}`;
            } else if (validation.isUsername && !fieldValue.match(/^\S*$/)) {
                this.formErrors[
                    fieldKey
                    ] = `${fieldName} ${VueUiStrings.functionsJs.validationErrors.noSpace}`;
            } else if (validation.isUrl && !fieldValue.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g)) {
                this.formErrors[
                    fieldKey
                    ] = `${fieldName} ${VueUiStrings.functionsJs.validationErrors.validEmail}`;
            } else {
                delete this.formErrors[fieldKey];
            }
        },

        /**
         * Function to get all job types
         * to use this function define page,perPage,sortField,sort and jobTypes variables in your component data
         * We do this to keep this binding and avoid duplicates names
         * example
         * data(){
         *     return{
         *         page:1,
         *         sortField: id,
         *         sort:'desc',
         *         jobTypes:[]
         *
         *
         *     }
         * }
         *
         *
         * @param searchText
         */
        getAllJobTypes(searchText = undefined) {
            this.loading = true;
            const data = new FormData();
            data.append("action", "get_job_types");
            data.append("page", this.page);
            data.append("perPage", -1);
            data.append("sortBy", this.sortField);
            data.append("order", this.sort);
            data.append("searchText", searchText);
            data.append("searchFields", "name");

            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: {"Content-Type": "multipart/form-data"},

            })
                .then(function (response) {
                    if (response.data.data.length) {
                        that.jobTypes = response.data.data.map((jobType) => {
                            return {
                                label: jobType.name,
                                value: jobType
                            }
                        });
                    } else {
                        that.jobTypes = response.data.data;
                    }
                    that.loading = false;
                })
                .catch(function (error) {
                    console.log(error);
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error(`${VueUiStrings.functionsJs.errorOccurred}`);
                    }
                    that.loading = false;
                    that.error = true;
                });

        },


        /**
         * Function to get all currencies
         * to use this function define page and currencies variables in your component data
         * We do this to keep this binding and avoid duplicates names
         * example
         * data(){
         *     return{
         *         page:1,
         *         currencies:[]
         *
         *
         *     }
         * }
         *
         *
         * @param searchText
         */
        getCurrencies(searchText = undefined) {
            this.loading = true;
            if (searchText !== undefined) {
                const data = new FormData();
                data.append("searchText", searchText);
                data.append("searchFields", "code");
                data.append("action", "get_currencies");
                data.append("page", this.page);
                data.append("perPage", -1);
                const that = this;
                axios({
                    method: "post",
                    url: ajaxurl,
                    data: data,
                    headers: {"Content-Type": "multipart/form-data"},
                })
                    .then(function (response) {
                        if (response.data.data.length) {
                            that.currencies = response.data.data.map((currency) => {
                                return {
                                    label: currency.code,
                                    value: currency
                                }
                            })
                        } else {
                            that.currencies = response.data.data;
                        }


                        that.loading = false;

                    })
                    .catch(function (error) {
                        console.log(error);
                        if (error.response) {
                            toastr.error(error.response.data.data);
                        } else {
                            toastr.error(`${VueUiStrings.functionsJs.errorOccurred}`);
                        }
                        that.loading = false;
                        that.error = true;
                    });
            }

        },

        /**
         * Function to get all jobs dependencies
         * to use this function define currencies,jobCategories,jobTypes variables in your component data
         * We do this to keep this binding and avoid duplicates names
         * example
         * data(){
         *     return{
         *         jobCategories:[],
         *         currencies:[],
         *         jobTypes:[]
         *
         *
         *     }
         * }
         *
         *
         */
        getJobDependencies(front_end = true) {
            this.loading = true
            const data = new FormData();
            data.append("page", 1);
            data.append("perPage", -1);
            if (front_end) {
                data.append('frontEnd', front_end);
            }

            data.append("action", "get_categories_job_types_and_currencies");
            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: {"Content-Type": "multipart/form-data"},
            })
                .then(function (response) {
                    if (response.data) {
                        that.jobCategories = response.data.job_categories.data.map((category) => {
                            return {
                                label: category.name,
                                value: category.id
                            }
                        });


                        that.jobTypes = response.data.job_types.data.map((jobType) => {
                            return {
                                label: jobType.name,
                                value: jobType.id
                            }
                        });
                        that.currencies = response.data.currencies.data.map((currency) => {
                            return {
                                label: currency.name,
                                value: currency.id
                            }
                        });

                        if (!front_end) {
                            that.accounts = response.data.accounts.data.map((account) => {
                                return {
                                    label: account.name,
                                    value: account.id
                                }
                            });
                        }


                    }

                    if (front_end) {
                        //check if editing or creating
                        if (that.$route?.params?.slug) {
                            that.getJob()
                        } else {
                            that.bindModal();
                        }
                    } else {
                        that.loading = false
                    }


                    that.loading = false
                })
                .catch(function (error) {
                    console.log(error);
                    that.error = true;
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error(`${VueUiStrings.functionsJs.errorOccurred}`);
                    }

                });
        },

        getCategories(searchText = '') {

            const data = new FormData();
            data.append("page", this.page);
            data.append("perPage", this.perPage);
            data.append("sortBy", this.sortField);
            data.append("order", this.sort);
            data.append("searchText", searchText);
            data.append("searchFields", "name");
            data.append("action", "get_categories");

            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: {"Content-Type": "multipart/form-data"},
            })
                .then(function (response) {
                    if (response.data?.data.length) {
                        that.jobCategories = response.data.data.map((category) => {
                            return {
                                label: category.name,
                                value: category
                            }
                        });

                    } else {

                        that.jobCategories = response.data.data
                    }


                })
                .catch(function (error) {
                    console.log(error);
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error(`${VueUiStrings.functionsJs.errorOccurred}`);
                    }

                });

        },
        /**
         * Function to get job
         * to use this function define jobId,formData variables in your component data
         * We do this to keep this binding and avoid duplicates names
         * example
         * data(){
         *     return{
         *         jobId:1,
         *         formData:{}
         *
         *
         *     }
         * }
         *
         *
         */
        getJob() {
            const data = new FormData();
            data.append("job_slug", this.$route.params.slug);
            data.append("action", "get_job");
            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: {"Content-Type": "multipart/form-data"},
            })
                .then(function (response) {

                    if (response.data) {
                        response.data.currency_id = parseInt(response.data.currency_id) ? response.data.currency_id : undefined;
                        response.data.salary = parseInt(response.data.salary) ? response.data.salary : undefined;
                        // response.data.cv_required = parseInt(that.formData.cv_required);
                        that.formData = response.data;
                        that.loading = false;
                        that.error = false;
                        that.closeLoader();
                    }
                })
                .catch(function (error) {
                    console.log(error)
                    that.loading = false;
                    that.error = true;
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error(`${VueUiStrings.functionsJs.errorOccurred}`);
                    }
                    that.closeLoader();
                });
        },

        getJobAndRelatedJobs(job_application = false) {

            this.loading = true;
            const data = new FormData();
            data.append("job_slug", this.jobSlug);
            data.append("action", "get_job_and_related_jobs");
            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: {"Content-Type": "multipart/form-data"},
            })
                .then(function (response) {

                    if (response.data) {

                        // console.log("response.data.jobresponse.data.job");
                        // console.log(response.data.job)
                        if (response.data.job){
                            that.job = response.data.job
                            that.job.cv_required = parseInt(that.job.cv_required) ? true : false
                            that.job.motivation_required = parseInt(that.job.motivation_required) ? true : false
                        }

                        if (response.data.related_jobs?.data?.length) {
                            that.relatedJobs = response.data.related_jobs.data;
                        }
                        if (job_application) {
                            that.platform_fee = {};
                            if (that.job.job_type_name.includes('paid')) {
                                that.platform_fee['price'] = price_per_paid_internship;
                            } else {

                                that.platform_fee['price'] = hs_price_per_job;
                            }
                        }

                        setTimeout(() => {
                            if (!job_application) {
                                that.loading = false;
                                if (that.job){
                                    that.createHtmlElement('p', 'job-description', that.job['description']);
                                }

                                that.closeLoader();
                            } else {
                                that.bindApplicationModal();
                                that.bindModal();
                            }


                        }, 100)

                        that.error = false;

                    }
                })
                .catch(function (error) {
                    console.log(error)
                    that.loading = false;
                    that.error = true;
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error(`${VueUiStrings.functionsJs.errorOccurred}`);
                    }
                    that.closeLoader();
                });
        },
        jobsDate(dateString) {
            var months = ["January", "February", "March", "April", "May", "June", "July",
                "August", "September", "October", "November", "December"]
            let date = new Date(dateString);
            const monthIndex = date.getUTCMonth();
            const month = months[monthIndex];
            const year = date.getUTCFullYear()
            const day = date.getUTCDate()
            return month + ' ' + day + ', ' + year;
        },

        /**
         * Function to get all jobs
         * to use this function define page,perPage,sortBy,order,account_id,jobs,pages,loading,error variables in your component data
         * and also define emit account_jobs
         * We do this to keep this binding and avoid duplicates names
         * example
         * emits:['account_jobs']
         * data(){
         *     return{
         *         page:1,
         *         perPage:21,
         *         sortBy:'id',
         *         order:'desc',
         *         searchText:'',
         *         searchFields:'',
         *         account_id : '',
         *
         *
         *
         *     }
         * }
         *
         *
         */
        getAllJobs(limitPerPage = 0, first_request = false) {
            this.loading = true;
            const data = new FormData();
            data.append("action", "get_jobs");
            data.append("page", this.page ?? 1);
            data.append("perPage", this.perPage ?? 15);
            data.append("limitPerPage", limitPerPage);
            data.append("sortBy", this.sortField ?? 'id');
            data.append("order", this.sort ?? 'desc');
            data.append("account_id", this.account_id ?? '')
            data.append("searchText", this.searchText ?? '');
            data.append("searchFields", this.searchFields ?? '');

            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: {"Content-Type": "multipart/form-data"},
            })
                .then(function (response) {
                    if (response.data) {
                        that.jobs = response.data.data;
                        that.pages = response.data.totalPages;
                        /**
                         * Calculate pages if job limit is set
                         */

                        // if (first_request) {
                        if (that.job_limit && parseInt(that.job_limit) !== -1 && (parseInt(response.data.totalItems) > parseInt(that.job_limit))) {
                            that.pages = Math.ceil(parseInt(that.job_limit) / parseInt(that.perPage))
                        }
                        // }
                        that.$emit('account_jobs', response.data.totalItems)

                        if (that.total !== undefined) {
                            that.total = response.data.totalItems;
                        }
                        if (!that.account_id){
                            window.scrollTo({
                                top: 0,
                                left: 0,
                                behavior: 'smooth'
                            });
                        }


                        that.loading = false;
                        that.closeLoader();
                    }

                    that.error = false;
                })
                .catch(function (error) {
                    console.log(error);
                    if (error.response) {
                        toastr.error(error.response.data.data);
                    } else {
                        toastr.error(`${VueUiStrings.functionsJs.errorOccurred}`);
                    }
                    that.loading = false;

                    that.error = true;
                    that.closeLoader();
                });
        },
        getIp(callback) {
            axios({
                method: "get",
                "url": "https://ip2c.org/s",
            }).then((res) => {
                let country_code = 'cm'
                if (res.status === 200) {
                    country_code = res.data.split(";")[1].toLowerCase();
                }
                return country_code
            }).catch(() => {
                return 'cm'
            }).then((res) => callback(res))
        },

        setUpPhoneNumber() {
            const phone = document.getElementById("phone-number");
            if (phone) {
                window.intlTelInput(phone, {
                    utilsScript: this.jsUrl + "/intl-tel-utils.js",
                    customPlaceholder: function (selectedCountryPlaceholder, selectedCountryData) {
                        return "e.g. " + selectedCountryPlaceholder;
                    },
                    initialCountry: "auto",
                    geoIpLookup: this.getIp,
                });
            }

        },

        /**
         * get file Url
         * @param file:array
         * @returns file base64url
         */
        async getDataURL(file) {
            return new Promise((resolve, reject) => {
                const reader = new FileReader()
                reader.onload = () => {
                    resolve(reader.result)
                }
                reader.onerror = reject

                reader.readAsDataURL(file)
            })
        },
        async getFile(e) {
            const files = e.target.files || e.dataTransfer.files
            if (!files.length) return
            const file = files[0];
            const file_size = (file.size / 1024 / 1024).toFixed(2) //in megabytes
            if (parseInt(file_size) <= maximum_upload) {
                // file.name = file.name.replace(/[^a-zA-Z0-9.-]/g, '-')
                const imageFile = {
                    url: '',
                    file: file,
                    name: file.name,
                    type: file.type,
                    size: file.size,
                    lastModified: file.lastModified,
                    ...file,
                    base64: '',
                }

                imageFile.url = URL.createObjectURL(file);

                // imageFile.base64 =  await this.getDataURL(file);
                if (this.form) {
                    this.form.fileUpload = imageFile;
                    this.form.fileUpload.base64 = await this.getDataURL(file);
                }
                if (this.formData) {
                    this.formData.fileUpload = imageFile;
                    this.formData.fileUpload.base64 = await this.getDataURL(file);
                }
            } else {
                toastr.error(`${file.name},file size exceeded maximum upload size ${ maximum_upload}MB` );
                e.target.value = ''
            }
        },
        dataURLtoFile(fileData) {
            let base64 = fileData.base64;
            let arr = base64.split(','),
                mime = arr[0].match(/:(.*?);/)[1],
                bstr = atob(arr[1]),
                n = bstr.length,
                u8arr = new Uint8Array(n);
            while (n--) {
                u8arr[n] = bstr.charCodeAt(n);
            }
            const file = new File([u8arr], fileData.name, {type: mime});
            const results = {file: file, url: URL.createObjectURL(file)}
            return results;
        },
        pdfPreview(srcUrl, fileName) {
            // const url =
            document.removeEventListener("adobe_dc_view_sdk.ready", this.pdfCallBack(srcUrl, fileName))
            document.addEventListener("adobe_dc_view_sdk.ready", this.pdfCallBack(srcUrl, fileName));
        },
        pdfCallBack(srcUrl, fileName) {
            var adobeDCView = new AdobeDC.View({clientId: "4c21823e6a9343f2948ee5776d336838", divId: "adobe-dc-view"});
            adobeDCView.previewFile(
                {
                    content: {location: {url: srcUrl}},
                    metaData: {fileName: fileName}
                });
        },
        selectedCountry(countryCode) {
            if (countryCode) {
                if (countryCode.length > 2 || countryCode.length == 0) {
                    return countryCode;
                } else if (countryCode.length === 2) {
                    const countries = WorldCountries;
                    return countries.filter((country) => country.value.toLowerCase() === countryCode.toLowerCase())[0].label;
                }
            } else {
                return 'Cameroon'
            }
        },
        setImage(url, background = true) {
            let src = url ? url : this.imgUrl + '/no_img.jpg'
            if (!background) {
                return src;
            }
            return "background-image:url(" + src.toString() + ")";
        },
        reduceString(value, showChars = 100, ellipsesText = "...") {
            let reduced_text = value;
            if (reduced_text.length > showChars) {
                reduced_text = reduced_text.substr(0, showChars) + ellipsesText;
            }
            return reduced_text;
        },
        cvUrl(url,returnExtension=false) {
            let extension = url.split('.')[1];
            if (returnExtension){
                return extension
            }else{
                return extension == 'pdf' ? THEME_URL + '/document-type/pdf-logo.png' : THEME_URL + '/document-type/microsoft-word.png'
            }
        },
        getObjectIndex(objectArr, objValue) {
            return objectArr.findIndex(object => {
                return object.value === objValue;
            })
        },
        showToolTip(currentToolTip, tooltip) {
            if (this.isMobile()){
                return false;
            }
            return currentToolTip ? currentToolTip.toLowerCase() === tooltip.toLowerCase() : false;
        },
        decodeHtml(html) {
            const txt = document.createElement("textarea");
            txt.innerHTML = html;
            return txt.value;
        },
        // isMobile
        isMobile() {
            // device detection
            if (
                /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(
                    navigator.userAgent
                ) ||
                /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
                    navigator.userAgent.substr(0, 4)
                )
            ) {
                return true;
            }
            return false;
        },
        createHtmlElement(tag, tagClass, content, parentTag = 'div', parentSelector = 'id', parentSelectorText = 'job-desc', htmlContent = true) {
            const htmlEle = document.createElement(`${tag}`);
            htmlEle.classList.add(`${tagClass}`)
            if (htmlContent) {
                content = this.decodeHtml(content)
            }
            let selector = '';
            htmlEle.innerHTML = content;
            if (parentSelector === 'class') {
                selector = parentTag + '.' + parentSelectorText
            } else if (parentSelector === 'id') {
                selector = parentTag + '#' + parentSelectorText
            }
            if (selector) {
                const parentEle = document.querySelector(`${selector}`);
                if (parentEle) {
                    parentEle.innerHTML = content;
                }
            }
        },
        closeLoader() {
            let loadingScreen = document.getElementById('loading-screen');
            if (loadingScreen) {
                document.querySelector('body').classList.remove('loading');
                loadingScreen.classList.add("d-none");

            }

        },
        startLoader() {
            let loadingScreen = document.getElementById('loading-screen');
            if (loadingScreen) {
                loadingScreen.classList.remove("d-none");
                document.querySelector('body').classList.add('loading');
            }
        },
        logOutUser() {
            let userLogout = localStorage.getItem('userLogout') ? JSON.parse(localStorage.getItem('userLogout')) : '';
            let logout = false;
            let now = new Date();
            now = now.getTime();
            if (userLogout && now > userLogout) {
                logout = true
            }
            if (logout) {
                this.loggingOut = true;
                this.logOut()
            } else {
                this.closeLoader();
            }

        },
        logOut() {
            this.showLoader = true;
            const data = new FormData();
            data.append("action", "hs_log_out");
            data.append("logout", true);
            const that = this;
            axios({
                method: "post",
                url: ajaxurl,
                data: data,
                headers: {"Content-Type": "multipart/form-data"},
            })
                .then(function (response) {
                    if (response.status <= 300) {

                        localStorage.removeItem('user');
                        localStorage.removeItem('active_account');
                        localStorage.setItem("userLogout",null);
                        localStorage.removeItem("userLogout");
                        localStorage.clear();
                        toastr.warning(VueUiStrings.functionsJs.sessionExpired, 'Success');
                    }
                    that.loggingOut = false;
                    setTimeout(()=>{
                        window.location.href = home_url;
                    },300)

                })
                .catch(function (error) {
                    console.log("LOGOUT ERROR");
                    console.log(error);
                    that.loggingOut = false;
                });
        },
        setUserLogout(limit = 30) {
            let now = new Date();
            let logged_in_session = login_session || limit;
            let userLogoutTime = now.getTime() + (parseInt(logged_in_session) * 60 * 1000);
            localStorage.setItem('userLogout', JSON.stringify(userLogoutTime));
        },
        setJActiveLink(hrefPart){
        //    get all primary menu anchor elements
            let menuLinks = document.getElementsByClassName("menu-link");
            if (menuLinks && menuLinks.length){
                for (const link of menuLinks) {
                    const href = link.getAttribute('href');
                    if (href.includes(hrefPart)){
                       let parentElement = link.parentElement;
                       if (!parentElement.classList.contains('current-menu-item')){
                            parentElement.classList.add('current-menu-item');
                       }
                       break;
                    }


                }
            }

        }

    },

};

export {ReusableFunctions};
