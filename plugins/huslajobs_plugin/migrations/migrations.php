<?php

namespace huslajobs;

$packages = new HuslaMigration( 'packages' );
$packages->id();
$packages->string( 'name', 60 );
$packages->text( 'description' )->nullable();
$packages->integer( 'price' )->unsigned();
$packages->integer( 'profiles' )->default( - 1 );
$packages->integer( 'jobs' )->default( - 1 );
$packages->integer( 'job_applications' )->default( - 1 );
$packages->integer( 'duration' );
$packages->text('benefits');
$packages->bigInt( 'wp_user_id' )->default( 0 );
$packages->softDelete();
$packages->timestamps();


$jobs = new HuslaMigration( 'jobs' );
$jobs->id();
$jobs->bigInt( 'job_type_id' );
$jobs->integer( 'salary' )->nullable();
$jobs->string( 'name', 200 );
$jobs->text( 'description' )->nullable();
$jobs->string( 'city' )->nullable();
$jobs->string( 'state' )->nullable();
$jobs->string( 'country' )->nullable();
$jobs->string( 'country_name' )->nullable();
$jobs->string( 'application_email' )->nullable();
$jobs->string( 'slug');
$jobs->string( 'work' )->nullable();
$jobs->integer( 'experience' )->nullable();
$jobs->bigInt( 'category_id' )->nullable();
$jobs->bigInt( 'wp_user_id' );
$jobs->bigInt( 'company_id' )->default(0);
$jobs->boolean('cv_required')->default(0);
$jobs->boolean('motivation_required')->default(0);
$jobs->bigInt( 'available' )->default( 1 );
$jobs->bigInt( 'currency_id' )->nullable();
$jobs->string( 'crawled_job_url' )->nullable();
$jobs->string( 'crawled_categories' )->nullable();
$jobs->string( 'crawled_job_types' )->nullable();
$jobs->text( 'crawled_job_summary' )->nullable();
$jobs->string( 'crawled_job_location' )->nullable();
$jobs->softDelete();
$jobs->timestamps();

$job_types = new HuslaMigration( 'job_types' );
$job_types->id();
$job_types->string( 'name' );
$job_types->text( 'description' )->nullable();
$job_types->timestamps();

$categories = new HuslaMigration( 'categories' );
$categories->id();
$categories->string( 'name' );
$categories->text( 'description' )->nullable();
$categories->timestamps();

$job_applications = new HuslaMigration( 'job_applications' );
$job_applications->id();
$job_applications->bigInt( 'wp_user_id' );
$job_applications->bigInt( 'job_id' );
$job_applications->bigInt( 'profile_id' );
//$job_applications->string( 'applicant_name' );
//$job_applications->string( 'applicant_email' );
//$job_applications->string( 'cv' )->nullable();
$job_applications->text( 'motivation' )->nullable();
//$job_applications->string( 'city' )->nullable();
//$job_applications->string( 'country' )->nullable();
$job_applications->timestamps();

$package_limits = new HuslaMigration( 'package_limits' );
$package_limits->id();
$package_limits->bigInt( 'package_id' );
$package_limits->bigInt( 'job_type_id' );
$package_limits->integer( 'limit' );
$package_limits->timestamps();

$profiles = new HuslaMigration( 'profiles' );
$profiles->id();
$profiles->bigInt( 'wp_user_id' );
$profiles->text( 'description' )->nullable();
$profiles->string( 'username' );
$profiles->string( 'image' )->nullable();
$profiles->string( 'email' );
$profiles->string( 'title' )->nullable();
$profiles->string( 'phone_number')->nullable();
$profiles->text( 'address' )->nullable();
$profiles->boolean( 'completed' )->default(0);;
$profiles->integer( 'years_of_experience' )->nullable();
$profiles->string('cv');
$profiles->timestamps();


$subscriptions = new HuslaMigration( 'subscriptions');
$subscriptions->id();
$subscriptions->bigInt( 'wp_user_id' );
$subscriptions->bigInt( 'package_id' );
$subscriptions->dateTime( 'start_date' );
$subscriptions->dateTime( 'end_date' );
$subscriptions->boolean( 'status' )->default( 1 );
$subscriptions->timestamps();

//
$accounts = new HuslaMigration( 'companies' );
$accounts->id();
$accounts->bigInt( 'wp_user_id' );
$accounts->string( 'name' );
$accounts->text( 'address_1' )->nullable();
$accounts->text( 'address_2' )->nullable();
$accounts->string( 'website' )->nullable();
$accounts->string( 'email' );
$accounts->string( 'phone_number' )->nullable();
$accounts->softDelete();
$accounts->timestamps();

$profile_category = new HuslaMigration( 'profile_categories' );
$profile_category->id();
$profile_category->bigInt( 'profile_id' );
$profile_category->bigInt( 'category_id' );
$profile_category->timestamps();

$currencies = new HuslaMigration( 'currencies' );
$currencies->id();
$currencies->string( 'name' );
$currencies->string( 'code' );
$currencies->timestamps();

$profile_category = new HuslaMigration( 'archived_users' );
$profile_category->id();
$profile_category->bigInt( 'wp_user_id' );
$profile_category->dateTime( 'delete_date' );
$profile_category->timestamps();


