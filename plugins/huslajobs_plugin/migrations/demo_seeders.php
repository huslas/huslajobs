<?php

namespace huslajobs;

use Faker\Generator;

//  Categories
new HuslaSeeder( function ( Generator $faker ) {
	$seeder              = new Category();
	$seeder->name        = 'Capentry';
	$seeder->description = $faker->text( 30 );
	$seeder->save();
} );

new HuslaSeeder( function ( Generator $faker ) {
	$seeder              = new Category();
	$seeder->name        = "Tailoring";
	$seeder->description = $faker->text( 30 );
	$seeder->save();
} );

// Job type
new HuslaSeeder( function ( Generator $faker ) {
	$seeder              = new JobType();
	$seeder->name        = 'Paid Internship';
	$seeder->description = $faker->text( 30 );
	$seeder->save();
} );

new HuslaSeeder( function ( Generator $faker ) {
	$seeder              = new JobType();
	$seeder->name        = 'Full Time';
	$seeder->description = $faker->text( 30 );
	$seeder->save();
} );


// Currency
//new HuslaSeeder( function ( Generator $faker ) {
//	$seeder         = new Currency();
//	$currency       = $faker->currencyCode;
//	$seeder->name   = $currency;
//	$seeder->symbol = $currency;
//	$seeder->code   = $currency;
//	$seeder->save();
//}, 10 );


// Accounts
new HuslaSeeder( function ( Generator $faker ) {
	global $wpdb;
    $users_table = $wpdb->prefix.'users';
	$total_query           = "SELECT MAX(id) as total FROM $users_table";
	$total                 = intval( $wpdb->get_var( $total_query ) );
	$total                 = $total > 30 ? 30 : $total;
	$seeder                = new Account();
	$seeder->wp_user_id    = $faker->numberBetween( 1, $total );
	$seeder->name          = $faker->firstName;
	$seeder->city          = $faker->city;
	$seeder->state         = $faker->state;
	$seeder->country       = $faker->country;
	$seeder->account_type  = $faker->randomElement( [ 'individual_job_seeker', 'company_job_seeker', 'company_recruiter','individual_recruiter'] );
	$seeder->website       = $faker->domainName;
	$seeder->email = $faker->email;
 	$seeder->phone_number  = $faker->phoneNumber;
 	$seeder->bio           = $faker->text( 100 );
 	$seeder->profile_image = $faker->imageUrl();
	$seeder->save();

}, 30 );

// Packages
new HuslaSeeder( function ( Generator $faker ) {
//	$last_currency_id = Currency::orderBy( 'id', 'desc' )->first()->id;
	$last_account_id  = Account::orderBy( 'id', 'desc' )->first()->id;

	$seeder              = new Package();
	$seeder->name        = "Classic";
	$seeder->description = $faker->text( 30 );
	$seeder->price       = 40000;
	$seeder->duration    = $faker->numberBetween( 1, 40 );
    $seeder->jobs    = $faker->numberBetween( 1, 20 );
    $seeder->job_applications    = $faker->numberBetween( 1, 20 );
//	$seeder->currency_id = $faker->numberBetween( 1, $last_currency_id );
	$seeder->profiles    = $faker->numberBetween( 1, 20 );
	$seeder->wp_user_id  = $faker->numberBetween( 1, $last_account_id );
	$seeder->save();
} );

new HuslaSeeder( function ( Generator $faker ) {
//	$last_currency_id = Currency::orderBy( 'id', 'desc' )->first()->id;
	$last_account_id  = Account::orderBy( 'id', 'desc' )->first()->id;


	$seeder              = new Package();
	$seeder->name        = "Premium";
	$seeder->description = $faker->text( 30 );
	$seeder->price       = 100000;
	$seeder->duration    = $faker->numberBetween( 1, 40 );
//	$seeder->currency_id = $faker->numberBetween( 1, $last_currency_id );
	$seeder->profiles    = $faker->numberBetween( 1, 20 );
    $seeder->jobs    = $faker->numberBetween( 1, 20 );
    $seeder->job_applications    = $faker->numberBetween( 1, 20 );
    $seeder->benefits    = $faker->text( 20 ).','.$faker->text( 15 ).','.$faker->text( 10 );
	$seeder->wp_user_id  = $faker->numberBetween( 1, $last_account_id );
	$seeder->save();
} );

//// Package Limits
//new HuslaSeeder( function ( Generator $faker ) {
//	$last_package_id     = Package::orderBy( 'id', 'desc' )->first()->id;
//	$last_job_type_id    = JobType::orderBy( 'id', 'desc' )->first()->id;
//	$seeder              = new PackageLimit();
//	$seeder->package_id  = $faker->numberBetween( 1, $last_package_id );
//	$seeder->job_type_id = $faker->numberBetween( 1, $last_job_type_id );
//	$seeder->limit       = $faker->numberBetween( 1, 20 );
//	$seeder->save();
//}, 10 );

// Subscriptions
new HuslaSeeder( function ( Generator $faker ) {
	$last_package_id = Package::orderBy( 'id', 'desc' )->first()->id;
	$last_account_id = Account::orderBy( 'id', 'desc' )->first()->id;

	$seeder             = new Subscription();
	$seeder->wp_user_id = $faker->numberBetween( 1, $last_account_id );
	$seeder->package_id = $faker->numberBetween( 1, $last_package_id );
	$seeder->start_date = $faker->date();
	$seeder->end_date   = $faker->date();
	$seeder->status     = $faker->numberBetween( 0, 1 );
	$seeder->save();
}, 30 );

// Jobs
new HuslaSeeder( function ( Generator $faker ) {
	$last_job_type_id = JobType::orderBy( 'id', 'desc' )->first()->id;
	$last_account_id  = Account::orderBy( 'id', 'desc' )->first()->id;


	$seeder                             = new Job();
	$seeder->job_type_id                = $faker->numberBetween( 1, $last_job_type_id );
	$seeder->salary                     = 50000;
	$seeder->name                       = $faker->text( 10 );
	$seeder->description                = $faker->text( 100 );
//	$seeder->location                   = $faker->address;
	$seeder->experience                 = $faker->numberBetween( 1, 10 );
	$seeder->category_id                 = $faker->numberBetween( 1, 10 );
	$seeder->account_id                 = $faker->numberBetween( 1, $last_account_id );
	$seeder->available                  = $faker->numberBetween( 0, 1 );
	$seeder->work                       = $faker->randomElement( [ 'on site', 'remote'] );
	$seeder->application_email          = $faker->randomElement( [ 'admin@huslajobs.com', 'roland@huslajobs.com'] );
	$seeder->currency_id                = $faker->text( 10 );


	$seeder->save();
},100 );

new HuslaSeeder( function ( Generator $faker ) {
	$last_job_type_id = JobType::orderBy( 'id', 'desc' )->first()->id;
	$last_account_id  = Account::orderBy( 'id', 'desc' )->first()->id;


	$seeder                             = new Job();
	$seeder->job_type_id                = $faker->numberBetween( 1, $last_job_type_id );
	$seeder->salary                     = $faker->randomNumber( 5 );
	$seeder->name                       = "Teacher needed at University of Buea";
	$seeder->description                = $faker->text( 100 );
	$seeder->location                   = $faker->address;
	$seeder->experience                 = $faker->numberBetween( 1, 10 );
	$seeder->category_id                 = $faker->numberBetween( 1, 10 );
	$seeder->account_id                 = $faker->numberBetween( 1, $last_account_id );
	$seeder->available                  = $faker->numberBetween( 0, 1 );
	$seeder->work                       = $faker->randomElement( [ 'on site', 'remote'] );
	$seeder->application_email          = $faker->randomElement( [ 'admin@huslajobs.com', 'roland@huslajobs.com'] );
	$seeder->currency_id                = $faker->text( 10 );


	$seeder->save();
} );

new HuslaSeeder( function ( Generator $faker ) {
	$last_job_type_id = JobType::orderBy( 'id', 'desc' )->first()->id;
	$last_account_id  = Account::orderBy( 'id', 'desc' )->first()->id;


	$seeder                             = new Job();
	$seeder->job_type_id                = $faker->numberBetween( 1, $last_job_type_id );
	$seeder->salary                     = $faker->randomNumber( 5 );
	$seeder->name                       = "Software Engineer at MTN";
	$seeder->description                = $faker->text( 100 );
	$seeder->location                   = $faker->address;
	$seeder->experience                 = $faker->numberBetween( 1, 10 );
	$seeder->category_id                 = $faker->numberBetween( 1, 10 );
	$seeder->account_id                 = $faker->numberBetween( 1, $last_account_id );
	$seeder->available                  = $faker->numberBetween( 0, 1 );
	$seeder->work                       = $faker->randomElement( [ 'on site', 'remote'] );
	$seeder->application_email          = $faker->randomElement( [ 'admin@huslajobs.com', 'roland@huslajobs.com'] );
	$seeder->currency_id                = $faker->text( 10 );


	$seeder->save();
} );

// Job Applications
new HuslaSeeder( function ( Generator $faker ) {
	$last_job_id        = Job::orderBy( 'id', 'desc' )->first()->id;
	$last_account_id    = Account::orderBy( 'id', 'desc' )->first()->id;
	$seeder             = new JobApplication();
	$seeder->account_id = $faker->numberBetween( 1, $last_account_id );
	$seeder->job_id     = $faker->numberBetween( 1, $last_job_id );
	$seeder->save();
}, 5 );

new HuslaSeeder( function ( Generator $faker ) {
	$last_job_id         = Job::orderBy( 'id', 'desc' )->first()->id;
	$last_category_id    = Category::orderBy( 'id', 'desc' )->first()->id;
	$seeder              = new JobCategory();
	$seeder->category_id = $faker->numberBetween( 1, $last_category_id );
	$seeder->job_id      = $faker->numberBetween( 1, $last_job_id );
	$seeder->save();
}, 10 );
