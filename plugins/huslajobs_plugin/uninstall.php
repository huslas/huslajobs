<?php
namespace huslajobs;
require plugin_dir_path( __FILE__ ) . 'inc/HuslaMigration.php';

// If uninstall not called from WordPress, then exit
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
    exit();
}

/**
 * Uninstall operations
 */
function wpg_uninstall() {
//    global $wpdb;
//    $prefix = $wpdb->prefix . 'husla_';
//    $tables = [
//        'packages',
//        'jobs',
//        'job_types',
//        'categories',
//        'job_applications',
//        'package_limits',
//        'profiles',
//        'subscriptions',
//        'accounts',
//        'profile_categories',
//        'currencies'
//    ];
//    foreach ($tables as $table){
//        $table_name = $prefix.trim($table);
//        $wpdb->query( "DROP TABLE IF EXISTS " .$table_name  );
//    }

    HuslaMigration::dropAll();
}

//run
wpg_uninstall();

