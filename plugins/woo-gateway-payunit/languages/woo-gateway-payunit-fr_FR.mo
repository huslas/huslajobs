��          �      L      �  �   �     �     �     �     �     �     �     �     �  @   �     ;     B     O  (   [  `   �  K   �  :   1     l  �   o  �   h     c     k     s     x     �     �     �     �  @   �               "  (   .  �   W  X   �  @   8     y                 	                                                            
                       <strong>%s</strong> is enabled and WooCommerce is not forcing the SSL certificate on your checkout page. Please ensure that you have a valid SSL certificate and that you are <a href="%s">forcing the checkout pages to be secured.</a> Amount Currency Date No transaction Order id Page Payment failed Payment method Sorry, we were unable to initiate transaction. Please try again. Status Transactions WGP Options Woocommerce PayUnit Gateway Transactions Woocommerce PayUnit Gateway does not support store currency.Supported currencies are XAF and USD Woocommerce PayUnit Gateway requires WooCommerce to be installed and active You will be redirected to PayUnit to complete your payment of Project-Id-Version: 
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 3.0.1
 <strong>%s</strong>est activé et WooCommerce ne force pas le certificat SSL sur votre page de paiement. Veuillez vous assurer que vous avez un certificat SSL valide et que vous <a href=“%s”>forcez les pages de paiement à être sécurisées.</a> Montant Monnaie Date Aucune transaction Identité de la commande Page Échec du paiement Mode de paiement Sorry, we were unable to initiate transaction. Please try again. Statut Transactions WGP Options Woocommerce PayUnit Gateway Transactions La passerelle PayUnit de Woocommerce ne prend pas en charge la devise du magasin, les devises prises en charge étant le XAF et le USD. La passerelle PayUnit de Woocommerce nécessite que WooCommerce soit installé et actif. Vous serez redirigé vers PayUnit pour effectuer votre paiement. de 