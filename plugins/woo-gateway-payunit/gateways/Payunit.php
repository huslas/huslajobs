<?php


/**
 *
 */
class WC_Gateway_Payunit extends WC_Payment_Gateway
{

    private $host = 'https://app.payunit.net/api';
    private $notify = 'https://payunit.net';
    private $order_id_pay;
    private $headers;

    // Setup our Gateway's id, description and other values
    /**
     * @var false|mixed|void
     */
    private $payunit_username;
    /**
     * @var false|mixed|void
     */
    private $payunit_secret;
    /**
     * @var false|mixed|void
     */
    private $payunit_api_key;
    /**
     * @var false|mixed|void
     */
    private $payunit_env;

    function __construct()
    {

        // The global ID for this Payment method
        $this->id = "woogatewaypayunit";

        // The Title shown on the top of the Payment Gateways Page next to all the other Payment Gateways
        $this->method_title = __("PayUnit", 'woo-gateway-payunit');

        // The description for this Payment Gateway, shown on the actual Payment options page on the backend
        $this->method_description = __("PayUnit Payment Gateway Plug-in for WooCommerce", 'woo-gateway-payunit');

        // The title to be used for the vertical tabs that can be ordered top to bottom
        $this->title = __("PayUnit", 'woo-gateway-payunit');

        $this->order_button_text = __("Pay with PayUnit", 'woo-gateway-payunit');

        // If you want to show an image next to the gateway's name on the frontend, enter a URL to an image.
        $this->icon = WOO_PAYUNIT_URL . '/assets/images/pu-logo.png';

        // Bool. Can be set to true if you want payment fields to show on the checkout
        // if doing a direct integration, which we are doing in this case
        $this->has_fields = false;

        // This basically defines your settings which are then loaded with init_settings()
        $this->init_form_fields();

        // Load the settings.
        $this->init_settings();

        foreach ($this->settings as $setting_key => $value) {
            $this->$setting_key = $value;
        }
        $this->payunit_username = $this->get_option('payunit_username');
        $this->payunit_secret = $this->get_option('payunit_secret');
        $this->payunit_api_key = $this->get_option('payunit_api_key');
        $this->payunit_env = $this->get_option('payunit_env');
        // This action hook saves the settings
        add_action('woocommerce_update_options_payment_gateways_' . $this->id, [$this, 'process_admin_options']);
        $this->headers = [
            'Authorization' => 'Basic ' . base64_encode($this->payunit_username . ":" . $this->payunit_secret),
            'Content-Type' => 'application/json',
            'x-api-key' => $this->payunit_api_key,
            'mode' => $this->payunit_env,
            'Accept' => 'application/json'
        ];
        // Let's check if we are on the WC Orders page
        add_action('woocommerce_thankyou', [$this, 'update_order_status']);

    }


    // Check if we are forcing SSL on checkout pages
    // Custom function not required by the Gateway
    public function do_ssl_check()
    {
        if ($this->enabled == "yes") {
            if (get_option('woocommerce_force_ssl_checkout') == "no") {
                echo "<div class=\"error\"><p>" . sprintf(__("<strong>%s</strong> is enabled and WooCommerce is not forcing the SSL certificate on your checkout page. Please ensure that you have a valid SSL certificate and that you are <a href=\"%s\">forcing the checkout pages to be secured.</a>", 'woo-gateway-payunit'), $this->method_title, admin_url('admin.php?page=wc-settings&tab=checkout')) . "</p></div>";
            }
        }
    }

    // Build the administration fields for this specific Gateway
    public function init_form_fields()
    {
        $this->form_fields = array(
            'enabled' => array(
                'title' => __('Enable / Disable', 'woo-gateway-payunit'),
                'label' => __('Enable this payment gateway', 'woo-gateway-payunit'),
                'type' => 'checkbox',
                'default' => 'no',
            ),
            'title' => array(
                'title' => __('Title', 'woo-gateway-payunit'),
                'type' => 'text',
                'desc_tip' => __('Payment title the customer will see during the checkout process.', 'woo-gateway-payunit'),
                'default' => __('PayUnit', 'woo-gateway-payunit'),
            ),
            'description' => array(
                'title' => __('Description', 'woo-gateway-payunit'),
                'type' => 'textarea',
                'desc_tip' => __('Payment description the customer will see during the checkout process.', 'woo-gateway-payunit'),
                'default' =>  __('You will be redirected to PayUnit to complete your payment.Pay with MoMo, Orange Money, and Visa Card', 'woo-gateway-payunit'),
                'css' => 'max-width:350px;'
            ),
            'payunit_username' => array(
                'title' => __('PayUnit Username', 'woo-gateway-payunit'),
                'type' => 'text',
                'desc_tip' => __('PayUnit Username.', 'woo-gateway-payunit'),
            ),
            'payunit_secret' => array(
                'title' => __('PayUnit Secret', 'woo-gateway-payunit'),
                'type' => 'text',
                'desc_tip' => __('PayUnit Secret.', 'woo-gateway-payunit'),
            ),
            'payunit_api_key' => array(
                'title' => __('PayUnit API Key', 'woo-gateway-payunit'),
                'type' => 'text',
                'desc_tip' => __('Client Secret.', 'woo-gateway-payunit'),
            ),
            'payunit_env' => array(
                'title' => __('PayUnit Environment', 'woo-gateway-payunit'),
                'type' => 'text',
                'default' => 'test',
                'desc_tip' => __('Merchant Key', 'woo-gateway-payunit'),
            ),
        );
    }

    // Display custom payment fields
    public function payment_fields()
    {
        $message =  __('You will be redirected to PayUnit to complete your payment.Pay with MoMo, Orange Money, and Visa Card', 'woo-gateway-payunit');
        $momo_image = WOO_PAYUNIT_URL. '/assets/images/mtn.png';
        $orange_image = WOO_PAYUNIT_URL. '/assets/images/orange.png';
        $paypal_image = WOO_PAYUNIT_URL. '/assets/images/paypal.png';
        echo "<div><p style='margin: 0 !important;'>$message</p><img src='$momo_image' alt='momo' style='height: 22px;width: 32px'><img src='$orange_image' alt='orange money' style='height: 22px;width: 32px'><img src='$paypal_image' alt='paypal' style='height: 22px;width: 32px'></div>";
    }

    /**
     * Validate frontend fields.
     *
     * Validate payment fields on the frontend.
     *
     * @return bool
     */
    public function validate_fields()
    {
        // There are no fields to validate
        return true;
    }


    public function process_payment($order_id)
    {
        global $user_ID;

        $order = wc_get_order($order_id);
        $amount = $order->get_total();//WC()->cart->cart_contents_total;
        $this->order_id_pay = $order_id . '-' . time();
        $currency_code = get_woocommerce_currency();

        $body = json_encode([
            'total_amount' => $amount,
            'currency' => $currency_code,
            'transaction_id' => $this->order_id_pay,
            'return_url' => WOO_PAYUNIT_URL . 'gateways/return-page.php',
//            'notify_url' => get_option('siteurl') . '/wp-json/woopayunit/notify_url',
            'name' => get_bloginfo(),
            'purchaseRef' => $this->order_id_pay,
        ]);
        $wgp_transaction  = new WGPDatabaseHelper();

        $wgp_transaction->save([
            'total_amount' => $amount,
            'currency' => $currency_code,
            'transaction_id' => $this->order_id_pay,
            'return_url' => WOO_PAYUNIT_URL . 'gateways/return-page.php',
            'order_id' =>$order_id,
            'user_id' => $user_ID,
        ]);
        $args = [
            'body' => $body,
            'headers' => $this->headers
        ];

        $response = wp_remote_post($this->host . '/gateway/initialize', $args);


        $results = wp_remote_retrieve_body($response);
        $results = json_decode($results);

        if ($results->data->transaction_url) {
            return [
                'result' => 'success',
                'redirect' => $results->data->transaction_url
            ];
        } else {
            wc_add_notice(__('Sorry, we were unable to initiate transaction. Please try again.', 'woo-gateway-payunit'), 'error');
        }

        return [
            'result' => 'danger'
        ];
    }

    // Check if we are on the WC Orders page
    // then check if an order has just been processed successfully
    // and set the order status to paid
    public function update_order_status($order_id)
    {
        // Get the order
        $order = wc_get_order($order_id);

        // Check transaction status
        $response = $this->get_transfer_status();


        $status_code = wp_remote_retrieve_response_code($response);

        if ($status_code === 200) {
            $response_body = wp_remote_retrieve_body($response);
            $status = (json_decode($response_body))->status;
            if ($status === 'SUCCESS') {
                // Set order status to paid
                $order->update_status('completed');
            }

        }
    }

    /**
     * Get the status of a transfer
     *
     * @access public
     * @param  $referenceId          string reference to the request
     * @return $parsedResponse       decoded JSON response or null if the request failed
     */
    public function get_transfer_status()
    {
        return wp_remote_post($this->host . '/gateway/transaction/' . $this->order_id_pay, [
            'headers' => $this->headers,
        ]);

//        return $results->data->transaction->transaction_status;
    } // end get_transfer_status

}
