<?php

require_once('../../../../wp-load.php');


// Redirect if payment is complete
if (isset($_GET['transaction_id']) && !empty($_GET['transaction_id'])) {
    $transaction_status = $_GET['transaction_status'];

    if ($transaction_status == 'SUCCESS') {
        $transaction_id = $_GET['transaction_id'];
        $order_id = explode('-', $transaction_id)[0];
        $order_id = intval($order_id);
        $order = wc_get_order($order_id);
        global $woocommerce;
        $order = new WC_Order($order_id);

        //Complete payment
        $order->payment_complete();
        // Remove cart
        $woocommerce->cart->empty_cart();
        // $order = WC_get_order($order_id);

        // Get the order key
        $order_key = $order->get_order_key();

        $wgp_transactions = new WGPDatabaseHelper();
        //get transaction
        $transaction = $wgp_transactions->where('transaction_id','=',"'" . $transaction_id . "'")->first();
        if ($transaction){
            $fields=[
                'status' =>'completed'
            ];
            $wgp_transactions->update($transaction->id,$fields);
        }

        wp_redirect(get_option('siteurl') . '/checkout/order-received/' . $order_id . '/?key=' . $order_key);

    } else {
        wc_add_notice(__('Payment failed', 'woo-gateway-payunit'), 'error');

        if (wp_redirect(wc_get_checkout_url())) {
            exit;
        }
    }
}