<?php
/**
 * Plugin Name: Woocommerce PayUnit Gateway
 * Plugin URI: https://gitlab.com/AkomboNeville
 * Description: Easy payments using woocommerce and Payunit.
 * Author: Akombo Neville Akwo
 * Author URI: https://gitlab.com/AkomboNeville
 * Version: 1.0
 * Text Domain: woo-gateway-payunit
 * Domain Path: /languages
 */
/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Copyright 2022 Akombo Neville Akwo
 */

//namespace dm;

if (!defined('ABSPATH')) {
    exit;
}

define('WOO_PAYUNIT_URL', plugin_dir_url(__FILE__));
define('WOO_PAYUNIT_DIR', plugin_dir_path(__FILE__));
define('WOO_PAYUNIT_MENU_SLUG', 'wgp-transactions');
// Include our Gateway Class and Register Payment Gateway with WooCommerce
function wg_payunit_init()
{
    $currency_code = get_woocommerce_currency();
    if (!($currency_code == 'USD' || $currency_code == 'XAF')) {
        add_action('admin_notices', 'wg_payunit_currency');
        return;
    }

    // If the parent WC_Payment_Gateway class doesn't exist
    // it means WooCommerce is not installed on the site
    // so do nothing
    if (!class_exists('WC_Payment_Gateway')) return;
    load_plugin_textdomain('woo-gateway-payunit', false, basename(dirname(__FILE__)) . ' / languages');
    require WOO_PAYUNIT_DIR . 'inc/WGPDatabaseHelper.php';
    require WOO_PAYUNIT_DIR . 'gateways/apis/RestEndpointPayUnit.php';
    require WOO_PAYUNIT_DIR . 'inc/admin/WGPAdmin.php';
    $wgp_database = new WGPDatabaseHelper();
    $wgp_database->createTable();
    new RestEndpointPayUnit();
    new WGPAdmin();

    require WOO_PAYUNIT_DIR . 'gateways/Payunit.php';

    // Now that we have successfully included our class,
    // Lets add it to WooCommerce
    function wg_payunit_add_gateway($methods)
    {
        $methods[] = 'WC_Gateway_Payunit';
        return $methods;
    }

    add_filter('woocommerce_payment_gateways', 'wg_payunit_add_gateway');

}

add_action('plugins_loaded', 'wg_payunit_init', 0);

function wg_payunit_activation(){
    $in_array = in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')));
    if (!$in_array) {
        add_action('admin_notices', 'wg_payunit_woocommerce');
        die();
//        return;
    }
}
register_activation_hook(__FILE__, 'wg_payunit_activation');

function wg_payunit_currency()
{
    echo '<div class="notice notice-error error"><p>' . __('Woocommerce PayUnit Gateway does not support store currency.Supported currencies are XAF and USD', 'woo-gateway-payunit') . '</p></div>';
}

function wg_payunit_woocommerce()
{
    echo '<div class="notice notice-error error"><p>' . __('Woocommerce PayUnit Gateway requires WooCommerce to be installed and active', 'woo-gateway-payunit') . '</p></div>';
}