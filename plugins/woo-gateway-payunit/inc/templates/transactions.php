<?php

?>
<div class="wgp-container">
    <div class="wgp-header">
        <h1>
            <?php echo __('Transactions', 'woo-gateway-payunit') ?>
        </h1>

    </div>
    <div class="wgp-content">
        <?php

        //get transactions
        $wgp_database_helper = new WGPDatabaseHelper();
        $page_number = isset($_GET['page_number']) ? intval($_GET['page_number']) : 1;
        $response = $wgp_database_helper->paginate(1, $page_number)->get();
        $transactions = $response['data'];
        $total_pages = $response['total_pages'];

        if (sizeof($transactions)): ?>

            <div class="wgp-table-wrapper">
                <table>
                    <thead>
                    <th>
                        <?php echo __('Order id', 'woo-gateway-payunit') ?>
                    </th>
                    <th>
                        <?php echo __('Amount', 'woo-gateway-payunit') ?>
                    </th>
                    <th>
                        <?php echo __('Currency', 'woo-gateway-payunit') ?>
                    </th>
                    <th>
                        <?php echo __('Payment method', 'woo-gateway-payunit') ?>
                    </th>
                    <th>
                        <?php echo __('Status', 'woo-gateway-payunit') ?>
                    </th>
                    <th>
                        <?php echo __('Date', 'woo-gateway-payunit') ?>
                    </th>
                    </thead>
                    <tbody>

                    <?php foreach ($transactions as $transaction): ?>
                        <tr>
                            <td> <?php echo $transaction->order_id ?></td>
                            <td> <?php echo $transaction->total_amount ?></td>
                            <td> <?php echo $transaction->currency ?></td>
                            <td> <?php echo $transaction->payment_method ?></td>
                            <td> <?php echo $transaction->status ?></td>
                            <td> <?php echo $transaction->updated_at ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>

            <div class="wgp-pagination-wrapper">
                <?php include WOO_PAYUNIT_DIR . 'inc/templates/pagination.php' ?>
            </div>
        <?php else: ?>
            <div>
                <h3><?php echo __('No transaction', 'woo-gateway-payunit') ?></h3>
            </div>
        <?php endif; ?>
    </div>
</div>