<?php

if (isset($total_pages) && $total_pages > 1):

    $protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";

    $url = $protocol . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    $page_number = isset($_GET['page_number']) ? $_GET['page_number'] : 0;
    if ($page_number) {
        $url = explode('&page_number', $url)[0];
    }
    ?>
    <div class="wgp-pagination">
        <div class="wgp-left">
            <span><?php echo __('Page') ?></span>
            <span><?php echo $page_number ?: 1 ?></span>
            <span><?php echo __('of') ?></span>
            <span><?php echo $total_pages ?></span>
        </div>
        <ul class="wgp-right">
            <li>
                <?php
                $previous_link = $page_number - 1;
                if ($previous_link):
                    ?>
                    <a href="<?php echo $url . '&page_number=' . $previous_link ?>"><</a>
                <?php else: ?>
                    <a><</a>
                <?php endif; ?>
            </li>
            <?php

            $start = 1;
            $number_of_links = 5;
            $limit = $total_pages;

            if ($total_pages > $number_of_links && $page_number >3){
                $start = $page_number -2;
                $next_limit =  $page_number + 2;
                if ($next_limit<= $total_pages){
                    $limit = $next_limit;
                }
                else{
                    $start = $page_number -3 ;
                }
            }
            while ($start <= $limit):

                $link_url = $url . '&page_number=' . $start;
                ?>
                <li>
                    <a href="<?php echo $link_url; ?>" <?php if ($start == $page_number): ?> class="active" <?php endif; ?> ><? echo $start; ?></a>
                </li>

                <?php
                $start++;
            endwhile;
            ?>
            <li>
                <?php
                $next_link = $page_number +1;
                if ($next_link < $total_pages):
                    ?>
                    <a href="<?php echo $url . '&page_number=' . $next_link ?>"> <?php echo ">" ?> </a>
                <?php else: ?>
                    <a><?php echo ">" ?> </a>
                <?php endif; ?>
            </li>
        </ul>
    </div>
<?php endif; ?>