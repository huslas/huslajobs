<?php
/**
 * The header for Astra Theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astra
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

?><!DOCTYPE html>
<?php astra_html_before(); ?>

<html <?php language_attributes(); ?>>
<head>
    <?php astra_head_top(); ?>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <style>
        *{
            padding: 0;
            margin: 0;
        }
        body.loading{
            overflow: hidden;
            -ms-overflow-style: none;  /* IE and Edge */
            scrollbar-width: none;  /* Firefox */
        }
        body.loading::-webkit-scrollbar { width: 0 !important }
        #loading-screen {
            position: fixed;
            background: white;
            z-index: 999999;
            height: 100%;
            width: 100%;
        }
        #loading-screen.loaded{
            display: none;
        }
        #loading-screen .loading-content{
            margin: auto;
            display: flex;
            flex-direction: column;
            align-items: center;
            position: absolute;
            left: 50%;
            top:50%;
            overflow: hidden;
            transform: translate(-50%,-50%);
        }
        @keyframes km-img-loading {
            0% {
                opacity: 0.5;
                transform: scale(0.5);
            }

            50% {
                opacity: 1;
                transform: scale(1);
            }

            100% {
                opacity: 0.5;
                transform: scale(0.5);
            }
        }

    </style>

    <?php wp_head(); ?>
    <?php astra_head_bottom(); ?>
    <script>
        (function ($) {
            'use strict';

        $(document).ready(function() {
            function hidePreloader() {
                if ($("body").find("div.vue-component").length === 0){
                    document.querySelector('body').classList.remove('loading');
                    $('#loading-screen').addClass('d-none');
                }
            }
            $("#husla-logout").click(e => {
                localStorage.clear();
            });
            const url = new URL(window.location.href);
            const action = url.searchParams.get("action");
            if (action  && action== 'logout'){
                localStorage.clear();
            }
            hidePreloader();
        });

        }(jQuery))
    </script>
</head>
<body <?php astra_schema_body(); ?> <?php body_class('loading'); ?>>
<div id="loading-screen">
    <div class="loading-content">
        <img src="<?php echo HUSLA_JOBS_IMAGE_URL ?>/logo.png" style="animation-name: km-img-loading;
          animation-duration: 2s;
          animation-iteration-count: infinite;
          width: 110px !important;
"/>
        <h5>Loading...</h5>
    </div>
</div>
<?php astra_body_top(); ?>
<?php wp_body_open(); ?>

<a
    class="skip-link screen-reader-text"
    href="#content"
    role="link"
    title="<?php echo esc_html( astra_default_strings( 'string-header-skip-link', false ) ); ?>">
    <?php echo esc_html( astra_default_strings( 'string-header-skip-link', false ) ); ?>
</a>

<div
    <?php
        echo astra_attr(
            'site',
            array(
                'id'    => 'page',
                'class' => 'hfeed site',
            )
        );
    ?>
>
    <?php
        astra_header_before();

        astra_header();

        astra_header_after();

        astra_content_before();

    ?>

    <div id="content" class="site-content">
        <div class="ast-container">
            <?php astra_content_top(); ?>
