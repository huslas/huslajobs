<?php
/**
 * Template for Primary Header
 *
 * The header layout 2 for Astra Theme. ( No of sections - 1 [ Section 1 limit - 3 )
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @see https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package     Astra
 * @author      Astra
 * @copyright   Copyright (c) 2020, Astra
 * @link        https://wpastra.com/
 * @since       Astra 1.0.0
 */

?>

<div class="main-header-bar-wrap">
	<div <?php echo astra_attr( 'main-header-bar' ); ?>>
		<?php astra_main_header_bar_top(); ?>
		<div class="ast-container">
			<div class="desktop ast-flex main-header-container">
				<?php astra_masthead_content(); ?>
			</div><!-- Main Header Container -->
		</div><!-- ast-row -->

                      <div id="ast-mobile-header" class="ast-mobile-header-wrap custom-mobile" data-type="dropdown">
                      		<div class="ast-mobile-header-content content-align-flex-start ">
                      			<div class="ast-builder-menu-mobile ast-builder-menu ast-builder-menu-mobile-focus-item ast-builder-layout-element site-header-focus-item" data-section="section-header-mobile-menu">

                                    <?php do_action( 'husla_mobile_header_custom_menu' ); ?>
                             </div>

                      	</div>
                      </div>

		<?php astra_main_header_bar_bottom(); ?>

	</div> <!-- Main Header Bar -->
</div> <!-- Main Header Bar Wrap -->
