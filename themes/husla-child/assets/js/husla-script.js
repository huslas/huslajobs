(function ($) {
    'use strict';

    $(document).ready(function () {
        $(document).on('click','.menu-toggle.main-header-menu-toggle',function (e){
            e.preventDefault();
            if ($('.ast-button-wrap .menu-toggle .icon-menu-bars svg:nth-child(2)').hasClass('d-inline-block')){
                $('.ast-button-wrap .menu-toggle .icon-menu-bars svg:nth-child(2)').addClass('d-none').removeClass('d-inline-block')
                $('.ast-button-wrap .menu-toggle .icon-menu-bars svg:nth-child(1)').addClass('d-inline-block').removeClass('d-none')
                $('.ast-mobile-header-wrap.custom-mobile .ast-mobile-header-content').addClass('d-none').removeClass('d-block')
            }else{
                $('.ast-button-wrap .menu-toggle .icon-menu-bars svg:nth-child(2)').addClass('d-inline-block').removeClass('d-none')
                $('.ast-button-wrap .menu-toggle .icon-menu-bars svg:nth-child(1)').addClass('d-none').removeClass('d-inline-block')
                $('.ast-mobile-header-wrap.custom-mobile .ast-mobile-header-content').addClass('d-block').removeClass('d-none')
            }

        });

        $(document).on('click','#user-dropdown-btn',function (e){
            e.preventDefault()
            if ($('#hs-user-dropdown-menu').hasClass('d-block')){
                $('#hs-user-dropdown-menu.dropdown-menu').removeClass('d-block')
            }else {
                $('#hs-user-dropdown-menu.dropdown-menu').addClass('d-block');

            }
        });

        $(document).on('click',function(e){

            if(!(($(e.target).closest("#hs-user-dropdown-menu").length > 0 ) || ($(e.target).closest("#user-dropdown-btn").length > 0))){
                $('#hs-user-dropdown-menu.dropdown-menu').removeClass('d-block');
            }
        });

    });




}(jQuery))