# Husla Jobs Child Theme

Husla jobs child theme 


## Installation
- [ ] Install Astra theme
- [ ] Download husla-child theme
- [ ] Extract and copy the husla-child theme to the wp-content/themes directory of your WordPress installation
- [ ] Activate the husla-child theme
- [ ] Create a page with title Homepage and publish the page